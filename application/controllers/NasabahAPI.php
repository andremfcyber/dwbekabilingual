<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class NasabahAPI extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $nasabah = $this->db->get('nasabah')->result();
        } else {
            $this->db->where('id', $id);
            $nasabah = $this->db->get('nasabah')->result();
        }
        $this->response($nasabah, 200);
    }

    //Masukan function selanjutnya disini
// function index_post() {
//         $data = array(
//                     'id'           => $this->post('id'),
//                     'nama'          => $this->post('nama'),
//                     'nomor'    => $this->post('nomor'));
//         $insert = $this->db->insert('kontak', $data);
//         if ($insert) {
//             $this->response($data, 200);
//         } else {
//             $this->response(array('status' => 'fail', 502));
//         }
//     }

    // function index_put() {
    //     $id = $this->put('id');
    //     $data = array(
    //                 'id'       => $this->put('id'),
    //                 'nama'          => $this->put('nama'),
    //                 'nomor'    => $this->put('nomor'));
    //     $this->db->where('id', $id);
    //     $update = $this->db->update('kontak', $data);
    //     if ($update) {
    //         $this->response($data, 200);
    //     } else {
    //         $this->response(array('status' => 'fail', 502));
    //     }
    // }

    // function index_delete() {
    //     $id = $this->delete('id');
    //     $this->db->where('id', $id);
    //     $delete = $this->db->delete('kontak');
    //     if ($delete) {
    //         $this->response(array('status' => 'success'), 201);
    //     } else {
    //         $this->response(array('status' => 'fail', 502));
    //     }
    // }

}
?>