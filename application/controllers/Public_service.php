<?php

class Public_service extends Admin_Controller{

    function __construct(){
        parent::__construct();
        // map_y('Hai');
    }
    public function get_product_detail_kredit($id = ''){
        if(!$id){
            return response_json([
                'message' => 'Params not found',
            ]);
        }
        $get = $this->db->query("SELECT c.id, c.product_type_id, c.name, c.deskripsi, b.bunga, b.jenis_bunga, c.thumbnail FROM product c
            LEFT JOIN conf_bunga_kredit b ON b.id_kredit_produk = c.id
            WHERE id = '$id'")->result_array();
        $get = @$get[0];
        $gambar = '';
        if(!isset($get['thumbnail']) || $get['thumbnail'] == ''){
            $gambar = base_url('themes/public/materialkit').'/img_frontend/tangan.png';
         }else{
            $gambar = base_url('upload/thumbnail/'.$get['thumbnail']);
         }
        $get = [
            'id' => $get['id'],
            'product_type_id' => $get['product_type_id'],
            'name' => $get['name'],
            'deskripsi' => spoiler($get['deskripsi']),
            'bunga' => $get['bunga'],
            'jenis_bunga' => $get['jenis_bunga'],
            'gambar' => $gambar,
        ];
        return response_json($get);
    }   

    public function get_product_detail_deposito($id = '', $jenis_bunga = ''){
        if(!$id){
            return response_json([
                'message' => 'Params not found',
            ]);
        }
        
        if(!$jenis_bunga){
            $get = $this->db->query("SELECT c.id as id,c.product_type_id, c.name, c.deskripsi, b.bunga, b.jenis_bunga, b.jenis_bunga, b.bulan, b.bunga as id_conf FROM conf_bunga_deposito b
                LEFT JOIN product c ON b.id_deposit_produk = c.id
                WHERE c.id = '$id' ORDER BY b.bulan::int ASC")->result_array();
                // map_y($get);
        }else{
            $get = $this->db->query("SELECT c.id as id,c.product_type_id, c.name, c.deskripsi, b.bunga, b.jenis_bunga, b.jenis_bunga, b.bulan, b.bunga as id_conf FROM conf_bunga_deposito b
                LEFT JOIN product c ON b.id_deposit_produk = c.id
                WHERE c.id = '$id' AND b.jenis_bunga = '$jenis_bunga' ORDER BY b.bulan::int ASC")->result_array();
        }

        $get = @$get;

        $get[0] = [
            'id' => @$get[0]['id'],
            'product_type_id' => @$get[0]['product_type_id'],
            'name' => @$get[0]['name'],
            'deskripsi' => spoiler($get[0]['deskripsi']),
            'bunga' => @$get[0]['bunga'],
            'jenis_bunga' => @$get[0]['jenis_bunga'],
            'bulan' => @$get[0]['bulan'],
            'id_conf' => @$get[0]['id_conf'],
        ];
        if($id && !$jenis_bunga){
            $res = [
                'product' => $get[0],
            ];
        }else if($id && $jenis_bunga){
            $res = [
                'product' => @$get[0],
                'detail' => @$get,
            ];
        }

        // map_y($res);
        return response_json($res);
    }  
    
    public function provinsi(){
        $request = $_REQUEST;
        $get = $this->db->query("SELECT id, provinsi as name FROM _tbl_provinsi")->result_array();
        return response_json($get);
    }

    public function kota(){
        $request = $_REQUEST;
        if(isset($request['provinsi_id'])){
            $provinsi_id = $request['provinsi_id'];
            $get = $this->db->query("SELECT id, kabupaten_kota as name FROM _tbl_kabkot WHERE provinsi_id = '$provinsi_id'")->result_array();

        }else{

            $get = $this->db->query("SELECT id, kabupaten_kota as name FROM _tbl_kabkot")->result_array();
        }
        return response_json($get);
    }

    public function kecamatan(){
        $request = $_REQUEST;
        if(isset($request['kota_id'])){
            $kabkot_id = $request['kota_id'];
            $get = $this->db->query("SELECT id, kecamatan as name FROM _tbl_kecamatan WHERE kabkot_id = '$kabkot_id'")->result_array();

        }else{

            $get = $this->db->query("SELECT id, kecamatan as name FROM _tbl_kecamatan")->result_array();
        }
        return response_json($get);
    }

    public function kelurahan(){
        $request = $_REQUEST;
        if(isset($request['kecamatan_id'])){
            $kecamatan_id = $request['kecamatan_id'];
            $get = $this->db->query("SELECT id, kelurahan as name, kd_pos as kodepos FROM _tbl_kelurahan WHERE kecamatan_id = '$kecamatan_id'")->result_array();

        }else{

            $get = $this->db->query("SELECT id, kelurahan as name, kd_pos as kodepos FROM _tbl_kelurahan")->result_array();
        }
        return response_json($get);
    }

    public function ajax_select2_jateng_only(){
        $table =  'v_wilayah';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        $keyword = isset($request->term) ? $request->term : '';
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $columns = [
            'id as id',
            'kelurahan as text',
            'kelurahan as kelurahan',
            'kecamatan as kecamatan',
            'kabupaten_kota as kabupaten_kota',
            'provinsi as provinsi',
            'kodepos as kodepos',
        ];
        
        $conditions = [
            "WHERE provinsi_id = 13",
            "AND (kelurahan::text ILIKE '%".$keyword."%' 
                OR kodepos::text ILIKE '%".$keyword."%' 
                OR kecamatan::text ILIKE '%".$keyword."%' 
                OR kabupaten_kota::text ILIKE '%".$keyword."%'
                OR provinsi::text ILIKE '%".$keyword."%')"
        ];
        
        $query = "SELECT ".implode(', ', $columns)." FROM ".$table." ".implode(' ', $conditions)." ";

        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= 'OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
        /* EndCount */
        $endCount = $offset + $resultCount;
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );

        return response_json($results, 200);
    }

    public function get_bunga_efektif(){
        $request = $_REQUEST;
        $nper = $request['nper'];
        $pmt = $request['pmt'];
        $pv = $request['pv'] * -1;
        $fv = 0;
        $type = 0;
        $guess = 0;
        
        $hasil = RATE($nper, $pmt, $pv, $fv, $guess) * 1200;
        $hasil = round($hasil, 10);
        return response_json($hasil);
    }
    public function get_bulan_kredit($id = ''){
      
        $get = $this->db->query("SELECT id, bulan  FROM conf_bulan_kredit
         WHERE id_produk_kredit = '$id'")->result_array();
        $get = @$get;
        $html = '';
        foreach($get as $dt)
        {
            $html .= "<option value='".$dt['bulan']."'>".$dt['bulan']." Bulan</option>";
        }
        echo $html;
        
    }

    public function get_musiman_kredit($id = ''){
      
        $get = $this->db->query("SELECT musiman  FROM conf_musiman_kredit
         WHERE id_kredit_produk = '$id'")->result_array();
        return response_json($get);
    }

}