<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller{

	/**
	 * Theme name
	 *
	 * @var string
	 * @access public
	 */
	public $theme = 'material';

	/**
	 * Theme layout
	 *
	 * @var string
	 * @access public
	 */
	public $theme_layout = 'main';

	/**
	 * Theme module
	 *
	 * @var string
	 * @access public
	 */
	public $theme_module = '';

	/**
	 * Theme URL
	 *
	 * @var string
	 * @access public
	 */
	public $theme_url = '';

	/**
	 * CSS path in theme folder
	 *
	 * @var string
	 * @access public
	 */
	public $css_path = 'css';

	/**
	 * JS path in theme folder
	 *
	 * @var string
	 * @access public
	 */
	public $js_path = 'js';

	/**
	 * Image path in theme folder
	 *
	 * @var string
	 * @access public
	 */
	public $image_path = 'img';

	/**
	 * Header template name
	 *
	 * @var string
	 * @access public
	 */
	public $header = 'header.php';

	/**
	 * Main template name
	 *
	 * @var string
	 * @access public
	 */
	public $template = 'index.php';

	/**
	 * Footer template name
	 *
	 * @var string
	 * @access public
	 */
	public $footer = 'footer.php';

	/**
	 * Page Title
	 *
	 * @var string
	 * @access public
	 */
	public $title = '';

	/**
	 * Menu active
	 *
	 * @var string
	 * @access public
	 */
	public $menu = 'home';

	/**
	 * Body Class
	 *
	 * @var string
	 * @access public
	 */
	public $body_class = '';

	/**
	 * Extra Main Section Class
	 *
	 * @var string
	 * @access public
	 */
	public $section_class = '';

	/**
	 * Theme vars - preparing variables to be used in template file
	 *
	 * @var string
	 * @access protected
	 */
	protected $_theme_vars = array();

	/*
	* Site Settings
	*/
	public $settings;

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url', 'theme');
		$this->load->library('session');
		$this->load->database();

		$this->_theme_vars['active_theme'] = "material";
		$this->_theme_vars['active_admin_theme'] = "adminlte";

		$this->_get_active_theme("website");
		$this->_get_active_theme("admin");

		$this->setup_theme();
		$this->get_settings();

		/** Load Core Lang */
		$this->load_lang();
		$this->set_vars(['use_header' => TRUE]);
	}

	/**
	* Set Language
	*/
	public function switch_lang( $site_lang = NULL )
	{
		$site_lang = is_null($site_lang) ? 'english' : $site_lang;
		$this->session->set_userdata('site_lang', $site_lang);

		$this->settings_model->update_by(['name' => 'site_lang'], ['value' => $site_lang]);
		return $this;
	}

	/**
	* Load Language File
	*/
	public function load_lang( $lang_files = '', $lang = FALSE )
	{
		if (is_string($lang_files) && !empty($lang_files))
			$lang_files = explode(",", $lang_files);

		$lang_files = is_array($lang_files) ? array_merge($lang_files, ['core', 'menu']) : ['core', 'menu'];

		$this->lang->load($lang_files, (!$lang ? $this->settings->site_lang : $lang));
		return $this;
	}

	function setup_theme( $config = array() )
	{
		// setup theme url
		$theme = ( isset( $config[ 'theme' ] ) ? $config[ 'theme' ] : $this->theme );
		$this->set_theme( $theme );

		// setup default title page
		$title = ( isset( $config[ 'title' ] ) ? $config[ 'title' ] : NULL );
		$this->set_title( $title );

		// setup default path for css, js and images
		$this
			->set_css_path( isset( $config[ 'css_path' ] ) ? $config[ 'css_path' ] : $this->css_path )
			->set_js_path( isset( $config[ 'js_path' ] ) ? $config[ 'image_path' ] : $this->js_path )
			->set_image_path( isset( $config[ 'image_path' ] ) ? $config[ 'image_path' ] : $this->image_path );
	}

	/*
	* Get Site Settings
	*/
	public function get_settings()
	{
		$this->settings = new stdClass();

		/* get general Settings */
		$this->load->model('Settings_model', 'settings_model');
		$settings = $this->settings_model->get_all();

		foreach($settings as $setting)
		{
			$this->settings->{$setting->name} = $setting->value;
		}
	}

	/*
	* Set page title
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_title( $title = NULL )
	{
		if ( is_null( $title ) )
		{
			$router =& load_class('Router', 'core');
			// $title = $router->fetch_class() . ' | '. $router->fetch_method();
			$title = $router->fetch_method();
		}
		$this->title = ucwords( $title );
		return $this;
	}

	/*
	* Set CSS path
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_css_path( $path = NULL )
	{
		if ( is_null( $path ) ) $path = 'css';
		$this->css_path = $path;
		return $this;
	}

	/*
	* Set JS path
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_js_path( $path = NULL )
	{
		if ( is_null( $path ) ) $path = 'js';
		$this->js_path = $path;
		return $this;
	}

	/*
	* Set Image path
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_image_path( $path = NULL )
	{
		if ( is_null( $path ) ) $path = 'images';

		$this->image_path = $path;
		return $this;
	}

	/* add css in theme folder
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_css( $css_files )
    {
        // if $css_files is string, then convert into array
        $css_files = is_array( $css_files ) ? $css_files : explode( ",", $css_files );

        foreach( $css_files as $css )
        {
            // remove white space if any
            $css = trim($css);

            // go to next when passing empty space
            if ( empty( $css ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate css to be included
            $this->_theme_vars[ 'css_files' ][ sha1( $css ) ] = base_url("themes/{$this->theme}/{$this->css_path}/{$css}");
        }
        return $this;
    }

	/* add js in theme folder
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_js( $js_files )
    {
        // if $js_files is string, then convert into array
        $js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

        foreach( $js_files as $js )
        {
            // remove white space if any
            $js = trim( $js );

            // go to next when passing empty space
            if ( empty( $js ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate js to be included
            $this->_theme_vars[ 'js_files' ][ sha1( $js ) ] = base_url( "themes/{$this->theme}/{$this->js_path}/{$js}" );
        }
        return $this;
    }

	/* add inline js in theme folder
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_inline_js( $js_scripts )
    {
        $js = trim( $js_scripts );

		// go to next when passing empty space
		if ( empty( $js ) ) return $this;

		// using sha1( $js ) as a key to prevent duplicate js to be included
		$this->_theme_vars[ 'inline_js' ][ sha1( $js ) ] = $js;
        return $this;
    }

	/* add i18n js in theme folder
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_i18n_js( $js_files )
    {
        // if $js_files is string, then convert into array
        $js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

        foreach( $js_files as $js )
        {
            // remove white space if any
            $js = trim( $js );

            // go to next when passing empty space
            if ( empty( $js ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate js to be included
            $this->_theme_vars[ 'inline_js' ][ sha1( $js ) ] = $this->jsi18n->translate( "/themes/{$this->theme}/{$this->js_path}/{$js}" );
        }
        return $this;
    }

	/* add i18n js in theme folder
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_i18n_js_with( $js_files, $langs )
    {
        // if $js_files is string, then convert into array
        $js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

        foreach( $js_files as $js )
        {
            // remove white space if any
            $js = trim( $js );

            // go to next when passing empty space
            if ( empty( $js ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate js to be included
            $this->_theme_vars[ 'inline_js' ][ sha1( $js ) ] = $this->jsi18n->translate_with( "/themes/{$this->theme}/{$this->js_path}/{$js}", $langs );
        }
        return $this;
    }

	/* add css from external resources
  *
	* @access  public
  * @param   mixed
  * @return  chained object
  */
    function add_external_css( $css_files, $path = FALSE )
    {
        // if $css_files is string, then convert into array
        $css_files = is_array( $css_files ) ? $css_files : explode( ",", $css_files );

        foreach( $css_files as $css )
        {
            // remove white space if any
            $css = trim( $css );

            // go to next when passing empty space
            if ( empty( $css ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate css to be included
            $this->_theme_vars[ 'css_files' ][ sha1( $css ) ] = (is_string($path) ? $path.$css : $css);
        }
        return $this;
    }

	/* add js from external resources
     *
	 * @access  public
     * @param   mixed
     * @return  chained object
     */
    function add_external_js( $js_files, $path = FALSE )
    {
        // if $js_files is string, then convert into array
        $js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

        foreach( $js_files as $js )
        {
            // remove white space if any
            $js = trim( $js );

            // go to next when passing empty space
            if ( empty( $js ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate js to be included
            $this->_theme_vars[ 'js_files' ][ sha1( $js ) ] = (is_string($path) ? $path.$js : $js);
        }
        return $this;
    }

	/* adding css from plugins folder */
 	function add_css_plugins( $css_files )
 	{
 		// if $css_files is string, then convert into array
 		$css_files = is_array( $css_files ) ? $css_files : explode( ",", $css_files );

 		foreach( $css_files as $css )
 		{
 			// remove white space if any
 			$css = trim( $css );

 			// go to next when passing empty space
 			if ( empty( $css ) ) continue;

 			// using sha1( $css ) as a key to prevent duplicate css to be included
 			$this->_theme_vars[ 'css_files' ][ sha1( $css ) ] = base_url( "themes/{$this->theme}/plugins" ) . "/{$css}";
 		}

 		return $this;
 	}

 	/* adding js from plugins folder */
 	function add_js_plugins( $js_files, $is_i18n = FALSE )
 	{
 		if ( $is_i18n )
 			return $this->add_jsi18n_plugins( $js_files );

 		// if $css_files is string, then convert into array
 		$js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

 		foreach( $js_files as $js )
 		{
 			// remove white space if any
 			$js = trim( $js );

 			// go to next when passing empty space
 			if ( empty( $js ) ) continue;

 			// using sha1( $js ) as a key to prevent duplicate js to be included
 			$this->_theme_vars[ 'js_files' ][ sha1( $js ) ] = base_url( "themes/{$this->theme}/plugins" ) . "/{$js}";
 		}

 		return $this;
 	}

 	/* adding from plugins folder */
 	function add_jsi18n_plugins( $js_files )
 	{
 		// if $css_files is string, then convert into array
 		$js_files = is_array( $js_files ) ? $js_files : explode( ",", $js_files );

 		foreach( $js_files as $js )
 		{
 			// remove white space if any
 			$js = trim( $js );

 			// go to next when passing empty space
 			if ( empty( $js ) ) continue;

 			// using sha1( $js ) as a key to prevent duplicate js to be included
 			$this->_theme_vars[ 'js_files_i18n' ][ sha1( $js ) ] = $this->jsi18n->translate( "/themes/{$this->theme}/plugins/{$js}" );
 		}
 		return $this;
 	}

 	/*
 	* Load CSS and JS from plugins folder
 	*/
 	function load_plugins( $plugins = FALSE )
 	{
 		$plugins = is_array( $plugins ) ? $plugins : explode( ",", $plugins );

 		foreach( $plugins as $plugin )
 		{
 			// remove white space if any
 			$plugin = trim( $plugin );

 			// go to next when passing empty space
 			if ( empty( $plugin ) ) continue;

 			switch( strtolower( $plugin ) )
 			{
 				case 'bootbox' :
 					$this->add_js_plugins( "bootbox/bootbox.min.js" );
 					break;

 				case 'datepicker' :
 					$this
 						->add_css_plugins( "datepicker/datepicker3.css" )
 						->add_js_plugins( "datepicker/bootstrap-datepicker.js" )
 						->add_js_plugins( "datepicker/bootstrap-datepicker.config.js" );
 					break;

 				case 'icheck' :
 					$this
 						->add_css_plugins( "icheck/skins/flat/blue.css" )
 						->add_js_plugins( "icheck/icheck.js" )
 						->add_js_plugins( 'icheck/icheck.config.js', TRUE );
 						break;

 				case 'inputmask' :
 					$this
 						->add_js_plugins( "inputmask/jquery.inputmask.bundle.min.js" )
 						->add_js_plugins( 'inputmask/jquery.inputmask.config.js', TRUE );
 						break;

 				case 'select2' :
 					$this
 						->add_css_plugins( 'select2/select2.min.css' )
 						->add_js_plugins( 'select2/select2.min.js' )
 						->add_js_plugins( 'select2/select2.config.js', TRUE );
 					break;
 			}
		}
 		return $this;
 	}

	/*
	* Set theme active
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_theme( $theme = 'default' )
	{


		$this->theme = $theme;

		// theme changed, so change theme url too
		$this->theme_url = base_url( '/themes/public/' .$this->theme );
		return $this;
	}

	public function set_admin_theme( $theme = 'default' , $layout = 'main')
	{



		$this->theme = $theme;
		$this->theme_layout = $layout;

		// theme changed, so change theme url too
		$this->theme_url = base_url( '/themes/admin/' .$this->theme );
		return $this;
	}

	/*
	* Set template
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_template( $template_file = 'index.php' )
	{
		// make sure that $template_file has .php extension
        $template_file = substr( $template_file, -4 ) == '.php' ? $template_file : ( $template_file . ".php" );

		$this->template = $template_file;
		return $this;
	}

	/*
	* Set header template
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_header( $template_file = 'header.php' )
	{
		// make sure that $template_file has .php extension
    $template_file = substr( $template_file, -4 ) == '.php' ? $template_file : ( $template_file . ".php" );

		$this->header = $template_file;
		return $this;
	}

	/*
	* Set footer template
	*
	* @params string
	* @access public
	* @return object
	*/
	public function set_footer( $template_file = 'footer.php' )
	{
		// make sure that $template_file has .php extension
        $template_file = substr( $template_file, -4 ) == '.php' ? $template_file : ( $template_file . ".php" );

		$this->footer = $template_file;
		return $this;
	}

	/*
	* Render theme
	*
	* @params template string
	* @params data array
	* @access public
	* @return object
	*/
	public function render( $data = array(), $template = NULL )
	{
		// check if user passing $template name, overide current template
		if ( !is_null( $template ) )	$this->set_template( $template );

		// make sure base css theme included
		$this->add_css( $this->theme . '.css' );

		$this->refresh_vars();

		$data = array_merge( $this->_theme_vars, $data );

		$theme_header = "themes/{$this->theme}/{$this->header}";
		$theme_template = "themes/{$this->theme}/{$this->template}";
		$theme_footer = "themes/{$this->theme}/{$this->footer}";

		// load template from theme folder
		if (!file_exists( FCPATH.$theme_header) &&
				!file_exists( FCPATH.$theme_template) &&
			 		!file_exists( FCPATH.$theme_footer))
						die('Template File for '.$this->theme.' not found.');

		if (file_exists( FCPATH."themes/{$this->theme}/{$this->header}"))
			$this->load->view( "../../themes/{$this->theme}/{$this->header}", $data );

		if (file_exists( FCPATH."themes/{$this->theme}/{$this->template}"))
			$this->load->view( "../../themes/{$this->theme}/{$this->template}", $data );

		if (file_exists( FCPATH."themes/{$this->theme}/{$this->footer}"))
			$this->load->view( "../../themes/{$this->theme}/{$this->footer}", $data );
	}

	public function render_content( $view = '' )
	{
		$vars = $this->refresh_vars();
		$data[ 'content' ] = ( empty($view) ? $view : $this->load->view( $view, $vars, TRUE) );
		$this->render( $data );
	}
        
    public function render_view( $view = '' , $template = NULL, $data = array()){
            
        // check if user passing $template name, overide current template
		if ( !is_null( $template ) )	$this->set_template( $template );

		// map_y($this->theme);
		// make sure base css theme included
		$this->add_css( $this->theme . '.css' );

		$this->refresh_vars();
                
		if($view!=''){
			$view = $view.'.php';
		}
                
		
		$data = array_merge( $this->_theme_vars, $data );
		// map_y($data);

		$theme_header = "themes/public/{$this->theme}/{$this->header}";
		$theme_view = "themes/public/{$this->theme}/{$view}";
		$theme_footer = "themes/public/{$this->theme}/{$this->footer}";

		// load template from theme folder
		if (!file_exists( FCPATH.$theme_header) &&
				!file_exists( FCPATH.$theme_view) &&
			 		!file_exists( FCPATH.$theme_footer))
						die('Template File for '.$this->theme.' not found.');

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$this->header}"))
			$this->load->view( "../../themes/public/{$this->theme}/{$this->header}", $data );

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$view}"))
			$this->load->view( "../../themes/public/{$this->theme}/{$view}", $data );

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$this->footer}"))
			$this->load->view( "../../themes/public/{$this->theme}/{$this->footer}", $data );
                        
                        
            
    }

     public function render_view_dukcapil( $view = '' , $template = NULL, $data = array()){
            
                // check if user passing $template name, overide current template
		if ( !is_null( $template ) )	$this->set_template( $template );

		// make sure base css theme included
		$this->add_css( $this->theme . '.css' );

		$this->refresh_vars();
                
                if($view!=''){
                    $view = $view.'.php';
                }
                

		$data = array_merge( $this->_theme_vars, $data );

		$theme_header = "themes/public/{$this->theme}/{$this->header}";
		$theme_view = "themes/public/{$this->theme}/{$view}/$data";
		$theme_footer = "themes/public/{$this->theme}/{$this->footer}";

		// load template from theme folder
		if (!file_exists( FCPATH.$theme_header) &&
				!file_exists( FCPATH.$theme_view) &&
			 		!file_exists( FCPATH.$theme_footer))
						die('Template File for '.$this->theme.' not found.');

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$this->header}"))
			$this->load->view( "../../themes/public/{$this->theme}/{$this->header}", $data );

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$view}$data"))
			$this->load->view( "../../themes/public/{$this->theme}/{$view}$data", $data );

		if (file_exists( FCPATH."themes/public/{$this->theme}/{$this->footer}"))
			$this->load->view( "../../themes/public/{$this->theme}/{$this->footer}", $data );
                        
                        
            
	}
	
	public function render_to_admin($html = '<h1>THIS CONTENT</h1>', $template = NULL, $data = array())
	{
		if (!is_null($template))	$this->set_template($template);

		// make sure base css theme included
		$this->add_css($this->theme . '.css');

		$this->refresh_vars();

		$data = array_merge($this->_theme_vars, $data);
		$data['html'] = $html;


		$theme = "themes/admin/{$this->theme}";

		// map_y($theme_layout);
		// load template from theme folder
		if (!file_exists(FCPATH . $theme)) {
			die('Template File for ' . $this->theme . ' not found.');
		}
		// map_y("../../themes/admin/{$this->theme}/layout/{$this->theme_layout}");
		$this->load->view("../../themes/admin/{$this->theme}/layout/{$this->theme_layout}", $data);
	}

    public function render_admin_view( $view = '' , $template = NULL, $data = array()){
       	if( !is_null( $template ) )	$this->set_template( $template );

		// make sure base css theme included
		$this->add_css( $this->theme . '.css' );

		$this->refresh_vars();
                
                if($view!=''){
                    $view = $view.'.php';
                }
                

		$data = array_merge( $this->_theme_vars, $data );
		$data['module_view'] = $view;
		
		$theme_view = "themes/admin/{$this->theme}/module/{$this->theme_module}/{$view}";

		$theme_layout = "themes/admin/{$this->theme}/layout/{$this->theme_layout}";

		// map_y($theme_layout);
		// load template from theme folder
		if (!file_exists( FCPATH.$theme_view) &&
			 		!file_exists( FCPATH.$theme_layout))
						die('Template File for '.$this->theme.' not found.');

		if (file_exists( FCPATH."themes/admin/{$this->theme}/module/{$this->theme_module}/{$view}"))
			$this->load->view( "../../themes/admin/{$this->theme}/layout/{$this->theme_layout}", $data );
                        
                        
            
    }

    public function render_cinta_view( $template = NULL, $data = array()){
        // map_y($);
       	if ( !is_null( $template ) )	$this->set_template( $template );

		// make sure base css theme included
		$this->add_css( $this->theme . '.css' );

		$this->refresh_vars();
               
		$data = array_merge( $this->_theme_vars, $data );
		$theme_layout = "themes/admin/{$this->theme}/layout/{$this->theme_layout}";
		$this->load->view( "../../themes/admin/{$this->theme}/layout/{$this->theme_layout}", $data );
                               
    }


	/*
	* Set custom vars
	*/
	function set_vars( $prop, $val = FALSE)
	{
		// check wether $prop is array
		if ( is_array( $prop ) )
		{
			foreach( $prop as $_prop => $_val )
				$this->_set_vars( $_prop, $_val );
		}
		else
			$this->_set_vars( $prop, $val );

		return $this;
	}

	/*
	* protected set var
	*/
	protected function _set_vars( $prop, $val )
	{
		// if property belongs to class then assign to its property
		if ( isset( $this->{$prop} ) )
			$this->{$prop} = $val;
		else
			// if not, then set new property value
			$this->_theme_vars[ $prop ] = $val;
	}

	public function listFolderFiles($dir)
    {
        $fileInfo     = scandir($dir);
        $allFileLists = [];

        foreach ($fileInfo as $folder) {
            if ($folder !== '.' && $folder !== '..') {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $folder) === true) {
                    $allFileLists[$folder . '/'] = $this->listFolderFiles($dir . DIRECTORY_SEPARATOR . $folder);
                } else {
                    $allFileLists[$folder] = $folder;
                }
            }
        }

        return $allFileLists;
    }

	protected function _get_active_theme($type){

		if($type!=null){

            if($type=="admin"){

                $parent_dir = "admin";
                $themes_path = $_SERVER['DOCUMENT_ROOT'].'/themes/'.$parent_dir;
                

            }else{

                $parent_dir = "public";
                $themes_path = $_SERVER['DOCUMENT_ROOT'].'/themes/'.$parent_dir;
               
            }


            $scan_path = $this->listFolderFiles($themes_path);

            $themes_dir = array_keys($scan_path);

            for($i=0;$i < count($themes_dir);$i++){

                $json = $themes_path.'/'.$themes_dir[$i].'theme.json';         

                if(file_exists($json)){

                    $meta_theme = file_get_contents($json);
                    $info = json_decode($meta_theme,true);
                    
                    if(isset($info['enabled'])){

                    	if($info['enabled']){

                    		if($type=="admin"){

                    			$this->_theme_vars['active_admin_theme'] = trim(str_replace("/","",$themes_dir[$i]));

                    		}else{

                    			$this->_theme_vars['active_theme'] = trim(str_replace("/","",$themes_dir[$i]));

                    		}

                    		break;
                    		
                    	}

                    }
                    
                }

            }
            

        }

	}

	/*
	* Refresh theme_vars value
	*/
	public function refresh_vars()
	{
		foreach( $this as $prop => $val )
			$this->_theme_vars[ $prop ] = $val;

		return $this->_theme_vars;
	}

	/*
	* Default 404 Page
	*/
	public function not_found( $message = NULL )
	{
		$this
			->set_title( i18n('404_title') )
			->set_template( '404' );

		$this->set_vars([
			'heading' => i18n('404_title'),
			'message' => is_null( $message ) ? i18n('page_not_found') : $message
		]);

		$vars = $this->refresh_vars();
		$this->render($vars);
	}

	function forbiden( $message = NULL )
	{
		$this->set_title( i18n('403_title') )
			->set_header('blank_header')
			->set_template( '404' )
			->set_footer('blank_footer');

		$this->set_vars([
			'heading' => i18n('403_title'),
			'message' => is_null( $message ) ? i18n('forbiden_page') : $message
		]);

		$vars = $this->refresh_vars();
		$this->render($vars);
	}

	/**
	* Send Email
	*/
	public function send_email( $config = FALSE, $debug = FALSE)
	{
		$default = ['to' => 'arifrahmanhakim.net@gmail.com', 'subject' => 'Email Subject', 'message' => 'Message Body '];
		$config = array_merge($default, $config);

		$this->config->load( 'email' );
		$this->load->library('email');

		$this->email->from($this->settings->sender_email, $this->settings->sender_name);
		$this->email->to( $config['to'] );

		$this->email->bcc('arman.is.nice@gmail.com') ;
		//$this->email->bcc('info@diginiq.com') ;

		$this->email->subject($config['subject']);
		$this->email->message($config['message']);
		$sent = $this->email->send();

		if ($debug)
			echo $this->email->print_debugger();
	}

	public function test_email()
	{
		$this->send_email([], TRUE);
	}

	/**
	* Extends _set_user_variables
	*/
	public $user_data = NULL;

	protected function _set_user_variables()
	{
		parent::_set_user_variables();
		$this->user_data = $this->auth_data;
	}

	protected function debug_vars( $vars )
	{
		echo '<pre>';
		print_r($vars);
		echo '</pre>';
	}
}

/**
 * Admin Controller - Base Controller for Logged in Users
 * must force to login to access this controller
 * can be extends for different roles and level access
 */

class Admin_Controller extends MY_Controller{
}

/**
 * Public Controller for public access page
 *
 * @author      Arif Rahman Hakim
 * @copyright   Copyright (c) 2016, Arif Rahman Hakim. (http://github.com/arif-rh)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://github.com/arif-rh/
 */

class Public_Controller extends MY_Controller{
}


/* End of file MY_Controller.php */
/* Location: /application/core/MY_Controller.php */
