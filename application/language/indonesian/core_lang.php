<?php

/**
 * Core English language
 */

$lang['application_name'] = 'Rakuten';
$lang['copyright'] = 'Copyright &copy; 2016';

$lang['404_title'] = '404 Error - Not Found';
$lang['page_not_found'] = 'This page is not found or missing. Please, make sure you type correct url.';
$lang['403_title'] = '403 Error - Forbiden';
$lang['forbiden_page'] = 'You have no access right to this page/module. Please contact your Administrator.';

$lang['back_to_top_button'] = 'Back to Top';
$lang['submit_button'] = 'Submit';
$lang['confirm_button'] = 'Confirm';
$lang['edit_button'] = 'Edit';
$lang['OK_button'] = 'OK';
$lang['save_button'] = 'Save';
$lang['add_button'] = 'Add';

// valdiation message
$lang['must_use_hiragana'] = '%s Must be Hiragana';
$lang['email_has_been_regitered'] = 'Email has already been registered';
$lang['username_has_been_regitered'] = 'Username has alredy been used';

$lang['ui_day'] = 'dd';
$lang['ui_month'] = 'mm';
$lang['ui_year'] = 'yyyy';

// Fields
$lang['tel'] = 'TEL';
$lang['mobile'] = 'mobile';
$lang['email'] = 'email';
$lang['fax'] = 'FAX';
$lang['address'] = 'Address';
$lang['postalCode'] = 'Postal Code';
$lang['male'] = '男性';
$lang['female'] = '女性';
$lang['organization'] = 'Organization';
$lang['action'] = 'Action';

/* Co-Workers lang */
$lang['firstName'] = 'First Name';
$lang['lastName'] = 'Last Name';
$lang['fullName'] = 'Co Worker Name';
$lang['firstNameKana'] = 'First Name (Kana)';
$lang['lastNameKana'] = 'Last Name (Kana)';
$lang['fullNameKana'] = 'Co Worker Name (Kana)';
$lang['gender'] = 'Gender';
$lang['dateofBirth'] = 'Date of Birth';
/* Project Schedule */
$lang['start'] = 'Start';
$lang['end'] = 'End';
$lang['toDo'] = 'To Do';
$lang['schedule'] = 'Schedule';

/* Past Application */
$lang['price'] = 'Price';
$lang['result'] = 'Result';

/* other app */
$lang['year'] = 'Year';
