<?php

if ( ! function_exists('renderMenus')){

    function renderMenus($menus)
    {

        $ci =& get_instance();
        $ci->load->model('page/Mpage','mpage');

        $langs = $ci->db->get_where('page_languages', array('status' => '1'))->result_array();

        $html = '';

        if(count($menus)>0){

              $html .= '<ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">';

            foreach ($menus as $menu) {
                
                $childs = $ci->mpage->getMenuChilds($menu['id']);
            
                $html .= '<li style="display: list-item;margin-bottom: 5px;margin-top: 5px;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_'.$menu['id'].'" data-foo="bar">';
                $html .= '<div class="menuDiv">';
                $html .= '<span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">';
                $html .= '<span></span>';
                $html .= '</span>';
                $html .= '<span title="Click to show/hide item editor" data-id="'.$menu['id'].'" class="expandEditor ui-icon ui-icon-triangle-1-n">';
                $html .= '<span></span>';
                $html .= '</span>';
                $html .= '<span>';
                $html .= '<span data-id="'.$menu['id'].'" class="itemTitle"></span>';
                $html .= '</span>';
                $html .= '<div id="menuEdit'.$menu['id'].'" class="menuEdit">';
                $html .= '<p>';
                $html .= $menu['title'];
                $html .= '<a href="javascript:editMenu('.$menu['id'].');" class="btn btn-sm btn-primary pull-right"><i class="fa fa-pencil"></i></a>';
                $html .= '<a href="javascript:deleteMenu('.$menu['id'].');" class="btn btn-sm btn-danger pull-right" style="margin-right: 5px;"><i class="fa fa-remove"></i></a>';

                $html .= '<form id="frm_menu_edit_'.$menu['id'].'" style="padding: 10px; display: none">';
                
                $html .= '<input type="hidden" name="id" value="'.@$menu['id'].'">';
                   
                $html .= '<div class="form-group">';
                $html .= '<label>Ikon</label>';
                $html .= '<input type="text" name="icon" value="'.@$menu['icon'].'" class="form-control" placeholder="contoh : fa-cogs">';
                $html .= '</div>';
                   
                $html .= '<div class="form-group">';
                $html .= '<label>Label & Link</label>';
                $html .= '<ul class="nav nav-tabs" role="tablist">';
                           
                            
                foreach($langs as $key => $value){
                    if($key == 0){
                        $class="active";
                    }else{
                        $class=""; 
                    } 
                        
                    $html .= '<li role="presentation" class="'.$class.'"><a href="#menu_'.$value['id'].'_'.$menu['id'].'" aria-controls="menu_'.$value['id'].'_'.$menu['id'].'" role="tab" data-toggle="tab">'.$value['name'].'</a></li>';
                }
                
                $html .= '</ul>';
                $html .= '<div class="tab-content">';
                            
                foreach($langs as $key => $value){
                    if($key == 0){
                        $class="active";
                    }else{
                        $class=""; 
                    }
                    
                    $page_contents = $ci->db->get_where('page_contents',array('language_id' => $value['id']))->result_array();
                    $menu_content = $ci->db->get_where('page_menu_contents',array('language_id' => $value['id'],'page_menu_id' => $menu['id']))->result_array();
                            
                            
                    $html .= '<div role="tabpanel" class="tab-pane '.$class.'" id="menu_'.$value['id'].'_'.$menu['id'].'">';
                    $html .= '<br>';
                    $html .= '<div class="form-group">';
                    $html .= '<label>Label ('.$value['name'].')</label>';
                    $html .= '<input type="text" name="label_'.$value['id'].'" class="form-control" value="'.@$menu_content[0]['label'].'" placeholder="Label menu dalam '.$value['name'].'">';
                    $html .= '</div>';
                    $html .= '<div class="form-group">';
                    $html .= '<label>Halaman ('.$value['name'].')</label>';
                    $html .= '<select class="form-control" name="page_content_'.$value['id'].'">';
                    $html .= '<option value=""> --- </option>';
                            foreach($page_contents as $page){
                                if($page['id'] == @$menu_content[0]['page_content_id']){
                                    $selected = "selected";
                                }else{
                                    $selected = "";
                                }
                                $html .= '<option '.$selected.' value="'.$page['id'].'">'.$page['link_label'].'</option>';
                            }
                    $html .= '</select>';
                    $html .= '</div>';
                    $html .= '<div class="form-group">';
                    $html .= '<label>Url (jika ingin gunakan halaman eksternal)</label>';
                    $html .= '<input type="text" name="url_'.$value['id'].'" class="form-control" value="'.@$menu_content[0]['url'].'" placeholder="contoh : http://www.google.com">';
                    $html .= '</div>';
                    $html .= '</div>';

                } 
                $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="form-group">';
                $html .= '<label>Status</label>';
                $html .= '<select class="form-control" name="status">';
                
                if($menu['status']=="1"){

                    $html .= '<option value="0"> Draft </option>';
                    $html .= '<option selected value="1"> Publish </option>';

                }else{

                    $html .= '<option value="0" selected> Draft </option>';
                    $html .= '<option value="1"> Publish </option>';

                }
                
                $html .= '</select>';
                $html .= '</div>';
                $html .= '<br><button type="button" class="btn btn-primary" onclick="update_menu('.@$menu['id'].');">Update</button>';
                $html .= '</form>';

                $html .= '</p>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= renderMenus($childs);
                $html .= '</li>';
                
            }

            $html .= '</ol>';

        }

        return $html;

    }

}

if ( ! function_exists('renderNavbarMenus')){

    function renderNavbarMenus()
    {
        $ci =& get_instance();
        $ci->load->model('page/Mpage','mpage');
        $lang = $ci->uri->segment(3,'ID');
        $selected_lang = $ci->db->get_where('page_languages', array('code' => $lang))->result_array();
        $language_id = @$selected_lang[0]['id'];

        if($language_id == null){
            $selected_lang = $ci->db->get_where('page_languages', array('code' => 'ID'))->result_array();
            $language_id = @$selected_lang[0]['id'];
        }

        $query_menu = "SELECT * FROM v_page_menu WHERE language_id='$language_id' AND parent IS NULL";
        $data = $ci->db->query($query_menu)->result_array();

        return renderRecursiveNavbarMenu($data, true);

    }

}

if ( ! function_exists('renderRecursiveNavbarMenu')){

    function renderRecursiveNavbarMenu($menus, $parent_menu = false)
    {
        $ci =& get_instance();
        $ci->load->model('page/Mpage','mpage');

        if($parent_menu){

            $html = '<ul class="navbar-nav navbar-right ml-auto">';

            foreach ($menus as $menu) {
                
                $page = $ci->db->get_where('v_page', array("page_content_id" => $menu['page_content_id']))->result_array();

                $childs = $ci->mpage->getMenuChildByLangID($menu['id'],$menu['language_id']);

                if(count($childs)>0){

                    $html.='<li class="nav-item dropdown fade-down">';
                    $html.='<a class="nav-link dropdown-toggle" href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$menu['label'].'</a>';
                    $html.= renderRecursiveNavbarMenu($childs,false);
                    $html.='</li>';

                }else{

                    $link = base_url().'post/article/'.$menu['code'].'/'.$menu['page_id'].'/'.@$page[0]['permalink'];

                    $html.= '<li class="nav-item active">';
                    $html.= '<a class="nav-link" href="'.$link.'">'.$menu['label'].'</a>';
                    $html.= '</li>';

                }

            }

            $html.='</ul>';

        }else{

            $html = '<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">';

            foreach ($menus as $menu) {
                
                $page = $ci->db->get_where('v_page', array("page_content_id" => $menu['page_content_id']))->result_array();

                $childs = $ci->mpage->getMenuChildByLangID($menu['id'],$menu['language_id']);

                if(count($childs)>0){

                    $html.='<li class="dropdown-submenu">';
                    $html.='<a class="dropdown-item" href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$menu['label'].'</a>';
                    $html.= renderRecursiveNavbarMenu($childs,false);
                    $html.='</li>';

                }else{

                    $link = base_url().'post/article/'.$menu['code'].'/'.$menu['page_id'].'/'.@$page[0]['permalink'];

                    $html.= '<li class="dropdown-item">';
                    $html.= '<a href="'.$link.'">'.$menu['label'].'</a>';
                    $html.= '</li>';

                }

            }

            $html.='</ul>';


        }
       
        return $html;
    }

}

if ( ! function_exists('renderLangSelector')){

    function renderLangSelector()
    {
        $ci =& get_instance();
        $lang = $ci->uri->segment(3,'ID');
        $languages = $ci->db->get_where('page_languages', array('status' => '1'))->result_array();

        $html = '<select id="lang_selector" class="selectpicker" data-width="fit">';

        foreach ($languages as $lang_item) {
            
            if($lang_item['code'] == $lang){
                $selected='selected';
            }else{
                $selected='';
            }

            $html.='<option '.$selected.' value="'.$lang_item['code'].'" data-content=\'<span class="flag-icon '.$lang_item['flag_icon'].'"></span> '.$lang_item['name'].'\'>'.$lang_item['name'].'</option>';

        }
        
        $html .='</select>';
        return $html;  
    }
}