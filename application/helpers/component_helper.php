<?php

if ( ! function_exists('renderContent')){

    function renderContent($content)
    {
       
        preg_match_all("/\[([^\]]*)\]/", $content, $matches);
        
        foreach ($matches[1] as $component) {
            $comp_attrs = explode(" ",$component);

            if($component=='home_slider/'){
                $content = str_replace("[home_slider/]", getHomeSlider(), $content);
            }

            if($component=='section_1'){
                $content = str_replace("[section_1]", getSection('open_tag'), $content);
                $content = str_replace("[/section_1]", getSection('closing_tag'), $content);
            }

            if($component=='section_2'){
                $content = str_replace("[section_2]", getSection2('open_tag'), $content);
                $content = str_replace("[/section_2]", getSection2('closing_tag'), $content);
            }

            if($component=='section_column'){
                $content = str_replace("[section_column]", getSection3('open_tag'), $content);
                $content = str_replace("[/section_column]", getSection3('closing_tag'), $content);
            }

            if($component=='column_left'){
                $content = str_replace("[column_left]", getLeftColumn('open_tag'), $content);
                $content = str_replace("[/column_left]", getLeftColumn('closing_tag'), $content);
            }

            if($component=='column_right'){
                $content = str_replace("[column_right]", getRightColumn('open_tag'), $content);
                $content = str_replace("[/column_right]", getRightColumn('closing_tag'), $content);
            }

            if($component=='services/'){
                $content = str_replace("[services/]", getServices(), $content);
            }

            if(@$comp_attrs[0]=='button'){
                $attr = explode("=",@$comp_attrs[1]);
                if(@$attr[0]=="url"){
                    $content = str_replace("[button url=".@$attr[1]."]",'<a href="'.@$attr[1].'" class="btn btn-warning radius">',$content);
                    $content = str_replace("[/button]",'</a>',$content);
                }
            }

            if(@$comp_attrs[0]=='image'){
                $attr = explode("=",@$comp_attrs[1]);
                if(@$attr[0]=="url"){
                    $content = str_replace("[image url=".@$attr[1]." /]",'<img src="'.base_url().'upload/noimg.webp" data-src="'.base_url().'/assets/dist/img/empeloye.png" data-src="'.@$attr[1].'" class="lazy img-thumbnaild" />',$content);
                }
            }
        }
       

        return $content;

    }
    

}

if ( ! function_exists('getServices')){

    function getServices()
    {

        $ci =& get_instance();
        $ci->load->model('Mpub','m');
        
        $d = h_get_icon('i_kredit','value');
        $d2 = h_get_icon('i_tabungan','value');
        $d3 = h_get_icon('i_deposito','value');

        if(isset($d->value)){ 
            $img1 = base_url('upload/photo/').$d->value; 
        }else{
            $img1= $theme_url."/img_frontend/Ic-Pinjaman.png";
        }

        if(isset($d2->value)){ 
            $img2 = base_url('upload/photo/').$d2->value; 
        }else{
            $img2= $theme_url."/img_frontend/Ic-Tabungan.png";
        }

        if(isset($d3->value)){ 
            $img3 = base_url('upload/photo/').$d3->value; 
        }else{
            $img3 = $theme_url."/img_frontend/Ic-Deposito.png";
        }

        $html ='<div class="container">
                    <div class="row pt-2" style="justify-content: center;">
                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".1s" >
                            <div class="text-center">
                                
                                <a href="'.base_url('public/home/kredit').'" target="_blank">
                                
                                    <img src="'.base_url().'upload/noimg.webp" data-src="'.$img1.'"  class="lazy rounded cf" alt="...">
                                </a>
                                <br>
                                <p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Kredit</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".2s" >
                            <div class="text-center">
                                
                                <a href="'.base_url('public/home/tabungan').'" target="_blank">
                                    
                                    <img src="'.base_url().'upload/noimg.webp" data-src="'.$img2.'"  class="lazy rounded cf" alt="...">
                                </a>
                                <br>
                                <p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Tabungan </p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".3s" >
                            <div class="text-center">
                                
                                <a href="'.base_url('public/home/deposito').'" target="_blank">
                                    
                                    <img src="'.$img3.'"  class="rounded cf" alt="...">
                                </a>
                                <br>
                                <p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Deposito </p>
                            </div>
                        </div>
                    </div>
                </div>';
        return $html;
    }

}

if ( ! function_exists('getHomeSlider')){

    function getHomeSlider()
    {

        $ci =& get_instance();
        $ci->load->model('Mpub','m');
        $sliders = $ci->m->getslide();

        $html = '<section class="cepiri-slider">
        <div id="carouselExampleCaptions" class="carousel slide carousel-fade " data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">';

        if($sliders){ 
            $no = 1; 
            foreach($sliders as $slider) { 
                if($no == 1){ 
                    $action = "active"; 
                }else{ 
                    $action = ""; 
                }
                $html.='<div class="carousel-item '.$action.'">';
                $html.='<img src="'.base_url().'upload/slideno.jpg" data-src="'.base_url('upload/photo/').$slider['foto'].'" class="d-block w-100 lazy" alt="...">'; 
                $html.='</div>';
                $no++; 
            } 
        }
        $html.='</div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
        </section>';

        return $html;

    }
    

}

if ( ! function_exists('getSection')){

    function getSection($tag)
    {

        $html = '';
        
        if($tag=='open_tag'){
            $html.='<section class="add-section _spad_pad">
                        <div class="container">
                            <div class="add-warp">
                                <div class="row add-text-warp text-produk pt-5  ">
                                    <div class="col-lg-12 text-center text-hero wow animated fadeInUp animated" data-wow-delay=".1s" >';
        }else{
            $html.='                </div> 
                                </div>
                            </div>
                        </div>
                    </section>
                    <div style="background: #f8f9fa;"> 
                    <img src="'.base_url().'assets/images/bannerbottom.webp" width="100%">
                    </div>';
        }
        
        return $html;
    
    }
    
}

if ( ! function_exists('getSection2')){

    function getSection2($tag)
    {

        $html = '';
        
         if($tag=='open_tag'){
            $html.='<section class="add-section _spad_pad" style="background: #f8f9fa;">
                        <div class="container">
                            <div class="add-warp">
                                <div class="row add-text-warp text-produk pt-5  ">
                                    <div class="col-lg-12 text-center text-hero wow animated fadeInUp animated" data-wow-delay=".1s" >';
        }else{
            $html.='                </div> 
                                </div>
                            </div>
                        </div>
                    </section>';
        }
        
        return $html;
    
    }
    
}

if ( ! function_exists('getSection3')){

    function getSection3($tag)
    {

        $html = '';
        
         if($tag=='open_tag'){
            $html.='<section class="review-section" style="background-image:url(\''.base_url().'assets/images/bgvectorredx1.png\');background-size:cover;">
                        <div class="container">
                            <div class="row flex">';
        }else{
            $html.='        </div>
                        </div>
                    </section>';
        }
        
        return $html;
    
    }
    
}

if ( ! function_exists('getLeftColumn')){

    function getLeftColumn($tag)
    {

        $html = '';
        
         if($tag=='open_tag'){
            $html.='<div class="col-lg-6">';
        }else{
            $html.='</div>';
        }
        
        return $html;
    
    }
    
}

if ( ! function_exists('getRightColumn')){

    function getRightColumn($tag)
    {

        $html = '';
        
         if($tag=='open_tag'){
            $html.='<div class="col-lg-6 align-selft text-center">';
        }else{
            $html.='</div>';
        }
        
        return $html;
    
    }
    
}