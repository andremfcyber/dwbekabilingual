<?php
/**
 * Created by PhpStorm.
 * User: indra
 * Date: 20/04/17
 * Time: 11:26
 */

if ( ! function_exists('setWidget')){

    function setWidget($section)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $widget = $ci->MDashboard->getWidget($section);

        foreach ($widget as $w){

            getWidget($w['name'],$w['widget_id']);

        }


    }

}

if ( ! function_exists('getWidget')){

    function getWidget($name,$id)
    {

       switch ($name){

           case "article_count":

               generateWidgetArticleCount($id);

               break;
           case "upcoming_event_count":

               generateWidgetUpcomingEventCount($id);

               break;
           case "visitor_by_city":

               generateWidgetVisitorByCity($id);

               break;
           case "latest_registered_user":

               generateWidgetLatestRegisteredUser($id);

               break;
           case "subscriber_count":

               generateWidgetSubscriberCount($id);

               break;
           case "active_ads":

               generateWidgetActiveAdsCount($id);

               break;
           case "current_year_visitor":

               generateWidgetVisitor($id);

               break;
           case "top_viewer_article":

               generateWidgetTopArticleView($id);

               break;
           case "top_comment_article":

               generateWidgetTopArticleComment($id);

               break;
           case "current_month_event":

               generateWidgetCurrentMonthEvent($id);

               break;
           case "upcoming_event":

               generateWidgetUpcomingEvent($id);

               break;
           case "top_upcoming_event":

               generateWidgetTopUpcomingEvent($id);

               break;


       }


    }

}
if ( ! function_exists('generateWidgetArticleCount')){

    function generateWidgetArticleCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $article_count = $ci->MDashboard->getArticleStatCount();


        $html = '<div class="small-box bg-aqua" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$article_count.'</h3>
        
                        <p>Articles</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-paper"></i>
                    </div>
                    <a href="'.base_url().'magazine/magazine_article" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetUpcomingEventCount')){

    function generateWidgetUpcomingEventCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $upcoming_event_count = $ci->MDashboard->getUpcomingEventStatCount();


        $html = ' <div class="small-box bg-green" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$upcoming_event_count.'</h3>
        
                        <p>Upcoming Events</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-calendar"></i>
                    </div>
                    <a href="'.base_url().'event/event" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetSubscriberCount')){

    function generateWidgetSubscriberCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $subscribers_count = $ci->MDashboard->getSubscriberStatCount();


        $html = '<div class="small-box bg-yellow" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$subscribers_count.'</h3>
        
                        <p>Subscribers</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-at"></i>
                    </div>
                    <a href="'.base_url().'newsletter/subscriber" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetActiveAdsCount')){

    function generateWidgetActiveAdsCount($id)
    {

        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $active_ads_count = $ci->MDashboard->getAdsActiveStatCount();


        $html = ' <div class="small-box bg-red" id="'.$id.'">
                    <div class="inner">
                        <h3>'.$active_ads_count.'</h3>
        
                        <p>Active Ads</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-earth"></i>
                    </div>
                    <a href="'.base_url().'magazine/ads" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetVisitorByCity')){

    function generateWidgetVisitorByCity($id)
    {

        $html = '<div class="box box-success">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Visitors By City</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="pad">
                                    <!-- Map will be created here -->
                                    <div id="world-map-markers" style="height: 360px;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetLatestRegisteredUser')){

    function generateWidgetLatestRegisteredUser($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $lastest_registered_users = $ci->MDashboard->getLatestRegisteredUsers();

        $html = ' <div class="box box-danger" id="'.$id.'">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Registered User</h3>

                <div class="box-tools pull-right">
                    <span class="label label-danger">8 latest user</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">';

            foreach ($lastest_registered_users as $user){

                $date = date_create($user['date_created']);
                $join_date = date_format($date,"d-m-Y");

                if($user['picture'] != ""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $file = $upload_path.'/'.$user['picture'];

                    if(file_exists($file)){

                        $path = base_url().'upload/photo/'.$user['picture'];

                    }else{

                        $path = base_url().'assets/dist/img/default_profile.png';

                    }

                }else {

                    $path = base_url().'assets/dist/img/default_profile.png';

                }

                $html .= '<li>
                            <div class="image-container">
                                <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                            </div>
                            <a class="users-list-name" href="#">'.$user['first_name'].' '.$user['last_name'].'</a>
                            <span class="users-list-date">'.$join_date.'</span>
                          </li>';

            }

        $html .='</ul>
                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="'.base_url().'user/user" class="uppercase">View All Users</a>
            </div>
            <!-- /.box-footer -->
        </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetVisitor')){

    function generateWidgetVisitor($id)
    {


        $html = '<div class="box box-solid bg-teal-gradient">
                    <div class="box-header" id="'.$id.'">
                        <i class="fa fa-th"></i>
        
                        <h3 class="box-title">Visitor In '.date('Y').'</h3>
        
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div>
                    <div class="box-footer no-border">
                        <div class="row">
                            <div class="col-md-12"><span class="label label-primary">yAxis : Visitor Count</span> <span class="label label-danger">xAxis : Month</span></div>
                        </div>
                    </div>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetTopArticleView')){

    function generateWidgetTopArticleView($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $top_view_articles = $ci->MDashboard->getTopViewArticle();

        $html = '<div class="box box-primary">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Top Viewer Articles</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                        if(count($top_view_articles)>0) {

                            foreach ($top_view_articles as $article) {


                                $date = date_create($article['created_at']);
                                $create_date = date_format($date, "d-m-Y");

                                if ($article['cover'] != "") {

                                    $upload_path = $_SERVER['DOCUMENT_ROOT'] . '/upload/article/';
                                    $file = $upload_path . '/' . $article['cover'];

                                    if (file_exists($file)) {

                                        $path = base_url() . 'upload/article/' . $article['cover'];

                                    } else {

                                        $path = base_url() . 'assets/dist/img/default_img.jpg';

                                    }
                                } else {

                                }

                                $html.= '
                                            <li class="item">
                                                <div class="product-img">
                                                    
                                                    <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                      
                                                </div>
                                                <div class="product-info">
                                                    <a href="javascript:void(0)" class="product-title">' . $article['title'] . ' <span class="label label-primary pull-right"><i class="fa fa-eye"></i> '.$article['page_view'].'</span></a>
                                                    <span class="product-description">
                                                        <span><i class="fa fa-user"></i> ' . $article['author'] . '</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> ' . $create_date . '</span>
                                                    </span>
                                                </div>
                                            </li>
                                        ';

                            }
                        }else{

                            $html.= "<p class='no-data'>No data available</p>";

                        }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetTopArticleComment')){

    function generateWidgetTopArticleComment($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $top_comment_articles = $ci->MDashboard->getTopCommentArticle();

        $html = ' <div class="box box-success">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Top Comment Articles</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                            if(count($top_comment_articles)>0) {

                                foreach ($top_comment_articles as $article) {


                                    $date = date_create($article['created_at']);
                                    $create_date = date_format($date, "d-m-Y");

                                    if ($article['cover'] != "") {

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'] . '/upload/article/';
                                        $file = $upload_path . '/' . $article['cover'];

                                        if (file_exists($file)) {

                                            $path = base_url() . 'upload/article/' . $article['cover'];

                                        } else {

                                            $path = base_url() . 'assets/dist/img/default_img.jpg';

                                        }
                                    } else {

                                    }

                                    $html.='
                                                    <li class="item">
                                                        <div class="product-img">
                                                            <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                        </div>
                                                        <div class="product-info">
                                                            <a href="javascript:void(0)" class="product-title">' . $article['title'] . '<span class="label label-success pull-right"><i class="fa fa-comment"></i> '.$article['comment_count'].'</span></a>
                                                            <span class="product-description">
                                                                <span><i class="fa fa-user"></i> ' . $article['author'] . '</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> ' . $create_date . '</span>
                                                            </span>
                                                        </div>
                                                    </li>
                                                ';

                                }

                            }else{

                                $html.="<p class='no-data'>No data available</p>";

                            }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetCurrentMonthEvent')){

    function generateWidgetCurrentMonthEvent($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $event_current_month = $ci->MDashboard->getEventOnCurrentMonth();

        $html='<div class="box box-solid bg-green-gradient">
            <div class="box-header" id="'.$id.'">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">Event on this month</h3>

                <div class="pull-right box-tools">
   
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bars"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="<?php echo base_url(); ?>event/event/addnew">Add new event</a></li>
                        </ul>
                    </div>
                    <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>

            </div>

            <div class="box-body no-padding">
                <div id="calendar" style="width: 100%"></div>
            </div>

            <div class="box-footer text-black">
                <div class="row">';

                    foreach($event_current_month as $event){

                        $s_date = date_create($event['start_date']);
                        $e_date = date_create($event['end_date']);

                        $start_date = date_format($s_date,'d-m-Y');
                        $end_date = date_format($e_date,'d-m-Y');

                        $html.='<div class="col-sm-6" style="color: #666;">
                                  <div class="clearfix">
                                    <span class="pull-left"><span class="label label-success"><i class="fa fa-calendar"></i> '.$start_date.' - '.$end_date.'</span></span>
                                    <small class="pull-right"></small>
                                   </div>
                                   <div class="clearfix">
                                    <p>'.$event['name'].'</p>
                                  </div>
                                </div>';

                    }

        $html.='</div>
            </div>
        </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetUpcomingEvent')){

    function generateWidgetUpcomingEvent($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $upcoming_events = $ci->MDashboard->getLastUpcomingEvent();

        $html = ' <div class="box box-warning">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Upcoming Event</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                            if(count($upcoming_events)>0){

                                foreach($upcoming_events as $event){

                                    $s_date=date_create($event['start_date']);
                                    $start_date = date_format($s_date,"d-m-Y");

                                    $e_date=date_create($event['end_date']);
                                    $end_date = date_format($e_date,"d-m-Y");

                                    if($event['cover'] != ""){

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/event/';
                                        $file = $upload_path.'/'.$event['cover'];

                                        if(file_exists($file)){

                                            $path = base_url().'upload/event/'.$event['cover'];

                                        }else{

                                            $path = base_url().'assets/dist/img/default_img.jpg';

                                        }
                                    }else{

                                    }

                                    $html.='
                                                        <li class="item">
                                                            <div class="product-img">
                                                                <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                            </div>
                                                            <div class="product-info">
                                                                <a href="javascript:void(0)" class="product-title">'.$event['name'].'</a>
                                                                <span class="product-description">
                                                                    <span><i class="fa fa-user"></i> '.$event['author'].'</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> '.$start_date.' - '.$end_date.'</span>
                                                                </span>
                                                            </div>
                                                        </li>
                                                    ';

                                }

                            }else{

                                $html.="<p class='no-data'>No data available</p>";

                            }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}

if ( ! function_exists('generateWidgetTopUpcomingEvent')){

    function generateWidgetTopUpcomingEvent($id)
    {
        $ci =& get_instance();
        $ci->load->model('MDashboard');

        $top_upcoming_events = $ci->MDashboard->getTopUpcomingEventByMemberCount();

        $html = ' <div class="box box-danger">
                    <div class="box-header with-border" id="'.$id.'">
                        <h3 class="box-title">Top Upcoming Event By Member</h3>
        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="min-height: 303px;">
                        <ul class="products-list product-list-in-box">';

                            if(count($top_upcoming_events)>0) {

                                foreach ($top_upcoming_events as $event) {


                                    $s_date = date_create($event['start_date']);
                                    $start_date = date_format($s_date, "d-m-Y");

                                    $e_date = date_create($event['end_date']);
                                    $end_date = date_format($e_date, "d-m-Y");

                                    if ($event['cover'] != "") {

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'] . '/upload/event/';
                                        $file = $upload_path . '/' . $event['cover'];

                                        if (file_exists($file)) {

                                            $path = base_url() . 'upload/event/' . $event['cover'];

                                        } else {

                                            $path = base_url() . 'assets/dist/img/default_img.jpg';

                                        }
                                    } else {

                                    }

                                    $html.='
                                            <li class="item">
                                                <div class="product-img">
                                                    <img src="'.$path.'" alt="User Image" class="scale" data-scale="best-fill" data-align="center">
                                                </div>
                                                <div class="product-info">
                                                    <a href="javascript:void(0)" class="product-title">' . $event['name'] . ' <span class="label label-danger pull-right"><i class="fa fa-user"></i> '.$event['member_count'].'</span></a>
                                                    <span class="product-description">
                                                        <span><i class="fa fa-user"></i> ' . $event['author'] . '</span>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i> ' . $start_date . ' - ' . $end_date . '</span>
                                                    </span>
                                                </div>
                                            </li>
                                        ';

                                }

                            }else{

                                $html.="<p class='no-data'>No data available</p>";

                            }


        $html.='</ul>
                    </div>
                    <div class="box-footer text-center">
                    </div>
                </div>';

        echo $html;

    }

}