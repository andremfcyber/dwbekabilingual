<style>
    #map-canvas{
        margin: 10px auto;
        width: calc(100% - 5px);
        height: 400px;
        border-radius: 10px;
        border: 2px #ddd solid;
    }
    .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
    
    /* .modal {
        z-index: 20;   
    }
    .modal-backdrop{
        z-index: 10;
    } */
    .pac-container {
        z-index: 1051 !important;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <form id="pertemuan" action="<?= $url_pertemuan ?>">
            <div class="form-group col-md-6" style="padding: 0px 4px;">
                <label for="tanggal_appoiment">
                    Tanggal Pertemuan
                </label>
                <input type="text" name="tanggal_appoiment" class="form-control" id="tanggal_appoiment" placeholder="dd/mm/yyyy ">
            </div>

            <div class="form-group col-md-6" style="padding: 0px 4px;">
                <label for="waktu_appoiment">
                    Waktu Pertemuan
                </label>
                <input type="text" name="waktu_appoiment" class="form-control" id="waktu_appoiment" placeholder="hh:mm">
            </div>
            <div class="form-group col-md-12" style="padding: 0px 4px;">
                <label for="lokasi">
                    Lokasi
                </label>
                <input type="text" class="form-control textbox" name="lokasi" id="searchmap">
                <!-- <textarea name="lokasi" class="form-control" id="lokasi" placeholder="Lokasi" style="height: 120px"></textarea> -->
                <div id="map-canvas"></div>
                <input type="hidden" id="lat" name="lat">
                <input type="hidden" id="lng" name="lng">
            </div>

            <!-- <div class="form-group col-md-12">
                <textarea class="form-control" name="pesan"></textarea>
            </div> -->
            <div class="form-group col-md-12">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>
<script src="<?= base_url('assets/dist/js/jquery.mask.js') ?>"></script>

<script type="text/javascript">
    $(function() {
        set_map(0,0)
        $('#tanggal_appoiment').mask('00/00/0000') 
        $('#waktu_appoiment').mask('00:00') 
        $('#no_hp').mask('000000000000') 

        $('#pertemuan').submit((e) => {
            e.preventDefault()
            blockUI();
            let validate = check_is_valid();
            if(validate){
                var data = $('#pertemuan').serializeArray();
                var target = $('#pertemuan').attr('action');
                // console.log(data);
                setTimeout(function() {
                    $.post(target, data, function(res) {
                        if(res.status == 1){
                            $.unblockUI();
                            toastr.success(res.msg);
                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        }else{
                            $.unblockUI();
                            toastr.error(res.msg);
                        }
                    });
                }, 1000);
            }else{
                $.unblockUI();
                toastr.error('Silahkan Lengkapi Form');
            }
        })
    });

    function set_map(lat,lng){
        lng = parseFloat(lng);
        lat = parseFloat(lat);
        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: lat ? lat : 0,
                lng: lng ? lng : 0
            },
            zoom: 16
        });

        var marker = new google.maps.Marker({
            position: {
                lat: lat ? lat : 0,
                lng: lng ? lng : 0
            },
            map: map,
            draggable: true
        });
        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
        
        google.maps.event.addListener(searchBox, 'places_changed', function(){

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place=places[i]; i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(16);

        });

        google.maps.event.addListener(marker,'position_changed', function(){

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);

        });
    }
    function check_is_valid(){
        let validate = $('#pertemuan').serializeArray();
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }
</script>