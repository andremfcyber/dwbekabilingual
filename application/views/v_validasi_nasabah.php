<?php if (!$isName) {?>
	<div class="row d_wrap" style="text-align: center">
		<i class="fa fa-exclamation-triangle fa-5x text-danger"></i>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="alert alert-danger">
				<i class="fa fa-exclamation-triangle"></i>
				<span>Nama calon nasabah tidak sesuai dengan data di DUKCAPIL.</span>
				<p> Nama yang terdaftar <strong><?= $nasabah['nama'] ?></strong>, nama di Dukcapil <strong><?= isset($dukcapil->NAMA_LGKP)? $dukcapil->NAMA_LGKP : '-'; ?></strong></p>
			</div>
		</div>
	</div>
<?php } else {?>
	<div class="row d_wrap" style="text-align: center">
		<i class="fa fa-check-circle fa-5x text-success"></i>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="alert alert-success">
				<i class="fa fa-check-circle"></i>
				<strong>Data calon nasabah sesuai, silahkan lakukan validasi !</strong>
			</div>
		</div>
	</div>
<?php } ?>

<table class="table table-bordered" style="text-transform: capitalize;">
    <tr>
        <td>Nik</td>
        <td colspan="2"><?= isset($dukcapil->NIK)? $dukcapil->NIK : null ?></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td colspan="2"><?= isset($dukcapil->NAMA_LGKP)? strtolower($dukcapil->NAMA_LGKP) : null ?></td>
    </tr>
    <tr>
        <td>No.KK</td>
        <td colspan="2"><?= isset($dukcapil->NO_KK)? $dukcapil->NO_KK : null ?></td>
    </tr>
    <tr>
        <td>Tempat Lahir</td>
        <td colspan="2"><?= isset($dukcapil->TMPT_LHR)? strtolower($dukcapil->TMPT_LHR) : null ?></td>
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td colspan="2"><?= isset($dukcapil->TGL_LHR)? $dukcapil->TGL_LHR : null ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td colspan="2"><?= isset($dukcapil->JENIS_KLMIN)? strtolower($dukcapil->JENIS_KLMIN) : null ?></td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td colspan="2"><?= isset($dukcapil->JENIS_PKRJN)? strtolower($dukcapil->JENIS_PKRJN) : null ?></td>
    </tr>
    <tr>
        <td>Status Perkawinan</td>
        <td colspan="2"><?= isset($dukcapil->STATUS_KAWIN)? strtolower($dukcapil->STATUS_KAWIN) : null ?></td>
    </tr>
    <tr>
        <td>Agama</td>
        <td colspan="2"><?= isset($dukcapil->AGAMA)? strtolower($dukcapil->AGAMA) : null ?></td>
    </tr>
    <tr>
        <td>Nama Ibu</td>
        <td colspan="2"><?= isset($dukcapil->NAMA_LGKP_IBU)? strtolower($dukcapil->NAMA_LGKP_IBU) : null ?></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td colspan="2">
            <?= isset($dukcapil->ALAMAT)? strtolower($dukcapil->ALAMAT) : null ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>RT/RW</td>
        <td>
            <?= isset($dukcapil->NO_RT)? $dukcapil->NO_RT : null; ?>/<?= isset($dukcapil->NO_RW)? $dukcapil->NO_RW : null; ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>Desa/Kelurahan</td>
        <td><?= isset($dukcapil->KEL_NAME)? strtolower($dukcapil->KEL_NAME) : null; ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Kecamatan</td>
        <td><?= isset($dukcapil->KEC_NAME)? strtolower($dukcapil->KEC_NAME) : null; ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Kab/Kota</td>
        <td><?= isset($dukcapil->KAB_NAME)? strtolower($dukcapil->KAB_NAME) : null; ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Provinsi</td>
        <td><?= isset($dukcapil->PROP_NAME)? strtolower($dukcapil->PROP_NAME) : null; ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Kode Pos</td>
        <td><?= isset($dukcapil->KODE_POS)? $dukcapil->KODE_POS : null; ?></td>
    </tr>
</table>

<script>
var dukcapil = <?= json_encode($dukcapil) ?>
</script>