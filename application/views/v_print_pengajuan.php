<?php 
	$id_nasabah = $detail_nasabah['id_nasabah'];
	$id_product = $nasabah['id_product'];
	$dir = $this->router->fetch_directory();
	$class = $this->router->fetch_class();

	$x = explode("/", $dir);
	$module = $x[2];

	$module_url = base_url().$module.'/'.$class;
?>
<style type="text/css">
	body{
		text-decoration: none;
		color:#666666;
		font-family:"Montserrat", "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
		font-weight: 400;
		font-size: 12px;
	}
	.wrapper {
		padding: 10px;
		border: 1px dotted #aaa;
	}
	.img-logo{
		content: "";
		width: 400px;
		height: auto;
		background: url(https://dev.bprnusambacepiring.com/themes/public/materialkit/img_frontend/logo.png); 
		background-size: cover;
		background-repeat: no-repeat;
	}
	.divider{
		content: " ";
		display: block;
		margin: 12px 0px;
		height: 3px;
		background-color: #bbb;
		box-shadow: 1px 2px #eeeeeeff;
		border-radius: 3px;
	}
	strong{
		font-weight: 500;
		display: block;
	}
	table tr td {
		border : 1px dotted #aaa;
	}
	table {
		border-collapse: collapse;
	}
	h3{
		font-size: 18px;
	}
</style>

	<div class="wrapper">
		<img align="center" alt="Image" border="0" class="center autowidth" src="<?= base_url();?>/themes/public/materialkit/img_frontend/logo.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: 100px; width: 100%; max-width: 300px; display: block;" title="Image" width="100"/>
		<br/>
		<h3 style="text-align: center">Form Pengajuan <?= $nasabah['name']?></h3>
		<div class="divider"></div>
		<?php
    	if (count($header_form > 0)){
    		foreach ($header_form as $key => $value) {
    	?>
    	<strong><?= $value['name_header']?></strong>
    		<table width="100%">
    			<?php 
	        		$query = "SELECT * FROM form_fileds WHERE form_header_id = ".$value['id']." ORDER BY urutan";
			        $form_attr = $this->db->query($query)->result_array();
			        foreach ($form_attr as $fa_key => $fa_value) {
			        	$field_name = strtolower(str_replace(' ', '_', $fa_value['label']));
							        $field_name = (preg_replace('/[^A-Za-z0-9\_]/', '', $field_name));
		        	$value_detail = $this->db->get_where('data_formulir_nasabah', array('id_nasabah' => $id_nasabah, 'id_product'=>$id_product, 'id_form_field'=>$fa_value['id']))->row_array();
		        	$data_value = "";
		        	$data_value = isset($detail_nasabah[$field_name]) ? $detail_nasabah[$field_name] : $data_value;
		        	$data_value = isset($value_detail) ? $value_detail['value'] : $data_value;
			        ?>
			        <tr>
			        	<td style="width: 30%;"><?= $fa_value['label']?></td>
			        	<td style="width: 70%"><?= $data_value?></td>
			        </tr>
			        <?php
			        }
    			?>
    		</table>
    		<br/>
    	<div class="divider"></div>
    	<?php 
    		}
    	}?>
    	<!-- <table width="100%" border="0">
                  <tr>
                    <td colspan="2"><strong>Data Detail Nasabah</strong></td>
                  </tr>
                  <tr>
                    <td >Nama Nasabah</td>
                    <td width="79%">:<?php echo $detail_nasabah['nama']?></td>
                  </tr>
                  <tr>
                    <td >NIK Nasabah</td>
                    <td width="79%">:<?php echo $detail_nasabah['nik']?></td>
                  </tr>
                  <tr>
                    <td >No Telepon</td>
                    <td width="79%">:<?php echo $detail_nasabah['no_tlp']?></td>
                  </tr>
                  <tr>
                    <td >Email</td>
                    <td width="79%">:<?php echo $detail_nasabah['email']?></td>
                  </tr>
                </table>
                <br><br>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="2"><strong>Data Detail Nasabah Dukcapil</strong></td>
                  </tr>
                  <tr>
                    <td >No Kartu Keluarga</td>
                    <td width="79%">:<?php echo $detail_nasabah['kk']?></td>
                  </tr>
                  <tr>
                    <td >Nama Ibu Kandung</td>
                    <td width="79%">:<?php echo $detail_nasabah['nama_ibu']?></td>
                  </tr>
                  <tr>
                    <td >Tempat Lahir</td>
                    <td width="79%">:<?php echo $detail_nasabah['tempat_lahir']?></td>
                  </tr>
                  <tr>
                    <td >Tanggal Lahir</td>
                    <td width="79%">:<?php echo $detail_nasabah['tanggal_lahir']?></td>
                  </tr>
                  <tr>
                    <td >Alamat</td>
                    <td width="79%">:<?php echo $detail_nasabah['alamat']?></td>
                  </tr>
                  <tr>
                    <td >No RT</td>
                    <td width="79%">:<?php echo $detail_nasabah['rt']?></td>
                  </tr>
                  <tr>
                    <td >No RW</td>
                    <td width="79%">:<?php echo $detail_nasabah['rw']?></td>
                  </tr>
                  <tr>
                    <td >Kelurahan</td>
                    <td width="79%">:<?php echo $detail_nasabah['kelurahan']?></td>
                  </tr>
                  <tr>
                    <td >Kecamatan</td>
                    <td width="79%">:<?php echo $detail_nasabah['kecamatan']?></td>
                  </tr>
                  <tr>
                    <td >Kabupaten</td>
                    <td width="79%">:<?php echo $detail_nasabah['kota']?></td>
                  </tr>
                  <tr>
                    <td >Provinsi</td>
                    <td width="79%">:<?php echo $detail_nasabah['provinsi']?></td>
                  </tr>
                  <tr>
                    <td >Kode Pos</td>
                    <td width="79%">:<?php echo $detail_nasabah['kode_pos']?></td>
                  </tr>
                  <tr>
                    <td >Jenis Kelamin</td>
                    <td width="79%">:<?php echo $detail_nasabah['jenis_kelamin']?></td>
                  </tr>
                  <tr>
                    <td >Agama</td>
                    <td width="79%">:<?php echo $detail_nasabah['agama']?></td>
                  </tr>
                  <tr>
                    <td >Jenis Pekerjaan</td>
                    <td width="79%">:<?php echo $detail_nasabah['pekerjaan']?></td>
                  </tr>
                  <tr>
                    <td >Status Kawin</td>
                    <td width="79%">:<?php echo $detail_nasabah['status_kawin']?></td>
                  </tr>
                  
                </table> -->
	</div>
	
