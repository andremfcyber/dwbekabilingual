<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Perjanjian</title>
</head>

<body>
    <div style="margin-bottom: 10px;">
        <p style="text-align: center; font-weight: bold; font-size: 16px">SURAT PERNYATAAN</p>
    </div>

    <div style="margin-bottom: 10px;">
        <p>Yang bertandatangan dibawah ini :</p>
        <table style="width: 50%">
            <tr>
                <td style="width: 20%">Nama</td>
                <td style="width: 5%">:</td>
                <td><?= $nasabah['nama'] ? $nasabah['nama'] : '' ?></td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td><?= $nasabah['nik'] ? $nasabah['nik'] : '' ?></td>
            </tr>
        </table>
    </div>
    <div style="margin-bottom: 15px;text-indent: 30px; text-align: justify">
        <p>Dengan ini saya menyatakan dan menyetujui untuk dilakukan pengecekan NIK, secara online yang terhubung dengan sistem Ditjen Dukcapil, dan hasil pengecekannya akan dikelola sebagaimana mestinya untuk keperluan <?= front_config('front_profil', 'nama')?></p>
        <p>Demikian Surat Pernyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
    </div>

    <p><?= date('d/m/Y/H:i') ?></p>

    <table style="width: 100%;">
        <tr>
            <td style="width: 49.0662%;">
                <p>Yang menyatakan</p>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <p style="height: 100px">

                </p>
                <p>( <?= $nasabah['nama'] ? $nasabah['nama'] : '' ?> )</p>
            </td>
            <td style="width: 48.5569%;">Petugas Pengecekan NIK
                <p><?= front_config('front_profil', 'nama') ?></p>
                <br>
                <br>
                <br><br>
                <br>
                <br>
                <p style="height: 100px">

                </p>
                <p><?= isset($sales['nama_sales']) ? '('.$sales['nama_sales'].')' : '(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)' ?> </p>
            </td>
        </tr>
    </table>

</body>

</html>