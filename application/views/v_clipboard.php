<div class="row">
    <div class="col-md-12">
        Link Referal berdasarkan produk
        <?php if(!isset($all_links) || count($all_links) == 0) { ?>
            <h3>Produk Tidak Tersedia</h3>
        <?php } ?>
        <table class="table table-bordered">
            <?php foreach($all_links as $id => $_d) {?>
                <tr>
                    <td style="color: #000 !important"><?= $_d['nama_product'] ?></td>
                    <td><a id="link_<?=$id?>" href="<?= $_d['link_referal'] ?>"><?= $_d['link_referal'] ?></a></td>
                    <td>
                        <button class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Salin Alamat" onclick="copyToHeart('#link_<?=$id?>')"><i class="fa fa-clipboard"></i></button>
                        <a data-toggle="tooltip" data-placement="top" title="Bagikan Ke Facebook" class="btn btn-sm btn-primary" href="https://www.facebook.com/sharer/sharer.php?u=<?= $_d['link_referal'] ?>" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>