<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Detail Nasabah</title>
</head>
<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    .table tr td {
        padding: 10px 15px;
        border: 1px solid #000; 
    }
    .header2{
       font-weight: bold;
    }
    .table_head{
        margin-bottom: 10px;
    }
    .table_foot{
        margin-top: 10px;
        border-collapse: collapse;
    }
    .table_foot tr td{
        padding: 5px 15px;
        border: 1px solid #000; 
    }
</style>
<body>
    <table class="table">
        <tr>
            <td>Tanggal Pertemuan</td>
            <td><?= $appointment['tanggal_appoiment'] ?></td>
        </tr>
        <tr>
            <td>Waktu Pertemuan</td>
            <td><?= $appointment['waktu_appoiment'] ?></td>
        </tr>
        <tr>
            <td>Lokasi</td>
            <td><?= isset($appointment['lokasi']) ? $appointment['lokasi'] : '' ?></td>
        </tr>
        <tr>
            <td>Pihak lain yg dapat dihubungi</td>
            <td>
                <?= $appointment['pihak_lain'] ?>
            </td>
        </tr>
        <tr>
            <td>Hubungan</td>
            <td>
                <?= $appointment['hubungan'] ?>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <?= $appointment['alamat_pihak_lain'] ?>
            </td>
        </tr>
        <tr>
            <td>Kontak Pihak Lain</td>
            <td>
                <a href="https://wa.me/<?= isset($appointment['no_telp_pihak_lain']) ? $appointment['no_telp_pihak_lain'] : '-' ?>" target="_blank"><img style="height: 45px;"><?= $appointment['no_telp_pihak_lain'] ?></a>
            </td>
        </tr>
    </table>
</body>
</html>