<div class="row">
    <div class="col-md-12">
        <form action="<?= $url_assign ?>" id="form-pindah">
            <div class="form-group">
                <label>Pilih</label>
                <select id="select2_sales" class="form-control"></select>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#form-pindah').submit((e) => {
        // console.log(e.target.action)
        blockUI("Selalu berdo'a semuanya lancar");
        e.preventDefault()
        let select_sales = $('#select2_sales').val();
        let keterangan = $('#keterangan').val();
        if(!select_sales || !keterangan){
            alert('Harap Lengkapi');
            $.unblockUI();
            return false;
        }
        $.post(e.target.action,{
            'sales_id' : select_sales,
            'keterangan' : keterangan,
        }).done((res) => {
            if(res.status == 1){
                toastr.success('Berhasil', res.msg);
                setTimeout(() => {
                    window.location.reload()
                }, 2000);
            }else{
                toastr.error('Gagal', res.msg);
                $.unblockUI();
            }
        }).fail((xhr) => {

        })
    })
    function formatSales(data) {
        if (data.loading) {
            return data.text;
        }
        let foto = base_url+"assets/dist/img/default-avatar.png"
        
        if(data.foto){
            foto = base_url+"upload/photo/"+data.foto
        }

        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__avatar'><img src='"+foto+"'/ style='width: 60px; height: 60px;object-fit: cover;'></div>" +
            "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'></div>" +
                "<div class='select2-result-repository__description'></div>" +
                "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><i class='fa fa-phone'></i> </div>" +
                "<div class='select2-result-repository__stargazers'><i class='fa fa-institution'></i> </div>" +
                "<div class='select2-result-repository__watchers'><i class='fa fa-circle-o'></i> </div>" +
                "</div>" +
            "</div>" +
            "</div>"
        );
        
        $container.find(".select2-result-repository__title").text(data.nama_sales);
        $container.find(".select2-result-repository__description").text(data.alamat);
        $container.find(".select2-result-repository__forks").append(data.no_telp);
        $container.find(".select2-result-repository__stargazers").append(data.kantor);
        $container.find(".select2-result-repository__watchers").append();

        return $container;
    }

    function formatSalesSelection(data) {
        return data.nama_sales || data.text;
    }
    function select2_sales(){
        $('#select2_sales').empty();
        $('#select2_sales').select2({
            width: '100%',
            placeholder: "Pilih Orang",
            delay: 3000,
            // multiple: true,
            ajax: {
                url: `${base_url}/sales/sales/list_sales`,
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1,
                    }
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;

                    return {
                        'results': data.results,
                        'pagination': {
                            'more': data.pagination.more
                        },
                    }
                },
                cache: true,
            },
            // minimumInputLength: 10,
            templateResult: formatSales,
            templateSelection: formatSalesSelection
        });
    }
    $(() => {
        setTimeout(() => {
            select2_sales();
        }, 500);
    })
</script>