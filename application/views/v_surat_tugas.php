<style>
    #map-canvas{
        margin: 10px auto;
        width: calc(100% - 5px);
        height: 400px;
        border-radius: 10px;
        border: 2px #ddd solid;
    }
    .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
    .pac-container {
        z-index: 1051 !important;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <form id="frm_surat_tugas" >
            <div class="form-group col-md-12" style="padding: 0px 4px;">
                <label for="surat_tugas">
                    Upload Surat
                </label>
                <input type="file" name="surat_tugas" class="form-control" id="surat_tugas" placeholder="hh:mm">
            </div>
            <div class="form-group col-md-12">
                <button type="button" class="btn btn-primary" id="btn_update_surat" onclick="update_surtag()">Update</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    function check_is_valid(){
        let validate = $('#surat_tugas').serializeArray();
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }
    function update_surtag(){

        var target = "<?= base_url('collector/surattugas/upload_surat/').$id ?>";
        var formData = new FormData($("#frm_surat_tugas")[0]);

        if($("#frm_surat_tugas").validationEngine('validate')) {

            showLoadingUpProf("btn_update_surat");
            
            blockUI();
            
            $.ajax({
                url: target,
                type: 'POST',
                data: formData,
                dataType: "json",
                async: false,
                success: function (data) {

                    if (data.status == "1") {

                        toastr.success(data.msg, 'Response Server');

                        setTimeout('window.location.reload();', 3000);

                    } else {
                        toastr.error(data.msg, 'Response Server');
                        $.unblock();
                    }

                    hideLoadingUpProf("btn_update_surat");

                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
</script>