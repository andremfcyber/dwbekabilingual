<div class="row">
    <div class="col-md-12">
        <form action="<?= $url_c_penagihan ?>" id="form-pindah">
           <div class="form-body">
           <p><?= $pesan ?></p>
           </div>
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#form-pindah').submit((e) => {
        console.log(e.target.action)
        // blockUI("Selalu berdo'a semuanya lancar");
        e.preventDefault()
            $.post(e.target.action, function(res){
                if(res.status == "1"){
                    toastr.success(res.msg, 'Response Server');
                    setTimeout(() => {
                        window.location.reload(); 
                    }, 1500);
                }else{
                    toastr.error(res.msg, 'Response Server');
                }

            },'json');

            return false;
    })
    
</script>