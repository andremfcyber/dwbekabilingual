<style type="text/css">
	.notif-valid {
		display: block;
		margin: 5px;
		color: #fff;
		padding: 5px;
		border-radius: 4px; 
		font-weight: 500;
		text-align: center;
		max-height: 0;
		overflow:hidden;
		transition: max-height 0.5s, overflow 0s;
	}
	.notif-valid-success{
		background-color: #85D6F5 !important;
	}
	.notif-valid-error{
		background-color: #F9707C !important;
	}
	.notif-valid-show{
		max-height: 5em;
		overflow:auto;
		transition: max-height 0.5s, overflow 0.5s 0.5s;
	}
</style>
<div class="row" style="text-align: center">
	<i class="fa fa-exclamation-circle  fa-5x text-success"></i>
	<h4>Apakah Nama Ibu atau No Kartu Keluarga dari nasabah <?= $nasabah['nama'];?> adalah </h4>
	<p>Nama Ibu : <strong><?= $detail_nasabah['nama_ibu']?></strong></p>
	<p>No. KK : <strong><?= $detail_nasabah['kk']?></strong></p>
	<p>Jika Benar pilih YA untuk verifikasi data?</p>
	<p></p>
	<p class="notif-valid">test</p>
	<a class="btn btn-primary" href="javascript:onVerify(<?= $detail_nasabah['id_nasabah']?>)">Ya</a>
	<a class="btn btn-danger" href="javascript:cancelVerify(<?=$detail_nasabah['id_nasabah']?>)" data-dismiss="modal">Tidak</a>
</div>

<script type="text/javascript">
	function cancelVerify(id){
		return false;
	}

	function onVerify(id){
		setTimeout(function(){
			var nasabah = <?= json_encode($nasabah);?>;
			var detail_nasabah = <?= json_encode($detail_nasabah)?>;
			var data = {
				nasabah : JSON.stringify(nasabah),
				detail_nasabah : JSON.stringify(detail_nasabah)
			}
			$.post(module_url + '/do_verify/'+id, data, function(response){
				if (response.status === "1"){
					$(".notif-valid").html(response.msg);
					$(".notif-valid").addClass('notif-valid-success notif-valid-show');
					setTimeout(function(){
						$('#dttable').dataTable().fnDestroy();
					    InitDatatable();
						$('#dynamicModal').modal('hide');
						$(".notif-valid").removeClass('notif-valid-success notif-valid-error notif-valid-show');
					}, 9000);
				}
			}, 'json');	
		}, 2000);
		
	}

</script>