<style>
    .text{
        border: 1px #ddd solid;
        padding: 14px;
        border-radius: 3px;
    }
    .argan{
        resize: vertical; 
        border: 1px #ddd solid;
        width: 100%;
        margin-top: 3px; 
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- <?= $url_reject ?> -->
        <form id="form-penolakan" action="<?= $url_reject ?>">
            <div class="form-group">
                <label> Pesan yang akan dikirim ke nasabah </label>
                <div class="text">
                    Maaf Pengajuan <?= $produk ?>  anda di BPR Nusamba Cepiring tidak di dapat setuju dengan alasan
                    <textarea name="keterangan" class="argan" placeholder="Isi Sendiri"></textarea>
                    Silahkan datang ke kantor BPR Nusamba Cepiring kantor kas (sesuai dengan kantor AO yang memproses) / Lakukan pengajuan ulang dengan data yang valid
                </div>
                <!-- <label>Keterangan Diterima :</label> -->
                    
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#form-penolakan').submit((e) => {
        // console.log(e.target.action)
        blockUI("Selalu berdo'a semuanya lancar");
        e.preventDefault()
        let keterangan = $('[name="keterangan"]').val();
        if(!keterangan){
            alert('Keterangan Wajib Diisi');
            $.unblockUI();
            return false;
        }
        $.post(e.target.action,{
            'keterangan' : keterangan,
        }).done((res) => {
            if(res.status == 1){
                toastr.success('Berhasil', res.msg);
                setTimeout(() => {
                    window.location.reload()
                }, 2000);
            }else{
                toastr.error('Gagal', res.msg);
                $.unblockUI();
            }
        }).fail((xhr) => {

        })
    })
</script>