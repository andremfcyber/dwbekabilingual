<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "ae7658e9bf89a9658cb2eaa7a96e51e3";

/**
 * RajaOngkir account type: starter or basic
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "basic";
