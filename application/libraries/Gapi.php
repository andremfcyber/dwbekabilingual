<?php

/**
 * 
 */


class Gapi extends Admin_Controller
{
	private $ga = 'vendor/autoload.php';
	private $ci;

	function __construct(){

		$this->ci =& get_instance();

	}

	

	public function initializeAnalytics()
	{
	  // Creates and returns the Analytics Reporting service object.

	  // Use the developers console and download your service account
	  // credentials in JSON format. Place them in this directory or
	  // change the key file location if necessary.
	  // $KEY_FILE_LOCATION = __DIR__ . '/ga-bprnusambacepiring-72dff9cfa956.json';
	  require_once FCPATH . $this->ga;
	  $KEY_FILE_LOCATION = $this->ci->config->item('ga_key');


	  // Create and configure a new client object.
	  $client = new Google_Client();
	  $client->setApplicationName("Analytics Bank EKA");
	  $client->setAuthConfig($KEY_FILE_LOCATION);
	  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	  $analytics = new Google_Service_Analytics($client);

	  return $analytics;
	}

	public function getFirstProfileId($analytics) {
	  // Get the user's first view (profile) ID.

	  // Get the list of accounts for the authorized user.
	  $accounts = $analytics->management_accounts->listManagementAccounts();

	  if (count($accounts->getItems()) > 0) {
	    $items = $accounts->getItems();
	    $firstAccountId = $items[0]->getId();

	    // Get the list of properties for the authorized user.
	    $properties = $analytics->management_webproperties
	        ->listManagementWebproperties($firstAccountId);

	    if (count($properties->getItems()) > 0) {
	      $items = $properties->getItems();
	      $firstPropertyId = $items[0]->getId();

	      // Get the list of views (profiles) for the authorized user.
	      $profiles = $analytics->management_profiles
	          ->listManagementProfiles($firstAccountId, $firstPropertyId);

	      if (count($profiles->getItems()) > 0) {
	        $items = $profiles->getItems();

	        // Return the first view (profile) ID.
	        return $items[0]->getId();

	      } else {
	        throw new Exception('No views (profiles) found for this user.');
	      }
	    } else {
	      throw new Exception('No properties found for this user.');
	    }
	  } else {
	    throw new Exception('No accounts found for this user.');
	  }
	}

	public function getResult($time_range, $metric, $dimension = null){
		$analytics = $this->initializeAnalytics();
		$profileId = $this->getFirstProfileId($analytics);

		if (is_null($dimension) || empty($dimension) || !isset($dimension)) {
			$result= $analytics->data_ga->get(
				'ga:'.$profileId,
				$time_range['start'],
				$time_range['end'],
				$metric
			);
		} else {
			$result= $analytics->data_ga->get(
				'ga:'.$profileId,
				$time_range['start'],
				$time_range['end'],
				$metric,
				$dimension
			);
		}
		
		return $result;
	}
}