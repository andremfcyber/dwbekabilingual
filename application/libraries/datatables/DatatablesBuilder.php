<?php defined('BASEPATH') OR exit('No direct script access allowed');

  /**
  * CIgniter DataTables
  * CodeIgniter library for Datatables server-side processing / AJAX, easy to use :3
  *
  * @package    CodeIgniter
  * @subpackage libraries
  * @version    1.5
  *
  * @author     Izal Fathoni (izal.fat23@gmail.com)
  * @link 		https://github.com/nacasha/CIgniter-Datatables
  */
class DatatablesBuilder
{

    private $CI;
    private $searchable 	= array();
    private $searchable_columns 	= array();
    private $orderable 	= array();
    private $style 			= '';
	private $connection 	= 'default';

	private $dt_options		= array(
		'searchDelay' 	=> '500',
		'autoWidth' 	=> 'false'
	);
	private $ax_options 	= '';

    /**
     * Load the necessary library from codeigniter and caching the query
     * We use Codeigniter Active Record to generate query
     */
    public function __construct()
    {
        $this->CI =& get_instance();

        $this->_db = $this->CI->load->database($this->connection, TRUE);
        $this->CI->load->helper('url');
        $this->CI->load->library('table');

        $this->_db->start_cache();
    }

    public function __destruct()
    {
        $this->_db->stop_cache();
        $this->_db->flush_cache();
    }

    /**
     * Select column want to fetch from database
     *
     * @param  string
     * @return object
     */
    public function select($columns)
    {
        $this->_db->select($columns);

        $this->searchable = $columns;
        return $this;
    }

    public function join($table, $on, $type = '')
    {
        if($type){
            $this->_db->join($table, $on, $type);
        }else{
            $this->_db->join($table, $on);
        }
        return $this->_db;
    }

    public function from($table)
    {
        $this->_db->from($table);

        $this->table = $table;
        return $this->_db;
    }

    public function where($data,$table = '')
    {
        $this->_db->where($data);
        $this->table = $table;
        return $this->_db;
    }
    
    public function where_in($field,$param,$table = '')
    {
        $this->_db->where_in($field,$param);
        $this->table = $table;
        return $this->_db;
    }
    
    public function group_by($data)
    {
        $this->_db->group_by($data);
        return $this->_db;
    }
    
    public function like($data,$table = '')
    {
        $this->_db->like($data);
        $this->table = $table;
        return $this->_db;
    }

    public function style($data)
    {
        foreach ($data as $option => $value) {
            $this->style .= "$option=\"$value\"";
        }

        return $this;
    }

    /**
     * Set heading for the table
     *
     * @param  string $label    heading label
     * @param  string $source   column names
     * @param  method $function formatting the output
     * @return object
     */
    public function column($label, $source, $function = null)
    {
        $this->table_heading[] 		= $label;
        $this->columns[] 			= array($label, $source, $function);

        return $this;
    }

    /**
     * Initialize Datatables
     */
    public function init($dt_name)
    {
		if (isset($_REQUEST['dt_name'])) {
			if ($_REQUEST['dt_name'] == $dt_name) {
				if(isset($_REQUEST['draw']) && isset($_REQUEST['length']) && isset($_REQUEST['start']))
				{
					$this->json();
					exit;
				}
			}
        }
    }

    /**
     * Set searchable columns from table
     *
     * @param  string $data columns name
     * @return object
     */
    public function searchable($data)
    {
        $this->searchable = $data;
        return $this;
    }
    public function searchable_columns($data)
    {
        $this->searchable_columns = $data;
        return $this;
    }
    public function orderable($data)
    {
        $this->orderable = $data;
        return $this;
    }

    /**
     *	Add options to datatables jquery
     *
     * @param array / string 	$option options name
     * @param string 			$value  value
     */
    public function set_options($option, $value = null)
    {
		if ($option == 'ajax.data') {
			$this->ax_options .= $value;
		} else {
			if(is_array($option)) {
				foreach ($option as $opt => $value) {
					$this->dt_options[$opt] = $value;
				}
			} else {
				$this->dt_options[$option] = $value;
			}
		}

        return $this;
	}

	/**
     * Generate the datatables table (lol)
     *
     * @return html table
     */
    public function generate($id)
    {
		$this->CI->table->set_template(array(
            'table_open' => "<table id=\"$id\" $this->style>"
        ));
        $this->CI->table->set_heading($this->table_heading);

        echo $this->CI->table->generate();
    }
    
    /**
     * Jquery for datatables
     *
     * @return javascript
     */
    public function jquery($id, $param = '')
    {
		$dt_options	= '';
        $ax_options = $this->ax_options;
        $filter = '';

        if(count($this->searchable_columns)>0){
            $col_params = json_encode($this->searchable_columns);
            $filter = " var col_params = JSON.parse('".$col_params."');
                    if( $(\"#{$id} thead tr\").length == 1){
                        
                        $(\"#{$id} thead tr\").clone(true).appendTo( \"#{$id} thead\");
                        $(\"#{$id} thead tr:eq(1) th\").each( function (i) {
                            var title = $(this).text();
                            var id = $(this).attr(\"id\");
                            var class_attr = \"\";
                            var selector = \"\";
                            
                            id = title.toLowerCase();
                            id = id.replace(\" \", \"_\");
                            class_attr = id;
                            
                            if (col_params[i]){

                                switch(col_params[i].type){
                                    case 'text':

                                        $(this).html( \"<input type='text' id='\" + id + \"' class='form-control \"+class_attr+\"' style='width: 100%;' placeholder='Find by \" + title + \"' />\" );
                        
                                        $( 'input', this ).on( 'keyup change', function () {
                                            if ( erTable_{$id}.column(i).search() !== this.value ) {
                                                erTable_{$id}
                                                    .column(i)
                                                    .search( this.value )
                                                    .draw();
                                            }
                                        });

                                    break;

                                    case 'number':

                                        $(this).html( \"<input type='number' id='\" + id + \"' class='form-control \"+class_attr+\"' style='width: 100%;' placeholder='Find by \" + title + \"' />\" );
                        
                                        $( 'input', this ).on( 'keyup change', function () {
                                            if ( erTable_{$id}.column(i).search() !== this.value ) {
                                                erTable_{$id}
                                                    .column(i)
                                                    .search( this.value )
                                                    .draw();
                                            }
                                        });

                                    break;

                                    case 'date':

                                        $(this).html( \"<input type='text' id='\" + id + \"' class='form-control \"+class_attr+\"' style='width: 100%;' placeholder='Find by \" + title + \"' />\" );
                        
                                        $( 'input', this ).on( 'keyup change', function () {
                                            if ( erTable_{$id}.column(i).search() !== this.value ) {
                                                erTable_{$id}
                                                    .column(i)
                                                    .search( this.value )
                                                    .draw();
                                            }
                                        });

                                        $('#'+id).datepicker({
                                            format: 'yyyy-mm-dd',
                                            autoclose: true
                                        });

                                    break;
                                    case 'time':

                                        var input = \"\";
                                        input += \"<div class='input-group bootstrap-timepicker timepicker'>\";
                                        input += \"<input id='\"+id+\"' type='text' class='form-control input-small \"+class_attr+\"'>\";
                                        input += \"<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>\";
                                        input += \"</div>\";

                                        $(this).html(input);
                                        
                                        $('#'+id).timepicker({
                                             showMeridian: false,
                                             showSeconds: true,
                                             defaultTime: false
                                        });

                                        $( 'input', this ).on( 'keyup change', function () {
                                            if ( erTable_{$id}.column(i).search() !== this.value ) {
                                                erTable_{$id}
                                                    .column(i)
                                                    .search( this.value )
                                                    .draw();
                                            }
                                        });

                                     

                                    break;
                                    case 'daterange':
                                        var input = \"\";
                                        input += \"<div class='input-group' style='display: inline;'>\";
                                        input += \"<a class='btn btn-default \"+class_attr+\"' id='\"+id+\"' style='width: 100%'>\";
                                        input += \"<i class='fa fa-calendar'></i> <span id='reportrange'><span>Tanggal</span></span>\";
                                        input += \"<i class='fa fa-caret-down'></i>\";
                                        input += \"</a>\";
                                        input += \"</div>\";

                                        $(this).html(input);

                                        $('#'+id).daterangepicker({
                                            ranges: {
                                                'Hari ini': [moment(), moment()],
                                                'Kemarin': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                                '7 Hari yang lalu': [moment().subtract('days', 6), moment()],
                                                '30 Hari yang lalu': [moment().subtract('days', 29), moment()],
                                                'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                                                'Bulan kemarin': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
                                                'Tahun ini': [moment().startOf('year').startOf('month'), moment().endOf('year').endOf('month')],
                                                'Tahun kemarin': [moment().subtract('year', 1).startOf('year').startOf('month'), moment().subtract('year', 1).endOf('year').endOf('month')]
                                            },
                                            showDropdowns: true,
                                            format: 'YYYY-MM-DD'
                                        },
                                        function(start, end) {

                                            $('#reportrange span').html(start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY'));
                                            
                                            erTable_{$id}
                                                    .column(i)
                                                    .search( start.format('YYYY-MM-DD') + ',' + end.format('YYYY-MM-DD') )
                                                    .draw();
                                            
                                        });
                                    break;

                                    case 'select':
                                        var input = \"\";
                                        input += \"<select class='form-control \"+class_attr+\"' id='\"+id+\"'>\";
                                        input += \"<option value=''>-- \" + title + \" --</option>\";
                                        $.each(col_params[i].data, function(i,item){

                                            input += \"<option value='\"+i+\"'>\" +item+ \"</option>\";

                                        });

                                        input += \"</select>\";

                                        $(this).html(input);

                                        $('#' + id).change(function(){
                                            var val = $('#' + id).val();
                                            erTable_{$id}
                                                    .column(i)
                                                    .search(val)
                                                    .draw();

                                        });
                                        
                                    break;
                                    default:
                                    $(this).html(\"\");
                                    break;
                                }
                                
                            }else{
                                $(this).html(\"\");
                            }    
                        });

                    }";
        }
    
		foreach ($this->dt_options as $opt => $value) {
            if($opt == 'columns'){
                $value = json_encode($value);
            }
			$dt_options .= "$opt: $value, \n";
		}

		$output = "
        <script type=\"text/javascript\" defer=\"defer\">
            var info_dt = '';
            var erTable_{$id};
            function createDatatable() {
                ".$filter."
				erTable_{$id} = $(\"#{$id}\").DataTable({
                    processing: true,
                    serverSide: true,
                    orderCellsTop: true,
                    {$dt_options}
                    ajax: {
                        url: \"". site_url(uri_string()).$param."\",
						type: \"POST\",
                        data: function (d, dt) {
							d.dt_name = \"{$id}\"

							{$ax_options}
						}
					}
                });
                info_dt = erTable_{$id}.page.info();
            };

            $(document).ready(function(){
                
                createDatatable();
                
                
            });
            
        </script>";

        echo $output;
    }

    /**
     * Generate JSON for datatables
     *
     * @return json
     */
    public function json()
    {
        $draw		= $_REQUEST['draw'];
        $length		= $_REQUEST['length'];
        $start		= $_REQUEST['start'];
        $order_by	= isset($_REQUEST['order'][0]['column']) ? $_REQUEST['order'][0]['column'] : '';
        $order_dir	= isset($_REQUEST['order'][0]['dir']) ? $_REQUEST['order'][0]['dir'] : '';
        $search		= $_REQUEST['search']["value"];
        $columns = $_REQUEST['columns'];
       
        
        $output['data'] 	= array();
        
        if($this->searchable == '*') {
            $field = $this->_db->list_fields($this->table);
            $this->searchable = implode(',', $field);
        }
        

        foreach ($columns as $key => $value) {
            if($value['search']['value']!=""){
                $col = $this->searchable_columns[$key];
                if($col['type'] == 'date'){

                    $this->_db->where($this->columns[$key][1]." >= '".$value['search']['value']."' AND ".$this->columns[$key][1]." < '".$value['search']['value']."'");
                    
                }else if($col['type'] == 'daterange') {
                   
                    $xdate = explode(',',$value['search']['value']);
                    $start_date = @$xdate[0];
                    $end_date = @$xdate[1];

                    $this->_db->where($this->columns[$key][1]." >= '".$start_date."' AND ".$this->columns[$key][1]." < '".$end_date."'");

                }else{

                    if($col['operator']=='LIKE'){

                        $this->_db->where($this->columns[$key][1]." ILIKE ('%".$value['search']['value']."%')");

                    }else{

                        $this->_db->where($this->columns[$key][1]." ".$col['operator']." '".$value['search']['value']."'");

                    }

                }
            }
        }

        if(is_array($this->searchable)){
            $column = $this->searchable;
        }else{
            $column = explode(',', $this->searchable);
        }
		$this->searchable = array();

        foreach($column as $key => $col) {
            $col = strtolower($col);
            $col = strstr($col, ' as ', true) ?: $col;
            $this->searchable[] = $col;
		}

		if(is_array($this->searchable)) {
            $this->searchable = implode(',', $this->searchable);
		}
        if($search != "") {
            $this->_db->where("CONCAT($this->searchable) ILIKE ('%$search%')");
        }
        /** ---------------------------------------------------------------------- */
        /** Count records in database */
        /** ---------------------------------------------------------------------- */

        $total = $this->_db->count_all_results();

        $output['query_count'] 	= $this->_db->last_query();
        $output['recordsTotal'] = $output['recordsFiltered'] = $total;

        /** ---------------------------------------------------------------------- */
        /** Generate JSON */
		/** ---------------------------------------------------------------------- */

		if ($length != -1) {
			$this->_db->limit($length, $start);
        }
        if(isset($this->columns[$order_by][1])){
            $this->_db->order_by($this->columns[$order_by][1], $order_dir);
        }

        $result 			= $this->_db->get()->result_array();
        $output['query'] 	=  $this->_db->last_query();
        foreach ($result as $row) {
            $arr = array();
            foreach ($this->columns as $key => $column) {
                $row_output = $row[$column[1]];
                if(isset($this->columns[$key][2])){
                    $row_output = call_user_func_array($this->columns[$key][2], array($row_output, $row));
                }
                $arr[] = $row_output;
            }
            $output['data'][] = $arr;
        }

        echo json_encode($output);
    }

}
