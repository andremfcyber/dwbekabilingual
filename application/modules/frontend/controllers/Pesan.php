<?php

class Pesan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->theme_module = "pesan";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_hubungi_kami";
        $this->dttModel = "Mdpesan";
        
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Pesan";
        $data['page_subtitle'] = "Modul pesan";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Fax";
            $data['page_subtitle'] = "Modul pesan";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.pesan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(
           
            "add_perm" => $this->mcore->checkPermission($this->user_group, "pesan_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "pesan_view"),
            
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "pesan_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "pesan_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/pesan/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(


            "nama_pengirim" => array(

                "data" => "b.namalengkap",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "no_hp" => array(

                "data" => "b.no_hp",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "email" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "pesan" => array(

                "data" => "b.pesan",
                "searchable" => true,
                "orderable" => true,
              

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
//     private function _get_fields_edit(){


//         return array(
            
//             "nama" => array(

//                 "label" => "Nama Pelanggan",
//                 "type" => "text",
//                 "placeholder" => "Nama Pelanggan",
//                 "class" => "form-control validate[required]",
        
//             ),     

//             "cv" => array (
//                 "label" => "Image",
//                 "type" => "upload_file",
//                 "placeholder" => "Image",
//                 "class" => "form-control",
//                 "file_path" => 'upload/photo/'
//             ),

//             "id_career" => array(

//                 "label" => "Pekerjaan",
//                 "type" => "text",
//                 "placeholder" => "Pekerjaan",
//                 "class" => "form-control validate[required]",
        
//             ), 

//             "keterangan" => array(

//                 "label" => "Keterangan",
//                 "type" => "editor",
//                 "placeholder" => "Keterangan",
//                 "class" => "form-control validate[required]",
             

//             ),     
            

//         );

// // 
//     }

    

    // public function remove(){

    //     if(count($_POST)>0){

    //         $data['params']['action'] = "delete";
    //         $data['params']['table'] = $this->table;
    //         $data['params']['pk'] = $this->pk;
    //         $data['params']['id'] = $_POST['id'];

    //         $this->load->library("Cinta",$data);
    //         $this->cinta->process();

    //     }

    // }

}