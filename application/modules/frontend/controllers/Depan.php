<?php

class Depan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "depan";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_depan";
        $this->dttModel = "Mddepan";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Setting Gambar";
        $data['page_subtitle'] = "Modul Frontend";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        // map_y($this->_get_datatable_columns());
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Setting Gambar";
        $data['page_subtitle'] = "Modul frontend";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.depan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "baner_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "baner_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "baner_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "baner_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
               
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/depan/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "key" => array(

                "data" => "b.key",
                "searchable" => true,
                "orderable" => true,

            ),

            "value" => array(

                "data" => "b.value",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){let html = `<img src='".base_url()."upload/photo/\${data}' class='scale' data-scale='best-fill' data-align='center' style='width:100px; height:100px;'></img>`; return html }"
            ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(
            "key" => array(

                "label" => "Kata Kunci",
                "type" => "text",
                "placeholder" => "Kata Kunci",
                "class" => "form-control",
             
            ),

            "value" => array (

                "label" => "Icon/Gambar",
                "type" => "upload_file",
                "placeholder" => "Icon/Gambar",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            
            ),
               
        );

    }

    public function add(){

        if(count($_POST)>0){
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_value']);
            
            if(count($_FILES)>0){
                if($_FILES["value"]["name"]!=""){

                    if($_FILES["value"]["type"]=="image/png" or
                        $_FILES["value"]["type"]=="image/jpg" or
                        $_FILES["value"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['value']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["value"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['value'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }
                }
            }
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah Gambar ' . $_POST['key'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Gambar";
            $data['page_subtitle'] = "Modul Gambar";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_value']);

            if(count($_FILES)>0){
                if($_FILES["value"]["name"]!=""){

                    if($_FILES["value"]["type"]=="image/png" or
                        $_FILES["value"]["type"]=="image/jpg" or
                        $_FILES["value"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['value']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/photo/'.get_file_name('front_depan','id',$id,'value');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["value"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['value'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Setting Gambar' . $_POST['key'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit depan";
            $data['page_subtitle'] = "Modul depan";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    

}