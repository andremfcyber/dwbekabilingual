<?php

class Profil extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "profil";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_profil";
        $this->dttModel = "Mdprofil";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Profil";
        $data['page_subtitle'] = "Modul profil";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Profil";
            $data['page_subtitle'] = "Modul profil";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.profil' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "profil_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "profil_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "profil_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "profil_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."profil/profil/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            // "label" => array(

            //     "data" => "b.label",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),
            
            "logo" => array(
                "data" => "b.logo",
                "searchable" => false,
                "orderable" => false,
                "render" => "function(data, type, row){ let src = ''; if(!data){ src = base_url+'upload/noimg.webp'; }else{ src = base_url+'upload/logo/'+data; } let html = `<img src='\${src}' style='object-fit: cover;'>`; return html; }"
            ),
            

            "Nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
              

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        // map_y($image);
        $data =  array(
            

            "nama" => array(

                "label" => "Nama profil",
                "type" => "text",
                "placeholder" => "Nama profil",
                "class" => "form-control validate[required]",
            ),

            "logo" => array (
                "label" => "Logo",
                "type" => "upload_file",
                "placeholder" => "Logo",
                "class" => "form-control",
                "file_path" => 'upload/logo/'
            ),

            // "sejarah" => array(

            //     "label" => "Sejarah Singkat",
            //     "type" => "editor",
            //     "placeholder" => "Sejarah Singkat",
            //     "class" => "form-control validate[required]",

            // ),

            // "tujuan" => array(

            //     "label" => "Tujuan Utama Pendirian",
            //     "type" => "editor",
            //     "placeholder" => "Tujuan Utama Pendirian",
            //     "class" => "form-control validate[required]",

            // ),
            
            // "perkembangan" => array(

            //     "label" => "Perkembangan Perusahaan",
            //     "type" => "editor",
            //     "placeholder" => "Perkembangan Perusahaan",
            //     "class" => "form-control validate[required]",

            // ),
            // "corevalue" => array(

            //     "label" => "Core Value",
            //     "type" => "editor",
            //     "placeholder" => "Core Value",
            //     "class" => "form-control validate[required]",

            // ),
            // "visi" => array(

            //     "label" => "Visi",
            //     "type" => "editor",
            //     "placeholder" => "Visi",
            //     "class" => "form-control validate[required]",

            // ),
            // "misi" => array(

            //     "label" => "Misi",
            //     "type" => "editor",
            //     "placeholder" => "Misi",
            //     "class" => "form-control validate[required]",

            // ),
            // "marque" => array(

            //     "label" => "Text Berjalan (Marque)",
            //     "type" => "textarea",
            //     "placeholder" => "Text",
            //     "class" => "form-control validate[required]",

            // ),
            // "photo" => array(

            //     "type" => "custom",
            //     "html" => "<div class='form-group'><span class='required_label'>*</span><b>Foto (bisa lebih dari 1 foto)</b><input type='file' class='form-control' multiple='multiple' name='photo[]' id='photo'></div>",
            // ),
            
        
        );


        // $c = $this->db->query('SELECT * FROM front_profil ')->row();
        // $id = $c->id;
        // $img = $this->db->query("SELECT * FROM media_cepiring a where a.id_nya = $id AND a.module = 'front_profil'")->result_array();
        // $image= array();
        // if($img){
        //     foreach ($img as $value) {
        //         $data[$value['file']] = array(
        //             'type' => 'custom',
        //             'html' => "<div class='col-md-2'><img src='".base_url().$value['path'].$value['file']."' class='form-control' style='width:200px !important; height:200px !important;'></img></div>"
                 
        //         );
        //     }
        // }
        // map_y($data);


        return $data;
// 
    }

    public function add(){

        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah profil dari dashboard atas nama ' . $_POST['nama_profil'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah profil";
            $data['page_subtitle'] = "Modul profil";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_logo']);
           
            if(count($_FILES)>0){
                if($_FILES["logo"]["name"]!=""){

                    if($_FILES["logo"]["type"]=="image/png" or
                        $_FILES["logo"]["type"]=="image/jpg" or
                        $_FILES["logo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/logo/';
                        $array = explode('.', $_FILES['logo']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/logo/'.get_file_name('front_profil','id',$id,'logo');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['logo'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            if(isset($_FILES['photo']) ){  

                foreach($_FILES['photo']['name'] as $key => $name ){

                    if(!empty($_FILES['photo']['name'][$key])){

                        $this->db->delete('media_cepiring', array('id_nya' => $id));        
                                     
                    }
                        
                }

            }

            for ($i=0; $i < count($_FILES['photo']['name']); $i++) { 

                if($_FILES["photo"]["name"][$i]!=""){

                    if($_FILES["photo"]["type"][$i]=="image/png" or
                        $_FILES["photo"]["type"][$i]=="image/jpg" or
                        $_FILES["photo"]["type"][$i]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo';
                        $array[$i] = explode('.', $_FILES['photo']['name'][$i]);
                        $extension[$i] = end($array[$i]);
                        $photo[$i] = md5(uniqid(rand(), true)).".".$extension[$i];


                        $old_photo = $upload_path.'/'.@$_POST['old_photo'];

                        if(file_exists($old_photo)){

                            @unlink($old_photo);

                        }



                        if (move_uploaded_file($_FILES["photo"]["tmp_name"][$i], $upload_path."/".$photo[$i])) {

                        }else{

                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }


                        $dataphoto = array(
                            "file" => $photo[$i],
                            "path" => 'upload/photo/',
                            "type" => "image",
                            "format" => $extension[$i],
                            "module" => "front_profil",
                            "created_by" => $this->session->userdata('id'),
                            "created_at" => date("Y-m-d h:i:s"),
                            "id_nya" =>$id,
                        );

                        $this->db->insert('media_cepiring', $dataphoto);

                    }else{

                        $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{

                    // $_POST['photo'] = $this->input->post("old_photo");

                }

            }
            if(isset($_POST['old_photo'])){
                unset($_POST['old_photo']);
            }
            if(isset($_POST['tmp_name'])){
                unset($_POST['tmp_name']);
            }
          
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Profil ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

            //  if(count($_FILES)>0){

            //     for ($i=0; $i < count($_FILES['photo']['name']); $i++) { 
                  
            //          if($_FILES["photo"]["name"][$i]!=""){
                        
            //             if($_FILES["photo"]["type"][$i]=="image/png" or
            //                 $_FILES["photo"]["type"][$i]=="image/jpg" or
            //                 $_FILES["photo"]["type"][$i]=="image/jpeg"){

            //                 $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo';
            //                 $array[$i] = explode('.', $_FILES['photo']['name'][$i]);
            //                 $extension[$i] = end($array[$i]);
            //                 $photo[$i] = md5(uniqid(rand(), true)).".".$extension[$i];
                            
            //                 if (move_uploaded_file($_FILES["photo"]["tmp_name"][$i], $upload_path."/".$photo[$i])) {

            //                 }else{

            //                     $res['msg'] = "Oops! Something went wrong!";
            //                     $res['status'] = "0";

            //                     echo json_encode($res);
            //                     exit;

            //                 }
            //                 $id = $this->db->query('SELECT id FROM front_profil ORDER BY id DESC')->row()->id;
            //                 $media = array(

            //                     "file" => $photo[$i],
            //                     "path" => 'upload/photo/',
            //                     "type" => "image",
            //                     "format" => $extension[$i],
            //                     "module" => "front_profil",
            //                     "created_by" => $this->session->userdata('id'),
            //                     "created_at" => date("Y-m-d h:i:s"),
            //                     "id_nya" =>$id,
            
            //                 );
            
            //                 $this->db->insert("media_cepiring", $media); 
      
            //             }else{

            //                 $res['msg'] = "Invalid photo file! Should be PNG/JPG/JPEG.";
            //                 $res['status'] = "0";

            //                 echo json_encode($res);
            //                 exit;

            //             }
            //         }
            //     }

            // }
            // if(isset($_POST['tmp_name'])){
            //     unset($_POST['tmp_name']);
            // }    
            // if($insert){

            //     echo json_encode(array("status" => "1", "msg" => "Data berhasil disimpan!"));

            // }else{

            //     echo json_encode(array("status" => "0", "msg" => "Oop! Something went wrong!"));

            // }

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit profil";
            $data['page_subtitle'] = "Modul profil";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}