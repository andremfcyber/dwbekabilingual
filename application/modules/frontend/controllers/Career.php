<?php

class Career extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "career";

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }

        $this->table = "front_career";
        $this->dttModel = "Mdcareer";
        $this->pk = "id";

    }

    function index(){
         
        $pelamar = "<a href='".base_url()."frontend/pelamar' class='btn btn-info'><i class='fa fa-info'></i> Pelamar Career </a> ";
        $jeniscareer = "<a href='".base_url()."/frontend/kategoricareer' class='btn btn-info'><i class='fa fa-plus'></i> Jenis Career </a> ";
        $typecareer = "<a href='".base_url()."frontend/tipecareer' class='btn btn-info'><i class='fa fa-plus'></i> Type Career </a> ";

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Career ";
        $data['page_subtitle'] = "Modul career | ".$pelamar.$jeniscareer.$typecareer;
        
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Career";
            $data['page_subtitle'] = "Modul career";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.career' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "career_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "career_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "career_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "career_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':vketeranganble'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/career/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama_kerja" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,

            ),

            "kategori" => array(

                "data" => "c.kategori_career",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "tipe_kerja" => array(

                "data" => "d.tipe_career",
                "searchable" => true,
                "orderable" => true,

              
            ),

            // "isi" => array(

            //     "data" => "b.isi",
            //     "searchable" => true,
            //     "orderable" => true,dd
              
            // ),
            "status" => array(
                "data" => "b.status",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(row, meta, data){
                    let checked = '';
                    if(row == 1){
                        checked = 'checked';
                    }


                    return `<input type='checkbox' \${checked} onchange='changeStatusCareer(\${data.b.id}, this)'>`
                }"
            ),
            "action" => array(
                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            ),

        );

    }
    
    private function _get_fields_edit(){


        return array(
            "nama" => array(

                "label" => "Nama Pekerjaan",
                "type" => "text",
                "placeholder" => "Nama Pekerjaan",
                "class" => "form-control validate[required]",
             
            ),    
            "id_kategori" => array(

                "label" => "Kategori",
                "type" => "sourcequery",
                "source" => $this->Mprofil->getkategori_kerja(),
                "keydt" => "id",
                "valuedt" => "kategori_career",
                "class" => "form-control select2 validate[required]",
               
            ),
            "type_kerja" => array(

                "label" => "Tipe Pekerjaan",
                "type" => "sourcequery",
                "source" => $this->Mprofil->gettipekerja(),
                "keydt" => "id",
                "valuedt" => "tipe_career",
                "class" => "form-control select2 validate[required]",
               
            ),
           
            "keterangan" => array(

                "label" => "Deskripsi",
                "type" => "editor",
                "placeholder" => "Deskripsi",
                "class" => "form-control validate[required]",
             
            ),     

        );

// 
    }

    public function add(){

        if(count($_POST)>0){
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
           
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah career ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah career";
            $data['page_subtitle'] = "Modul career";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
           
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Career' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit career";
            $data['page_subtitle'] = "Modul career";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

    public function change_status($id, $status){
        $this->db->where([
            'id' => $id
        ]);

        // map_y([
        //     $id,
        //     $status
        // ]);
        $updated = $this->db->update('front_career', [
            'status' => $status
        ]);

        if($updated){
            return response_json([
                'message' => 'Berhasil Update Status'
            ]);
        }

        return response_json([
            'message' => 'Terjadi Kesalahan'
        ], 400);
    }

}