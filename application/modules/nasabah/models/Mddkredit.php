<?php

class Mddkredit extends MY_Model implements DatatableModel{

    function __construct(){
        parent::__construct();

        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'kredit_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'kredit_delete');
        $this->allow_validasi = $this->mcore->checkPermission($this->user_group, "kredit_validasi");
        $this->allow_verifikasi = $this->mcore->checkPermission($this->user_group, "kredit_verifikasi");
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, "kredit_detail");
        $this->allow_print = $this->mcore->checkPermission($this->user_group, "kredit_print");
        $this->allow_cek_status = $this->mcore->checkPermission($this->user_group, "kredit_cek_status");
        $this->allow_approval = $this->mcore->checkPermission($this->user_group, "kredit_approval");
        $this->allow_pertemuan = $this->mcore->checkPermission($this->user_group, "kredit_pertemuan");
        $this->allow_send_wa = $this->mcore->checkPermission($this->user_group, "kredit_wa_send");
        $this->allow_pindah_sales = $this->mcore->checkPermission($this->user_group, "kredit_pindah_sales");
        
    }
    public function appendToSelectStr() {
        // map_y($this->allow_approval);
        $buttons = [];
        
        $btn_style = 'border-radius: 10px; border: 2px #ddd solid !important;';
        $role_id = get_role();

        $url_l = base_url('nasabah/kredit');

        // map_y($role_id);
        
        if($this->allow_edit){
            $buttons[] = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>';
        }

        if($this->allow_delete){
            $buttons[] = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" href="javascript:remove(\',b.id,\');"><i class="fa fa-trash"></i></a>';
        }

        if($this->allow_validasi) {
            // $buttons[] = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Validasi" href="javascript:validasiSub(\',b.id,\');" id="validasi_\',b.id,\'"><i class="fa fa- fa-check-square-o"></i></a>';
            $buttons[] = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Validasi" href="'.base_url().'nasabah/kredit/validasi/\',b.id,\'" id="validasi_\',b.id,\'"><i class="fa fa- fa-check-square-o"></i></a>';
        }

        if($this->allow_cek_status) {
            $buttons[] = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Cek Status" href="javascript:checkSub(\',b.id,\');" id="verify_\',b.id,\'"><i class="fa fa-exclamation-circle"></i></a>';
        }

        if($this->allow_detail) {
            $buttons[] = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" href="javascript:detailSub(\',b.id,\');" id="detail_\',b.id,\'"><i class="fa fa-send"></i></a>';
        }

        if($this->allow_print) {
            $buttons[] = '<a class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Print" href="javascript:on_report(\',b.id,\', `'.$url_l.'`);" id="print_\',b.id,\'"><i class="fa fa-print"></i></a>';
        }

        // if ($this->allow_verifikasi) {
        //     $buttons[] = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Cek Status" href="javascript:verifySub(\',b.id,\');" id="verify_\',b.id,\'"> <i class="fa fa-info-circle"></i></a>';
        // }

        if($this->allow_approval){
            $buttons[] = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Terima Nasabah" href="javascript:on_accept(\',b.id,\', `'.$url_l.'`);" id="accept_\',b.id,\'"><i class="fa fa-check"></i></a>';
            $buttons[] = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Tolak Nasabah" href="javascript:on_reject(\',b.id,\', `'.$url_l.'`);" id="reject_\',b.id,\'"><i class="fa fa-close"></i></a>';
        }

        if($this->allow_pertemuan){
            $buttons[] = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Buat Pertemuan" href="javascript:on_pertemuan(\',b.id,\', `'.$url_l.'`);" id="pertemuan_\',b.id,\'"><i class="fa fa-map"></i></a>';
        }

        if($this->allow_send_wa){
            $buttons[] = '<a class="btn btn-sm btn-success" target="_blank" data-toggle="tooltip" data-placement="top" title="Send Wa" href="https://wa.me/\',b.no_tlp,\'" id="wa_\',b.id,\'"><i class="fa fa-whatsapp"></i></a>';
        }
        
        if($this->allow_pindah_sales){
            $buttons[] = '<a class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Pindah Sales" href="javascript:on_assign(\',b.id,\', `'.$url_l.'`);" id="assign_\',b.id,\'"><i class="fa fa-user"></i></a>';
        }
        

        $html = '
        <div class="dropdown dropdown-action">'.
            '<button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="poscietydropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="'.$btn_style.'">'.
            '<span class="caret"></span>'.
            '</button>'.
            '<ul class="dropdown-menu">';
            foreach($buttons as $button){
                $html .= '<li>'.$button.'</li>'; 
            }    
        $html .=    
            '</ul>'.
        '</div>';
      
        $op = "concat('".$html."')";
        // map_y($op);
        return [
            "status" => 'b.status as',
            "op" => $op,
        ];
    }

    public function fromTableStr() {
        return "nasabah b";
    }

    public function joinArray(){
        // return null;
        return array(
            "product c |left" => "b.id_product = c.id",
            "product_type d |left" => "d.id = c.product_type_id",
            "master_status sta | left" => "b.status = sta.id",  
        );
    }

    public function whereClauseArray(){
        $role_id = get_role();
        $sales_id = sales_by_user_id()['id'];

        if($role_id == '18'){
            return [
                'd.id' => '5',
                'b.sales_id' => $sales_id,
            ];
        }

        if($role_id == '22'){
            return [
                'd.id' => '5',
                // 'b.status' => '5'
            ];
        }

        return [
            'd.id' => '5'
        ];
    }

    public function orderBy(){
        return [
            'b.tgl_pengajuan' => 'DESC',
        ];
    }


}