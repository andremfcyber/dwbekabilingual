<?php

class Mdsales extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'sales_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'sales_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'sales_detail');
        $this->allow_reaset = $this->mcore->checkPermission($this->user_group, 'sales_reaset_password');


    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $reaset = '';
        $url_l = base_url('sales/sales');
        $str = '';

        // if($this->allow_detail){
        //     $detail = '<a class="btn btn-sm btn-success" href="'.base_url('form_fields/show/').'\',b.id,\'"><i class="fa fa-file"></i></a>&nbsp;';
        // }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_reaset){
            $reaset = '<a class="btn btn-sm btn-warning" href="javascript:reaset_password(\',b.user_id,\', `'.$url_l.'`);" data-toggle="tooltip" data-placement="left" title="Reaset Password Sales" ><i class="fa fa-refresh "></i></a>';
        }

        if($edit!='' || $delete!=''){

            $salin = '<a class="btn btn-sm btn-default" href="javascript:clipboard_modal(`\',b.kode_referal,\'`)"><i class="fa fa-clipboard"></i></a>&nbsp;';

            $op = "concat('".$edit.$delete.$salin.$reaset."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "sales b";
    }

    public function joinArray(){
        return array(
            "jabatan_agen c|left" => "b.id_jabatan = c.id",
        );
    }

    public function whereClauseArray(){
        return null;
    }
    


}