<style>
    .meta-container{
        background-color: #eee;
        padding: 15px;
        border: solid 1px #ccc;
    }
    .btn-meta{
        width: 100%;
        border-radius: 0px !important;
        background-color: #fff !important;
        color: #46ab1d;
        border: solid 1px #46ab1d !important;
    }
    .btn{
        border-radius: 0px !important;
    }
</style>
<div class="row">
     <div class="col-md-12">
        <div class="box box-skin">
            <div class="box-header with-border">
                <h3 class="box-title">Konten Halaman [<?php echo $page[0]['title']; ?>]</h3>
            </div>
            <div class="box-body">
                 <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                       
                        foreach($langs as $key => $value){
                        if($key == 0){
                            $class="active";
                        }else{
                            $class=""; 
                        } 
                    ?>
                    <li role="presentation" class="<?php echo $class; ?>"><a href="#<?php echo $value['code']; ?>" aria-controls="<?php echo $value['code']; ?>" role="tab" data-toggle="tab"><?php echo $value['name']; ?></a></li>
                    <?php } ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <?php
                       
                        foreach($langs as $key => $value){
                        if($key == 0){
                            $class="active";
                        }else{
                            $class=""; 
                        }
                        
                        $content = $this->db->get_where("page_contents",array("language_id" => $value['id'],"page_id" => @$page[0]['id']))->result_array();
                        $meta_tags = $this->db->get_where("page_metas",array("language_id" => $value['id'], "page_id" => @$page[0]['id']))->result_array();
                    ?>
                    <div role="tabpanel" class="tab-pane <?php echo $class; ?>" id="<?php echo $value['code']; ?>">
                       
                            <div class="rows">
                                <div class="col-md-6">
                                    <h4>Konten (<?php echo $value['name']; ?>)</h4>
                                     <form id="frm_content_<?php echo $value['id']; ?>">
                                        <input type="hidden" name="id" value="<?php echo @$content[0]['id']; ?>">
                                        <input type="hidden" name="page_id" value="<?php echo @$page[0]['id']; ?>">
                                        <input type="hidden" name="language_id" value="<?php echo $value['id']; ?>">
                                        <div class="form-group">
                                            <label>Permalink</label>
                                            <input type="text" name="permalink" id="permalink_<?php echo $value['id']; ?>" class="form-control" onkeyup="check_permalink(<?php echo $value['id']; ?>);" placeholder="contoh : hubungi-kami" value="<?php echo @$content[0]['permalink']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Link Label</label>
                                            <input type="text" name="link_label" class="form-control" placeholder="contoh : Hubungi Kami" value="<?php echo @$content[0]['link_label']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Konten</label>
                                            <textarea class="form-control tiny" name="content" id="content_<?php echo $value['id']; ?>"><?php echo @$content[0]['content']; ?></textarea>
                                        </div>
                                        <button type="button" class="btn btn-primary" onclick="save_content('<?php echo $value['id']; ?>');">Save</button>
                                        <button type="button" class="btn btn-primary" <?php echo (@$content[0]['id'] != null) ? '' : 'disabled'; ?> onclick="load_preview('<?php echo @$content[0]['id']; ?>');">Preview</button>
                                     </form>
                                </div>
                                <div class="col-md-6">
                                    <h4>SEO - Meta Tags (<?php echo $value['name']; ?>)</h4>
                                    <button type="button" class="btn btn-large btn-primary btn-meta" onclick="add_meta_tag('<?php echo $value['id']; ?>');">Tambah Meta Tag</button>
                                    <?php
                                        $i = 1; 
                                        foreach($meta_tags as $meta){ 
                                    ?>
                                        <h4>#<?php echo $i ?></h4>
                                        <div class="meta-container">
                                            <form id="frm-meta-<?php echo $meta['id']; ?>">
                                                <input type="hidden" name="id" value="<?php echo $meta['id']; ?>">
                                                <div class="form-group">
                                                    <label>Property</label>
                                                    <input type="text" name="meta_property" class="form-control" value="<?php echo $meta['meta_property']; ?>" placeholder="contoh : og:title">
                                                </div>
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="meta_name" class="form-control" value="<?php echo $meta['meta_name']; ?>" placeholder="contoh : keyword">
                                                </div>
                                                <div class="form-group">
                                                    <label>Content</label>
                                                    <textarea class="form-control" name="meta_content"><?php echo $meta['meta_content']; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Charset</label>
                                                    <input type="text" name="meta_charset" class="form-control" value="<?php echo $meta['meta_charset']; ?>" placeholder="contoh : utf-8">
                                                </div>
                                                <div class="form-group">
                                                    <label>Http-equiv</label>
                                                    <input type="text" name="meta_http_equiv" class="form-control" value="<?php echo $meta['meta_http_equiv']; ?>" placeholder="contoh : X-UA-Compatible">
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-primary" onclick="update_meta('<?php echo $meta['id']; ?>');">Update</button>
                                            <button type="button" class="btn btn-danger" onclick="delete_meta('<?php echo $meta['id']; ?>');">Delete</button>
                                        </div>
                                    <?php
                                        $i++; 
                                        } 
                                    ?>
                                </div>
                            </div>
                       
                    </div>
                    <?php } ?>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="modal-add-meta">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tambah Meta Tag</h4>
            </div>
            <div class="modal-body">
                <form id="add_meta_tag">
                    <input type="hidden" name="page_id" value="<?php echo $page[0]['id']; ?>">
                    <input type="hidden" name="language_id" id="meta_language_id">

                    <div class="form-group">
                        <label>Property</label>
                        <input type="text" name="meta_property" class="form-control" placeholder="contoh : og:title">
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="meta_name" class="form-control" placeholder="contoh : keyword">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea class="form-control" name="meta_content"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Charset</label>
                        <input type="text" name="meta_charset" class="form-control" placeholder="contoh : utf-8">
                    </div>
                    <div class="form-group">
                        <label>Http-equiv</label>
                        <input type="text" name="meta_http_equiv" class="form-control" placeholder="contoh : X-UA-Compatible">
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success " onclick="new_meta()"><i class="fa fa-save"></i> Tambah</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
            selector: '.tiny'
        });
        
    });

    function check_permalink(id){

        var val = $("#permalink_"+id).val();
        
        $("#permalink_"+id).val(val.replace(/\s/g, '-').toLowerCase());

    }

    function load_preview(id){

        blockUI('Sedang mengalihkan');
        $.get(base_url + 'page/get_page_content/'+id, function(res){

            if(res.status){
                window.open( base_url + 'preview/page/'+res.data.code+'/'+res.data.id+'/'+res.data.permalink, '_blank');
            }else{
                toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            }
            $.unblockUI();
        },'json');

    }

    function delete_meta(id){

        Swal.fire({

            title: 'Apakah anda yakin?',
            text: 'Data Meta ini akan dihapus.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'

            }).then((result) => {

            if (result.value) {
                var url = base_url + 'page/delete_meta/'+id;

                blockUI('Sedang memproses ...');

                $.get(url, function(data){

                if(data.status){

                    toastr.success(data.message, "Response Server");

                    setTimeout(function(){
                        window.location.reload();
                    },1000);

                }else{

                    toastr.error(data.message, "Response Server");

                }

                $.unblockUI();
                

                },'json').fail(function(){

                toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

                $.unblockUI();

                });

            }
        });

    }

    function new_meta(){

        var target = base_url + 'page/add_meta';
        var data = $("#add_meta_tag").serialize();

        blockUI('Sedang memproses');

        $.post(target, data, function(res){
            if(res.status){
                toastr.success(res.message, "Response Server");
                setTimeout(function(){
                    window.location.reload();
                },1000);
            }else{
                toastr.error(res.message, "Response Server");
            }
            $.unblockUI();
        },'json').fail(function(err){
            toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            $.unblockUI();
        });

    }

    function add_meta_tag(lang_id){

        $("#meta_language_id").val(lang_id);
        $("#modal-add-meta").modal({ backdrop : 'static', keyboard: false });

    }

    function save_content(lang_id){
        
        var content = tinyMCE.activeEditor.getContent();

        var data = $("#frm_content_"+lang_id).serialize();
        var target = base_url + 'page/save_content';
    
        blockUI('Sedang memproses');

        $.post(target, data + '&new_content='+content, function(res){
            if(res.status){
                toastr.success(res.message, "Response Server");
            }else{
                toastr.error(res.message, "Response Server");
            }
            $.unblockUI();
        },'json').fail(function(err){
            toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            $.unblockUI();
        });

    }

    function update_meta(id){

        var data = $("#frm-meta-"+id).serialize();
        var target = base_url + 'page/update_meta';
    
        blockUI('Sedang memproses');

        $.post(target, data, function(res){
            if(res.status){
                toastr.success(res.message, "Response Server");
            }else{
                toastr.error(res.message, "Response Server");
            }
            $.unblockUI();
        },'json').fail(function(err){
            toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            $.unblockUI();
        });

    }
</script>