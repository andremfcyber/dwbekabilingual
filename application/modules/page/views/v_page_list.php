<style>
  .btn {
    border-radius: 0% !important;
  }
</style>
<div class="row">
     <div class="col-md-12">
        <div class="box box-skin">
            <div class="box-header with-border">
                <h3 class="box-title">Halaman</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                  <?php
                    if (class_exists('datatables') && isset($dt_name)) {
                      $this->datatables->generate($dt_name); 
                    } 
                  ?>
                </div>
               
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tambah Halaman</h4>
            </div>
            <div class="modal-body">
                <form id="add_page">
                   
                    <div class="form-group">
                         <label style="color:#888893">Judul</label>
                         <input type="text" class="form-control"  name="title" placeholder="Nama Halaman">      
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success " onclick="new_page()"><i class="fa fa-save"></i> Tambah</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade in" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Halaman</h4>
            </div>
            <div class="modal-body">
                <form id="edit_page">
                    <input type="hidden" name="id" id="page_id">
                    <div class="form-group">
                         <label style="color:#888893">Judul</label>
                         <input type="text" class="form-control"  name="title" id="title" placeholder="Nama Halaman">      
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success " onclick="update_page()"><i class="fa fa-save"></i> Update</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

function add(){

  $("#modal-add").modal({ backdrop : 'static', keyboard : false });

}

function edit(id){

  var target = base_url + 'page/get_page/' + id;
  blockUI('Sedang memuat');

  $.get(target, function(res){

    if(res.status){

      $("#page_id").val(res.data.id);
      $("#title").val(res.data.title);

      $("#modal-edit").modal({ backdrop : 'static', keyboard : false });

    }else{
      toastr.error(res.message, "Response Server");
    }

    $.unblockUI();

  },'json').fail(function(err){
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
    $.unblockUI();
  });

}

function new_page(){

  var target = base_url + 'page/new';
  var data = $("#add_page").serialize();

  blockUI('Sedang memproses');

  $.post(target, data, function(res){

    if(res.status){
      toastr.success(res.message, "Response Server");

      $("#modal-add").modal('hide');

      erTable_dtPage.draw();

    }else{
      toastr.error(res.message, "Response Server");
    }

    $.unblockUI();

  },'json').fail(function(err){

    console.log(err);
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
    $.unblockUI();

  });

}

function add_content(id){
    
  window.location.href = base_url + 'page/add_content/'+id;

}

function update_status(id){

  var status = $("#status_"+id).val();
  var target = base_url + 'page/update_status';

  $.post(target, { id : id, status : status }, function(res){
    if(res.status){

      toastr.success(res.message, "Response Server");

      erTable_dtPage.draw();

    }else{

      toastr.error(res.message, "Response Server");

    }

    $.unblockUI();
  },'json').fail(function(err){
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
  });

}

function update_page(){

   var target = base_url + 'page/edit';
  var data = $("#edit_page").serialize();

  blockUI('Sedang memproses');

  $.post(target, data, function(res){

    if(res.status){
      toastr.success(res.message, "Response Server");

      $("#modal-edit").modal('hide');

      erTable_dtPage.draw();

    }else{
      toastr.error(res.message, "Response Server");
    }

    $.unblockUI();

  },'json').fail(function(err){

    console.log(err);
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
    $.unblockUI();

  });

}


function do_selected_action(){

  var selected_action = $("#selected-action").val();
  var rows_selected = erTable_dtPage.column(0).checkboxes.selected();

  rows_selected = rows_selected.join(',');

  if(rows_selected.trim() == ""){

    Swal.fire(
      'Oops',
      'Belum ada data yang diilih',
      'error'
    );

  }else{

    switch (selected_action) {
      case "drop":
        delete_all(rows_selected);
        break;
    }
  }

}

function delete_all(ids){

   Swal.fire({

      title: 'Apakah anda yakin?',
      text: 'Data halaman yang dipilih akan dihapus.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'

      }).then((result) => {

      if (result.value) {
        var url = base_url + 'page/delete_selected_data/';

        blockUI('Sedang memproses ...');

        $.post(url,{ ids : ids }, function(data){

          if(data.status){

            toastr.success(data.message, "Response Server");

            erTable_dtPage.draw();

          }else{

            toastr.error(data.message, "Response Server");

          }

          $.unblockUI();
          

        },'json').fail(function(){

          toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

          $.unblockUI();

        });

      }
  });

}

function confirm_delete(id){

  Swal.fire({

      title: 'Apakah anda yakin?',
      text: 'Data Halaman ini akan dihapus.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'

      }).then((result) => {

      if (result.value) {
        var url = base_url + 'page/delete/'+id;

        blockUI('Sedang memproses ...');

        $.get(url, function(data){

          if(data.status){

            toastr.success(data.message, "Response Server");

            erTable_dtPage.draw();

          }else{

            toastr.error(data.message, "Response Server");

          }

          $.unblockUI();
          

        },'json').fail(function(){

          toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

          $.unblockUI();

        });

      }
  });

}
</script>