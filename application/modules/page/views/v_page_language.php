<style>
  .btn {
    border-radius: 0% !important;
  }
</style>
<div class="row">
     <div class="col-md-12">
        <div class="box box-skin">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Bahasa</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                  <?php
                    if (class_exists('datatables') && isset($dt_name)) {
                      $this->datatables->generate($dt_name); 
                    } 
                  ?>
                </div>
               
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tambah Bahasa</h4>
            </div>
            <div class="modal-body">
                <form id="add_language">
                    
                    <div class="form-group">
                         <label style="color:#888893">Kode</label>
                         <input type="text" class="form-control validate[required]"  name="code" placeholder="Kode Bahasa">      
                    </div>

                    <div class="form-group">
                         <label style="color:#888893">Nama</label>
                         <input type="text" class="form-control validate[required]"  name="name" placeholder="Nama Bahasa">      
                    </div>

                    <div class="form-group">
                         <label style="color:#888893">Ikon (Flag Icon)</label>
                         <input type="text" class="form-control"  name="flag_icon" placeholder="Ikon">      
                    </div>
                    
                    <div class="form-group">
                         <label style="color:#888893">Status</label>
                         <select class="form-control" name="status">
                            <option value="1">Aktif</option>
                            <option value="0">Non Aktif</option>
                         </select>      
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success " onclick="new_language()"><i class="fa fa-save"></i> Tambah</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade in" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Bahasa</h4>
            </div>
            <div class="modal-body">
                <form id="edit_language">
                    <input type="hidden" name="id" id="language_id">
                    
                    <div class="form-group">
                         <label style="color:#888893">Kode</label>
                         <input type="text" class="form-control validate[required]" id="code"  name="code" placeholder="Kode Bahasa">      
                    </div>

                    <div class="form-group">
                         <label style="color:#888893">Nama</label>
                         <input type="text" class="form-control validate[required]"  id="name" name="name" placeholder="Nama Bahasa">      
                    </div>

                    <div class="form-group">
                         <label style="color:#888893">Ikon (Flag Icon)</label>
                         <input type="text" class="form-control"  id="flag_icon" name="flag_icon" placeholder="Ikon">      
                    </div>
                    
                    <div class="form-group">
                         <label style="color:#888893">Status</label>
                         <select class="form-control" name="status" id="status">
                            <option value="1">Aktif</option>
                            <option value="0">Non Aktif</option>
                         </select>      
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success " onclick="update_language()"><i class="fa fa-save"></i> Update</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

$(document).ready(function() {
   $("#add_language").validationEngine(); 
   $("#edit_language").validationEngine(); 
});

function add(){

  $("#modal-add").modal({ backdrop : 'static', keyboard : false });

}

function edit(id){

  var target = base_url + 'page/get_language/' + id;
  
  blockUI('Sedang memuat');

  $.get(target, function(res){

    if(res.status){

      $("#language_id").val(res.data.id);
      $("#name").val(res.data.name);
      $("#code").val(res.data.code);
      $("#flag_icon").val(res.data.flag_icon);
      $("#status").val(res.data.status);

      $("#modal-edit").modal({ backdrop : 'static', keyboard : false });

    }else{
      toastr.error(res.message, "Response Server");
    }

    $.unblockUI();

  },'json').fail(function(err){
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
    $.unblockUI();
  });

}

function new_language(){

    var target = base_url + 'page/new_language';
    var data = $("#add_language").serialize();
    
    if($("#add_language").validationEngine('validate')){

        blockUI('Sedang memproses');

        $.post(target, data, function(res){

            if(res.status){
            toastr.success(res.message, "Response Server");

            $("#modal-add").modal('hide');

            erTable_dtPageLanguage.draw();

            }else{
            toastr.error(res.message, "Response Server");
            }

            $.unblockUI();

        },'json').fail(function(err){

            console.log(err);
            toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            $.unblockUI();

        });

    }

}

function update_status(id){

  var status = $("#status_"+id).val();
  var target = base_url + 'page/update_language_status';

  $.post(target, { id : id, status : status }, function(res){
    if(res.status){

      toastr.success(res.message, "Response Server");

      erTable_dtPageLanguage.draw();

    }else{

      toastr.error(res.message, "Response Server");

    }

    $.unblockUI();
  },'json').fail(function(err){
    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
  });

}

function update_language(){

    var target = base_url + 'page/edit_language';
    var data = $("#edit_language").serialize();

    if($("#edit_language").validationEngine('validate')){

        blockUI('Sedang memproses');

        $.post(target, data, function(res){

            if(res.status){
            toastr.success(res.message, "Response Server");

            $("#modal-edit").modal('hide');

            erTable_dtPageLanguage.draw();

            }else{
            toastr.error(res.message, "Response Server");
            }

            $.unblockUI();

        },'json').fail(function(err){

            console.log(err);
            toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");
            $.unblockUI();

        });

    }
  
}


function do_selected_action(){

  var selected_action = $("#selected-action").val();
  var rows_selected = erTable_dtPageLanguage.column(0).checkboxes.selected();

  rows_selected = rows_selected.join(',');

  if(rows_selected.trim() == ""){

    Swal.fire(
      'Oops',
      'Belum ada data yang diilih',
      'error'
    );

  }else{

    switch (selected_action) {
      case "drop":
        delete_all(rows_selected);
        break;
    }
  }

}

function delete_all(ids){

   Swal.fire({

      title: 'Apakah anda yakin?',
      text: 'Data bahasa yang dipilih akan dihapus.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'

      }).then((result) => {

      if (result.value) {
        var url = base_url + 'page/delete_selected_language/';

        blockUI('Sedang memproses ...');

        $.post(url,{ ids : ids }, function(data){

          if(data.status){

            toastr.success(data.message, "Response Server");

            erTable_dtPageLanguage.draw();

          }else{

            toastr.error(data.message, "Response Server");

          }

          $.unblockUI();
          

        },'json').fail(function(){

          toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

          $.unblockUI();

        });

      }
  });

}

function confirm_delete(id){

  Swal.fire({

      title: 'Apakah anda yakin?',
      text: 'Bahasa ini akan dihapus.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'

      }).then((result) => {

      if (result.value) {
        var url = base_url + 'page/delete_language/'+id;

        blockUI('Sedang memproses ...');

        $.get(url, function(data){

          if(data.status){

            toastr.success(data.message, "Response Server");

            erTable_dtPageLanguage.draw();

          }else{

            toastr.error(data.message, "Response Server");

          }

          $.unblockUI();
          

        },'json').fail(function(){

          toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

          $.unblockUI();

        });

      }
  });

}
</script>