<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />

<style>
    .mjs-nestedSortable-error {
        background: #fbe3e4;
        border-color: transparent;
    }

    #tree {
        width: 550px;
        margin: 0;
    }

    ol {
        max-width: 450px;
        padding-left: 25px;
    }

    ol.sortable,ol.sortable ol {
        list-style-type: none;
    }

    .sortable li div {
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        cursor: move;
        border-color: #D4D4D4 #D4D4D4 #BCBCBC;
        margin: 0;
        padding: 3px;
    }

    li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
        border-color: #999;
    }

    .disclose, .expandEditor {
        cursor: pointer;
        width: 20px;
        display: none;
    }

    .sortable li.mjs-nestedSortable-collapsed > ol {
        display: none;
    }

    .sortable li.mjs-nestedSortable-branch > div > .disclose {
        display: inline-block;
    }

    .sortable span.ui-icon {
        display: inline-block;
        margin: 0;
        padding: 0;
    }

    .menuDiv {
        background: #EBEBEB;
    }

    .menuEdit {
        background: #FFF;
    }

    .itemTitle {
        vertical-align: middle;
        cursor: pointer;
    }

    .deleteMenu {
        float: right;
        cursor: pointer;
    }
    .btn{
        border-radius:5px !important;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="box box-skin">
            <div class="box-header with-border">
                <h3 class="box-title">Buat Menu Baru</h3>
            </div>
            <div class="box-body">
                <form id="frm-menu">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="title" class="form-control validate[required]" placeholder="contoh : Tentang Kami">
                    </div>
                    <div class="form-group">
                        <label>Induk Menu</label>
                        <select class="form-control" name="parent">
                            <option value=""> --- </option>
                            <?php foreach($menus as $menu){ ?>
                            <option value="<?= $menu['id']; ?>"> <?= $menu['title']; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Ikon</label>
                        <input type="text" name="icon" class="form-control" placeholder="contoh : fa-cogs">
                    </div>
                    <div class="form-group">
                        <label>Urutan</label>
                        <input type="number" name="sequence" class="form-control" placeholder="contoh : 1">
                    </div>
                    <div class="form-group">
                        <label>Label & Link</label>
                        <ul class="nav nav-tabs" role="tablist">
                            <?php
                            
                                foreach($langs as $key => $value){
                                if($key == 0){
                                    $class="active";
                                }else{
                                    $class=""; 
                                } 
                            ?>
                            <li role="presentation" class="<?php echo $class; ?>"><a href="#<?php echo $value['code']; ?>" aria-controls="<?php echo $value['code']; ?>" role="tab" data-toggle="tab"><?php echo $value['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            
                                foreach($langs as $key => $value){
                                if($key == 0){
                                    $class="active";
                                }else{
                                    $class=""; 
                                }
                                
                                $page_contents = $this->db->get_where('page_contents',array('language_id' => $value['id']))->result_array();
                            
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo $class; ?>" id="<?php echo $value['code']; ?>">
                                <br>
                                <div class="form-group">
                                    <label>Label (<?php echo $value['name']; ?>)</label>
                                     <input type="text" name="label_<?php echo $value['id']; ?>" class="form-control validate[required]" placeholder="Label menu dalam <?php echo $value['name']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Halaman (<?php echo $value['name']; ?>)</label>
                                    <select class="form-control" name="page_content_<?php echo $value['id']; ?>">
                                        <option value=""> --- </option>
                                        <?php foreach($page_contents as $page){ ?>
                                        <option value="<?php echo $page['id']; ?>"> <?php echo $page['link_label']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Url (jika ingin gunakan halaman eksternal)</label>
                                    <input type="text" name="url_<?php echo $value['id']; ?>" class="form-control" placeholder="contoh : http://www.google.com">
                                </div>
                            </div>

                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status">
                            <option value="0"> Draft </option>
                            <option value="1"> Publish </option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="add_menu();">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-skin">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Menu</h3>
            </div>
            <div class="box-body">
               <?= renderMenus($menus); ?>
            </div>
            <div class="box-footer">
                <button id="serialize" type="button" class="btn btn-primary">Save Config</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
     $(document).ready(function(){

        $("#frm-menu").validationEngine(); 
        var ns = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false,
            change: function(){
                console.log('Relocated item');
            }
        });

        $('.expandEditor').attr('title','Click to show/hide item editor');
        $('.disclose').attr('title','Click to show/hide children');
        $('.deleteMenu').attr('title', 'Click to delete item.');

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
        });

        $('.expandEditor, .itemTitle').click(function(){
            var id = $(this).attr('data-id');
            $('#menuEdit'+id).toggle();
            $(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
        });

        $('.deleteMenu').click(function(){
            var id = $(this).attr('data-id');
            $('#menuItem_'+id).remove();
        });

        $('#serialize').click(function(){
            var serialized = $('ol.sortable').nestedSortable('serialize');
            var target = '<?php echo base_url(); ?>page/update_menu_position';

            console.log(serialized);
            
            $.post(target, serialized, function(res){

                if(res.status=="1"){
                    toastr.success(res.msg, 'Response Server');
                }else{
                    toastr.error(res.msg, 'Response Server');
                }

            },'json');

        })

        $('#toHierarchy').click(function(e){
            hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
            hiered = dump(hiered);
            (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
                $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
        })

        $('#toArray').click(function(e){
            arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
            arraied = dump(arraied);
            (typeof($('#toArrayOutput')[0].textContent) != 'undefined') ?
                $('#toArrayOutput')[0].textContent = arraied : $('#toArrayOutput')[0].innerText = arraied;
        });

        $("#frm-menu").submit(function(){
            var data = $(this).serialize();
            var target = '<?php echo base_url(); ?>dashboard/insert_menu';

            $.post(target, data, function(res){

                if(res.status=="1"){

                    toastr.success(res.msg, 'Response Server');

                    setTimeout('window.location.reload();',3000);

                }else{

                    toastr.error(res.msg, 'Response Server');

                }

            },'json');

            return false;

        });
        $('.disabled').click(function(e){
            e.preventDefault();
        });
    });
    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function editMenu(id){

        $("#frm_menu_edit_"+id).slideToggle("slow",function(){

        });

    }

    function update_menu(id){

        var data = $("#frm_menu_edit_"+id).serialize();
        var target = '<?php echo base_url(); ?>page/update_menu';

        $.post(target, data, function(res){

            if(res.status=="1"){

                toastr.success(res.message, 'Response Server');

                setTimeout('window.location.reload();',3000);

            }else{

                toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

            }

        },'json').fail(function(err){
            console.log(err);
        });

    }

    function deleteMenu(id){

        Swal.fire({

            title: 'Apakah anda yakin?',
            text: 'Data Halaman ini akan dihapus.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'

            }).then((result) => {

            if (result.value) {
                var url = base_url + 'page/delete_menu/'+id;

                blockUI('Sedang memproses ...');

                $.get(url, function(data){

                if(data.status){

                    toastr.success(data.message, "Response Server");

                    setTimeout('window.location.reload();',3000);

                }else{

                    toastr.error(data.message, "Response Server");

                }

                $.unblockUI();
                

                },'json').fail(function(){

                    toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

                    $.unblockUI();

                });

            }
        });

    }
    function add_menu(){

        var target = base_url + 'page/add_menu';
        var data = $("#frm-menu").serialize();

        if($("#frm-menu").validationEngine('validate')){

            blockUI('Sedang memproses');

            $.post(target,data, function(res){
                
                if(res.status){

                    toastr.success(res.message, "Response Server");

                    setTimeout(function(){
                        window.location.reload();
                    },1000);

                }else{

                    toastr.error(res.message, "Response Server");

                }

                $.unblockUI();

            },'json').fail(function(err){

                toastr.error("Oops! Telah terjadi kesalahan. Mohon coba kembali nanti.", "Response Server");

                $.unblockUI();

            });

        }

    }
</script>