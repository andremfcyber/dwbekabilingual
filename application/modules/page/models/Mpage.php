<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class Mpage extends CI_Model
{
    function getParentMenu(){

        $result = $this->db->query("SELECT * FROM page_menus WHERE parent IS NULL")->result_array();
        return $result;

    }

    function getMenuChilds($parent){

        $result = $this->db->query("SELECT * FROM page_menus WHERE parent = '$parent'")->result_array();
        return $result;

    }

    function getMenuChildByLangID($parent,$lang){

        $result = $this->db->query("SELECT * FROM v_page_menu WHERE parent = '$parent' AND language_id='$lang'")->result_array();
        return $result;

    }
    
    function setParentMenu($id, $parent_id, $seq){

        $sql = "UPDATE page_menus SET parent = ".$parent_id.", sequence = '".$seq."' WHERE id = '".$id."'";
        $query = $this->db->query($sql);
        return $query;

    }
}