<?php

class Page extends Admin_Controller{

    function __construct(){

        parent::__construct();

        $this->load->model('Mcore', 'mcore');
        $this->load->model('Mpage', 'mpage');

        $this->load->library('datatables/Datatables');

        $this->dt_dom = '"<\'row\'<\'col-sm-2\'l><\'col-sm-4\'<\'toolbar\'>><\'col-sm-6\'B>> <\'row\'<\'col-sm-12\'tr>><\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>"';
        $this->dt_button = "[
            {
                extend:    'print',
                text:      '<i class=\"fa fa-print\"></i>',
                titleAttr: 'Print',
                tag: \"button\",
                className: \"\",
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend:    'excelHtml5',
                text:      '<i class=\"fa fa-file-excel-o\"></i>',
                titleAttr: 'Excel',
                tag: \"button\",
                className: \"\",
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                className: 'addBtn',
                text: '<i class=\"fa fa-plus\"></i> Tambah',
                action: function ( e, dt, node, config ){
                    add()
                }
            },
            'colvis'
        ]";

    }


    function index(){

        $this->_theme_vars['page_title'] = 'Halaman Website';
        $this->_theme_vars['page_subtitle'] = 'Managemen Halaman Website';
        $this->_theme_vars['parent_menu'] = 'page_management';
        $this->_theme_vars['submenu'] = 'pages';

        $dt = $this->datatables->init();
        $dt->select('a.*')->from('pages a');
        $dt->set_options([
            "columns" => [
                [ "orderable" => false ],
                [ "orderable" => true ],
                [ "orderable" => true ],
                [ "orderable" => true ],
                [ "orderable" => false ],
            ],
            'columnDefs' => "[
                {
                    'targets': 0,
                    'createdCell':  function (td, cellData, rowData, row, col){
                        if(rowData[2] === 'Home Page'){
                            this.api().cell(td).checkboxes.disable();
                        }
                    },  
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ]",
            'select'=> "{
                'style': 'multi'
            }",
            "order" => "[[ 1, 'asc' ]]",
            "lengthMenu" => "[[10,25,50,100,-1], [10,25,50,100,'All']]",
            "dom" => $this->dt_dom,
            "buttons" =>  $this->dt_button,
            "initComplete" => "function(settings, json) {
                let toolbar = \"\";
                toolbar += \"<label> Selected data \";
                toolbar += \"<select class='form-control' id='selected-action'>\";
                toolbar += \"<option>-- action ---</option>\";
                toolbar += \"<option value='drop'>Delete</option>\";
                toolbar += \"</select>\";
                toolbar += \"<button type='button' onclick='do_selected_action();' class='btn btn-primary'>Go</button>\";
                toolbar += \"</label>\";
                $(\"div.toolbar\").html(toolbar);
            }"
        ]);
        $dt->searchable_columns([
            [ "type" => "none" ],
            [ 
                "type" => "text", 
                "operator" => "=" 
            ],
            [ 
                "type" => "text", 
                "operator" => "LIKE" 
            ],
        ]);
        $dt->style([
            'class' => 'table table-striped table-bordered',
        ]);
        $dt->column('#', 'id', function($data, $row) {
            if($row['title'] == 'Home Page'){
                $data = '';
            }

            return $data;
        });
        $dt->column('ID', 'id', function($data, $row) {
            return $data;
        });

        $dt->column('Judul', 'title', function($data, $row) {
            return $data;
        });

        $dt->column('Status', 'status', function($data, $row) {

            $html = '';

            if($row['title'] != 'Home Page'){
                $html .= '<select class="form-control" onchange="update_status('.$row['id'].');" id="status_'.$row['id'].'">';
                if($data=="1"){
                    $html .= '<option value="0">Draft</option>';
                    $html .= '<option value="1" selected>Published</option>';
                }else{
                    $html .= '<option value="0" selected>Draft</option>';
                    $html .= '<option value="1">Published</option>';
                }
                $html .= '</select>';
            }else{
                $html.='Published';
            }
            return $html;
        });

        $dt->column('Aksi', 'id', function($data, $row) {
            $btn = '<div class="btn-toolbar">';
            
            if($row['title'] != 'Home Page'){
                $btn .= '<button class="btn btn-sm btn-success" onclick="edit(\''.$row['id'].'\');"><i class="fa fa-pencil-square-o"></i></button> ';
                $btn .= '<button class="btn btn-sm btn-danger" onclick="confirm_delete(\''.$row['id'].'\');"><i class="fa fa-trash-o"></i></button> ';
            }
            
            $btn .= '<button class="btn btn-sm btn-info" onclick="add_content(\''.$row['id'].'\');"><i class="fa fa-cogs"></i></button> ';
            
        
            $btn .= '</div>';
            return $btn;
        });

        $data['dt_name'] = 'dtPage';
        
        $this->datatables->create('dtPage', $dt); 

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $data['user_id'] = $this->session->userdata('id');
        $html = $this->load->view('v_page_list', $data, TRUE);
        $this->render_to_admin($html);

    }

    function languages(){

        $this->_theme_vars['page_title'] = 'Master Bahasa';
        $this->_theme_vars['page_subtitle'] = 'Managemen Halaman Website';
        $this->_theme_vars['parent_menu'] = 'page_management';
        $this->_theme_vars['submenu'] = 'languages';

        $dt = $this->datatables->init();
        $dt->select('a.*')->from('page_languages a');
        $dt->set_options([
            "columns" => [
                [ "orderable" => false ],
                [ "orderable" => true ],
                [ "orderable" => true ],
                [ "orderable" => true ],
                [ "orderable" => true ],
                [ "orderable" => false ],
            ],
            'columnDefs' => "[
                  
                {
                    'targets': 0,
                    'createdCell':  function (td, cellData, rowData, row, col){
                        if(rowData[2] === 'Bahasa Indonesia'){
                            this.api().cell(td).checkboxes.disable();
                        }
                    },   
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ]",
            'select'=> "{
                'style': 'multi'
            }",
            "order" => "[[ 1, 'asc' ]]",
            "lengthMenu" => "[[10,25,50,100,-1], [10,25,50,100,'All']]",
            "dom" => $this->dt_dom,
            "buttons" =>  $this->dt_button,
            "initComplete" => "function(settings, json) {
                let toolbar = \"\";
                toolbar += \"<label> Selected data \";
                toolbar += \"<select class='form-control' id='selected-action'>\";
                toolbar += \"<option>-- action ---</option>\";
                toolbar += \"<option value='drop'>Delete</option>\";
                toolbar += \"</select>\";
                toolbar += \"<button type='button' onclick='do_selected_action();' class='btn btn-primary'>Go</button>\";
                toolbar += \"</label>\";
                $(\"div.toolbar\").html(toolbar);
            }"
        ]);
        $dt->searchable_columns([
            [ "type" => "none" ],
            [ 
                "type" => "text", 
                "operator" => "=" 
            ],
            [ 
                "type" => "text", 
                "operator" => "LIKE" 
            ],
        ]);
        $dt->style([
            'class' => 'table table-striped table-bordered',
        ]);
        $dt->column('#', 'id', function($data, $row) {
            
            if($row['name'] == 'Bahasa Indonesia'){
                $data = '';
            }

            return $data;
        });
        $dt->column('Kode', 'code', function($data, $row) {
            return $data;
        });

        $dt->column('Nama', 'name', function($data, $row) {
            return $data;
        });

        $dt->column('Ikon', 'flag_icon', function($data, $row) {
            return '<i class="flag-icon '.$data.'"></i>';
        });

        $dt->column('Status', 'status', function($data, $row) {

            $html = '';

            if($row['name'] != 'Bahasa Indonesia'){
                $html .= '<select class="form-control" onchange="update_status('.$row['id'].');" id="status_'.$row['id'].'">';
                if($data=="1"){
                    $html .= '<option value="0">Non Aktif</option>';
                    $html .= '<option value="1" selected>Aktif</option>';
                }else{
                    $html .= '<option value="0" selected>Non Aktif</option>';
                    $html .= '<option value="1">Aktif</option>';
                }
                $html .= '</select>';
            }else{
                $html.='Aktif';
            }
            return $html;
        });

        $dt->column('Aksi', 'id', function($data, $row) {
            $btn = '<div class="btn-toolbar">';
            
            if($row['name'] != 'Bahasa Indonesia'){
                $btn .= '<button class="btn btn-sm btn-success" onclick="edit(\''.$row['id'].'\');"><i class="fa fa-pencil-square-o"></i></button> ';
                $btn .= '<button class="btn btn-sm btn-danger" onclick="confirm_delete(\''.$row['id'].'\');"><i class="fa fa-trash-o"></i></button> ';
            }
            
        
            $btn .= '</div>';
            return $btn;
        });

        $data['dt_name'] = 'dtPageLanguage';
        
        $this->datatables->create('dtPageLanguage', $dt); 

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $data['user_id'] = $this->session->userdata('id');
        $html = $this->load->view('v_page_language', $data, TRUE);
        $this->render_to_admin($html);

    }

    public function add_content($id = null){

        if($id != null){

            $this->_theme_vars['page_title'] = 'Konten Halaman Website';
            $this->_theme_vars['page_subtitle'] = 'Managemen Halaman Website';
            $this->_theme_vars['parent_menu'] = 'page_management';
            $this->_theme_vars['submenu'] = 'pages';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $data['user_id'] = $this->session->userdata('id');
            $data['langs'] = $this->db->get_where('page_languages',array('status' => '1'))->result_array();
            $data['page'] = $this->db->get_where('pages',array('id' => $id))->result_array();
            $html = $this->load->view('v_page_content', $data, TRUE);
            $this->render_to_admin($html);

        }

    }

    public function menu_management(){

        $this->_theme_vars['page_title'] = 'Menu Halaman Website';
        $this->_theme_vars['page_subtitle'] = 'Managemen Menu Website';
        $this->_theme_vars['parent_menu'] = 'page_management';
        $this->_theme_vars['submenu'] = 'menus';

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $data['user_id'] = $this->session->userdata('id');
        $data['langs'] = $this->db->get_where('page_languages',array('status' => '1'))->result_array();
        $data['menus'] = $this->mpage->getParentMenu();
      
        $html = $this->load->view('v_page_menu', $data, TRUE);
        $this->render_to_admin($html);

    }

    public function new(){

        if(count($_POST)>0){

            $data = array(
                'title' => $this->input->post('title'),
                'status' => '0'
            );

            $insert = $this->db->insert('pages', $data);

            if($insert){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil dibuat!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function new_language(){

        if(count($_POST)>0){

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'flag_icon' => $this->input->post('flag_icon'),
                'status' => $this->input->post('status'),
            );

            $insert = $this->db->insert('page_languages', $data);

            if($insert){
                $response = array(
                    'status' => true,
                    'message' => 'Bahasa berhasil dibuat!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function update_menu(){

        if(count($_POST)>0){

            $id = $this->input->post('id');

            $data = array(
                'icon' => $this->input->post('icon'),
                'status' => $this->input->post('status'),
            );

            $this->db->where("id", $id);
            $update = $this->db->update("page_menus",$data);

            
            $langs = $this->db->get_where('page_languages',array('status' => '1'))->result_array();

            foreach ($langs as $lang) {
                
                $data = array(
                    'page_content_id' => @$_POST['page_content_'.$lang['id']],
                    'label' => @$_POST['label_'.$lang['id']],
                    'url' => @$_POST['url_'.$lang['id']],
                );

                $this->db->where("page_menu_id",$id);
                $this->db->where("language_id",$lang['id']);

                $this->db->update("page_menu_contents",$data);

            }

            if($update){
            
                $response = array(
                    'status' => true,
                    'message' => 'Menu berhasil diupdate!'
                );
            
            }else{
            
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            
            }

            echo json_encode($response);

        }

    }

    public function delete_menu($id = null){

        if($id != null){

            $this->db->where("id", $id);
            $delete = $this->db->delete("page_menus");

            if($delete){
                $this->db->where("page_menu_id",$id);
                $this->db->delete("page_menu_contents");

                $this->db->where("parent", $id);
                $this->db->update("page_menus", array("parent" => null));
                
                $response = array(
                    'status' => true,
                    'message' => 'Menu berhasil diupdate!'
                );

            }else{

                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );

            }

            echo json_encode($response);

        }

    }

    public function update_menu_position(){

        if(count($_POST)>0){

            $a = array_keys($_POST['menuItem']);
            $b = count($a);

            $parent_seq = 1;
            $child_seq = 1;

            for($i=0;$i < $b;$i++){

                $menu_id = $a[$i];

                $menu_parent = $_POST['menuItem'][$menu_id];

                if($menu_parent!="null"){

                    $this->mpage->setParentMenu($menu_id, $menu_parent, $child_seq);

                    $child_seq++;

                }else{

                    $child_seq = 1;

                    $this->mpage->setParentMenu($menu_id, $menu_parent, $parent_seq);

                    $parent_seq++;

                }
            }


            echo json_encode(array("status" => 1, "msg" => "Menus position successfully updated!"));
        }


    }

    public function add_menu(){

        if(count($_POST)>0){

            $data = array(
                'title' => $this->input->post('title'),
                'parent' => $this->input->post('parent'),
                'icon' => $this->input->post('icon'),
                'sequence' => $this->input->post('sequence'),
                'status' => $this->input->post('status'),
            );

            if($this->input->post('parent')==''){
                unset($data['parent']);
            }

            $insert = $this->db->insert('page_menus', $data);
            
            if($insert){
                
                $insert_id = $this->db->insert_id('page_menus_id_seq');

                $langs = $this->db->get_where('page_languages',array('status' => '1'))->result_array();

                foreach ($langs as $lang) {
                    
                    $data = array(
                        'page_menu_id' => $insert_id,
                        'page_content_id' => @$_POST['page_content_'.$lang['id']],
                        'label' => @$_POST['label_'.$lang['id']],
                        'url' => @$_POST['url_'.$lang['id']],
                        'language_id' => $lang['id']
                    );

                    if($data['page_content_id'] == null){

                        unset($data['page_content_id']);

                    }

                    if($data['label'] == null){

                        unset($data['label']);

                    }

                    if($data['url'] == null){

                        unset($data['url']);

                    }

                    $this->db->insert('page_menu_contents', $data);

                }

                $response = array(
                    'status' => true,
                    'message' => 'Menu berhasil dibuat!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function save_content(){

        if(count($_POST)>0){

            $data = array(
                'language_id' => $this->input->post('language_id'),
                'page_id' => $this->input->post('page_id'),
                'permalink' => $this->input->post('permalink'),
                'link_label' => $this->input->post('link_label'),
                'content' => $this->input->post('new_content'),
            );

            if($this->input->post('id') == ''){
                $query = $this->db->insert('page_contents', $data);
            }else{
                $this->db->where("id", $this->input->post('id'));
                $query = $this->db->update('page_contents', $data);
            }

            if($query){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil disimpan!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function add_meta(){

        if(count($_POST)>0){

            $data = array(
                'meta_property' => $this->input->post('meta_property'),
                'meta_name' => $this->input->post('meta_name'),
                'meta_content' => $this->input->post('meta_content'),
                'meta_charset' => $this->input->post('meta_charset'),
                'meta_http_equiv' => $this->input->post('meta_http_equiv'),
                'page_id' => $this->input->post('page_id'),
                'language_id' => $this->input->post('language_id'),
            );

            $insert = $this->db->insert('page_metas', $data);

            if($insert){
                $response = array(
                    'status' => true,
                    'message' => 'Data Meta berhasil ditambahkan!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function update_meta(){

        if(count($_POST)>0){

            $data = array(
                'meta_property' => $this->input->post('meta_property'),
                'meta_name' => $this->input->post('meta_name'),
                'meta_content' => $this->input->post('meta_content'),
                'meta_charset' => $this->input->post('meta_charset'),
                'meta_http_equiv' => $this->input->post('meta_http_equiv')
            );

            $this->db->where("id", $this->input->post('id'));

            $update = $this->db->update('page_metas', $data);

            if($update){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil diupdate!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function get_page_content($id = null){

        if($id != null){

            $data = $this->db->get_where('v_page', array('page_content_id' => $id))->result_array();

            if($data != null){
                $response = array(
                    'status' => true,
                    'message' => 'success',
                    'data' => @$data[0]
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'failed'
                );
            }

            echo json_encode($response);

        }

    }

    public function edit(){

        if(count($_POST)>0){

            $data = array(
                'title' => $this->input->post('title')
            );

            $this->db->where("id", $this->input->post('id'));

            $update = $this->db->update('pages', $data);

            if($update){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil diupdate!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function edit_language(){

        if(count($_POST)>0){

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'flag_icon' => $this->input->post('flag_icon'),
                'status' => $this->input->post('status'),
            );

            $this->db->where("id", $this->input->post('id'));

            $update = $this->db->update('page_languages', $data);

            if($update){
                $response = array(
                    'status' => true,
                    'message' => 'Bahasa berhasil diupdate!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function delete($id = null){

        if($id != null){

            $this->db->where("id", $id);
            $delete = $this->db->delete("pages");

            if($delete){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil dihapus!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }   

    }

    public function delete_meta($id = null){

        if($id != null){

            $this->db->where("id", $id);
            $delete = $this->db->delete("page_metas");

            if($delete){
                $response = array(
                    'status' => true,
                    'message' => 'Data Meta berhasil dihapus!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }   

    }

    public function delete_language($id = null){

        if($id != null){

            $this->db->where("id", $id);
            $delete = $this->db->delete("page_languages");

            if($delete){
                $response = array(
                    'status' => true,
                    'message' => 'Bahasa berhasil dihapus!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }   

    }

    function delete_selected_data(){

        if(count($_POST)>0){

            $ids = $this->input->post('ids');

            $this->db->where_in('id', explode(',',$ids));
            $delete = $this->db->delete('pages');

            if($delete){
                $response = array(
                    'status' => true,
                    'message' => 'Halaman berhasil dihapus!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    function delete_selected_language(){

        if(count($_POST)>0){

            $ids = $this->input->post('ids');

            $this->db->where_in('id', explode(',',$ids));
            $delete = $this->db->delete('page_languages');

            if($delete){
                $response = array(
                    'status' => true,
                    'message' => 'Data berhasil dihapus!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    function update_status(){

        if(count($_POST)>0){

            $id = $this->input->post('id');
            $status = $this->input->post('status');

            $this->db->where_in('id', $id);
            $update = $this->db->update('pages', array("status" => $status));

            if($update){
                $response = array(
                    'status' => true,
                    'message' => 'Status berhasil diupdate!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    function update_language_status(){

        if(count($_POST)>0){

            $id = $this->input->post('id');
            $status = $this->input->post('status');

            $this->db->where_in('id', $id);
            $update = $this->db->update('page_languages', array("status" => $status));

            if($update){
                $response = array(
                    'status' => true,
                    'message' => 'Status berhasil diupdate!'
                );
            }else{
                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );
            }

            echo json_encode($response);

        }

    }

    public function get_page($id=null){

        if($id != null){

            $data = $this->db->get_where("pages", array("id" => $id))->result_array();

            if($data != null){

                $response = array(
                    'status' => true,
                    'message' => 'success',
                    'data' => @$data[0]
                );

            }else{

                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );

            }

            echo json_encode($response);
        }

    }

    public function get_language($id=null){

        if($id != null){

            $data = $this->db->get_where("page_languages", array("id" => $id))->result_array();

            if($data != null){

                $response = array(
                    'status' => true,
                    'message' => 'success',
                    'data' => @$data[0]
                );

            }else{

                $response = array(
                    'status' => false,
                    'message' => 'Oops! Telah terjadi kesalahan. Mohon coba lagi nanti'
                );

            }

            echo json_encode($response);
        }

    }

}