<?php

class Jabatan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mjabatan");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }

        $this->table = "jabatan_agen";
        $this->dttModel = "Mdjabatan";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Jabatan";
        $data['page_subtitle'] = "Modul Master";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function rule_produk_update($id){
        $request = $_REQUEST;
        
        // map_y($request);

        $rule = [];
        $_rule = isset($request['rule']) ? $request['rule']: '';
        if($_rule && count($_rule) > 0){
            $i = 0;
            foreach($_rule as $key){
                $rule[$i]['id_produk'] = $key;
                $rule[$i]['id_jabatan'] = $id;
                $rule[$i]['type'] = 'allow';
                $i++;
            }
        }
        $update = $this->Mjabatan->update_rule($id, $rule);
        // if($update){
        return response_json([
            'msg' => 'Aturan Produk Diperbaharui'
        ]);
        // }
    }

    public function rule_produk($id){
        // map_y($id);
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Jabatan';
        $this->_theme_vars['page_subtitle'] = 'Aturan Produk';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = $this->_get_active_menu()['parent_menu'];
        $this->_theme_vars['submenu'] = $this->_get_active_menu()['submenu'];
        
        $data['produk'] = $this->Mjabatan->get_produk();
        $data['rule'] = $this->Mjabatan->get_rule($id);
        $data['jabatan'] = $this->Mjabatan->getjabatanById($id);
        // map_y($data);
        $this->render_admin_view('view/jabatan/rule_produk','', $data);
    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Detail Jabatan";
            $data['page_subtitle'] = "Modul Master";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    private function _get_active_menu(){

        return array(
            'parent_menu' => 'master', 
            'submenu' => 'jabatan_index' 
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "jabatan_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "jabatan_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "jabatan_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "jabatan_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(
            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF
        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."master/jabatan/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){

        return array(

            "Nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Deskripsi" => array(

                "data" => "b.description",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, type, row){
                    return data.substr(1, 111)+'...';
                }",
              

            ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,

            )

        );

    }
    
    private function _get_fields_edit(){

        return array(
            "nama" => array(
                "label" => "Nama Jabatan",
                "type" => "text",
                "placeholder" => "Nama Jabatan",
                "class" => "form-control validate[required]",
            ),

            "description" => array(
                "label" => "Deskripsi",
                "type" => "textarea",
                "placeholder" => "Deskripsi",
                "class" => "form-control",
            ),
        );

// 
    }

    public function add(){

        if(count($_POST) > 0){
           
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah Jabatan' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Jabatan";
            $data['page_subtitle'] = "Modul Master";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Jabatan' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Jabatan";
            $data['page_subtitle'] = "Modul Jabatan";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){
            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();
        }

    }

}