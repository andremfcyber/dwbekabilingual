<?php

class Mjabatan extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getJabatan(){
        $result = $this->db->query("SELECT * FROM jabatan_agen")->result_array();
        return @$result;
    }

    function getjabatanById($id){

        $result = $this->db->query("SELECT * FROM jabatan_agen WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM jabatan_agen")->result_array();
        return $result;

    }

    function get_produk(){
        $query = $this->db->query("SELECT * FROM product_type")->result_array();
        return @$query;
    }

    function get_rule($id_jabatan){
        $query = $this->db->query("SELECT * FROM conf_produk_jabatan WHERE id_jabatan = '$id_jabatan'")->result_array();
        return @$query;
    }

    function update_rule($id_jabatan, $rule_produk){
        if(count($rule_produk) > 0){
            $this->db->query("DELETE FROM conf_produk_jabatan WHERE id_jabatan = '$id_jabatan'");
            $this->db->insert_batch('conf_produk_jabatan', $rule_produk);
            return true;
        }
    }

}