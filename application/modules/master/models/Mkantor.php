<?php

class Mkantor extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getKantor(){
        $result = $this->db->query("SELECT * FROM master_kantor")->result_array();
        return @$result;
    }

    function getkantorById($id){

        $result = $this->db->query("SELECT * FROM master_kantor WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM master_kantor")->result_array();
        return $result;

    }

}