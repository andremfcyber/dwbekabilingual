<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class MUserGroup extends CI_Model
{
    function getUserGroupById($id){

        $result = $this->db->query("SELECT * FROM aauth_groups WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getUserGroup(){

        $result = $this->db->query("SELECT * FROM aauth_groups")->result_array();
        return $result;

    }
    function getUserGroupOnly($ids){
        $result = $this->db->query("SELECT * FROM aauth_groups WHERE id IN($ids)")->result_array();
        return $result;
    }
    function getUserGroupExcept($ids){
        $result = $this->db->query("SELECT * FROM aauth_groups WHERE id NOT IN($ids)")->result_array();
        return $result;
    }
}