<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class Permission extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MPermission");
        $this->theme_module = "user";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->mcore->checkPermission($this->user_group, 'permission_view', true);

        }


        $this->table = "aauth_perms";
        $this->dttModel = "MDPermission";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> Permission" => base_url()."user/permission"
        );

        $this->_theme_vars['page_title'] = 'User Management';
        $this->_theme_vars['page_subtitle'] = 'Permission';
        $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $this->_theme_vars['parent_menu'] = 'user_management';
        $this->_theme_vars['submenu'] = 'permission';
        $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group,'permission_add');
        $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'permission_update');
        $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group, 'permission_delete');
        
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('permission');


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");
            $insert = $this->aauth->create_perm($name, $definition);
            $last_id = $this->db->insert_id('aauth_perms_seq');
            $data = array(
                "module" => $this->input->post("module"),
            );
            $this->db->where($this->pk, $last_id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully add data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group,'permission_add', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> Permission" => base_url()."user/permission",
                "Add New" => base_url()."user/permission/addnew",
            );

            $data['page'] = 'permission_add';
            $this->_theme_vars['page_title'] = 'Permission';
            $this->_theme_vars['page_subtitle'] = 'Add Permission';
            $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $this->_theme_vars['parent_menu'] = 'user_management';
            $this->_theme_vars['submenu'] = 'permission';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('permission_add');

        }



    }

    function edit($id){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");
            $update = $this->aauth->update_perm($id, $name, $definition);
            $data = array(
                "module" => $this->input->post("module"),
            );
            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){
                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, 'permission_update', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> Permission" => base_url()."user/permission",
                "Edit" => base_url()."user/permission/edit/".$id,
            );

            $this->_theme_vars['page_title'] = 'Permission';
            $this->_theme_vars['page_subtitle'] = 'Edit Permission';
            $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $this->_theme_vars['dataedit'] = $this->MPermission->getPermissionById($id);
            $this->_theme_vars['parent_menu'] = 'user_management';
            $this->_theme_vars['submenu'] = 'permission';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('permission_edit');

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $delete = $this->aauth->delete_perm($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}