<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class User extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MUser");
        $this->load->model("MUserGroup");
        $this->theme_module = "user";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->mcore->checkPermission($this->user_group, 'user_view', true);

        }

        $this->table = "aauth_users";
        $this->dttModel = "MDUser";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> User App" => base_url()."user/user"
        );

     
        $this->_theme_vars['page_title'] = 'User Management';
        $this->_theme_vars['page_subtitle'] = 'User App';
        $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $this->_theme_vars['parent_menu'] = 'user_management';
        $this->_theme_vars['submenu'] = 'user';
        $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group,'user_add');
        $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'user_update');
        $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group,'user_delete');
        
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('user');


    }

    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){
        // map_y('FUCK');
        if(count($_POST)>0){

            $photo = "";

            /* upload cover */
            if(count($_FILES)>0){
                if($_FILES["photo"]["name"]!=""){

                    if($_FILES["photo"]["type"]=="image/png" or
                        $_FILES["photo"]["type"]=="image/jpg" or
                        $_FILES["photo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }

            $create_user = $this->aauth->create_user($this->input->post('email'),$this->input->post('password'),$this->input->post("username"));
            
            if($create_user){
                // map_y($_POST);
                $last_id = $this->db->insert_id('aauth_users_seq');
                $this->aauth->add_member($last_id, $this->input->post("user_group"));

                $data = array(
                    "first_name" => $this->input->post("first_name"),
                    "last_name" => $this->input->post("last_name"),
                    "picture" => $photo,
                    "status" => (integer) $this->input->post("status"),
                );

                if(!isset($photo) || $photo == ""){
                    unset($data['picture']);
                }

                $this->db->where($this->pk, $last_id);
                $update = $this->db->update($this->table, $data);

                if($update){
                    $res = array("status" => "1", "msg" => "Successfully Add User!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
                }
                return response_json($res, 200);
            }else{
                if(isset($_SESSION['errors_auth'])){
                    $html = '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Pemberitahuan!</h4> <ul></ul>';
                    
                    foreach($_SESSION['errors_auth'] as $error){
                        $html .= '<li>'.$error.'</li>';
                    }
                    
                    $html .= '</div>';

                    return response_json([
                        'status' => '2',
                        'msg' => $html,
                    ]);
                }
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Could not create user.");
                echo json_encode($res);
            }

        }else{

            $this->mcore->checkPermission($this->user_group, 'user_view', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User App" => base_url()."user/user",
                "Add New" => base_url()."user/user/addnew",
            );

            $this->_theme_vars['page'] = 'user_add';
            $this->_theme_vars['page_title'] = 'User App';
            $this->_theme_vars['page_subtitle'] = 'Add New User App';
            $this->_theme_vars['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css"
            );
            $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $this->_theme_vars['user_groups'] = $this->MUserGroup->getUserGroupExcept('16,19,18,22');
            $this->_theme_vars['parent_menu'] = 'user_management';
            $this->_theme_vars['submenu'] = 'user';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('user_add');

        }



    }

    function edit($id){

        if(count($_POST)>0){


            /* upload cover */

            if($_FILES["photo"]["name"]!=""){

                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    $old_photo = $upload_path.'/'.@$_POST['old_photo'];

                    if(file_exists($old_photo)){

                        @unlink($old_photo);

                    }


                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {

                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{


                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;

                }

            }else{

                $photo = $this->input->post("old_photo");

            }

            $data = array(
                "first_name" => $this->input->post("first_name"),
                "last_name" => $this->input->post("last_name"),
                "email" => $this->input->post("email"),
                "username" => $this->input->post("username"),
                "status" => $this->input->post("status"),
                "picture" => $photo,
            );

            $this->db->where($this->pk, $id);
            $update = $this->db->update($this->table, $data);

            if($update){

                if($this->input->post("old_user_group") != $this->input->post("user_group")){

                    if($this->input->post("old_user_group")!=""){
                        $this->aauth->remove_member($id, $this->input->post("old_user_group"));
                    }

                    $this->aauth->add_member($id, $this->input->post("user_group"));

                }

                $res = array("status" => "1", "msg" => "Successfully update data!");

            }else{

                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, 'user_update', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User App" => base_url()."user/user",
                "Edit" => base_url()."user/user/edit/".$id,
            );

           
             $this->_theme_vars['page_title'] = 'User App';
             $this->_theme_vars['page_subtitle'] = 'Edit User';
             $this->_theme_vars['custom_css'] = array(
                "employee" => base_url()."assets/modules/organization/assets/css/employee.css"
            );
             $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
             $this->_theme_vars['dataedit'] = $this->MUser->getUserById($id);
             $this->_theme_vars['user_groups'] = $this->MUserGroup->getUserGroup();
             $this->_theme_vars['parent_menu'] = 'user_management';
             $this->_theme_vars['submenu'] = 'user';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('user_edit');

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $user = $this->MUser->getUserById($id);

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';

            $photo = $upload_path.'/'.$user['picture'];

            if(file_exists($photo)){

                @unlink($photo);

            }

            $delete = $this->aauth->delete_user($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    function change_pass(){

        if(count($_POST)>0){

            $update = $this->aauth->update_user($this->input->post("id"), $this->input->post("email"), $this->input->post("new_password"), $this->input->post("username"));

            if($update){

                $res = array("status" => "1", "msg" => "Successfully update password!");

            }else{

                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");

            }

            echo json_encode($res);

        }

    }



}