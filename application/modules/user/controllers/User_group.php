<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 9:04
 */
class User_group extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MPermission");
        $this->load->model("MUserGroup");
        $this->theme_module = "user";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->mcore->checkPermission($this->user_group, 'user_group_view', true);

        }


        $this->table = "aauth_groups";
        $this->dttModel = "MDUserGroup";
        $this->pk = "id";

    }


    function index(){


        $breadcrumb = array(
            "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group"
        );

        $this->_theme_vars['page_title'] = 'User Management';
        $this->_theme_vars['page_subtitle'] = 'User Group';
        $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $this->_theme_vars['parent_menu'] = 'user_management';
        $this->_theme_vars['submenu'] = 'user_group';
        $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group, 'user_group_add');
        $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'user_group_update');
        $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group, 'user_group_delete');
        
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('user_group');


    }
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }
    function addnew(){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $insert = $this->aauth->create_group($name, $definition);
            
            if($insert){

                $keys = array_keys($_POST);
                $a = count($_POST);

                for($i=0;$i < $a;$i++){

                    $key = $keys[$i];
                    $ex = explode("_",$key);

                    if(count($ex)>1){
                        $perm_id = $ex[1];
                        $perm = $this->MPermission->getPermissionById($perm_id);
                        $this->aauth->allow_group($name,$perm['name']);
                    }


                }

                $res = array("status" => "1", "msg" => "Successfully add data!");

            }else{
                $msg = isset($_SESSION['errors_group']) ? $_SESSION['errors_group'] : '';
                $res = array("status" => "0", "msg" => $msg);
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, 'user_group_add', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group",
                "Add New" => base_url()."user/user_group/addnew",
            );

            
            $this->_theme_vars['page_title'] = 'User Group';
            $this->_theme_vars['page_subtitle'] = 'Add New User Group';
            $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $this->_theme_vars['perms'] = $this->MPermission->getPermissionModule();
            $this->_theme_vars['parent_menu'] = 'user_management';
            $this->_theme_vars['submenu'] = 'user_group';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('user_group_add');

        }



    }

    function edit($id){

        if(count($_POST)>0){

            $name = $this->input->post("name");
            $definition = $this->input->post("definition");

            $update = $this->aauth->update_group($id, $name, $definition);

            $perm_id = null;

            if($update){

                $keys = array_keys($_POST);
                $a = count($_POST);
                $perm_newdata = array();

                for($i=0;$i < $a;$i++){

                    $key = $keys[$i];
                    $ex = explode("_",$key);

                    if(count($ex)>1){

                        $perm_id = $ex[1];
                        $perm = $this->MPermission->getPermissionById($perm_id);

                        $this->aauth->allow_group($name,$perm['name']);

                        array_push($perm_newdata, $perm_id);


                    }


                }

                $permissions = $this->MPermission->getPermissionByGroupId($id);
                $perm_exist = array();
                foreach ($permissions as $prm){

                    array_push($perm_exist,$prm['id']);

                }


                $delete_perms = array();

                for($k=0;$k < count($perm_exist);$k++){

                    if(!in_array($perm_exist[$k], $perm_newdata)){
                        array_push($delete_perms, $perm_exist[$k]);
                    }

                }


                if(count($delete_perms)>0){

                    $c = count($delete_perms);

                    for($i=0;$i<$c;$i++){

                        $perm_id = $delete_perms[$i];
                        $p = $this->MPermission->getPermissionById($perm_id);
                        $this->aauth->deny_group($name,$p['name']);

                    }

                }

                $res = array("status" => "1", "msg" => "Successfully update data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);

        }else{

            $this->mcore->checkPermission($this->user_group, 'user_group_update', true);

            $breadcrumb = array(
                "<i class='fa fa-group'></i> User Group" => base_url()."user/user_group",
                "Edit" => base_url()."user/user_group/edit/".$id,
            );

            $data['page'] = 'user_group_edit';
            $this->_theme_vars['page_title'] = 'User Group';
            $this->_theme_vars['page_subtitle'] = 'Edit User Group';
            $this->_theme_vars['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $this->_theme_vars['dataedit'] = $this->MUserGroup->getUserGroupById($id);
            $this->_theme_vars['perms'] = $this->MPermission->getPermissionModule();
            $this->_theme_vars['parent_menu'] = 'user_management';
            $this->_theme_vars['submenu'] = 'user_group';
            
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('user_group_edit');

        }

    }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $delete = $this->aauth->delete_group($id);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }



}