<?php

class Kabid extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mkabid");
        $this->load->model("Master/Mkantor");
        $this->theme_module = "kabid";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "kabid";
        $this->dttModel = "Mdkabid";
        $this->pk = "id";

    }
    
    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Daftar Kepala Bidang";
        $data['page_subtitle'] = "Modul Kepala Bidang";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Kepala Bidang";
            $data['page_subtitle'] = "Modul Kepala Bidang";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    private function _get_active_menu(){

        return array(
            'parent_menu' => 'master', 
            'submenu' => 'kabid.kabid' 
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "kabid_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "kabid_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kabid_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "kabid_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."kabid/kabid/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            // "label" => array(

            //     "data" => "b.label",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),

            "nm_kabid" => array(

                "data" => "b.nama_kabid",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Nomor Telepon" => array(

                "data" => "b.no_telp",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "nm_kantor" => array(

                "data" => "c.nm_kantor",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Alamat" => array(

                "data" => "b.alamat",
                "searchable" => true,
                "orderable" => true,
              

            ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        $form = array(
            "nama_kabid" => array(
                "label" => "Nama Kepala Bidang",
                "type" => "text",
                "placeholder" => "Nama Kepala Bidang",
                "class" => "form-control validate[required]",
            ),

            "no_telp" => array(
                "label" => "Nomor Telepon",
                "type" => "text",
                "placeholder" => "Nomor Telepon",
                "class" => "form-control validate[required]",
            ),

            "id_kantor" => array(
                "label" => "Kantor",
                "type" => "sourcequery",
                "source" => $this->Mkantor->getKantor(),
                "keydt" => "id",
                "valuedt" => "nm_kantor",
                "class" => "form-control select2 validate[required]",
            ),

            "alamat" => array(

                "label" => "Alamat",
                "type" => "textarea",
                "placeholder" => "Alamat",
                "class" => "form-control validate[required]"

            ),
            
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]"

            ),

            "username" => array(

                "label" => "Username",
                "type" => "text",
                "placeholder" => "Username",
                "class" => "form-control validate[required]"

            ),

        );

        if($id){
            
          
            $user = get_user_by_kabid($id);

            $form["email" ] = array(
                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",
                "value" => $user->email
            );

            $form["username" ] = array(
                "label" => "Username",
                "type" => "text",
                "placeholder" => "Username",
                "class" => "form-control validate[required]",
                "value" => $user->username
            );

            
        }
        return $form;

// 
    }

    public function add(){

        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;

            $email = $_POST['email'];
            $username = $_POST['username'];
            $user_group = 'Kepala Bidang';
            $pass = "rahasia";

            /**
             * Create User
             */
            $user_id = $this->aauth->create_user($email, $pass, $username, $user_group);
            
            if($user_id){
                set_user_status($user_id, 1);

                $data = [
                    'nama_kabid' => $_POST['nama_kabid'],
                    'no_telp' => $_POST['no_telp'],
                    'alamat' => $_POST['alamat'],
                    'id_kantor' => $_POST['id_kantor'],
                    'user_id' => $user_id,
                ];
    
                $this->db->insert('kabid', $data); 
    
                $last_id = $this->db->insert_id('kabid_id_seq');
    
                /**
                 * Log
                 */
                insert_log('Tambah Kepala Bidang dari dashboard atas nama ' . $_POST['nama_kabid']);
                if($last_id){
                    $res = array("status" => "1", "msg" => "Data successfully inserted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }else{
                $msg = $_SESSION['errors_auth'];
                $html = '';
                foreach($msg as $_dt){
                    $html .= $_dt.', ';
                }
                $res = array("status" => "0", "msg" => $html);
                unset($_SESSION['errors_auth']);
                
                return response_json($res, 200);
            }
           

        }else{
            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Kepala Bidang";
            $data['page_subtitle'] = "Modul Kepala Bidang";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();
        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;
            
            $email = $_POST['email'];
            $username = $_POST['username'];

            $user_kabid = get_user_by_kabid($id);
            /**
             * Update User
             */
            // map_y($user_kabid);
            $user_id = kabid_user_id($id);
            // map_y($user_kabid);

            $update_user = $this->aauth->update_user($user_id, $email, $pass = FALSE, $username);
            // map_y($update_user ? 'true': 'false');
            if($update_user){
            
                $data = [
                    'nama_kabid' => $_POST['nama_kabid'],
                    'no_telp' => $_POST['no_telp'],
                    'alamat' => $_POST['alamat'],
                    'id_kantor' => $_POST['id_kantor'],
                ];

                $where = [
                    'id' => $id,
                ];
                
                $this->db->where($where);
                $this->db->update('kabid',$data);

                /**
                 * Log
                 */
                insert_log('Edit Kepala Bidang dari dashboard atas nama ' . $_POST['nama_kabid']);

                if($id){
    
                    $res = array("status" => "1", "msg" => "Data successfully inserted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }else{
                // map_y($_SESSION);
                $msg = $_SESSION['errors_auth'];
                
                $html = '';
                foreach($msg as $_dt){
                    $html .= $_dt.', ';
                }
                $res = array("status" => "0", "msg" => $html);

                unset($_SESSION['errors_auth']);
                
                return response_json($res, 200);
                
            }
            

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Kepala Bidang";
            $data['page_subtitle'] = "Modul Kepala Bidang";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){
            
            $id = $_POST['id'];
            
            $detail_kabid = $this->Mkabid->getkabidById($id);
            $user_id = $detail_kabid['user_id'];

            $delete_user = $this->db->query("DELETE FROM aauth_users WHERE id = '$user_id' ");
            $delete_kabid = $this->db->query("DELETE FROM kabid WHERE id = '$id' ");
            if($delete_kabid){
                if($id){
                    $res = array("status" => "1", "msg" => "Data successfully Deleted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }
            // $data['params']['action'] = "delete";
            // $data['params']['table'] = $this->table;
            // $data['params']['pk'] = $this->pk;
            // $data['params']['id'] = $_POST['id'];

            // $this->load->library("Cinta",$data);
            // $this->cinta->process();

        }

    }

}