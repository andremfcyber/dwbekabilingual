<?php
class Mkabupaten extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        
        $this->dbase = $this->load->database('default', TRUE);
        $this->table = '_tbl_kabkot';
    }
    
    /**
     * For select2
     */
    function list_jateng_only(){
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        $keyword = isset($request->term) ? $request->term : '';
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $columns = [
            'id as id',
            'kabupaten_kota as text',
        ];

        $conditions = [
            "WHERE provinsi_id = 13",
            "AND (kabupaten_kota::text LIKE '%".$keyword."%')"
        ];
        
        $query = "SELECT ".implode(', ', $columns)." FROM ".$this->table." ".implode(' ', $conditions)." ";

        /*Total Count */
        $count = count($this->dbase->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= 'OFFSET '.$offset;

        $results = $this->dbase->query($query)->result_array();
        /* EndCount */
        $endCount = $offset + $resultCount;
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        return $results;
    }

    /**
     * Show
     */
    function show($id){
        $get = $this->dbase->query("SELECT * FROM ".$this->table." WHERE id = '$id' LIMIT '1'")->row();
        return @$get;
    }

    /**
     * Create
     */
    function create($data){
        $this->dbase->insert($this->table, $data);
        return true;
    }

    /**
     * Update
     */
    function update($id, $data){
        $this->dbase->where(['id' => $id]);
        $this->dbase->update($this->table, $data);
        return true;
    }

    /**
     * Delete
     */
    function delete($id){
        $this->dbase->where(['id' => $id]);
        $this->dbase->delete($this->table);
        return true;
    }
}