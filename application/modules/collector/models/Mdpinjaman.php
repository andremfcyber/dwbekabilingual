<?php

class Mdpinjaman extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'pinjaman_collector_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'pinjaman_collector_delete');
        
    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',a.id_pinjaman,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',a.id_pinjaman,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        $detail = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail Pinjaman" href="'.base_url('collector/pinjaman/detail/').'\',a.id_pinjaman,\'"><i class="fa fa-eye"></i></a>&nbsp;';

        if($detail!='' || $delete!=''){
            $op = "concat('".$detail.$delete."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return 'emaus."Pinjaman" a';
    }

    public function joinArray(){
        return array(
            'emaus."Nasabah" b |left' => "b.no_rek = a.no_rek"
        );
    }

    public function whereClauseArray(){
        return null;
    }


}