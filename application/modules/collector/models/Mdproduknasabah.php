<?php

class Mdproduknasabah extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->load->library("session");
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'pinjaman_collector_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'pinjaman_collector_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'pinjaman_collector_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }


        $detail = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="List Angsuran" href="'.base_url('collector/detailpinjaman/show?idpinjaman=').'\',b.id,\'"><i class="fa fa-list-alt"></i></a>&nbsp;';

        if($edit!='' || $detail!='' || $delete!=''){
            $op = "concat('".$edit.$detail.$delete."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "c_pinjaman b";
    }

    public function joinArray(){
        // return null;
        return array(
            "product c |left" => "b.id_product = c.id",
            "product_type d |left" => "d.id = c.product_type_id",
            "master_status sta | left" => "b.status = sta.id",  
            "c_nasabah n | left" => "b.id_nasabah = n.id",  
        );
    }

    public function whereClauseArray(){
        
        return [

            'b.id_nasabah' => $_SESSION['idnasabah'],            
        ];

    }

    public function orderBy(){
        return [
            'b.created_at' => 'DESC',
        ];
    }


}