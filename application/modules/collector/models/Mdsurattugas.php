<?php

class Mdsurattugas extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_upload = $this->mcore->checkPermission($this->user_group, 'surat_tugas_upload');

    }
    
    public function appendToSelectStr() {
     
        $upload = '';
        $str = '';
        $url_l = base_url('collector/surattugas');

        if($this->allow_upload){
            $upload = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Upload Surat Tugas" href="javascript:on_upload_surat(\',b.id,\', `'.$url_l.'`);"><i class="fa fa-map"></i></a>';
        }

        if($upload!=''){
            $op = "concat('".$upload."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "c_inventory_collect b";
    }

    public function joinArray(){
        return array(
            "c_collector c |left" => "b.id_collector = c.id",
            "c_pinjaman d |left" => "b.id_pinjaman = d.id",
            "c_nasabah e | left" => "d.id_nasabah = e.id",
            "product f |left" => "d.id_product = f.id"  
        );
    }

    public function whereClauseArray(){
        return null;
    }


}