<?php

class Mpenagihan extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getTagihan(){
        $result = $this->db->query("SELECT * FROM v_c_tagihan")->result_array();
        return @$result;
    }

    function getTagihanByIdAngsuran($id){

        $result = $this->db->query("SELECT * FROM v_c_tagihan WHERE id_angsuran='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM v_c_tagihan")->result_array();
        return $result;

    }

}