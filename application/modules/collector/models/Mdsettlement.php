<?php

class Mdsettlement extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'kantor_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'kantor_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'kantor_detail');

    }
    public function appendToSelectStr() {
        $str = '';
        $detail = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail Settlement" href="'.base_url('collector/settlement/detail?id=').'\',b.ao,\'"><i class="fa fa-eye"></i></a>&nbsp;';

        $op = "concat('".$detail."')";
        $str = array(
            "op" => $op
        );
        
        return $str;
    }
    public function fromTableStr() {
        return "emaus.v_settlement b";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}