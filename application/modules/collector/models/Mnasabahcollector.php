<?php

class Mnasabahcollector extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getNasabahcollector(){
        $result = $this->db->query("SELECT * FROM c_nasabah")->result_array();
        return @$result;
    }

    function getNasabahcollectorById($id){

        $result = $this->db->query("SELECT * FROM c_nasabah WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM c_nasabah")->result_array();
        return $result;

    }

    function nasabah_by_id($id){
        $result = $this->db->query("SELECT * FROM c_nasabah n  WHERE n.id = $id")->result_array();
        return @$result[0];
    }

    function get_id_produk($produk){
        $result = $this->db->query("SELECT id FROM product  WHERE LOWER(name)=LOWER('".$produk."')")->result_array();
        
        return @$result[0];
    }
    function get_id_collector($collector){
        $result = $this->db->query("SELECT id FROM collector  WHERE LOWER(nama_collector)=LOWER('".$collector."')")->result_array();
        
        return @$result[0];
    }

}