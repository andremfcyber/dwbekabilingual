<?php

class Penagihan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->library('excel');
        $this->load->model("Mpenagihan");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "v_c_tagihan";
        $this->dttModel = "Mdpenagihan";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Penagihan";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.penagihan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "kantor_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "penagihan_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kantor_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "penagihan_send"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                     
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "email" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "product" => array(

                "data" => "b.nama_produk",
                "searchable" => true,
                "orderable" => true,
            
            ),
            
            "keterlambatan" => array(

                "data" => "b.telat",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        return null;

// 
    }

    public function  form_penagihan($id){
        $data['url_c_penagihan'] = base_url('collector/penagihan/send_email_penagihan/'.$id);
        $data['pesan'] = "Apakah anda ingin mengirimkan tagihan lewat email?";
        $this->load->view('v_c_penagihan', $data);
      
    }

    public function send_email_penagihan($id){
    
        $nasabah = $this->Mpenagihan->getTagihanByIdAngsuran($id);
        // map_y($nasabah);
        $tgl = $nasabah['tgl_penagihan']; 
        $hari = date_format(new DateTime($tgl),"D");
        $c['total_sisa_tagihan'] = format_rupiah($nasabah['total_sisa_tagihan']); 
        $c['tgl_penagihan'] = tgl_indo($nasabah['tgl_penagihan']);
        $c['hari_penagihan'] = hari_indo($hari);
      
        $c['nasabah'] = $nasabah;
        $data['subtitle'] = "SURAT PENAGIHAN";
        $data['content'] = $this->load->view('v_narasi_penagihan_emouse', $c, TRUE);;
        // map_y($data);

        $html_message = $this->load->view('email_template', $data, TRUE);
        $email = $nasabah['email'];

        $send_email = send_email($email, $html_message );

        $res =  [
            'status' => '1',
            'msg' => "Email tagihan telah dikirim"
        ];
        return response_json($res, 200);
    }
    
    public function  form_penagihan_wa($id){
        $data['url_c_penagihan'] = base_url('collector/penagihan/send_wa_penagihan/'.$id);
        $data['pesan'] = "Apakah anda ingin mengirimkan tagihan lewat Whatsapp?";
        $this->load->view('v_c_penagihan', $data);
    }


    public function send_wa_penagihan($id){
    
        $nasabah = $this->Mpenagihan->getTagihanByIdAngsuran($id);
        // map_y($nasabah);
        $id_nasabah  = $nasabah['id'];
        $dt = $this->db->query("select no_tlp from c_nasabah where id = ". $id_nasabah)->row_array();
        $tgl = $nasabah['tgl_penagihan']; 
        $hari = date_format(new DateTime($tgl),"D");
        $c['total_sisa_tagihan'] = format_rupiah($nasabah['total_sisa_tagihan']); 
        $c['tgl_penagihan'] = tgl_indo($nasabah['tgl_penagihan']);
        $c['hari_penagihan'] = hari_indo($hari);
        $html_message = "Halo
Kami dari bank BPR Nusamba Cepiring hendak memberitahukan bahwa tagihan anda sudah melewati waktu tenggang, mohon segera selesaikan pembayaran anda dengan rincian sbb        

Nama : ".$nasabah['nama']."
Jatuh Tempo : ".$nasabah['tgl_penagihan']."
Tagihan :".format_rupiah($nasabah['tagihan_bulanan'])." 

Terimakasih";
        $send_email = send_wa($dt['no_tlp'], $html_message);

        $res =  [
            'status' => '1',
            'msg' => "Tagihan telah dikirim Whatsapp"
        ];
        return response_json($res, 200);
    }

}