<?php

class Produknasabah extends Admin_Controller{

    function __construct()
    {
        parent::__construct();

        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("product/Mproduct");
        $this->theme_module = "dashboard";
       
        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_pinjaman";
        $this->dttModel = "Mdproduknasabah";
        $this->pk = "id";
      

    }

    function index(){
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Produk Nasabah Collector";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
        // map_y($_SESSION);

    }

    function show(){ 

        unset($_SESSION['idnasabah']);
        $_SESSION['idnasabah'] = $_GET['idnasabah'];
        
        $this->index();
    
    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.nasabahcollector' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."collector/produknasabah/add?idnasabah=".$this->uri->segment(4)."';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama" => array(

                "data" => "n.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "name" => array(

                "data" => "c.name",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tenor" => array(

                "data" => "b.tenor",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tagihan_bulanan" => array(

                "data" => "b.tagihan_bulanan",
                "searchable" => true,
                "orderable" => true,
            
            ),
        
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){


        return array(

            "id_product" => array(

                "label" => "Product",
                "type" => "sourcequery",
                "source" => $this->Mproduct->getProductByType(5),
                "keydt" => "id",
                "valuedt" => "name",
                "class" => "form-control select2 validate[required]",
               
            ),

            "tenor" => array (
                "label" => "Lama Angsuran(Bulan)",
                "type" => "text",
                "placeholder" => "Lama Angsuran Perbulan",
                "class" => "form-control",
            ),
            
            "tgl_penagihan" => array (
                "label" => "Tanggal Penagihan(pertama)",
                "type" => "datepicker",
                "placeholder" => "Tanggal Penagihan(pertama)",
                "class" => "form-control",
            ),

            "tagihan_bulanan" => array (
                "label" => "Nominal Tagihan /Bulan",
                "type" => "text",
                "placeholder" => "Nominal Tagihan /Bulan",
                "class" => "form-control",
            ),



        );

// 
    }

    public function add(){
      
        if(count($_POST)>0){
            unset($_POST['tmp_name']);
            
            if(count($_FILES)>0){
                if($_FILES["surat_tugas"]["name"]!=""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/surat_tugas/';
                    $array = explode('.', $_FILES['surat_tugas']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["surat_tugas"]["tmp_name"], $upload_path."/".$photo)) {
                        $_POST['surat_tugas'] = $photo;  
                    }else{
                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;
                    }
                }
            }
             // map_y($_SESSION);
            $_POST['id_nasabah'] = $_SESSION['idnasabah'];
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            // $data['params']['log'] = 'Tambah pinjaman collector dari dashboard atas nama ';

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            $id_last = $this->db->insert_id('c_pinjaman_id_seq');
           
            // loop detail pinjaman
                $date = $_POST['tgl_penagihan'];
                $_diff = $_POST['tenor']; // jumlah bulan yang dibayarkan
                $dateFormat = 'Y-m-j';

                for($i=0;$i<$_diff;$i++){ // pengulangan sampai batas cicilan
                    $angsuran_ke = 1 + $i;
                    $status = 0;
                    $dDiff = $_POST['tgl_penagihan'];

                    if ($i > 0){
                        $dDiff = substractMonth($date, +1, $dateFormat);
                        $date = date ($dateFormat , strtotime ( $dDiff) );
                    }

                    $data = array(
                        'id_pinjaman' => $id_last,
                        'tgl_penagihan' => $dDiff ,
                        'bayar_bulan' => $_POST['tagihan_bulanan'],
                        'angsuran_ke' => $angsuran_ke,
                        'status' => $status,                        
                    );
                    $this->db->insert('c_angsuran',$data);
                }

            
            if($insert){

                $res = array("status" => "1", "msg" => "Data berhasil Ditambah!");

            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{
            
            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Pinjaman";
            $data['page_subtitle'] = "Modul Collector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            unset($_POST['tmp_name']);
            
            if(count($_FILES)>0){
                if($_FILES["surat_tugas"]["name"]!=""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/surat_tugas/';
                    $array = explode('.', $_FILES['surat_tugas']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["surat_tugas"]["tmp_name"], $upload_path."/".$photo)) {
                        $_POST['surat_tugas'] = $photo;  
                    }else{
                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;
                    }

                }
            }
            
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Pinjaman porduk id ' . $_POST['id_product'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit pinjaman";
            $data['page_subtitle'] = "Modul Coolector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}