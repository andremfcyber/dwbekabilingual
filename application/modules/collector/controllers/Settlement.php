<?php

class Settlement extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Msettlement");
        $this->theme_module = "";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "emause.v_settlement";
        $this->dttModel = "Mdsettlement";
        $this->pk = "ao";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Tracking Collector";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.settlement' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => null,
            "read_perm" => $this->mcore->checkPermission($this->user_group, "settlement_view"),
            "edit_perm" => null,
            "delete_perm" => null,
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama_collector" => array(

                "data" => "b.nama_sales",
                "searchable" => false,
                "orderable" => true,
            
            ),
            "jml_bayar" => array(

                "data" => "b.tot_bayar",
                "searchable" => false,
                "orderable" => true,
            
            ),
            "aksi" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        return null;

// 
    }

    public function  form_reminders($id){
        $data['url_c_reminders'] = base_url('collector/reminders/send_email_reminders/'.$id);
        $data['pesan'] = "Apakah anda ingin mengirimkan tagihan lewat email?";
        $this->load->view('v_c_reminders', $data);
      
    }

    public function send_email_reminders($id){
    
        $_data = $this->Mreminders->getTagihanByIdAngsuran($id);
        $message = 'Kepada Babak/Ibu yang terhormat,';
        
        $data['content'] = '<p>'.$message.'</p>';
        $data['content'] .= "<br><p>Sedang test software Maaf</p> <p style='text-align: center;'></p>";  
        $html_message = $this->load->view('email_template', $data, TRUE);
        $email = $_data['email'];

        $send_email = send_email($email, $html_message );

        $res =  [
            'status' => '1',
            'msg' => "Email tagihan telah dikirim"
        ];
        return response_json($res, 200);
    }

    public function detail(){

        if($this->aauth->is_loggedin()){
                
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $id = isset($_GET['id']) ? $_GET['id'] : '';
            $this->_theme_vars['page_title'] = 'Sattlement';
            $this->_theme_vars['page_subtitle'] = 'Detail Sattlement';
            $this->_theme_vars['parent_menu'] = '';
            $this->_theme_vars['submenu'] = '';

            $data['settlement'] = $this->Msettlement->getSettlementbyAO($id);
            $data['list_nasabah'] = $this->Msettlement->getDetailSettlementbyAO($id);

            $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
            $this->render_admin_view('view/collector/detail_settlement','',$data);
       
        }else{
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"login");
            $this->_theme_vars['current_page'] = "login";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_admin_view('login');
        }
        
    }

    function reaset_settlement($id){
        // $penagihan = $this->Msettlement->getDetailSettlementbyAO($id);
        // foreach($penagihan as $dt){
        //     $data=array('status_settlement' => 'true');
        //     $this->db->update('emaus.penagihan', $data);
        // }
        
        $res =  [
            'status' => '1',
            'msg' => "Settlement Berhasil Di Setorkan"
        ];
        return response_json($res, 200);
    }
}