<?php

class Pinjaman extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->library('excel');
        $this->load->model("Mpinjaman");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = 'emaus."Pinjaman"';
        $this->dttModel = "Mdpinjaman";
        $this->pk = "id_pinjaman";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Pinjaman Nasabah";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.pinjaman' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(
            "add_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "pinjaman_collector_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            array(

                "text" => '<i class="fa fa-file-excel-o "></i> Download Template',
                "action" => "function ( e, dt, node, config ) { window.location.href = base_url + '/upload/templates/pinjaman.xlsx'}"

            ),
            array(

                "text" => '<i class="fa fa-file-excel-o "></i> Import Data',
                "action" => "function ( e, dt, node, config ) { $('#excel').trigger('click');}"
            ), 
            array(

                "text" => '<i class="fa fa-file-excel-o "></i> Export Data',
                "action" => "function ( e, dt, node, config ) { window.location.href = base_url + 'collector/pinjaman/export'}"

            ),
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            
            'colvis'

        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "norek" => array(

                "data" => "a.no_rek",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "jangka_waktu" => array(

                "data" => "a.jangka_waktu",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tgl_mulai" => array(

                "data" => "a.tgl_mulai",
                "searchable" => true,
                "orderable" => true,
            
            ),
            
            "tgl_jatuh_tempo" => array(

                "data" => "a.tgl_jatuh_tempo",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,

            )

        );

    }
    
    public function detail($id){
        $this->theme_module = '';
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Detail Pinjaman';
        $this->_theme_vars['page_subtitle'] = 'Collector';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = 'Emause';
        $this->_theme_vars['submenu'] = 'collector.pinjaman';
        
        $data['detail'] = $this->Mpinjaman->getPinjamanById($id);
        $this->render_admin_view('view/collector/pinjaman','', $data);
    }

    private function _get_fields_edit($id = ''){

        return null;

    }

    function import(){

        if($_FILES["excel"]["name"]!=""){

            if($_FILES["excel"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
                $_FILES["excel"]["type"]=="application/vnd.ms-excel" or
                $_FILES["excel"]["type"]=="application/x-msexcel" or
                $_FILES["excel"]["type"]=="application/x-ms-excel" or
                $_FILES["excel"]["type"]=="application/x-excel" or
                $_FILES["excel"]["type"]=="application/x-dos_ms_excel" or
                $_FILES["excel"]["type"]=="application/xls" or
                $_FILES["excel"]["type"]=="application/wps-office.xlsx" or
                $_FILES["excel"]["type"]=="application/x-xls" or
                $_FILES["excel"]["type"]=="application/msexcel"){

                // $upload_path = $_SERVER['DOCUMENT_ROOT'].'/cepiring/upload/excel';
                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/excel';
                $array = explode('.', $_FILES['excel']['name']);
                $extension = end($array);
                $file = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["excel"]["tmp_name"], $upload_path."/".$file)) {

                    $import = $this->import_from_excel($upload_path."/".$file); 

                    if($import){

                        @unlink($upload_path."/".$file);

                    }

                    $res = array("status" => "1", "msg" => "Data imported Successfully");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Something went wrong while uploading file");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid file extention. Must be .xls or .xlsx");

            }

            echo json_encode($res);

        }

    }

    function import_from_excel($file){

        $objReader= PHPExcel_IOFactory::createReader('Excel2007');

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 

        try{
            $this->db->trans_begin();

            $data_pinjaman_old = $this->Mpinjaman->getPinjaman();

            if($data_pinjaman_old){

                foreach($data_pinjaman_old as $data){
                    
                    $data_log = array(
                        'id_pinjaman'               => $data['id_pinjaman'],
                        'no_rek'                    => $data['no_rek'],
                        'plafond'                   => $data['plafond'],
                        'bakidebet'                 => $data['bakidebet'],
                        'angsuran_pokok'            => $data['angsuran_pokok'],
                        'angsuran_bunga'            => $data['angsuran_bunga'],
                        'tgk_bulan_pokok'           => $data['tgk_bulan_pokok'],
                        'tunggakan_pokok'           => $data['tunggakan_pokok'],
                        'tgk_bulan_bunga'           => $data['tgk_bulan_bunga'],
                        'tunggakan_bunga'           => $data['tunggakan_bunga'],
                        'jangka_waktu'              => $data['jangka_waktu'],
                        'koll_lama'                 => $data['koll_lama'],
                        'tgl_jatuh_tempo'           => $data['tgl_jatuh_tempo'],
                        'ao'                        => $data['ao'],
                        'koll_baru'                 => $data['koll_baru'],
                        'kantor'                    => $data['kantor'],
                        'min_bayar_pokok'           => $data['min_bayar_pokok'],
                        'min_bayar_bunga'           => $data['min_bayar_bunga'],
                        'total_bayar_perbaikan_koll'=> $data['total_bayar_perbaikan_koll'],
                        'tgl_mulai'                 => $data['tgl_mulai'],
                        'total_angsuran'            => $data['total_angsuran'],
                        'total_tunggakan'           => $data['total_tunggakan'],
                        'branch'                    => $data['branch'],
                        
                    );

                    $this->db->insert('emaus."pinjaman_log"',$data_log);
                    $last_id_pinjaman_log =$this->db->insert_id('pinjaman_log_id_seq');

                    $this->db->delete('emaus."Pinjaman"', array('id_pinjaman' => $data['id_pinjaman']));
                }
            }   

            $i=2;
            for($i=2; $i <= $totalrows ; $i++){

                $no_rek                     = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                $nama                       = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                $alamat                     = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                $no_hp                      = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                $plafond                    = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                $bakidebet                  = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
                $angsuran_pokok             = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                $angsuran_bunga             = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
                $tgk_bulan_pokok            = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
                $tunggakan_pokok            = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
                $tgk_bulan_bunga            = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
                $tunggakan_bunga            = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
                $jangka_waktu               = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
                $koll_lama                  = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
                $tgl_jatuh_tempo            = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
                $ao                         = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
                $koll_baru                  = $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
                $kantor                     = $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
                $min_bayar_pokok            = $objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
                $min_bayar_bunga            = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
                $total_bayar_perbaikan_koll = $objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
                $tgl_mulai                  = $objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
                $total_angsuran             = $objWorksheet->getCellByColumnAndRow(22,$i)->getCalculatedValue();
                $total_tunggakan            = $objWorksheet->getCellByColumnAndRow(23,$i)->getCalculatedValue();
                $branch                     = $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();

                $no_rek = $no_rek."";
                
                $data_nasabah = array(

                    'no_rek'=> $no_rek,
                    'nama'  => $nama,
                    'alamat'=> $alamat,
                    'no_hp' => $no_hp

                );

                $ceknasabah = $this->Mpinjaman->getNasabahByNorek($no_rek);
                if($ceknasabah){
                    $this->db->where('no_rek', $no_rek);
                    $update = $this->db->update('emaus."Nasabah"', $data_nasabah);
                }
                else{
                    $this->db->insert('emaus."Nasabah"',$data_nasabah);
                }

                $data_pinjaman = array(
                    
                    'no_rek'                    => $no_rek,
                    'plafond'                   => $plafond,
                    'bakidebet'                 => $bakidebet,
                    'angsuran_pokok'            => $angsuran_pokok,
                    'angsuran_bunga'            => $angsuran_bunga,
                    'tgk_bulan_pokok'           => $tgk_bulan_pokok,
                    'tunggakan_pokok'           => $tunggakan_pokok,
                    'tgk_bulan_bunga'           => $tgk_bulan_bunga,
                    'tunggakan_bunga'           => $tunggakan_bunga,
                    'jangka_waktu'              => $jangka_waktu,
                    'koll_lama'                 => $koll_lama,
                    'tgl_jatuh_tempo'           => $tgl_jatuh_tempo,
                    'ao'                        => $ao,
                    'koll_baru'                 => $koll_baru,
                    'kantor'                    => $kantor,
                    'min_bayar_pokok'           => $min_bayar_pokok,
                    'min_bayar_bunga'           => $min_bayar_bunga,
                    'total_bayar_perbaikan_koll'=> $total_bayar_perbaikan_koll,
                    'tgl_mulai'                 => $tgl_mulai,
                    'total_angsuran'            => $total_angsuran,
                    'total_tunggakan'           => $total_tunggakan,
                    'branch'                    => $branch,
                    
                );
                
                $this->db->insert('emaus."Pinjaman"',$data_pinjaman);
                $last_id_pinjaman =$this->db->insert_id('pinjaman_id_seq');
            }   

            if($this->db->trans_status()== FALSE){
                
                $this->db->trans_rollback();
                return false;
            }

            $this->db->trans_commit();
            return true;

        } catch (Exception $e) {
            
        }
    }

    function export(){

        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array(
            "NO REKENING", "NAMA", "ALAMAT", "NO HP", "PLAFOND", "BAKIDEBET", "ANGSURAN POKOK", "ANGSURAN BUNGA",
            "TGK BULAN POKOK", "TUNGGAKAN POKOK", "TGK BULAN BUNGA", "TUNGGAKAN BUNGA", "JANGKA WAKTU", "KOLL LAMA",
            "TGL JT TEMPO", "AO", "KOLL BARU", "KANTOR", "MIN BYR POKOK", "MIN BYR BUNGA", 
            "TOTAL BAYAR PERBAIKAN KOLL", "TGL MULAI", "TOTAL ANGSURAN", "TOTAL TUNGGAKAN", "BRANCH"
        );
        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $data = $this->Mpinjaman->getPinjaman();

        $excel_row = 2;

        foreach($data as $row){
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['no_rek']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, strtoupper($row['nama']));
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, strtoupper($row['alamat']));
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['no_hp']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['plafond']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['bakidebet']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['angsuran_pokok']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['angsuran_bunga']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['tgk_bulan_pokok']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['tunggakan_pokok']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['tgk_bulan_bunga']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['tunggakan_bunga']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row['jangka_waktu']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row['koll_lama']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, date("d/m/Y", strtotime($row['tgl_jatuh_tempo'])));
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row['ao']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row['koll_baru']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row['kantor']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row['min_bayar_pokok']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row['min_bayar_bunga']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row['total_bayar_perbaikan_koll']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, date("d/m/Y", strtotime($row['tgl_mulai'])));
            $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row['total_angsuran']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row['total_tunggakan']);
            $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row['branch']);

            $excel_row++;
        }

        foreach (range(0, 24) as $col) {
            $object->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
        }

        $highest = $object->setActiveSheetIndex(0)->getHighestRow();

        $styleArray = array(
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )
          );
             
          $object->getActiveSheet()->getStyle('A1:Y'.$highest)->applyFromArray($styleArray);
          $object->getActiveSheet()->getStyle('A1:Y1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'DDDDDD')
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            )
        )->getFont()->setBold(true);
        
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data Pinjaman Nasabah.xlsx"');
        $object_writer->save('php://output');
    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}