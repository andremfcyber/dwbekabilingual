<?php

class Nasabah2 extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mnasabah2");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "nasabah";
        $this->dttModel = "Mdnasabah2";
        $this->dttModel_dukcapil = "Mddukcapil";
        $this->pk = "id";

    }

    function index(){
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Nasabah";
        $data['page_subtitle'] = "Modul Nasabah";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    // function show($id = null){
    //     if($id != null){

    //         $_SESSION['id_product'] = $id;
        
    //     $data['theme'] = $this->_theme_vars['active_admin_theme'];
    //     $data['page_title'] = "List Nasabah";
    //     $data['page_subtitle'] = "Modul Nasabah";
    //     $data['current_class_dir'] = $this->router->fetch_directory();
    //     $data['current_class'] = $this->router->fetch_class();
    //     $data['permissions'] = $this->_get_permissions();
    //     $data['active_menu'] = $this->_get_active_menu();  
    //     $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
    //     $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
    //     $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
    //     $this->load->library("Cinta",$data);
    //     $this->cinta->browse();
    // }
    // }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    public function dataTable_dukcapil() {

        $this->load->library('Datatable', array('model' => $this->dttModel_dukcapil, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Developer Option', 
            'submenu' => 'Nasabah' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_detail"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."nasabah/nasabah/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nik" => array(

                "data" => "b.nik",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "type_product" => array(

                "data" => "d.nama_type_product",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "name" => array(

                "data" => "c.name",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "no_tlp" => array(

                "data" => "b.no_tlp",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "no_tlp" => array(

                "data" => "b.no_tlp",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "emaill" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
              

            ),

            // "photo_selfie" => array(

            //     "data" => "b.photo_selfie",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "nik" => array(

                "label" => "NIK",
                "type" => "text",
                "placeholder" => "NIK",
                "class" => "form-control validate[required]",
               

            ),

            "nama" => array(

                "label" => "Nama Nasabah",
                "type" => "text",
                "placeholder" => "Nama Nasabah",
                "class" => "form-control validate[required]",
               

            ),

            "no_tlp" => array(

                "label" => "Nomor HP",
                "type" => "text",
                "placeholder" => "Nomor HP",
                "class" => "form-control validate[required]",
               

            ),

            // "alamat" => array(

            //     "label" => "Alamat",
            //     "type" => "text",
            //     "placeholder" => "Alamat",
            //     "class" => "form-control validate[required]",
               

            // ),

            // "kelurahan" => array(

            //     "label" => "Kelurahan",
            //     "type" => "text",
            //     "placeholder" => "Kelurahan",
            //     "class" => "form-control validate[required]",
               

            // ),

            // "kecamatan" => array(

            //     "label" => "Kecamatan",
            //     "type" => "text",
            //     "placeholder" => "Kecamatan",
            //     "class" => "form-control validate[required]",
               

            // ),

            // "provinsi" => array(

            //     "label" => "provinsi",
            //     "type" => "text",
            //     "placeholder" => "provinsi",
            //     "class" => "form-control validate[required]",
               

            // ),


            
            

        );

// 
    }

    public function add(){

        if(count($_POST)>0){

            $_POST['id_product'] = $_SESSION['id_product'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Nasabah Key";
            $data['page_subtitle'] = "Modul Nasabah Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Nasabah";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}