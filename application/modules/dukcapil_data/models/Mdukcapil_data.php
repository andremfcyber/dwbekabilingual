
<?php

class Mdukcapil_data extends CI_Model
{
    function __construct(){
        parent::__construct();
        // $this->load->library("Aauth");
        // $this->load->model("mcore");

        // if($this->aauth->is_loggedin()) {
        //     $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        // }

    }
    function getKeyById($id){

        $result = $this->db->query("SELECT * FROM dukcapil_data WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM dukcapil_data")->result_array();
        return $result;

    }

    function getWa($id){

        $result = $this->db->query("SELECT a.nama, a.id_product, a.nik, a.no_tlp, a.email, b.id_nasabah, b.id_product, b.status, b.tgl_pengajuan, b.link_verifikasi FROM nasabah a, pengajuan b WHERE a.id='".$id."' AND a.id = b.id_nasabah ")->result_array();
        return @$result[0];

    }

    function getShowDukcapil($id_nasabah){

        $result = $this->db->query("SELECT a.id_nasabah, a.nama_lgkp, b.id, b.nik, b.nama FROM dukcapil_data a, nasabah b WHERE a.id_nasabah = '".$_SESSION['id_nasabah']."'" )->result_array();
        return @$result[0];

    }

    public function data_nasabah($id_nasabah)
    {
        $this->db->select('a.id_nasabah,
                           b.id,
                           a.nik,
                           b.nik,
                           a.nama_lgkp,
                           b.nama');
        $this->db->from('dukcapil_data a');
        $this->db->join('nasabah b','b.id = a.id_nasabah');
        $this->db->where('b.id = a.id_nasabah');
        $query = $this->db->get();
        return $query->result_array();      
    }

}