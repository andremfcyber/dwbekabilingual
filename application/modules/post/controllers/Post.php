<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Post extends Public_Controller {

    function __construct(){

        parent::__construct();
        $this->load->model('public/Mpub','m');
        $this->load->library('session');
        $this->session->keep_flashdata('msg');

        $_profil = $this->m->getprofil();
        $_kontak = $this->m->getkontak();
        $this->_profil = $this->m->getprofil();
        $this->_theme_vars['_profil'] = $_profil;
        $this->_theme_vars['_kontak'] = $_kontak;
    }

    public function article($lang=null,$id = null,$permalink = null){ 
        if($id != null && $permalink !=null){
            
            $lang = $this->uri->segment(3,'ID');
            $permalink = $this->uri->segment(5,'home');
            $language = $this->db->get_where('page_languages',array('code' => $lang))->result_array();
            $page_contents = $this->db->get_where('v_page', array('id' => $id, 'language_id' => @$language[0]['id']))->result_array();

            if($page_contents != null){

                if($page_contents[0]['permalink'] != $permalink){

                    redirect('post/article/'.$lang.'/'.$id.'/'.$page_contents[0]['permalink']);
                
                }

                $page_metas = $this->db->get_where('page_metas', array('page_id' => @$page_contents[0]['id'],'language_id' => @$page_contents[0]['language_id']))->result_array();

                $this->_theme_vars['page_content'] = @$page_contents[0]['content'];
                $this->_theme_vars['page_title'] = @$page_contents[0]['link_label'];
                $this->_theme_vars['page_metas'] = $page_metas;

                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "preview";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('page/preview');
                
            }
            
        }
       
    }

}