<?php

class Mdform_fields extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'form_fields_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'form_fields_delete');


    }
    public function appendToSelectStr() {
        $edit = '';
        $delete = '';
        $str = '';

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($edit!='' || $delete!=''){

            $op = "concat('".$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "form_fileds b";
    }

    public function joinArray(){
        return array(
            "form_header c |left" => "b.form_header_id=c.id"
        );
    }

    public function whereClauseArray(){
        return array(
            "b.form_header_id" => $_SESSION['form_header_id']
        );
    }


}