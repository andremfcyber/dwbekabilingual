<?php

class Mdpengajuan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'pengajuan_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'pengajuan_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'pengajuan_detail');
    }
    
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" href="'.base_url('detail_nasabah/show/').'\',b.id,\'/\',b.id_nasabah,\'"><i class="fa fa-send"></i></a>&nbsp;';
        }

        // if($this->allow_edit){
        //     $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        // }

        // if($this->allow_delete){
        //     $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        // }

        if($detail!='' || $edit!='' || $delete!=''){

            $op = "concat('".$detail.$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "pengajuan b";
    }

    public function joinArray(){
        // return null;
        return array(
            "nasabah c|left" => "b.id_nasabah = c.id",
            "product d|left" => "b.id_product = d.id",

        //     "product e|left" => "c.id_product = e.id"
          
        );
    }

    public function whereClauseArray(){
        // return null;
        return array(
            "b.id_nasabah" => $_SESSION['id_nasabah']
        );
    }


}