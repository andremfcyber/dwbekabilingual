<?php

class Mdproduct_type extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'product_type_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'product_type_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'product_type_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="List Product" href="'.base_url('product/').'\',b.nama_type_product,\'"><i class="fa fa-eye"></i></a>&nbsp;';
        }

        // if($this->allow_edit){
        //     $edit = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        // }

        // if($this->allow_delete){
        //     $delete = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        // }

        if($detail!='' || $edit!='' || $delete!=''){

            $op = "concat('".$detail.$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "product_type b";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}