<?php

class Detail_nasabah2 extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mdetail_nasabah2");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "detail_nasabah";
        $this->dttModel = "Mddetail_nasabah2";
        $this->pk = "id";

    }

    public function index(){

        // if($id != null){
        //     $_SESSION['id_pengajuan'] = $id;
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Detail Nasabah | Cek Data Ke Dukcapil";
        $data['page_subtitle'] = "Modul Detail Nasabah";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();

        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    function show($id = null, $nik = null){

        if($id != null){
            $_SESSION['id_pengajuan'] = $id;

        
            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "List Form";
            $data['page_subtitle'] = "Modul From";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons'] = $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();

            $data['data'] = json_decode($this->guzzle_get('/xnik/reqnik'));


            // var_dump($data['data']);die();

            $this->load->library("Cinta",$data);
            $this->cinta->browse();
        }
    }

    function show_detail($id = null){

        if($id != null){

            $pengajuan = $this->db->get_where("pengajuan", array("id" => $id))->result_array();

            $this->_theme_vars['id_product'] = @$pengajuan[0]['id_product'];
            $this->_theme_vars['id_nasabah'] = @$pengajuan[0]['id_nasabah'];

            $this->_theme_vars['form_header'] = $this->db->get_where('form_header', array('product_id' => @$pengajuan[0]['id_product']))->result_array();

            $this->_theme_vars['page_title'] = "Data Formulir Nasabah";
            $this->_theme_vars['page_subtitle'] = "Modul Product";
            $this->_theme_vars['pengajuan'] = $pengajuan;
        
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('data_formulir_nasabah');

        }

    }

    public function guzzle_get($id,$nik){

         $cekdata = $this->db->query("SELECT * FROM dukcapil_data WHERE nik='".$nik."'")->num_rows();

         if ($cekdata == 1) {

            // redirect('dukcapil_data/dukcapil_data');
            echo "<script type='text/javascript'>alert('Nik Sudah Terdaftar!');window.history.back();</script>";exit;

         }elseif($id != null){
            $_SESSION['id_nasabah'] = $id;

            $adms_server = 'http://117.54.3.201:9961';
            $client = new GuzzleHttp\Client(['base_uri' => $adms_server]);
            $response = $client->request('POST','/xnik/reqnik',[
                'headers' => [
                    'xapix' => '12345678',
                    'xincomingx' => 'LAS',
                    'Content-Type' => 'application/json'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    'nik' => $nik,
                    'username' => 'ad',
                    'address' => '12345678',
                ]
            ]);

            $res_body = $response->getBody()->getContents();

            $res_body = json_decode($res_body);

            // var_dump($res_body);die();

            if($res_body->rc != "00"){

                echo "<script type='text/javascript'>alert('Nik tidak valid!');window.history.back();</script>";exit;

            }else{

                $data = array(  
                    "id_nasabah" => $_SESSION['id_nasabah'],
                    "nik" => $res_body->nik,
                    "address" => $res_body->address,
                    "tmpt_lhr" => $res_body->TMPT_LHR,
                    "nama_lgkp_ibu" => $res_body->NAMA_LGKP_IBU,
                    "kel_name" => $res_body->KEL_NAME,
                    "no_kk" => $res_body->NO_KK,
                    "no_rt" => $res_body->NO_RT,
                    "rc" => $res_body->rc,
                    "alamat" => $res_body->ALAMAT,
                    "kab_name" => $res_body->KAB_NAME,
                    "jenis_klmin" => $res_body->JENIS_KLMIN,
                    "no_rw" => $res_body->NO_RW,
                    "prop_name" => $res_body->PROP_NAME,
                    "nama_lgkp" => $res_body->NAMA_LGKP,
                    "kec_name" => $res_body->KEC_NAME,
                    "agama" => $res_body->AGAMA,
                    "jenis_pkrjn" => $res_body->JENIS_PKRJN,
                    "rm" => $res_body->rm,
                    "status_kawin" => $res_body->STATUS_KAWIN,
                    "kode_pos" => $res_body->KODE_POS,
                    "tgl_lhr" => $res_body->TGL_LHR,
                    "username" => $res_body->username,
                );

                    $this->db->insert('dukcapil_data',$data);
             }  
                
        

            }

            
        
        // echo "<pre>";
        // print_r($res_body);
        // // print_r($res_body->nik);
        
        redirect('dukcapil_data/dukcapil_data');
        // exit;
        }
    

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Developer Option', 
            'submenu' => 'Pengajuan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "detail_nasabah2_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "detail_nasabah2_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "detail_nasabah2_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "detail_nasabah2_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "detail_nasabah2_detail"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."detail_nasabah/detail_nasabah/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){

        return array(

            "nik" => array(

                "data" => "c.nik",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "nama" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "type_product" => array(

                "data" => "e.nama_type_product",
                "searchable" => true,
                "orderable" => true,

            ),

            "product" => array(

                "data" => "d.name",
                "searchable" => true,
                "orderable" => true,

            ),

            "tgl_pengajuan" => array(

                "data" => "b.tgl_pengajuan",
                "searchable" => true,
                "orderable" => true,

            ),

            "status" => array(

                "data" => "b.status",
                "searchable" => true,
                "orderable" => true,
              

            ),

           
            "cekdukcapil" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "value" => array(

                "label" => "Value",
                "type" => "text",
                "placeholder" => "Value",
                "class" => "form-control validate[required]",
               

            ),

        );

// 
    }

    public function add(){

        if(count($_POST)>0){

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Blog Key";
            $data['page_subtitle'] = "Modul Blog Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Form";
            $data['page_subtitle'] = "Modul Form";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}