<?php

class Product extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mproduct");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "product";
        $this->dttModel = "Mdproduct";
        $this->pk = "id";

    }

    function index(){

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "List Product";
            $data['page_subtitle'] = "Modul Product";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    function show($id = null){

        if($id != null){

            $_SESSION['id_type_product'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "List Product";
            $data['page_subtitle'] = "Modul Product";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

       

    }

    function show_form_preview($id=null,$id_nasabah = null){

        if($id != null){

            $this->_theme_vars['page_title'] = "Form Preview";
            $this->_theme_vars['page_subtitle'] = "Modul Product";
            $this->_theme_vars['id_product'] = $id;
            $this->_theme_vars['id_nasabah'] = $id_nasabah;

            $this->db->order_by('urutan', 'ASC');
            $this->_theme_vars['form_header'] = $this->db->get_where('form_header', array('product_id' => $id))->result_array();
        
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('form_preview');

        }

    }

    function save_data_formulir(){

        if(count($_POST)>0){

            $keys = array_keys($_POST);
            $a = count($keys);

            for($i=0;$i < $a;$i++){

                $key = $keys[$i];

                $x = explode("_",$key);
                $input_type = @$x[0];
                $id_field = @$x[1];

             

                if($input_type=="checkbox"){
                    
                    $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();

                    $value = $id_field;

                    $data = array(
                        "id_form_field" => $id_field,
                        "id_header_form" => @$field[0]['form_header_id'],
                        "id_nasabah" => $_POST['id_nasabah'],
                        "id_product" => $_POST['id_product'],
                        "input_type" => "checkbox",
                        "value" => $value,
                    );

                    $this->db->insert("data_formulir_nasabah",$data);

                }else if($input_type=="text"){

                    $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();

                    $value = $_POST[$key];

                    $data = array(
                        "id_form_field" => $id_field,
                        "id_header_form" => @$field[0]['form_header_id'],
                        "id_nasabah" => $_POST['id_nasabah'],
                        "id_product" => $_POST['id_product'],
                        "input_type" => "text",
                        "value" => $value,
                    );

                    $this->db->insert("data_formulir_nasabah",$data);

                }


            }

            echo json_encode(array("status" => "1", "msg" => "Data berhasil tersimpan!"));

        }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(
            'parent_menu' => 'product2', 
            'submenu' => 'product2' 
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "product_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "product_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "product_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "product_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "product_detail"),
            "nasabah_perm" => $this->mcore->checkPermission($this->user_group, "product_nasabah"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "order" => [[ 0, "asc" ]],
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."product/product/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "name" => array(

                "data" => "b.name",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "type_product" => array(

                "data" => "c.nama_type_product",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "deskripsi" => array(

                "data" => "b.deskripsi",
                "searchable" => true,
                "orderable" => true,
              

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "name" => array(

                "label" => "Nama Product",
                "type" => "text",
                "placeholder" => "Nama Product",
                "class" => "form-control validate[required]",
               

            ),

            "name" => array(

                "label" => "Nama Product",
                "type" => "text",
                "placeholder" => "Nama Product",
                "class" => "form-control validate[required]",
               

            ),

            "deskripsi" => array(

                "label" => "Deskripsi",
                "type" => "editor",
                "placeholder" => "Deskripsi",
                "class" => "form-control validate[required]",
               

            ),
            

        );


    }

    public function add(){

        if(count($_POST)>0){

            $_POST['product_type_id'] = $_SESSION['id_type_product'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Product";
            $data['page_subtitle'] = "Modul Product";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Product";
            $data['page_subtitle'] = "Modul Product";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}