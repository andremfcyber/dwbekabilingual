<?php

class Mdproduct extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'product_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'product_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'product_detail');
        $this->allow_nasabah = $this->mcore->checkPermission($this->user_group, 'product_nasabah');
        $this->allow_show_preview = $this->mcore->checkPermission($this->user_group, 'product_nasabah');


    }
    public function appendToSelectStr() {
        $detail = '';
        $nasabah = '';
        $edit = '';
        $delete = '';
        $str = '';

        if($this->allow_nasabah){
            $nasabah = '<a class="btn btn-sm btn-primary" href="'.base_url('nasabah/show/').'\',b.id,\'"><i class="fa fa-user"></i></a>&nbsp;';
        }

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" href="'.base_url('form_header/show/').'\',b.id,\'"><i class="fa fa-send"></i></a>&nbsp;';
        }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_show_preview){
            $show_preview = '<a class="btn btn-sm btn-default" href="'.base_url('product/show_form_preview/').'\',b.id,\'"><i class="fa fa-eye"></i></a>';
        }

        if($nasabah!='' ||$detail!='' || $edit!='' || $delete!='' || $show_preview!=''){

            $op = "concat('".$nasabah.$detail.$edit.$delete.$show_preview."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "product b";
    }

    public function joinArray(){
        // return null();
        return array(
            "product_type c |left" => "b.product_type_id = c.id",
            "nasabah d |left" => "b.id = d.id_product"
        );
    }

    public function whereClauseArray(){
        // return null();
        return array(
            "b.product_type_id" => $_SESSION['id_type_product']
        );
    }


}