<?php

class Mdtabungan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'product_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'product_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'product_detail');

    }

    public function appendToSelectStr() {
        $edit = '';
        $delete = '';
        $detail = "";
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" href="javascript:edit(\',p.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" href="javascript:remove(\',p.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Form Builder" href="'.base_url('form_builder/show/').'\',p.id,\'"><i class="fa fa-list-alt"></i></a>&nbsp;';
        }



        if($edit!='' || $delete!='' || $detail != ''){

            $op = "concat('".$edit.$delete.$detail."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "product p";
    }

    public function joinArray(){
        // return null();
        return array(
            "product_type pt |left" => "p.product_type_id = pt.id",
        );
    }

    public function whereClauseArray(){
        return array(
            "pt.id" => 8
        );
    }


}