<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends Public_Controller {

    function __construct(){
        $this->load->library('form_validation');

        parent::__construct();
        $this->load->model('Mpub','m');
        $this->load->library('session');
        $this->session->keep_flashdata('msg');

        $_profil = $this->m->getprofil();
        $_kontak = $this->m->getkontak();
        $this->_profil = $this->m->getprofil();
        $this->_theme_vars['_profil'] = $_profil;
        $this->_theme_vars['_kontak'] = $_kontak;
    }

    public function testing_email(){
        send_email();
    }

    public function index_old(){
        $base_url = base_url();
        // echo $base_url; die;
        // map_y($base_url);
        if($base_url == 'https://dashboard.bankeka.dak.co.id/' || $base_url == 'http://dashboard.bankeka.com/'){
            redirect('admin');
        }
        // gen_no_pertemuan(235, 25);
        // map_y($this->theme);
        $_slide = $this->m->getslide();
        // $_mitra = $this->m->get_mitra();
        $_pengawas = $this->m->get_badan_pengawas();
        $_pressrealese = $this->m->getpressrealese();
        // $_kml = $this->m->getkml();
       
       
        // $this->_theme_vars['_kml'] = $_kml;

        $cerpel = $this->db->query("SELECT * FROM front_cerita_pelanggan")->result_array();
        // map_y($cerpel);
        $data['cerpel'] = $cerpel;
       
        $this->_theme_vars['_pengawas'] = $_pengawas;
        // $this->_theme_vars['_mitra'] = $_mitra;
        $this->_theme_vars['_slide'] = $_slide;
        $this->_theme_vars['_pressrealese'] = $_pressrealese;

        // map_y($this->_theme_vars);
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "home";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('index1', '', $data);
    }

    public function index($lang='ID'){


        $permalink = $this->uri->segment(5,'home');
        $language = $this->db->get_where('page_languages',array('code' => $lang))->result_array();
        $page_contents = $this->db->get_where('v_page', array('title' => 'Home Page', 'language_id' => @$language[0]['id']))->result_array();

        if($page_contents != null){

            $page_metas = $this->db->get_where('page_metas', array('page_id' => @$page_contents[0]['id'],'language_id' => @$page_contents[0]['language_id']))->result_array();

            $this->_theme_vars['page_content'] = @$page_contents[0]['content'];
            $this->_theme_vars['page_title'] = @$page_contents[0]['link_label'];
            $this->_theme_vars['page_metas'] = $page_metas;

            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = "preview";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_view('page/home');
            
        }

    }

    public function wow($message = ''){

        if(!$message){
            $message = isset($_SESSION['error_msg']) ? $_SESSION['error_msg'] : '';
        }
        // map_y($message);
        // map_y($this->theme);
        // $_profil = $this->m->getprofil();
        // $_kontak = $this->m->getkontak();
        // $_slide = $this->m->getslide()[0];

        // $this->_theme_vars['_slide'] = $_slide;
        // $this->_theme_vars['_profil'] = $_profil;
        // $this->_theme_vars['_kontak'] = $_kontak;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "wow";
        $this->_theme_vars['current_theme'] = $this->theme;
        
        $data['message'] = $message;
        $this->render_view('view/wow', '', $data);
        unset($_SESSION['error_msg']);
        
    }

    public function profil(){
        
        // $_profil = $this->m->getprofil();
        // $_kontak = $this->m->getkontak();
        $id=$this->_profil['id'];
        $foto = $this->db->query("SELECT * FROM media_cepiring a where a.id_nya = $id AND a.module = 'front_profil'")->result_array();
        $this->_theme_vars['_foto'] = $foto;
        // map_y($foto);
        // $this->_theme_vars['_kontak'] = $_kontak;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "profil";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('profil');
        
    }
    
    public function agenda($id = '1_agenda'){
        
        
        $_agenda = $this->m->getagenda();
       
       
        $this->_theme_vars['_agenda'] = $_agenda;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "agenda";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('agenda');
        
    }
    
    public function artikel($id = '1_artikel'){
       
        $table =  'front_artikel';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 4;

        $offset = ($page - 1) * $resultCount;
        $columns = [
            'id as id',
        ];
        
        if(empty($_GET['kategori'])){
            $query = "SELECT * FROM ".$table." ORDER BY id DESC  ";

        }else{
            $conditions = [
                "WHERE id_kategori = '".$_GET['kategori']."'"
            ];    
            
            $query = "SELECT * FROM ".$table." ".implode(' ', $conditions)." ORDER BY id DESC ";
        }
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );

        // map_y($results);
        
        $_kategori = $this->m->getkategori();

        $_lainnya = $this->m->getRecent();
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;
        // $this->_theme_vars['_artikel'] = $_artikel;
        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_artikel_lainnya'] = $_lainnya;
        
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "artikel";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('artikel');

    }
    
    public function artikel_detail($id = null, $baner = '1_artikel'){
        if($id != null){

            $_artikel_detail = $this->m->get_artikel_detail($id);
            $_kategori = $this->m->getkategori();
            $_lainnya = $this->m->getRecent();

            $this->_theme_vars['_kategori'] = $_kategori;
            $this->_theme_vars['_artikel_lainnya'] = $_lainnya;
            $this->_theme_vars['_artikel_detail'] = $_artikel_detail;

            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = "artikel-detail";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_view('artikel-detail');
        }
    }

    public function awards($baner = '1_awards'){
        
        $table =  'front_awards';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 6;

        $offset = ($page - 1) * $resultCount;
        $columns = [
            'id as id',
        ];

        $query = "SELECT * FROM ".$table." ORDER BY id DESC ";

        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "awards";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('awards');
        
    }
    
    public function blog_detail($id = null,$baner = '1_blog'){
        if($id != null){
        
       
        $_blog_detail = $this->m->get_blog_detail($id);
        // map_y($_kontak);
        $_lainnya = $this->m->getblog_limit();
        $_kategori = $this->m->getkategori();
       
        // $this->_theme_vars['_blog'] = $_blog;
        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_lainnya'] = $_lainnya;
        $this->_theme_vars['_blog_detail'] = $_blog_detail;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "blog-detail";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('blog-detail');
        }
    }
    
    public function blog($baner = '1_blog'){
      
        $table =  'cms_blog';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 4;

        $offset = ($page - 1) * $resultCount;
        $columns = [
            'id as id',
        ];
        
        if(empty($_GET['kategori'])){
            $query = "SELECT * FROM ".$table." ORDER BY id DESC ";

        }else{
            $conditions = [
                "WHERE id_kategori = '".$_GET['kategori']."'"
            ];    
            
            $query = "SELECT * FROM ".$table." ".implode(' ', $conditions)." ORDER BY id DESC ";
        }
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;

        $_lainnya = $this->m->getblog_limit();

        $_kategori = $this->m->getkategori();

        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_lainnya'] = $_lainnya;
       
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "blog";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('blog');
        
    }
    
    public function budaya_perusahaan($id="1_budaya"){
        $_budaya = $this->m->getbudaya();
        $this->_theme_vars['_budaya'] = $_budaya;
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "budaya-perusahaan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('budaya-perusahaan');
        
    }
    
    public function cerita_pelanggan(){

        $table =  'front_cerita_pelanggan';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 6;

        $offset = ($page - 1) * $resultCount;
            
        $query = "SELECT * FROM ".$table." ORDER BY id DESC ";
        
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "cerita-pelanggan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('cerita-pelanggan');
        
    }
    
    public function detail_download($tahun){
        
        $table =  'front_download';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 8;

        $offset = ($page - 1) * $resultCount;
        
      
            $conditions = [
                "WHERE tahun = '".$tahun."'"
            ];    
            
            $query = "SELECT * FROM ".$table." ".implode(' ', $conditions)." ORDER BY id DESC ";
        
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;
        // $_download = $this->m->get_download_by_tahun($tahun);
        
       
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "detail-download";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('detail-download');
        
    }
    
    public function detail_tabungan(){
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "detail-tabungan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('detail-tabungan');
        
    }
    
    public function download(){
        
        $_tahun = $this->m->get_tahun_download();
        
        $this->_theme_vars['_tahun'] = $_tahun;
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "download";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('download');
        
    }
    
    public function faq($baner = '1_faq'){
        
        $_faq = $this->m->getfaq();
        // $_slide = $this->m->getbaner($baner)[0];

        // $this->_theme_vars['_slide'] = $_slide;
       
        $this->_theme_vars['_faq'] = $_faq;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "faq";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('faq');
        
    }
    
    public function form_pengajuan(){
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "form-pengajuan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('form-pengajuan');
        
    }
    
    public function galeri(){
        $table =  'front_galeri';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 9;

        $offset = ($page - 1) * $resultCount;
        
      
        if(empty($_GET['kategori'])){
            $query = "SELECT * FROM ".$table." ORDER BY created_at DESC ";
            $status_tab = 0;
        }else{
            $conditions = [
                "WHERE id_kategori_galeri = '".$_GET['kategori']."'"
            ];    
            
            $query = "SELECT * FROM ".$table." ".implode(' ', $conditions)." ORDER BY created_at DESC ";
            $status_tab = $_GET['kategori'];
        }
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
    //    map_y($results);
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;

        // $_galeri = $this->m->getgaleri();
        
        // $_slide = $this->m->getslide()[0];

        // $this->_theme_vars['_slide'] = $_slide;
        $this->_theme_vars['status_tab'] = $status_tab;
       
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "galeri";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('galeri');
        
    }
    
    public function hubungi_kami($baner = '1_hubungi_kami'){
         
       
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "hubungi-kami";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('hubungi-kami');
        
    }
    
    public function karir($baner = '1_karir'){
        
        $table =  'front_career';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;
        
        if(!empty( $keyword = $this->input->post('job'))){
            $conditions = [
                "WHERE nama = '".$keyword."'",
                "AND status = 1",
            ]; 
            $query = "SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
            a.type_kerja, 
            c.tipe_career,
            a.keterangan, 
            a.created_at, 
            a.created_by, 
            a.updated_at, 
            a.updated_by
          FROM ".$table." a 
          left join front_kategori_career b on a.id_kategori = b.id
          left join front_tipe_career c on a.type_kerja = c.id ".implode(' ', $conditions)." ORDER BY id DESC ";
           
        }elseif(empty($_GET['tipe'])){
            $query = "SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
            a.type_kerja, 
            c.tipe_career,
            a.keterangan, 
            a.created_at, 
            a.created_by, 
            a.updated_at, 
            a.updated_by
          FROM ".$table." a 
          left join front_kategori_career b on a.id_kategori = b.id
          left join front_tipe_career c on a.type_kerja = c.id 
          WHERE status = 1
          ORDER BY id DESC ";
           
        }else{
            $conditions = [
                "WHERE type_kerja = '".$_GET['tipe']."'",
                "AND status = 1",
            ];    
            
            $query = "SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
            a.type_kerja, 
            c.tipe_career,
            a.keterangan, 
            a.created_at, 
            a.created_by, 
            a.updated_at, 
            a.updated_by
          FROM ".$table." a 
          left join front_kategori_career b on a.id_kategori = b.id
          left join front_tipe_career c on a.type_kerja = c.id ".implode(' ', $conditions)." ORDER BY id DESC ";
          
        }
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;

        // $_slide = $this->m->getbaner($baner)[0];
        
        // $_career = $this->m->getcareer();
        $_tipe_career = $this->m->gettipe_career();
        $_kategori_career = $this->m->getkategori_career();
        // getcareer

        $this->_theme_vars['_kategori_career'] = $_kategori_career;
        $this->_theme_vars['_tipe_career'] = $_tipe_career;
        // $this->_theme_vars['_career'] = $_career;
       
        // $this->_theme_vars['_slide'] = $_slide;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "karir";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('karir');
        
    }
    
    public function get_search_karir($id)
    {
        $_career = $this->m->get_karir_search($id);
     
        return response_json($_career);
        
    }
    public function karirsearch($baner = '1_karir'){
       
        $keyword = $this->input->post('job');
      
		$_career=$this->m->get_career_keywoard($keyword);
        
        $_slide = $this->m->getbaner($baner)[0];
        
       
        $_tipe_career = $this->m->gettipe_career();
        $_kategori_career = $this->m->getkategori_career();

        $this->_theme_vars['_kategori_career'] = $_kategori_career;
        $this->_theme_vars['_tipe_career'] = $_tipe_career;
        $this->_theme_vars['_career'] = $_career;
       
        $this->_theme_vars['_slide'] = $_slide;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "karir";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('karir');
        
    }
    
    public function kenapa_memilih_kami($id = '1_kml'){
        
        
        $_kml = $this->m->getkml();
       
       
        $this->_theme_vars['_kml'] = $_kml;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "kenapa-memilih-kami";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('kenapa-memilih-kami');
        
    }
    
    public function laporan(){
        // map_y($kategori);

        
        $_slide = $this->m->getslide()[0];
        $_kategori = $this->m->get_kategori_laporan();
       
        $_laporan = $this->m->get_laporan();  
        // map_y($_laporan);
        $_nm_kategori = '';
        $this->_theme_vars['_nm_kategori'] = $_nm_kategori;
        $this->_theme_vars['_laporan'] = $_laporan;
        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_slide'] = $_slide;
       
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "laporan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('laporan');
        
    }
    
    public function laporanbykategori($kategori){
        // map_y($kategori);

        
        $_slide = $this->m->getslide()[0];
        $_kategori = $this->m->get_kategori_laporan();
        $_nm_kategori = $this->m->get_kategori_laporan_by($kategori);
        
        $_laporan = $this->m->get_laporan_kategori($kategori);

        $this->_theme_vars['_nm_kategori'] = $_nm_kategori;
        $this->_theme_vars['_laporan'] = $_laporan;
        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_slide'] = $_slide;
       
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "laporan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('laporan');
        
    }
    
    public function press_realese($baner = '1_press_realese'){
      
        
        $table =  'front_press_realese';
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        // map_y($page);
        $resultCount = 4;

        $offset = ($page - 1) * $resultCount;
        $columns = [
            'id as id',
        ];
        
        if(empty($_GET['kategori'])){
            $query = "SELECT * FROM ".$table."  ORDER BY id DESC ";

        }else{
            $conditions = [
                "WHERE id_kategori = '".$_GET['kategori']."'"
            ];    
            
            $query = "SELECT * FROM ".$table." ".implode(' ', $conditions)." ORDER BY id DESC ";
        }
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= ' OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
       
        $endCount = $offset + $resultCount;

        // map_y($page);
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        
        $_kategori = $this->m->getkategori();
        $_lainnya = $this->m->getpressrealese_limit();
       
        $this->_theme_vars['page'] = $page;
        $this->_theme_vars['results'] = $results;
        $this->_theme_vars['_lainnya'] = $_lainnya;
        $this->_theme_vars['_kategori'] = $_kategori;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "press-realese";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('press-realese');
        
    }
    
    public function press_realese_detail($id = null, $baner = '1_press_realese'){
        if($id != null){

        $_pressrealese = $this->m->getpressrealese();
        $_pressrealese_detail = $this->m->get_pressrealese_detail($id);
        $_kategori = $this->m->getkategori();
        $_lainnya = $this->m->getpressrealese_limit();
       
        $this->_theme_vars['_lainnya'] = $_lainnya;
        $this->_theme_vars['_kategori'] = $_kategori;
        $this->_theme_vars['_pressrealese'] = $_pressrealese;
        $this->_theme_vars['_pressrealese_detail'] = $_pressrealese_detail;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "press-realese-detail";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('press-realese-detail');
        }
    }

    public function struktur($id="1_struktur"){
        
        redirect('/');

        // $_struktur = $this->m->getstruktur();
        
       
        // $this->_theme_vars['_struktur'] = $_struktur;
       
        
        // $this->set_theme($this->_theme_vars['active_theme']);
        // $this->_theme_vars['current_page'] = "struktur";
        // $this->_theme_vars['current_theme'] = $this->theme;
        // $this->render_view('struktur');
        
    }
    
    public function visi_misi($id = '1_visi_misi'){
        
        
       
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "visi-misi";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('visi-misi');
        
        
    }

    public function wizard(){
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "wizard";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('wizard');
        
    }

    function kredit($id = null){

        $show = $this->m->data_product_kredit();
        $this->_theme_vars['show'] = $show;
        
        
        
       
        
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "kredit";
        $this->_theme_vars['current_theme'] = $this->theme;
        
        $data['show'] = $show;
        $data['option_jenis_kredit'] = $show;
        $data['option_bulan'] = $this->db->query("SELECT * FROM master_bulan WHERE status = '1'")->result_array();
        $this->render_view('kredit',NULL,$data);
        
    }

    function deposito($id = null){

        $show = $this->m->data_product_deposito();
        
        
        $_slide = $this->m->getslide()[0];

        $this->_theme_vars['_slide'] = $_slide;
       

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "deposito";
        $this->_theme_vars['current_theme'] = $this->theme;

        $data['show'] = $show;
        $data['option_jenis_deposit'] = $show;

        // map_y($data);
        $this->render_view('deposito','', $data);
        
    }

    function tabungan($id = null){

        $show = $this->m->data_product_tabungan();

        $this->_theme_vars['show'] = $show;
        
        
        $_slide = $this->m->getslide()[0];

        $this->_theme_vars['_slide'] = $_slide;
       

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "tabungan";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('tabungan');
        
    }

    function verifikasi($id = null,$id_nasabah = null){

        if($id != null){

            $_SESSION['id_product'] = $id;  

        $wa = $this->m->getVerifikasi($id,$id_nasabah);

        if (count ($wa) > 0){
            $this->_theme_vars['wa'] = $wa;

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = $wa['name'];
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_view('verifikasi');    
        } else {
             redirect('public/home');
        }
        

    }
        
    }

    function verifikasi_gagal($id_product = null, $id_nasabah = null){

        $wa = $this->m->getVerifikasi($id_product,$id_nasabah);

        if (count ($wa) > 0){
            $this->_theme_vars['wa'] = $wa;

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = $wa['name'];
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_view('verifikasi');    
        } else {
             redirect('public/home');
        }
    
        
    }

    function validasi($id_product = null, $id_nasabah = null){


        if ($_POST['name'] == 'by_kk') {

                    $cekdata = $this->db->query("SELECT * FROM nasabah n left join detail_nasabah dn on n.id = dn.id_nasabah
                    WHERE dn.id_nasabah = '".$id_nasabah."' AND dn.kk='".$this->input->post('inputan')."'")->result_array();

                if (count($cekdata) >= 1) {
                    if ($cekdata[0]['status'] === '3'){
                        $newnasabah['status'] = '4';
                        $newnasabah['up_time'] = date('Y-m-d H:i:s');
                        $newnasabah['up_user'] = 'web public';
                        $this->db->update('nasabah', $newnasabah, array('id' => $id_nasabah));    
                    }
                    $this->db->update('nasabah', $newnasabah, array('id' => $id_nasabah));
                    $this->_insertlog('nasabah dengan id '.$id_nasabah.' melakukan verifikasi pengajuan untuk produk '. $id_product);
                    redirect('public/home/appointment/'.$id_product.'/'.$id_nasabah);  
                } else {

                    $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger">
                        No Kartu Keluarga Yang Anda Masukan Tidak Sesuai
                        </div>'
                    );
                    redirect('public/home/verifikasi_gagal/'.$id_product.'/'.$id_nasabah);
                } 

            } elseif ($_POST['name'] == 'by_ibu') {
                $nama_ibu = strtoupper($this->input->post('inputan'));
                $cekdata_ibu = $this->db->query("SELECT * FROM nasabah n left join detail_nasabah dn on n.id = dn.id_nasabah WHERE dn.id_nasabah = '".$id_nasabah."' AND dn.nama_ibu ='".$nama_ibu."'")->result_array();

                if (count($cekdata_ibu) >= 1) {
                    if ($cekdata_ibu[0]['status'] == '3'){
                        
                        $newnasabah['status'] = '4';
                        $newnasabah['up_time'] = date('Y-m-d H:i:s');
                        $newnasabah['up_user'] = 'web public';
                        $this->db->update('nasabah', $newnasabah, array('id' => $id_nasabah));    
                    }
                    redirect('public/home/appointment/'.$id_product.'/'.$id_nasabah);        

                } else{
                    $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger">
                        Ibu Kandung Yang Anda Masukan Tidak Sesuai
                        </div>'
                    );
                    redirect('public/home/verifikasi_gagal/'.$id_product.'/'.$id_nasabah);
                } 
            } 
        }

    function generate_form($id_product=null , $id_nasabah = null){

        if($id_nasabah != null){
            $nasabah = $this->m->getNasabah($id_nasabah);
            if ($nasabah['status'] >= 4){
               $header_form = $this->m->get_header_form($id_product);
                $this->_theme_vars['form_header'] =  $header_form;
                $detail_nasabah = $this->m->get_detail_nasabah($id_nasabah);
                $this->_theme_vars['detail_nasabah'] = $detail_nasabah;
                // $dukcapil = $this->db->get_where("pengajuan", array("id_nasabah" => $id_nasabah))->result_array();

                // $this->_theme_vars['dukcapil'] = $dukcapil;

                $this->_theme_vars['id_product'] = @$id_product;
                $this->_theme_vars['id_nasabah'] = @$id_nasabah;

                // $this->db->order_by('urutan', 'ASC');
                // $this->_theme_vars['form_header'] = $this->db->get_where('form_header', array('product_id' => @$dukcapil[0]['id_product']))->result_array();
            
                $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->render_view('generate_form'); 
            } else {
                 $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger">
                        Anda Belum Melakukan Verifikasi Data, Silahkan Lakukan verifikasi
                        </div>'
                    );
                    redirect('public/home/verifikasi_gagal/'.$id_product.'/'.$id_nasabah);
            }
            

        }

    }

    function show($id = null, $id_type_product = null){
        $_profil = $this->m->getprofil();
        $_kontak = $this->m->getkontak();
        
        $this->_theme_vars['_profil'] = $_profil;
        $this->_theme_vars['_kontak'] = $_kontak;

        $ref = isset($_GET['ref']) ? $_GET['ref'] : '';
        /**
         * Save current url
         */
        $_SESSION['current_url'] = current_url();
        if($id && $id_type_product){ 
            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = "home";
            $this->_theme_vars['current_theme'] = $this->theme;

            $detail_product = $this->db->query("SELECT * FROM product WHERE id = '$id'")->row();
            // map_y($detail_product);
            $data['id_product'] = $id;
            $data['id_type_product'] = $id_type_product;
            $data['ref'] = $ref;
            $data['nama_produk'] = $detail_product->name;
            // map_y($data);
            $this->render_view('home', '', $data);
        }
    }


    private function _do_upload()
    {
        $config['upload_path']      = 'assets/images/';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = 1000;
        $config['max_widht']        = 1000;
        $config['max_height']       = 1000;
        $config['file_name']        = round(microtime(true)*1000);
    
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('photo_selfie')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('',''));
        }
        return $this->upload->data('file_name');
    }

    private function _do_upload2()
    {
        $config['upload_path']      = 'assets/images/';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = 1000;
        $config['max_widht']        = 1000;
        $config['max_height']       = 1000;
        $config['file_name']        = round(microtime(true)*1000);
    
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('photo_ktp')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('',''));
        }
        return $this->upload->data('file_name');
    }

    public function add($id = null) {
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "pengajuan_berhasil";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('view/pengajuan_berhasil');

        if(count($_POST)>0){
            $_POST['id_product'] = $_SESSION['id_product'];
            $no_tlp = $_POST['no_tlp'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_tlp'] = $no_tlp;
        
            $data = array(
                'nama' => $this->input->post('nama'),
                'nik' => $this->input->post('nik'),
                'no_tlp' => $_POST['no_tlp'],
                'email' => $this->input->post('email'),
                'id_product' => $_POST['id_product'],
                'status' => 1,
                'cr_time' => date('Y-m-d H:i:s'),
                'cr_user' => 'web public',
                'up_time' => date('Y-m-d H:i:s')
            );

            if (!empty($_FILES['photo_selfie']['name'])) {
                if($_FILES["photo_selfie"]["type"]=="image/png" or
                $_FILES["photo_selfie"]["type"]=="image/jpg" or
                $_FILES["photo_selfie"]["type"]=="image/jpeg"){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                $array = explode('.', $_FILES['photo_selfie']['name']);
                $extension = end($array);
                $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo_selfie"]["tmp_name"], $upload_path."/".$photo)) {
                        $_POST['photo_selfie'] = $photo; 
                        $data['photo_selfie'] = $_POST['photo_selfie']  ;
                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }
                }else{
                $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                $res['status'] = "0";

                echo json_encode($res);
                exit;
                }
            }

            if (!empty($_FILES['photo_ktp']['name'])) {
                if($_FILES["photo_ktp"]["type"]=="image/png" or
                    $_FILES["photo_ktp"]["type"]=="image/jpg" or
                    $_FILES["photo_ktp"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo_ktp']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                    if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                        $_POST['photo_ktp'] = $photo;
                        $data['photo_ktp'] = $_POST['photo_ktp']  ;
                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }else{
                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;
                }
            }
            
            $this->m->insert($data);

            $id_nasabah = $this->db->insert_id('nasabah_id_seq');
            $this->_insertlog('nasabah dengan id '.$id_nasabah.' melakukan pengajuan baru produk '. $_POST['id_product']);
            $this->session->set_flashdata('msg', 
                '<div class="alert alert-success">
                    Data Sudah di ajukan, Tunggu Notifikasi Lewat Whatsapp, SMS, atau Email Untuk Proses Selanjutnya
                </div>'
            );
            // redirect('public/home/index');
            // redirect('public/home/verifikasi/'.$_SESSION['id_product'].'/'.$id_nasabah);
        }
    }

    /**
     * Pengajuan Baru
     * 
     * @author ArGan
     */
    public function pengajuan($id_product) {
        $id_product_type = $_GET['type_id'];
        $request = $_REQUEST;
        
        $config = [
            [
                'field' => 'nama_lengkap',
                'label' => 'Nama Lengkap',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'no_hp',
                'label' => 'No.HP',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                    'valid_email' => '%s Formatnya Salah, ex: contoh@email.com',
                ],
            ],
            [
                'field' => 'provinsi_nasabah',
                'label' => 'Provinsi Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'kota_nasabah',
                'label' => 'Kab/Kota Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'kecamatan_nasabah',
                'label' => 'Kecamatan Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'kelurahan_nasabah',
                'label' => 'Kelurahan Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'alamat_lengkap_nasabah',
                'label' => 'Alamat Lengkap Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            [
                'field' => 'kodepos_nasabah',
                'label' => 'Kode Pos Sekarang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s Wajib Di Isi',
                ],
            ],
            // [
            //     'field' => 'same_ktp',
            //     'label' => '',
            //     'rules' => 'required',
            //     'errors' => [
            //         'required' => '%s Pertanyaan Domisili Wajib Di isi',
            //     ],
            // ],
        ];
        
        $request['same_ktp'] = isset($request['same_ktp']) && $request['same_ktp'] == 'true' ? true : false;
        $is_same_ktp = $request['same_ktp'];
        
        if(!$request['same_ktp']){
            $optional = [
                [
                    'field' => 'provinsi_optional',
                    'label' => 'Provinsi Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
                [
                    'field' => 'kota_optional',
                    'label' => 'Kab/Kota Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
                [
                    'field' => 'kecamatan_optional',
                    'label' => 'Kecamatan Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
                [
                    'field' => 'kelurahan_optional',
                    'label' => 'Kelurahan Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
                [
                    'field' => 'alamat_lengkap_optional',
                    'label' => 'Alamat Lengkap Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
                [
                    'field' => 'kodepos_optional',
                    'label' => 'Kode Pos Ktp',
                    'rules' => 'required',
                    'errors' => [
                        'required' => '%s Wajib Di Isi',
                    ],
                ],
            ];
            $config = array_merge($config, $optional);
        }

        $validate = [];

        $this->form_validation->set_error_delimiters('', '|');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $f_validate = validation_errors();
            // map_y(map_error($validate) );
            $f_validate = map_error($f_validate);
            $validate = array_merge($validate, $f_validate);
        } 
        
         /**
         * Photo
         */
        $data = [];

        if (isset($_FILES['photo_selfi']['name']) && $_FILES['photo_selfi']['name']) {
            if($_FILES["photo_selfi"]["type"] == "image/png" || 
                $_FILES["photo_selfi"]["type"] == "image/jpg" || 
                $_FILES["photo_selfi"]["type"] == "image/webp" || 
                $_FILES["photo_selfi"]["type"] == "image/jpeg") {

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                $array = explode('.', $_FILES['photo_selfi']['name']);
                $extension = end($array);
                $photo = md5(uniqid(rand(), true)).".".$extension;
                if (move_uploaded_file($_FILES["photo_selfi"]["tmp_name"], $upload_path."/".$photo)) {
                    $_POST['photo_selfie'] = $photo; 
                    $data['photo_selfie'] = $_POST['photo_selfie'];

                }else{
                    return response_json([
                        'msg' => 'Upload Foto Selfi Gagal'
                    ],400);
                }
            }else{
                $validate[] = 'Format Foto Selfi Salah, harusnya PNG/JPG/JPEG';
            }

        }else{
            $validate[] = 'Foto Selfi Wajib Diisi';
        }

        /**
         * Ktp
         */
        if (isset($_FILES['photo_ktp']['name']) && $_FILES['photo_ktp']['name']) {
            if($_FILES["photo_ktp"]["type"]=="image/png" ||
                $_FILES["photo_ktp"]["type"]=="image/jpg" ||
                $_FILES["photo_ktp"]["type"]=="image/webp" ||
                $_FILES["photo_ktp"]["type"]=="image/jpeg") {

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                $array = explode('.', $_FILES['photo_ktp']['name']);
                $extension = end($array);
                $photo = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                    $_POST['photo_ktp'] = $photo;
                    $data['photo_ktp'] = $_POST['photo_ktp']  ;
                }else{
                    return response_json([
                        'msg' => 'Upload Foto Ktp Gagal'
                    ],400);
                }
            }else{
                $validate[] = 'Format Foto Ktp Salah, harusnya PNG/JPG/JPEG';
            }
        }else{
            $validate[] = 'Foto Ktp Wajib Diisi';
        }

        if(count($validate) > 0){
            return response_json($validate, 422);
        }

        $no_hp = $request['no_hp'];
        if ($no_hp[0] === '0') {
            $no_hp = substr_replace($no_hp, '62', 0, 1);
        } else {
            $no_hp = '62'.$no_hp;
        }
        $request['no_hp'] = $no_hp;

        if(!$this->pengajuan_terdahulu($request['email'])){
            return response_json([
                'Pengajuan anda sebelumnya masih dalam proses. Mohon cek di halaman tracking Anda. (Link telah kami kirim ke WA/Email)'
            ], 422);
        }

        /**
         * Generate uuid
         */
        $uid = uuid();

        $link_tracking = base_url().'public/home/tracking/'.$uid;

        $ref = isset($_GET['ref']) ? $_GET['ref'] : '';
        if($ref){
            $sales = $this->db->query("SELECT * FROM sales WHERE kode_referal = '$ref'")->result_array();
            if(!empty($sales)){
                $sales = @$sales[0];
                $data['ref'] = $ref;
            }
        }
        
        if(!isset($sales) || empty($sales)){
            $sales = $this->sales_near_by_nasabah(
                $request['kelurahan_nasabah'],
                $request['kecamatan_nasabah'],
                $request['kota_nasabah'],
                $request['provinsi_nasabah'],
                $id_product,
                $id_product_type
            );
        }
        

        $_d = [
            'nama'       => $request['nama_lengkap'],
            'nik'        => $request['nik'],
            'no_tlp'     => $request['no_hp'],
            'email'      => $request['email'],
            'id_product' => $id_product,
            'status'     => 1,
            'link_tracking' => $link_tracking,
            'provinsi_id' => $request['provinsi_nasabah'],
            'kabupaten_id' => $request['kota_nasabah'],
            'kecamatan_id' => $request['kecamatan_nasabah'],
            'kelurahan_id' => $request['kelurahan_nasabah'],
            'kode_pos' => $request['kodepos_nasabah'],
            'alamat_lengkap' => $request['alamat_lengkap_nasabah'],
            'alamat_sama_dengan_ktp' => $is_same_ktp,
            'provinsi_ktp' => !$is_same_ktp ? $request['provinsi_optional'] : $request['provinsi_nasabah'],
            'kabupaten_ktp' => !$is_same_ktp ? $request['kota_optional'] : $request['kota_nasabah'],
            'kecamatan_ktp' => !$is_same_ktp ? $request['kecamatan_optional'] : $request['kecamatan_nasabah'],
            'kelurahan_ktp' => !$is_same_ktp ? $request['kelurahan_optional'] : $request['kelurahan_nasabah'],
            'kode_pos_ktp' => !$is_same_ktp ? $request['kodepos_optional'] : $request['kodepos_nasabah'],
            'alamat_lengkap_ktp' => !$is_same_ktp ? $request['alamat_lengkap_optional'] :  $request['alamat_lengkap_nasabah'],
            'uid' => $uid,
            'cr_time'    => date('Y-m-d H:i:s'),
            'cr_user'    => 'web public',
            'up_time'    => date('Y-m-d H:i:s'),
            'tgl_pengajuan' => date('Y-m-d H:i:s'),
            'sales_id' => $sales['id'],
        ];
        $data = array_merge($data, $_d);
        // map_y($data);

        $this->m->insert($data);

        $id_nasabah = $this->db->insert_id('nasabah_id_seq');

        if($id_nasabah){
            
            $no_hp_sales = $sales['no_telp'];
            $message_sales = "Ada Calon Nasabah baru dengan nama ".$request['nama_lengkap']."/".$request['no_hp'].", Cek selengkapnya di Aplikasi. Terimakasih";
            send_wa($no_hp_sales, $message_sales);
            
            $this->_insertlog('nasabah dengan nama '.$request['nama_lengkap'].' melakukan pengajuan baru produk '. $id_product);
            
            $request['id_nasabah'] = $id_nasabah;
            $data['detail'] = $request;

            $no_hp = $request['no_hp'];
            $email = $request['email'];

            $nama_produk = get_name_product($id_product);
            /**
             * Send SMS
             */
            $sms_message = "Terima kasih atas kunjungan anda, Pengajuan ".$nama_produk." Plus anda di BPR Nusamba Cepiring akan segera kami proses. Tunggu proses validasi dari Team Digital Marketing kami. Salam Nusamba Cepiring";
            send_sms($no_hp, $sms_message);
            /**
             * Send WA
             */
            $wa_message = "Terima kasih atas kunjungan anda, Pengajuan ".$nama_produk." Plus anda di BPR Nusamba Cepiring akan segera kami proses. Tunggu proses validasi dari Team Digital Marketing kami. Salam Nusamba Cepiring";
            send_wa($no_hp, $wa_message);
            /**
             * Send Email
             */
            $email_message = "<p style='text-indent: 20px'>Terima kasih atas kunjungan anda, Pengajuan ".$nama_produk." Plus anda di BPR Nusamba Cepiring akan segera kami proses. Tunggu proses validasi dari Team Digital Marketing kami. Salam Nusamba Cepiring</p>";
            $email_message .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$link_tracking."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";

            // echo $email_message;
            // map_y($email_message);

            $data['content'] = $email_message;
            $html_message = $this->load->view('email_template', $data, TRUE);

            // map_y($html_message);
            send_email($email, $html_message);
            
            /**
             * Set Data Redirect
             */
            $data['message'] = $sms_message.'. Silahkan cek WA/Email Anda.';
            $_SESSION['data_redirect'] = $data;
            
            return response_json([
                'msg' => 'Pengajuan Berhasil Dilakukan'
            ],200);
        }
       
    }

     /**
     * Cheking is pengajuan
     */
    public function pengajuan_terdahulu($email){
        $allow = true;
        
        $total = $this->db->query("SELECT count(*) as total FROM nasabah WHERE email = '$email' AND status NOT IN(2,8)")->result_array()[0]['total'];
        // map_y($check);
        if($total > 0){
            $allow = false;
        }
        // map_y($allow);
        return $allow;
    }

    /**
     * Email Validate
     */
    private function is_email($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

        /**
     * Tracking Status Pengajuan
     */
    public function tracking($uid = '') {
        
        // $this->set_theme($this->_theme_vars['active_theme']);
        // $this->_theme_vars['current_page'] = "tracking";
        // $this->_theme_vars['current_theme'] = $this->theme;
        // $this->render_view('view/tracking', '', []);
        // exit;

        if($uid){
            $data['nasabah'] = $this->db->query("SELECT 
                b.id, b.nama, b.sales_id , b.tgl_pengajuan, b.uid, c.name as produk, d.value_status as status_label, e.nama_type_product as produk_type FROM nasabah b
                LEFT JOIN product c ON b.id_product = c.id
                LEFT JOIN product_type e ON c.product_type_id = e.id
                LEFT JOIN master_status d ON b.status = d.id
                WHERE uid = '$uid'")->result_array();
            // $data['nasabah'] = [];
            if($data['nasabah']){
                $data['nasabah'] = $data['nasabah'][0];
                $id_nasabah = $data['nasabah']['id'];
                $data['appointment'] = @$this->db->query("SELECT 
                    b.tanggal_appoiment, b.waktu_appoiment, b.lat, b.lng, b.lokasi, b.no_pertemuan, c.nama_sales, c.id as id_sales FROM appoiment b
                    LEFT JOIN sales c ON b.id_sales = c.id
                    WHERE id_nasabah = '$id_nasabah'")->result_array()[0];
                // $id_sales = $data['appointment']['id_sales'] ? $data['appointment']['id_sales'] : '';

                $id_sales = $data['nasabah']['sales_id'];
                if($id_sales){
                    $get_sales = $this->db->query("SELECT 
                        b.nama_sales, b.no_telp, b.alamat, c.picture, c.email FROM sales b
                        LEFT JOIN aauth_users c ON b.user_id = c.id
                        WHERE b.id = $id_sales")->result_array()[0];
                }

                $data['sales'] = isset($get_sales) ? $get_sales : '';
                // map_y($data['sales']);
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "tracking";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('view/tracking', '', $data);
            }else{
                /**
                 *  Redirect If Error Something
                 */
                $_SESSION['error_msg'] = "Nasabah Tidak Ditemukan";
                redirect('public/home/wow');
            }
        }else{
            /**
             *  Redirect If Error Something
             */
            $_SESSION['error_msg'] = "Terjadi Kesalahan";
            redirect('public/home/wow');
        }
    }
    /**
     * Penagjuan Berhasil
     */
    public function pengajuan_berhasil(){
        $data = isset($_SESSION['data_redirect']) ? $_SESSION['data_redirect'] : '';
        // map_y($data);
        if($data){
            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = "pengajuan_berhasil";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_view('view/pengajuan_berhasil', '', $data);
        }else{
            /**
             *  Redirect If Error Something
             */
            redirect('public/home/wow');
        }

        if(isset($_SESSION['data_redirect'])){
            unset($_SESSION['data_redirect']);
        }
    }

    /**
     * Pertemuan
     */
    public function new_appointment($id_product= null, $id_nasabah = null){
        if ($id_nasabah != null){
            $nasabah = $this->m->getNasabah($id_nasabah);
            // map_y($nasabah);
            if($nasabah['status'] == '3'){
                $nasabah_product = $this->m->getVerifikasi($id_product,$id_nasabah);
                $this->_theme_vars['nasabah_product'] = $nasabah_product;
                $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = $nasabah_product['name'];
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('appointment');
            }else if($nasabah['status'] == '7'){
                $_SESSION['error_msg'] = "Anda telah mengisi formulir pertemuan, silahkan cek halaman status";
                /**
                 *  Redirect If Error Something
                 */
                redirect('public/home/wow');
            }else{
                 $_SESSION['error_msg'] = "Anda telah mengisi formulir pertemuan, silahkan cek halaman status";
                /**
                 *  Redirect If Error Something
                 */
                redirect('public/home/wow');
            }
        }else{
            /**
             *  Redirect If Error Something
             */
            redirect('public/home/wow');
        }
    }


    public function save_data($id = null, $id_nasabah = null)
    {

        if(count($_POST)>0){

            $_POST['id_product'] = $_SESSION['id_product'];
            $_POST['id_nasabah'] = $_SESSION['id_nasabah'];

        // $id_nasabah = $this->db->insert_id('nasabah_id_seq');
        $data = array(
                'nama' => $this->input->post('nama'),
                'nik' => $this->input->post('nik'),
                'no_tlp' => $this->input->post('no_tlp'),
                'email' => $this->input->post('email'),
                'id_product' => $_POST['id_product'],
        );
            // var_dump($data);die();
            $this->m->insert($data);
            
            $this->session->set_flashdata('msg', 
                '<div class="alert alert-success">
                    Data Sudah di ajukan, Tunggu Notiifikasi Lewat Whatsapp Untuk Proses Selanjutnya
                    </div>'
                );

            // redirect('public/home/index');
            redirect('public/home/verifikasi/'.$_SESSION['id_product'].'/'.$id_nasabah);
        }
    // }
    }
    public function save_data_formulir($id_product = null, $id_nasabah = null){
        if (count($_POST) > 0){
            $this->db->delete('data_formulir_nasabah', array('id_nasabah' => $id_nasabah));
            foreach ($_POST as $key => $value) {
                $name = explode("_", $key);
                $input_type = @$name[0];
                $id_field = @$name[1];
                $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();
                if ($input_type === 'text'){
                    $data = array(
                        "id_form_field" => $id_field,
                        "id_header_form" => @$field[0]['form_header_id'],
                        "id_nasabah" => $id_nasabah,
                        "id_product" => $id_product,
                        "input_type" => "text",
                        "value" => $value,
                    );
                    $this->db->insert("data_formulir_nasabah",$data);
                } else if ($input_type === 'checkbox'){
                    $data = array(
                        "id_form_field" => $id_field,
                        "id_header_form" => @$field[0]['form_header_id'],
                        "id_nasabah" => $id_nasabah,
                        "id_product" => $id_product,
                        "input_type" => "checkbox",
                        "value" => $id_field,
                    );

                    $this->db->insert("data_formulir_nasabah",$data);
                }
            }

        }
        $newnasabah['status'] = '5';
        $newnasabah['up_time'] = date('Y-m-d H:i:s');
        $newnasabah['up_user'] = 'web public';
        $this->db->update('nasabah', $newnasabah, array('id' => $id_nasabah));
        $this->_insertlog('nasabah dengan id '.$id_nasabah.' melakukan kelengkapan data pengajuan untuk produk '. $id_product);
        echo json_encode(array("status" => "1", "msg" => "Data berhasil tersimpan!"));
    }
    // function save_data_formulir(){

    //     if(count($_POST)>0){

    //         $keys = array_keys($_POST);
    //         $a = count($keys);

    //         for($i=0;$i < $a;$i++){

    //             $key = $keys[$i];

    //             $x = explode("_",$key);
    //             $input_type = @$x[0];
    //             $id_field = @$x[1];

             

    //             if($input_type=="checkbox"){
                    
    //                 $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();

    //                 $value = $id_field;

    //                 $data = array(
    //                     "id_form_field" => $id_field,
    //                     "id_header_form" => @$field[0]['form_header_id'],
    //                     "id_nasabah" => $_POST['id_nasabah'],
    //                     "id_product" => $_POST['id_product'],
    //                     "input_type" => "checkbox",
    //                     "value" => $value,
    //                 );

    //                 $this->db->insert("data_formulir_nasabah",$data);

    //             }else if($input_type=="text"){

    //                 $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();

    //                 $value = $_POST[$key];

    //                 $data = array(
    //                     "id_form_field" => $id_field,
    //                     "id_header_form" => @$field[0]['form_header_id'],
    //                     "id_nasabah" => $_POST['id_nasabah'],
    //                     "id_product" => $_POST['id_product'],
    //                     "input_type" => "text",
    //                     "value" => $value,
    //                 );
    //                 $this->db->insert("data_formulir_nasabah",$data);
    //                 $this->session->set_flashdata('msg', 
    //                     '<div class="alert alert-success">
    //                         Data Sudah di ajukan, Tunggu Notiifikasi Lewat Whatsapp Untuk Proses Selanjutnya
    //                         </div>'
    //                     );

    //             }
    //             echo $data;
    //             // redirect('public/home/index');


    //         }

    //         echo json_encode(array("status" => "1", "msg" => "Data berhasil tersimpan!"));

    //     }

    // }

    public function appointment($id_product= null, $id_nasabah = null){
        if ($id_nasabah != null){
            $nasabah = $this->m->getNasabah($id_nasabah);
            if ((int)$nasabah['status'] >= 4 && (int)$nasabah['status'] < 6) {
                $wa = $this->m->getVerifikasi($id_product,$id_nasabah);
                $this->_theme_vars['wa'] = $wa;
                $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = $wa['name'];
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('appointment');
            } else if ((int)$nasabah['status'] >= 7){
                $this->session->set_flashdata('msg', 
                    '<div class="alert alert-primary">
                        Anda telah melakukan Appointment,
                    </div>'
                );
                redirect('public/home/verifikasi_gagal/'.$id_product.'/'.$id_nasabah);
            }else {
                $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger">
                        Anda Belum Melakukan Verifikasi Silahkan Lakukan Verifikasi Terlebih dahulu
                    </div>'
                );
                redirect('public/home/verifikasi_gagal/'.$id_product.'/'.$id_nasabah);
            }
        }
    }

    /**
     * Save Appointment
     */
    public function save_appointment($id_nasabah = null){
        // map_x($_POST);
        if (!is_null($id_nasabah)){
            // map_y($id_nasabah);
            /**
             * Sales Terdekat
             */
            $nasabah = $this->db->query("SELECT * FROM nasabah WHERE id = '$id_nasabah'")->row();
            $id_product = $nasabah->id_product;
            $sales_id = $nasabah->sales_id;

            $sales = $this->db->query("SELECT * FROM sales WHERE id = '$sales_id'")->result_array();
            $sales = @$sales[0];
            // map_y($sales);

            $tanggal = $_POST['tanggal_appoiment'];
            $waktu = $_POST['waktu_appoiment'];

            $new_appo['cr_time'] = date('Y-m-d H:i:s');
            $new_appo['no_pertemuan'] = gen_no_pertemuan($id_nasabah, $id_product);
            $new_appo['cr_user'] = "web public";
            $new_appo['up_time'] = date('Y-m-d H:i:s');
            $new_appo['id_nasabah'] = $id_nasabah;
            // $new_appo['kelurahan'] = $_POST['id_kelurahan'];
            $new_appo['lat'] = $_POST['lat'];
            $new_appo['lng'] = $_POST['lng'];
            $new_appo['pihak_lain'] = $_POST['pihak_lain'];
            $new_appo['alamat_pihak_lain'] = $_POST['alamat_pihak_lain'];
            $new_appo['hubungan'] = $_POST['hubungan'];
            $new_appo['tanggal_appoiment'] = $tanggal;
            $new_appo['waktu_appoiment'] = $waktu;
            $new_appo['lokasi'] = $_POST['lokasi'];
            $new_appo['id_sales'] = $sales_id;
            $no_tlp = $_POST['no_telp_pihak_lain'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = '62'.$no_tlp;
            }
            $new_appo['no_telp_pihak_lain'] = $no_tlp;

            // map_y($_POST);
            $insert = $this->db->insert('appoiment', $new_appo);
            if ($insert){
                $_nasabah['status'] = '7';
                $_nasabah['up_time'] = date('Y-m-d H:i:s');
                $_nasabah['up_user'] = 'web public';
                $this->db->update('nasabah', $_nasabah, ['id' => $id_nasabah]);
                /**
                 * log
                 */
                insert_log('nasabah dengan id '.$id_nasabah.' melakukan appointment pengajuan untuk produk '. $id_product.' di website bprnusambacepiring');
                $no_hp_sales = $sales['no_telp'];
                
                // map_y($nasabah);

                $message = "Ada Calon Nasabah baru dengan nama ".$nasabah->nama."/".$nasabah->no_tlp." membuat perjanjian petemuan dengan Anda, Cek selengkapnya di Aplikasi. Terimakasih";
                send_sms($no_hp_sales, $message);
                send_wa($no_hp_sales, $message);

                /**
                 * Send Email
                 */
                $email_message = "<p style='text-indent: 20px'>Selamat Anda telah membuat janji untuk bertemu dengan team kami. Jangan lupa tanggal nya. Ingat baik baik Ya !</p>";
                $email_message .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$nasabah->link_tracking."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";  

                $data['content'] = $email_message;
                $html_message = $this->load->view('email_template', $data, TRUE);

                send_email($nasabah->email, $html_message);

                $res['status'] = 1;
                $res['link_tracking'] = $nasabah->link_tracking; 
                $res['message'] = 'Jadwal Pertemuan Berhasil dikirim';
            } else {
                $res['status'] = 0;
                $res['message'] = 'Terjadi Kesalahan';
                
            }
            return response_json($res, 200);
        }
        $res['status'] = 0;
        $res['message'] = 'Param Null';
        return response_json($res, 200);
    }
    /**
     * Sales Terdekat
     */
    public function sales_terdekat($kelurahan_id, $id_product = 25, $product_type = 8){
        /**
         * Get Jabatan Configure
         */
        $id_jabatan = $this->db->query("SELECT * FROM conf_produk_jabatan where id_produk = $product_type LIMIT 1")->row()->id_jabatan;
        // map_y($id_jabatan);
        /**
         * Get Sales By Jabatan
         */
        // $get_sales = $this->db->query("SELECT * FROM sales WHERE id_jabatan = $id_jabatan")->result_array();

        // if(count($get_sales) == 0){
        //     return $this->m->getsales();
        // }

        // $_total_appointment_sales = [];
        
        // foreach($get_sales as $sales){
        //     $id_sales = $sales['id'];
        //     $total = $this->db->query("SELECT id FROM appoiment WHERE kelurahan = $kelurahan_id AND id_sales = $id_sales ")->result_array();
        //     $total = count($total);
        //     $_total_appointment_sales[$id_sales] = $total;
        // }
        // /**
        //  * Get Sales Lowest And Kelurahan
        //  */
        // // $_total_appointment_sales[] = 3;
        // // $_total_appointment_sales[] = 2;
        // // map_y($_total_appointment_sales);
        // $id_sales = array_keys($_total_appointment_sales, min($_total_appointment_sales));
        // $id_sales = isset($id_sales) ? $id_sales : '';
        // // return $id_sales;
        // /**
        //  * If null Randomly sales
        //  */
        // if($id_sales){
        //     $id_sales = $id_sales[0];
        //     return $this->db->query("SELECT * FROM sales WHERE id = $id_sales")->result_array()[0];
        // }else{
        //     return $this->m->getsales();
        // }
        
        if(count($get_appointment) > 0){
    	    $min = array_reduce($get_appointment, function($min, $details) {
	
                return min($min, $details['total_appoiment']);
            	
            }, PHP_INT_MAX);
            
            $get = array_filter($get_appointment, function($val, $i) use ($min) {
                return $val['total_appoiment'] == $min;
            }, ARRAY_FILTER_USE_BOTH);
            $get = array_values($get);
    	
    	    $get = $get[0];
    	    
    	    return $this->db->query("SELECT * FROM sales WHERE id = $id_sales")->result_array()[0];
    	}else{
            return $this->m->getsales();
    	}

    }

    /**
     * Get Sales
     * Rule: 
     *  1. Sales terdekat berdasarkan kelurahan <> kecamatan <> kabupaten <> provinsi
     *     Lalu diambil sales dengan total nasabah terkecil
     *  2. Jika diluar kondisi pertama di Random (Acak)
     */
    public function sales_near_by_nasabah($kelurahan_id = '31486', $kecamatan_id = '2572', $kabkot_id = '188', $provinsi_id = '13', $id_product = 25, $product_type = 5){
        /**
         * Get Jabatan Configure
         */
        $id_jabatan = $this->db->query("SELECT * FROM conf_produk_jabatan where id_produk = $product_type LIMIT 1")->row()->id_jabatan;
        
        $get_sales_by_wilayah = $this->db->query("SELECT a.id FROM sales a 
        LEFT JOIN wilayah_sales b ON b.id_sales = a.id
        WHERE (b.id_kelurahan = '$kelurahan_id' OR b.id_kecamatan = '$kecamatan_id' OR b.id_kabkot = '$kabkot_id' OR b.id_provinsi = '$provinsi_id')"
        )->result_array();

        
        
        // map_y($total_nasabah_by_sales);
        if(count($get_sales_by_wilayah) > 0){
            $get_sales_by_wilayah = '['.map_id($get_sales_by_wilayah).']';
            $get_sales_by_wilayah = json_decode($get_sales_by_wilayah);
            $get_sales_by_wilayah = array_unique($get_sales_by_wilayah);
            $get_sales_by_wilayah = implode(',', $get_sales_by_wilayah);

            $total_nasabah_by_sales = $this->db->query("SELECT a.id as id, a.nama_sales, COUNT(b.id) as total_nasabah
            FROM sales a 
            LEFT JOIN nasabah b ON b.sales_id = a.id
            LEFT JOIN wilayah_sales c ON c.id_sales = a.id
            WHERE a.id_jabatan = '$id_jabatan'
            AND a.id IN ($get_sales_by_wilayah)
            GROUP BY a.id")->result_array();

    	    $min = array_reduce($total_nasabah_by_sales, function($min, $details) {
	
                return min($min, $details['total_nasabah']);
            	
            }, PHP_INT_MAX);
            
            $get = array_filter($total_nasabah_by_sales, function($val, $i) use ($min) {
                return $val['total_nasabah'] == $min;
            }, ARRAY_FILTER_USE_BOTH);
            $get = array_values($get);
    	
            $sales_get = $get[0];
            $id_sales = $sales_get['id'];
    	    // map_y($get);
    	    $sales = $this->db->query("SELECT * FROM sales WHERE id = $id_sales")->result_array()[0];
    	}else{
            $sales = $this->m->getsales();
        }
        
        // map_y($sales);
        return $sales;
    }

    private function _insertlog($definition){
        $data = array (
            'definition' => $definition,
            'cr_time' => date('Y-m-d H:i:s'),
            'up_time' => date('Y-m-d H:i:s'),
            'cr_user' => 'web public',
        );
        $insert = $this->db->insert('trans_log', $data);
    }

    function detail_($id = null, $id_type_product = null){
        /**
         * Save current url
         */
        $_SESSION['current_url'] = current_url();

        if($id && $id_type_product){ 
           
            $_profil = $this->m->getprofil();
            $_kontak = $this->m->getkontak();
            $_slide = $this->m->getslide()[0];
            $_detail = $this->m->detail_product($id)[0];
         
            $this->_theme_vars['_slide'] = $_slide;
            $this->_theme_vars['_profil'] = $_profil;
            $this->_theme_vars['_kontak'] = $_kontak;
            $this->_theme_vars['_detail'] = $_detail;
        
            $this->set_theme($this->_theme_vars['active_theme']);
            $this->_theme_vars['current_page'] = "home";
            $this->_theme_vars['current_theme'] = $this->theme;

            $this->render_view('detail');

        }
    }


    public function lamaran() {
        // map_y($_FILES);
        // $_profil = $this->m->getprofil();
        // $_kontak = $this->m->getkontak();

        // $this->_theme_vars['_profil'] = $_profil;
        // $this->_theme_vars['_kontak'] = $_kontak;

        // $this->set_theme($this->_theme_vars['active_theme']);
        // $this->_theme_vars['current_page'] = "pengajuan_berhasil";
        // $this->_theme_vars['current_theme'] = $this->theme;
        // $this->render_view('view/pengajuan_berhasil');

        if(count($_POST)>0){
            // $_POST['id_product'] = $_SESSION['id_product'];
            $no_hp = $_POST['no_hp'];
            if ($no_hp[0] === '0') {
                $no_hp = substr_replace($no_hp, '62', 0, 1);
            } else {
                $no_hp = $no_hp;
            }
            $_POST['no_hp'] = $no_hp;
        
            $data = array(
                'nama' => $this->input->post('nama'),
                'nohp' => $_POST['no_hp'],
                'email' => $this->input->post('email'),
                'id_career' => $this->input->post('id_career'),
                'pesan' => $this->input->post('pesan'),
                'alamat' => $this->input->post('alamat'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 'web public',
            );
            if (!empty($_FILES['foto']['name'])) {
                // if( $_FILES["foto"]["type"]=="application/pdf"){
                if( $_FILES["foto"]["type"]=="image/png" or
                    $_FILES["foto"]["type"]=="image/jpg" or
                    $_FILES["foto"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['foto']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['foto'] = $photo; 
                            $data['foto'] = $_POST['foto']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            }
            if (!empty($_FILES['cv']['name'])) {
                if( $_FILES["cv"]["type"]=="application/pdf"){
                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/cv/';
                    $array = explode('.', $_FILES['cv']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["cv"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['cv'] = $photo; 
                            $data['cv'] = $_POST['cv']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            
            }
            if (!empty($_FILES['ijazah']['name'])) {
                if( $_FILES["ijazah"]["type"]=="application/pdf"){
                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['ijazah']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["ijazah"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['ijazah'] = $photo; 
                            $data['ijazah'] = $_POST['ijazah']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            
            }
            if (!empty($_FILES['transkrip']['name'])) {
               if( $_FILES["transkrip"]["type"]=="application/pdf"){
                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['transkrip']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["transkrip"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['transkrip'] = $photo; 
                            $data['transkrip'] = $_POST['transkrip']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            }
            if (!empty($_FILES['ktp']['name'])) {
               if( $_FILES["ktp"]["type"]=="application/pdf"){
                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['ktp']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["ktp"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['ktp'] = $photo; 
                            $data['ktp'] = $_POST['ktp']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            
            }
            if (!empty($_FILES['kk']['name'])) {
               if( $_FILES["kk"]["type"]=="application/pdf"){
                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['kk']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["kk"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['kk'] = $photo; 
                            $data['kk'] = $_POST['kk']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            
            }

            if (!empty($_FILES['skck']['name'])) {
               if( $_FILES["skck"]["type"]=="application/pdf"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['skck']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["skck"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['skck'] = $photo; 
                            $data['skck'] = $_POST['skck']  ;
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }
                }
            }
           
            $this->m->insert_pelamar($data);

        
            $this->_insertlog('pelamar dengan nama '.$_POST['nama'].'mengirimkan cv');
            $message = "<div class='text-center'> Lamaran anda sudah terkirim, Tunggu balasan selanjutnya untuk informasi lebih lanjut.</div> ";
            if( $data){
                $data['message'] = $message;
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "pengajuan_berhasil";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
            else{
                $data['message'] = "<div class='text-center'> Maaf Pesan Tidak Terkirim</div> ";
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
            // redirect('public/home/index');
            // redirect('public/home/verifikasi/'.$_SESSION['id_product'].'/'.$id_nasabah);
        }
    }

    public function pesan() {
    //    map_y($_POST);
        if(count($_POST)>0){
            // $_POST['id_product'] = $_SESSION['id_product'];
           
            $data = array(
                'namalengkap' => $this->input->post('namalengkap'),     
                'email' => $this->input->post('email'),
                'no_hp' => $this->input->post('no_hp'),
                'pesan' => $this->input->post('pesan'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 'web public',
            );

            $this->m->insert_pesan($data);
            $this->_insertlog('pesan atas nama '.$_POST['namalengkap']);
            $message = "<div class='text-center'> Pesan anda sudah terkirim, terimakasih telah berkunjung.</div> ";
            if( $data){
                $data['message'] = $message;
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "pengajuan_berhasil";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
            else{
                $data['message'] = "<div class='text-center'> Maaf Pesan Tidak Terkirim</div> ";
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
        }
    }

    public function restrukturisasi(){
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "restrukturisasi";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('restrukturisasi');
    }

    public function add_restrukturisasi() {
        //    map_y($_POST);
        if(count($_POST)>0){
            // $_POST['id_product'] = $_SESSION['id_product'];
            $no_hp = $_POST['no_hp'];
            if ($no_hp[0] === '0') {
                $no_hp = substr_replace($no_hp, '62', 0, 1);
            } else {
                $no_hp = '62'.$no_hp;
            }
            $data = array(
                'nama' => $this->input->post('nama'), 
                'norek1' => $this->input->post('norek1'),    
                'no_hp' => $no_hp,
                'alasan' => $this->input->post('alasan'),
                'alamat' => $this->input->post('alamat'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 'web public',
            );
            
            $query = $this->db->insert('front_restrukturisasi', $data);
        
            $this->_insertlog('restrukturisasi atas nama '.$_POST['nama']);
            $message = "<div class='text-center'> Restrukturisasi anda sudah terkirim, terimakasih telah berkunjung.</div> ";
            if( $query){
                $data['message'] = $message;
                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "pengajuan_berhasil";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
            else{
                $data['message'] = "<div class='text-center'> Maaf Pesan Tidak Terkirim</div> ";
                $this->render_view('view/pengajuan_berhasil','',$data);
            }
        }
    }

    public function cron_reminder_telat_bayar($status){
        
        $client = new GuzzleHttp\Client([
            'base_uri' => 'http://srv.co.id:9020/',
        ]);
        
        $response = $client->request('POST', 'test/send_email',[
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            GuzzleHttp\RequestOptions::JSON => [
                'type' => $status,
            ]
        
        ]);
    
        $results = $response->getBody()->getContents();
        
        $data = array(
            'created_at' => date('Y-m-d H:i:s'),
            'type' => 'email',
            'message' => $results,
        );
        $this->db->insert('c_cron_log', $data);

        
    }

    public function cron_reminder_menuju_bayar($status){
        $clienta = new GuzzleHttp\Client([
            'base_uri' => 'http://srv.co.id:9020/',
        ]);
        
        $responsea = $clienta->request('POST', 'test/send_wa',[
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            GuzzleHttp\RequestOptions::JSON => [
                'type' => $status,
            ]
        
        ]);
    
        $resultsa = $responsea->getBody()->getContents();
        
        $dataa = array(
            'created_at' => date('Y-m-d H:i:s'),
            'type' => 'wa',
            'message' => $resultsa,
        );
        $this->db->insert('c_cron_log', $dataa);
    }

    public function test_send_email(){
        // $CI =& get_instance();
        $email = 'sttadik@gmail.com'; 
        $message = 'aa'; 
        $subject = 'BPR Nusamba Cepiring';
        $client = new GuzzleHttp\Client([
            'base_uri' => URL_MAILAPI,
        ]);
        
      
       $response = $client->post('v1/send_email', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'api-key' => 'YUNNA-2019-ARGAN',
                    'mailjet_user' => 'de9a205b2f545f77b2ece89f48919d10',
                    'mailjet_pass' => 'a3059183f502963d44a6798ca515ceee'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    "from" => [
                        "email" => "noreply@bprnusambacepiring.com",
                        "name" => "BPR Nusamba Cepiring"
                    ],
                    "to" => [
                        "email" => $email,
                        "name" => ""
                    ],
                    "subject" => $subject,
                    "textpart" => "",
                    "html" => $message,
                    "customid" => "sendMail"
                ]
            ]);
    
           
            $results = $response->getBody()->getContents();
            
            $dataa = array(
                'created_at' => date('Y-m-d H:i:s'),
                'type' => 'waaaa',
                'message' => $results,
            );
            $this->db->insert('c_cron_log', $dataa);
    }

    public function print_surattugas($id){
        if($id){
            $nasabah = $this->m->get_detail_surat_tugas($id);
            // map_y($nasabah);
            $tgl = $nasabah['tgl_penagihan']; 
            $hari = date_format(new DateTime($tgl),"D");
            $data['bulanan'] = format_rupiah($nasabah['bayar_bulan']); 
            $data['total_sisa_tagihan'] = format_rupiah($nasabah['total_sisa_tagihan']); 
            $data['tgl_penagihan'] = tgl_indo($nasabah['tgl_penagihan']);
            $data['hari_penagihan'] = hari_indo($hari);
          
            $data['nasabah'] = $nasabah;
            $this->load->view('v_pdf_surat_tugas',$data);
        }   
    }

    public function download_surattugas($id){
        if($id){
            $nasabah = $this->m->get_detail_surat_tugas($id);
            // map_y($nasabah);
            $tgl = $nasabah['tgl_penagihan']; 
            $hari = date_format(new DateTime($tgl),"D");
            $data['bulanan'] = format_rupiah($nasabah['bayar_bulan']); 
            $data['total_sisa_tagihan'] = format_rupiah($nasabah['total_sisa_tagihan']); 
            $data['tgl_penagihan'] = tgl_indo($nasabah['tgl_penagihan']);
            $data['hari_penagihan'] = hari_indo($hari);
          
            $data['nasabah'] = $nasabah;
            $html = $this->load->view('v_pdf_surat_tugas', $data, TRUE);
            return print_pdf('surat_tugas', $html);
        }   
    }

    public function core_value(){
        $_corevalue = $this->m->getCorevalue();
        $this->_theme_vars['_corevalue'] = $_corevalue;
       
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "core-value";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('core-value');
        
    }

    function get_halaman($get){
        $front_halaman = $this->db->query("SELECT * FROM front_halaman WHERE halaman = '$get' ORDER BY id ASC ")->result_array(); 
        $this->_theme_vars['front_halaman'] = $front_halaman;
        return $front_halaman;
    }

    public function selamatdatang(){ 
        $v_halaman = $this->get_halaman('selamat-datang'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "selamatdatang_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/selamatdatang_view');
    }

    
    public function sejarah(){ 
        
        $v_halaman = $this->get_halaman('sejarah'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "sejarah_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/sejarah_view');
    }

    public function visimisi(){ 
        $v_halaman = $this->get_halaman('visi-misi'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "visimisi_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/visimisi_view');
    }
	
    
    public function identitasperusahaan(){ 
        $v_halaman = $this->get_halaman('identitas-perusahaan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "identitasperusahaan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/identitasperusahaan_view');
    }

    
    public function strukturorganisasi(){ 
        $v_halaman = $this->get_halaman('struktur-organisasi'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "strukturorganisasi+";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/strukturorganisasi_view');
    }

    
    public function budayaperusahaan(){ 
        $v_halaman = $this->get_halaman('budaya-perusahaan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "budayaperusahaan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/budayaperusahaan_view');
    }

    
    public function kodeetikperusahaan(){ 
        $v_halaman = $this->get_halaman('kodeetik-perusahaan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "kodeetikperusahaan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/kodeetikperusahaan_view');
    }

    
    public function penghargaan(){ 
        $v_halaman = $this->get_halaman('penghargaan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "penghargaan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/penghargaan_view');
    }

    public function mengapamemilihkami(){ 
        $v_halaman = $this->get_halaman('mengapa-memilihkami'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $_pengawas = $this->m->get_badan_pengawas();
        $this->_theme_vars['_pengawas'] = $_pengawas;
        
        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "mengapamemilihkami_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/mengapamemilihkami_view');
    }

    public function whistleblowingsystem(){  
        $v_halaman = $this->get_halaman('whistle-blowingsystem'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "whistleblowingsystem_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/whistleblowingsystem_view');
    }

    
    public function management(){  
        $v_halaman = $this->get_halaman('management'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "management_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/management_view');
    }

    public function dewankomisaris(){  
        $v_halaman = $this->get_halaman('dewan-komisaris'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "dewankomisaris_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/dewankomisaris_view');
    }

    
    public function direksi(){  
        $v_halaman = $this->get_halaman('direksi'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "direksi_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/direksi_view');
    }

    
    public function komiteperusahaan(){  
        $v_halaman = $this->get_halaman('komite-perusahaan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "komiteperusahaan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/komiteperusahaan_view');
    }


    public function kantorpusat(){  
        $v_halaman = $this->get_halaman('kantor-pusat'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "kantorpusat_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/kantorpusat_view');
    }

    public function kantorcabang(){  
        $v_halaman = $this->get_halaman('kantor-cabang'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "kantorcabang_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/kantorcabang_view');
    }

    
    public function lokasiatm(){  
        $v_halaman = $this->get_halaman('lokasi-atm'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "lokasiatm_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/lokasiatm_view');
    }

    public function jaringanatm(){  
        $v_halaman = $this->get_halaman('jaringan-atm'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "jaringanatm_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/jaringanatm_view');
    }

    
    public function smsbanking(){  
        $v_halaman = $this->get_halaman('sms-banking'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "smsbanking_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/smsbanking_view');
    } 
    
    public function edc(){  
        $v_halaman = $this->get_halaman('edc'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "edc_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/edc_view');
    }

    public function mobilebanking(){  
        $v_halaman = $this->get_halaman('mobile-banking'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "mobilebanking_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/mobilebanking_view');
    }

    
    public function syaratketentuan(){  
        $v_halaman = $this->get_halaman('syarat-ketentuan'); 
        $this->_theme_vars['v_halaman'] = $v_halaman;

        $this->set_theme($this->_theme_vars['active_theme']);
        $this->_theme_vars['current_page'] = "syaratketentuan_view";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->render_view('ekapage/syaratketentuan_view');
    }

      public function preview($id = null,$permalink = null){ 
        
        if($id != null && $permalink !=null){
            
            $page_contents = $this->db->get_where('page_contents', array('id' => $id))->result_array();

            if($page_contents != null){

                $page = $this->db->get_where('pages', array('id' => @$page_contents[0]['page_id']))->result_array();
                $this->_theme_vars['page_content'] = @$page_contents[0]['content'];
                $this->_theme_vars['page_title'] = @$page[0]['title'];

                $this->set_theme($this->_theme_vars['active_theme']);
                $this->_theme_vars['current_page'] = "preview";
                $this->_theme_vars['current_theme'] = $this->theme;
                $this->render_view('page/preview');
                
            }
            
        }
       
    }

}
	

    