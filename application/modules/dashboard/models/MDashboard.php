<?php


class MDashboard extends CI_Model{

    function __construct(){

        parent::__construct();


    }

    /* get all published article count */
    function getEventCount(){

        $sql = "SELECT * FROM events WHERE status = '1'";
        $query = $this->db->query($sql)->num_rows();
        return $query;

    }

    /* get all upcoming event stat count */
    function getUpcomingEventStatCount(){

        $sql = "SELECT * FROM events WHERE status='1' AND start_date > current_date";
        $query = $this->db->query($sql)->num_rows();
        return $query;

    }

     function getUpcomingEvent(){

        $sql = "SELECT * FROM events WHERE status='1' AND start_date > current_date";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    /* get all Active Ads count */
    function getMemberCount(){

        $sql = "SELECT * FROM members";
        $query = $this->db->query($sql)->num_rows();
        return $query;

    }

    /* get all registration member count */
    function getEventMemberStatCount(){

        $sql = "SELECT * FROM event_members";
        $query = $this->db->query($sql)->num_rows();
        return $query;


    }

    /* get all subscribers count */
    function getEventRegistrationCount(){

        $sql = "SELECT * FROM event_registrations";
        $query = $this->db->query($sql)->num_rows();
        return $query;


    }

    /* get all top view article */
    function getTopViewArticle(){

        $sql = "SELECT a.*, concat(b.first_name,' ',b.last_name) as author FROM mgz_articles a 
                LEFT JOIN aauth_users b ON b.id = a.created_by 
                WHERE a.status = '2' ORDER BY page_view DESC LIMIT 5";
        $query = $this->db->query($sql)->result_array();
        return $query;


    }

    /* get all top comment article */
    function getTopCommentArticle(){

        $sql = "SELECT a.*, concat(b.first_name,' ',b.last_name) as author FROM mgz_articles a 
                LEFT JOIN aauth_users b ON b.id = a.created_by 
                WHERE a.status = '2' ORDER BY comment_count DESC LIMIT 5";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    /* get 5 upcoming event */
    function getLastUpcomingEvent(){

        $sql = "SELECT a.*, concat(b.first_name,' ',b.last_name) as author FROM events a 
                LEFT JOIN aauth_users b ON b.id = a.created_by 
                WHERE a.status='1' AND start_date > current_date LIMIT 5";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    /* get 5 top upcoming event by member count */
    function getTopUpcomingEventByMemberCount(){

        $sql = "SELECT a.*,concat(b.first_name,' ',b.last_name) as author,(SELECT COUNT(*) FROM event_members WHERE event = a.event_id) as member_count  FROM events a 
                LEFT JOIN aauth_users b ON b.id = a.created_by 
                WHERE a.start_date > current_date ORDER BY member_count DESC LIMIT 5";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    function getEventDateOnCurrentMonth(){

        $sql = "SELECT start_date FROM events 
                WHERE status='1' AND  date_trunc('month',start_date)::date = date_trunc('month',current_date )::date";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $event){

            $date = date_create($event['start_date']);

            $dates[] = date_format($date,"d/n/Y");

        }

        if(!isset($dates)){
            $dates = array();
        }

        return $dates;
    }

    function getEventOnCurrentMonth(){

        $sql = "SELECT * FROM events 
                WHERE status='1' AND  date_trunc('month',start_date)::date = date_trunc('month',current_date )::date";
        $query = $this->db->query($sql)->result_array();

        return $query;
    }

    function getCurrentYearVisitorStat($month){

        $sql = "SELECT a.* FROM visitor_stats a WHERE date_trunc('month',a.date )::date = '".$month."' AND date_trunc('year',a.date )::date = date_trunc('year',current_date )::date";
        $query = $this->db->query($sql)->num_rows();

        return $query;

    }

    function getgetVisitorStatByCity(){

        $sql = "SELECT a.* FROM visitor_stats a WHERE date_trunc('year',a.date )::date = date_trunc('year',current_date )::date";
        $query = $this->db->query($sql)->result_array();


        if(count($query)>0){

            foreach ($query as $visitor){

                $data[] = array( "latLng" => array($visitor['latitude'],$visitor['longitude']), "name" => $visitor['city']);

            }

            return $data;

        }else{

            return array();
        }


    }

    function getLatestRegisteredUsers(){

        $sql = "SELECT * FROM aauth_users ORDER BY date_created DESC LIMIT 8";
        $query = $this->db->query($sql)->result_array();

        return $query;

    }

    function getWidget($section){

        $sql = "SELECT * FROM widgets WHERE section='".$section."' ORDER BY position";
        $query = $this->db->query($sql)->result_array();

        return $query;

    }

    function getSubByTypeProduct($time_range){
        $TR = '';
        if ($time_range > 1) {
            $TR .= " AND n.cr_time >= '".$time_range['start']."' ";
            $TR .= " AND n.cr_time <= '".$time_range['end']."' ";
        }
        $sql = "SELECT pt.nama_type_product, 
                COUNT (CASE WHEN n.id <> 0 $TR THEN 1 ELSE null END)from nasabah n 
                LEFT JOIN product p ON n.id_product = p.id 
                RIGHT OUTER JOIN product_type pt ON p.product_type_id = pt.id 
                group by pt.nama_type_product";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    function getSubByProduct($time_range = null){
        $TR = '';
        if ($time_range > 1) {
            $TR .= " AND n.cr_time >= '".$time_range['start']."' ";
            $TR .= " AND n.cr_time <= '".$time_range['end']."' ";
        }
        $sql = "SELECT pt.nama_type_product, p.name,
                COUNT (CASE WHEN n.id <> 0 $TR THEN 1 ELSE null END)from nasabah n 
                LEFT JOIN product p ON n.id_product = p.id 
                RIGHT OUTER JOIN product_type pt ON p.product_type_id = pt.id 
                group by pt.nama_type_product, p.name";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    function getSubByStatus($time_range =  null){
        $TR = '';
        if ($time_range > 1) {
            $TR .= " AND n.cr_time >= '".$time_range['start']."' ";
            $TR .= " AND n.cr_time <= '".$time_range['end']."' ";
        }
        $sql = "SELECT s.value_status, 
                COUNT (CASE WHEN n.id <> 0 $TR THEN 1 ELSE null END)from nasabah n 
                RIGHT JOIN master_status s ON n.status = s.id 
                
                group by s.value_status";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    function getSubBySex($time_range = null){
        $TR = '';
        if ($time_range > 1) {
            $TR .= " AND dn.cr_time >= '".$time_range['start']."' ";
            $TR .= " AND dn.cr_time <= '".$time_range['end']."' ";
        }
        $sql = "SELECT dn.jenis_kelamin, 
                COUNT (CASE WHEN dn.id <> 0 $TR THEN 1 ELSE null END)from detail_nasabah dn 
                --RIGHT JOIN master_status s ON n.status = s.id 
                
                group by dn.jenis_kelamin";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    function getSubByJob($time_range = null){
        $TR = '';
        if ($time_range > 1) {
            $TR .= " AND dn.cr_time >= '".$time_range['start']."' ";
            $TR .= " AND dn.cr_time <= '".$time_range['end']."' ";
        }
        $sql = "SELECT dn.pekerjaan, 
                COUNT (CASE WHEN dn.id <> 0 $TR THEN 1 ELSE null END)from detail_nasabah dn 
                --RIGHT JOIN master_status s ON n.status = s.id 
                
                group by dn.pekerjaan";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
}