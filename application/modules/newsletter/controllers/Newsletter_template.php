<?php
/**
* Created by PhpStorm.
* User: indra
* Date: 29/03/17
* Time: 9:04
*/
class Newsletter_template extends MX_Controller{

	function __construct()
	{
		parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('googlemaps');
        $this->load->model("mcore");
        $this->load->model("MNewsletterTemplate");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'newsletter_template_view';
            $this->add_perm = 'newsletter_template_add';
            $this->edit_perm = 'newsletter_template_update';
            $this->delete_perm = 'newsletter_template_delete';
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "newsletter_templates";
        $this->dttModel = "MDNewsletterTemplate";
        $this->pk = "newsletter_template_id";
	}
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
	function index(){

        $breadcrumb = array(
            "<i class='fa fa-rss-square'></i> Newsletter Template " => base_url()."newsletter/newsletter_template"
        );

        $data['page'] = 'newsletter_template';
        $data['page_title'] = 'Newsletter';
        $data['page_subtitle'] = 'Newsletter template';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'newsletter_management';
        $data['data']['submenu'] = 'newsletter_template';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $this->load->view('layout/body',$data);

	}
	public function dataTable() {
        
        $model = array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk);

        $this->load->library('Datatable', $model);
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    function addnew(){

    	if(count($_POST)>0){


    		$data = array(
                "title" => $this->input->post("title"),
                "content" => $this->input->post("content"),
                "status" => $this->input->post("status")
    		);
			$result = $this->db->insert($this->table, $data);

			if($result){
				$respon = array("status" => "1", "msg" => "Successfully add data!");
			}else{
				$respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
			}

			echo json_encode($respon);
			
    	}else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Newsletter Template" => base_url()."newsletter/newsletter_template",
                "Add New" => base_url()."newsletter/newsletter_template/addnew",
            );

            $data['page'] = 'newsletter_template_add';
            $data['page_title'] = 'Newsletter Template';
            $data['page_subtitle'] = 'Add New Newsletter Template';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
            );

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'newsletter_template';
            $this->load->view('layout/body',$data);

    	}

    }
    
    function edit($id){

    	if(count($_POST)>0){

            $data = array(
                "title" => $this->input->post("title"),
                "content" => $this->input->post("content"),
                "status" => $this->input->post("status")
            );

    		$this->db->where($this->pk, $id);
			$result = $this->db->update($this->table, $data);
			if($result){
				$respon = array("status" => "1", "msg" => "Successfully update data!");
			}else{
				$respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
			}

			echo json_encode($respon);

    	}else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Newsletter Template" => base_url()."newsletter/newsletter_template",
                "Edit" => base_url()."newsletter/newsletter_template/edit/".$id,
            );

            $data['page'] = 'newsletter_template_edit';
            $data['page_title'] = 'Newsletter Template';
            $data['page_subtitle'] = 'Edit Newsletter Template';
            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
            );
            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['dataedit'] = $this->MNewsletterTemplate->getNewsletterTemplateById($id);
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'newsletter_template';
            $this->load->view('layout/body',$data);

    	}

    }
    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }
    
}