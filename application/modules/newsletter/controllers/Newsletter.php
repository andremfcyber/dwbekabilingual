<?php
/**
* Created by PhpStorm.
* User: indra
* Date: 29/03/17
* Time: 9:04
*/
class Newsletter extends MX_Controller{

	function __construct()
	{
		parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("MNewsletter");
        $this->load->model("MNewsletterTemplate");
        $this->load->model("MSubscriber");

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }else{

            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
            $this->read_perm = 'newsletter_view';
            $this->add_perm = 'newsletter_add';
            $this->edit_perm = 'newsletter_update';
            $this->delete_perm = 'newsletter_delete';
            $this->blast_perm = 'newsletter_blast'; //specific only in this controller module
            $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        }

        $this->table = "newsletters";
        $this->dttModel = "MDNewsletter";
        $this->pk = "newsletter_id";

	}
    function createBreadcrumb($data){

        $keys = array_keys($data);
        $a = count($keys);

        $html = "";

        for($i=0;$i < $a; $i++){

            if(($a-1)==$i){
                $class="active";
            }else{
                $class="";
            }

            $html.= "<li ".$class."><a href='".$data[$keys[$i]]."'>".$keys[$i]."</a></li>";

        }

        return $html;

    }
	function index(){

        $breadcrumb = array(
            "<i class='fa fa-rss-square'></i> Newsletter " => base_url()."newsletter/newsletter"
        );

        $data['page'] = 'newsletter';
        $data['page_title'] = 'Newsletter';
        $data['page_subtitle'] = 'Manage Newsletter';
        $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
        $data['data']['parent_menu'] = 'newsletter_management';
        $data['data']['submenu'] = 'newsletter';
        $data['data']['add_perm'] = $this->mcore->checkPermission($this->user_group, $this->add_perm);
        $data['data']['edit_perm'] = $this->mcore->checkPermission($this->user_group, $this->edit_perm);
        $data['data']['delete_perm'] = $this->mcore->checkPermission($this->user_group, $this->delete_perm);
        $data['data']['blast_perm'] = $this->mcore->checkPermission($this->user_group, $this->blast_perm); //specific only in this controller module
        $this->load->view('layout/body',$data);

	}
	public function dataTable() {
        $model = array('model' => $this->dttModel, 'rowIdCol' => 'a.'.$this->pk);

        $this->load->library('Datatable', $model);
        $json = $this->datatable->datatableJson();

        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    function get_data($id=null){
        if($id!=null){
            $data = $this->MNewsletter->getNewsletterById($id);
            if(count($data)> 0){
                echo json_encode(array("status" => 1, "id" => $data['newsletter_id'], "content" => $data['content'], "subscriber" => $data['subscriber']));
            }else{
                echo json_encode(array("status" => 0,"content" => "No Data Found."));
            }
        }
    }
    function addnew(){

    	if(count($_POST)>0){

    		$data = array(
                "title" => $this->input->post("title"),
                "content" => $this->input->post("content"),
                "status" => $this->input->post("status"),
                "success" => 0,
                "failed" => 0,
    			"subscriber" => implode(",",$this->input->post("subscriber"))
    		);
			$result = $this->db->insert($this->table, $data);

			if($result){
				$respon = array("status" => 1, "msg" => "Successfully add data!");
			}else{
				$respon = array("status" => 0, "msg" => "Oop! Something went wrong!");
			}

			echo json_encode($respon);
			
    	}else{

            $this->mcore->checkPermission($this->user_group, $this->add_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Newsletter" => base_url()."newsletter/newsletter",
                "Add New" => base_url()."newsletter/newsletter/addnew",
            );

            $data['page'] = 'newsletter_add';
            $data['page_title'] = 'Newsletter';
            $data['page_subtitle'] = 'Add New Newsletter';

            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "duallistbox" => base_url()."assets/plugins/duallistbox/bootstrap-duallistbox.min.css",
            );
            $data['custom_js'] = array(
                "duallistbox" => base_url()."assets/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js",
            );

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['newslettertemplate'] = $this->MNewsletterTemplate->getNewsletterTemplate();
            $data['data']['subscriber'] = $this->MSubscriber->getSubscriber();
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'newsletter';
            $this->load->view('layout/body',$data);

    	}

    }
    
    function edit($id){

    	if(count($_POST)>0){


            $data = array(
                "status" => $this->input->post("status"),
                "title" => $this->input->post('title'),
                "content" => $this->input->post('content'),
                "subscriber" => implode(",",$this->input->post('subscriber'))
            );

    		$this->db->where($this->pk, $id);

			$result = $this->db->update($this->table, $data);
			if($result){
				$respon = array("status" => "1", "msg" => "Successfully update data!");
			}else{
				$respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
			}

			echo json_encode($respon);

    	}else{

            $this->mcore->checkPermission($this->user_group, $this->edit_perm, true);

            $breadcrumb = array(
                "<i class='fa fa-rss-square'></i> Newsletter" => base_url()."newsletter/newsletter",
                "Edit" => base_url()."newsletter/newsletter/edit/".$id,
            );

            $data['page'] = 'newsletter_edit';
            $data['page_title'] = 'Newsletter';
            $data['page_subtitle'] = 'Edit Newsletter';

            $data['custom_css'] = array(
                "program_category" => base_url()."assets/modules/event/assets/css/event.css",
                "duallistbox" => base_url()."assets/plugins/duallistbox/bootstrap-duallistbox.min.css",
            );
            $data['custom_js'] = array(
                "duallistbox" => base_url()."assets/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js",
            );

            $data['breadcrumb'] = $this->createBreadcrumb($breadcrumb);
            $data['data']['newslettertemplate'] = $this->MNewsletterTemplate->getNewsletterTemplate();
            $data['data']['subscriber'] = $this->MSubscriber->getSubscriber();
            $data['data']['dataedit'] = $this->MNewsletter->getNewsletterById($id);
            $data['data']['parent_menu'] = 'newsletter_management';
            $data['data']['submenu'] = 'newsletter';
            $this->load->view('layout/body',$data);

    	}

    }
    function blast(){
        if(count($_POST)>0){

            $parse_email = explode(",", $this->input->post('subscriber'));
            $email_from = $this->MNewsletter->getConfigurationByKey("EMAIL_FROM","EMAIL");

            $success = 0;
            $failed = 0;
            for($i=0;$i < count($parse_email);$i++){

                $sendmail = $this->send_mail(@$parse_email[$i], 'PERBARINDO Newsletter', $this->input->post('content'),  $email_from['value'], 'PERBARINDO');

                if($sendmail){
                    $success++;
                }else{
                    $failed++;

                }

            }

            $data = array("success" => $success, "failed" => $failed, "status" => "1", "last_blaster_at" => date("Y-m-d h:i:s"));
            $this->db->where($this->pk, $this->input->post('id'));
            $this->db->update($this->table, $data);
            
            $respon = array("status" => "1", "msg" => "Newsletter successfully sent!");
        }else{
            $respon = array("status" => "0", "msg" => "Oop! Something went wrong!");
        }

        echo json_encode($respon);
    }
    function send_mail($email, $subject, $message, $from, $company){

        $smtp_host = $this->MNewsletter->getConfigurationByKey("SMTP_HOST", "EMAIL");
        $smtp_port = $this->MNewsletter->getConfigurationByKey("SMTP_PORT", "EMAIL");
        $smtp_user = $this->MNewsletter->getConfigurationByKey("SMTP_USER", "EMAIL");
        $smtp_pass = $this->MNewsletter->getConfigurationByKey("SMTP_PASSWORD", "EMAIL");

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => @$smtp_host['value'],
            'smtp_port' => @$smtp_port['value'],
            'smtp_user' => @$smtp_user['value'],
            'smtp_pass' => @$smtp_pass['value'],
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from($from, $company);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
          return true;
        }
         else
        {
          return false;
        }
    }    
  //   function delete($id){

  //   	$this->db->where('id', $id);
		// $result = $this->db->delete('newsletters'); 
		// if($result){
		// 	$respon = array("status" => 1, "msg" => "Successfully delete data!");
		// }else{
		// 	$respon = array("status" => 0, "msg" => "Oop! Something went wrong!");
		// }

		// echo json_encode($respon);

  //   }

    function remove(){

        if(count($_POST)>0){

            $id = $this->input->post("id");

            $this->db->where($this->pk, $id);
            $delete = $this->db->delete($this->table);

            if($delete){
                $res = array("status" => "1", "msg" => "Successfully delete data!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            echo json_encode($res);
        }

    }

    
}