<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Subscriber</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" value="<?php echo $dataedit['email']; ?>">
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->


        </div>
        <div class="col-md-6">


        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){


        $("#form").submit(function(){

            showLoading();

            setTimeout('saveFormData();',3000);

            return false;

        });

    });

    function saveFormData(){

        var target = base_url+"newsletter/subscriber/edit/<?php echo $dataedit['subscriber_id']; ?>";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
</script>