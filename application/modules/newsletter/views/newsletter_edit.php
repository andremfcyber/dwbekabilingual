<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="content" id="post_description" value='<?php echo $dataedit['content']; ?>'>

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Newsletter</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Title</label>
                        <input data-prompt-position="topLeft:70" maxlength="50" type="text" id="title" name="title" class="form-control validate[required]" placeholder="Template Title ..." value="<?php echo $dataedit['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Template</label>
                        <select class="form-control" name="newslettertemplate" id="newslettertemplate">
                            <option value=""> - </option>
                            <?php
                            foreach ($newslettertemplate as $list) {
                                echo " <option value='".$list['content']."'>".$list['title']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea class="ckeditor" id="description" rows="10" cols="80"><?php echo $dataedit['content']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Member</label>
                        <select class="form-control" multiple="multiple" name="subscriber[]" id="subscriber">
                            <?php
                            $member = explode(",", $dataedit['subscriber']);
                            foreach ($subscriber as $list) {
                                if(in_array($list['email'], $member)){
                                    echo " <option value='".$list['email']."' selected>".$list['email']."</option>";
                                }else{
                                    echo " <option value='".$list['email']."'>".$list['email']."</option>";
                                }

                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0" <?php ($dataedit['status']=="0")? $attr="selected": $attr="" ;echo $attr;?>>Draft</option>
                            <option value="1" <?php ($dataedit['status']=="1")? $attr="selected": $attr="" ;echo $attr;?>>Publish</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Preview</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="template_preview" id="template_preview" style="padding:50px;"><?php echo $dataedit['content']; ?></div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#subscriber").bootstrapDualListbox({
            // see next for specifications
        });

        CKEDITOR.replace('description',ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

                var value = CKEDITOR.instances["description"].getData();
                $("#template_preview").html(value);


            });

        }

        $("#title").on("keyup blur change",function(){
            $("#title_prev").html($(this).val());
        });
        $("#newslettertemplate").on("keyup blur change",function(){
            CKEDITOR.instances['description'].setData($(this).val());
            $("#template_preview").html($(this).val());
        });

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"newsletter/newsletter/edit/<?php echo $dataedit['newsletter_id']; ?>";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            // resetForm();

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>