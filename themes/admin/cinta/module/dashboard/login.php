<div class="background-image"></div>
<div class="login-box content">
    <div class="login-logo" align="center">
        &nbsp;<a href="<?php echo base_url();?>"><img src="<?php echo $theme_url;?>/assets/img/logo.png" class="img-responsive" style="margin-left: auto; margin-right: auto"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Process</h4>
            Authenticating... Please wait!
        </div>
        <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
            Invalid email or password. Please check your email or password!
        </div>
        <div id="granted" class="alert alert-success alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Access Granted!</h4>
            Redirecting to Dashboard Page... Please Wait!
        </div>
        <form id="login">
            <div class="form-group has-feedback">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input id="remember" type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="submit_btn" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->