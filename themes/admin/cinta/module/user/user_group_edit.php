<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit User Group</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo $dataedit['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="url">Definition</label>
                        <textarea name="definition" class="form-control" id="definition"><?php echo $dataedit['definition']; ?></textarea>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Permission</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <?php foreach($perms as $pm){ ?>
                        <div class="box box-danger">

                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $pm['module']; ?></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">

                                <?php
                                $permissions = $this->MPermission->getPermissionByModule($pm['module']);

                                foreach ($permissions as $prm){

                                    if($this->aauth->is_group_allowed($prm['name'],$dataedit['name'])){
                                        $checked = "checked";
                                    }else{
                                        $checked = "";
                                    }

                                    echo "<input ".$checked." type='checkbox' name='perm_".$prm['id']."' value='".$prm['id']."'> ".$prm['definition']."<br/>";

                                }

                                ?>

                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            showLoading();

            setTimeout('saveFormData();',3000);

            return false;

        });

    });

    function saveFormData(){

        var target = base_url+"user/user_group/edit/<?php echo $dataedit['id']; ?>";
        var data = $("#form").serialize();
        
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
</script>