<?php
  if(!isset($this->theme)){
    $this->theme = $theme;
  }

  $app['base_url'] = $theme_url;
  $app['assetdir'] = "/assets";
  $app['skin'] = '';

  if($app['skin']==''){
      $app['skin'] = 'skin-red';
  }

  /*
   * skin-black, skin-black-light, skin-blue,
   * skin-blue-light, skin-green, skin-green-light,
   * skin-purple, skin-purple-light, skin-red,
   * skin-red-light, skin-yellow, skin-yellow-light
  */
  $app['title'] = "Admin Area";
  $app['css'] = array(
      
      "skin" => $app['base_url'].$app['assetdir'].'/css/skins/'.$app['skin'].'.min.css',
      "adminlte" => $app['base_url'].$app['assetdir'].'/css/AdminLTE.css',
      "style" => $app['base_url'].$app['assetdir'].'/css/style.css',

  );
  $app['header_js'] = array();
  $app['footer_js'] = array();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php

        $css_key = array_keys($app['css']);
        for($i=0;$i < count($app['css']);$i++){
            echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
        }

        /* generate custom css */

        if(isset($custom_css)){
            if(count($custom_css)>0){
                $custom_css_key = array_keys($custom_css);
                for($i=0;$i < count($custom_css);$i++){
                    echo '<link rel="stylesheet" href="'.$custom_css[$custom_css_key[$i]].'">';
                }
            }
        }

        /* generate js */
        $header_js_key = array_keys($app['header_js']);
        for($i=0;$i < count($app['header_js']);$i++){
            echo '<script src="'.$app['header_js'][$header_js_key[$i]].'"></script>';
        }

    ?>
    <?php _include_core_head(); ?>
    
   
</head>
<body class="hold-transition <?php echo $app['skin']; ?> sidebar-mini">
<div class="wrapper">

    <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/header"); ?>
    <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/sidebar"); ?>

    <div class="content-wrapper" style="min-height: 900px;">
        <section class="content-header">
            <h1>
                <?php (isset($page_title)) ? $str = $page_title: $str = "Page Title Not Set"; echo $str; ?>
                <small><?php (isset($page_subtitle)) ? $str = $page_subtitle: $str = "Page Sub Title Not Set"; echo $str; ?></small>
            </h1>
            <ol class="breadcrumb">
                <?php (isset($breadcrumb)) ? $str = $breadcrumb: $str = ""; echo $str; ?>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

        <?php

          if(isset($html)){
            echo $html;
          }else{
            echo $this->load->view("../../themes/admin/{$this->theme}/module/{$this->theme_module}/{$module_view}"); 
          }
          
         ?>

        </section>
    </div>

    <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/footer"); ?>
    <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/sidebar_control"); ?>

    <div class="control-sidebar-bg"></div>
</div>
<div class="modal" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-key"></i> Change Password</h4>
            </div>
            <div class="modal-body">
                <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Process</h4>
                    Authenticating... Please wait!
                </div>
                <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                    Invalid email or password. Please check your email or password!
                </div>
                <form id="frm_change_pass">
                    <div class="form-group has-feedback">
                        <input type="password" name="old_password" id="old_password" class="validate[required]  form-control" placeholder="Old Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="new_password" id="new_password" class="validate[required] form-control" placeholder="New Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="confirm_password" id="confirm_password" class="validate[required,equals[new_password]] form-control" placeholder="Confirm Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_update_pass" onclick="update_password()">Update</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal" id="edit_profile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-user"></i> Edit Profile</h4>
            </div>
            <div class="modal-body">
                <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Process</h4>
                    Authenticating... Please wait!
                </div>
                <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                    Invalid email or password. Please check your email or password!
                </div>
                <form id="frm_edit_profile">
                    <input type="hidden" name="old_pp" value="<?php echo $this->session->userdata('picture'); ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <div class="img-prev" id="pp_preview">
                                    <?php
                                    if($this->session->userdata('picture') != ""){

                                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                                        $file = $upload_path.'/'.$this->session->userdata('picture');

                                        if(file_exists($file)){

                                            $path = base_url().'upload/photo/'.$this->session->userdata('picture');

                                            echo "<img src='".$path."' class='img-responsive' />";

                                        }else{

                                            echo "<h1>Photo Preview</h1>";

                                        }
                                    }else{ ?>

                                        <h1>Photo Preview</h1>

                                    <?php } ?>
                                </div>
                                <br>
                                <input type="file" class="form-control" name="photo" id="pp">

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" required name="first_name" class="validate[required] form-control" id="first_name" value="<?php echo $this->session->userdata('first_name'); ?>" placeholder="First Name (Required)">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" required name="last_name" class="validate[required] form-control" id="last_name" value="<?php echo $this->session->userdata('last_name'); ?>" placeholder="Last Name (Required)">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_update_profile" onclick="update_profile()">Update</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
  var current_year = '<?php echo date("Y") ?>';
</script>
<?php _include_core_footer() ?>
<?php
    /* generate js */
    $footer_js_key = array_keys($app['footer_js']);
    for($i=0;$i < count($app['footer_js']);$i++){
        echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
    }

    /* generate custom js */
    if(isset($custom_js)){
        if(count($custom_js)>0){
            $custom_js_key = array_keys($custom_js);
            for($i=0;$i < count($custom_js);$i++){
                if($custom_js_key[$i]=="googlemaps"){
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'" async defer></script>';
                }else{
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'"></script>';
                }

            }
        }
    }
?>
</body>
</html>
