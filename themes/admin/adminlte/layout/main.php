<?php

  if(!isset($this->theme)){
    $this->theme = $theme;
  }
 
  $app['base_url'] = $theme_url;
  $app['assetdir'] = "/assets";
  $app['skin'] = 'skin-green-custom';

  $app['title'] = (isset($page_title)) ? " Admin  | ".$page_title : "  | Admin";
  $app['css'] = array();
  $app['header_js'] = array();
  $app['footer_js'] = [
        'select2' => base_url().'assets/dist/js/select2.min.js',
        'blockUI' => base_url().'assets/dist/js/jquery.blockUI.js',
        'numeraljs' => base_url().'assets/dist/js/numeral.min.js',
        // 'jquerynumber' => base_url().'assets/dist/js/jquery.number.min.js',
        'jquerymask' => base_url().'assets/dist/js/jquery.mask.js',
        'tinymce1' => base_url().'assets/plugins/tinymce/tinymce.min.js',
        'tinymce2' => base_url().'assets/plugins/tinymce/jquery.tinymce.min.js', 
        'tinymce3' => base_url().'assets/plugins/tinymce/plugins/paste/plugin.min.js', 
        'tinymce5' => base_url().'assets/plugins/tinymce/plugins/autolink/plugin.min.js',
        'tinymce6' => base_url().'assets/plugins/tinymce/plugins/anchor/plugin.min.js',
        'tinymce7' => base_url().'assets/plugins/tinymce/plugins/emoticons/plugin.min.js',
        'tinymce8' => base_url().'assets/plugins/tinymce/plugins/code/plugin.min.js',
        'tinymce9' => base_url().'assets/plugins/tinymce/plugins/save/plugin.min.js', 
        'tinymce10' => base_url().'assets/plugins/tinymce/plugins/nonbreaking/plugin.min.js', 
        
  ];
 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet"> 

    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/dist/css/AdminLTE.min.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/skins/skin-green-custom.min.css">
    <!-- <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/AdminLTE.css"> -->
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/style.css">
    <!-- <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>assets/dist/js/select2.min.css">    -->
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/font-awesome/css/font-awesome.min.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/Ionicons/css/ionicons.min.css">   
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/dist/css/skins/_all-skins.min.css">
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/morris.js/morris.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/jvectormap/jquery-jvectormap.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"> 
    <link rel="preload" media="all"  as="style" onload="this.onload=null;this.rel='stylesheet'"  href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">  

  <noscript> 
   <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/dist/css/AdminLTE.min.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/skins/skin-green-custom.min.css">
    <!-- <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/AdminLTE.css"> -->
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/css/style.css">
    <!-- <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/dist/js/select2.min.css">    -->
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/font-awesome/css/font-awesome.min.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/Ionicons/css/ionicons.min.css">   
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/morris.js/morris.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/jvectormap/jquery-jvectormap.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"> 
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>themes/admin/adminlte/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">  
  </noscript>
    <!-- ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
    
    <style type="text/css">
        body{
            font-family: 'Livvic', sans-serif !important;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #ecf0f5; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #808789; 
        }

        #map-canvas{
            margin: 10px auto;
            width: calc(100% - 5px);
            height: 400px;
            border-radius: 10px;
            border: 2px #ddd solid;
        }
        .btn-info {
            border-radius: 24px!important;
        }
        .btn-danger {
            border-radius: 24px!important;
            background-color: #e91e63!important;
            border: none!important;
        }
        .btn-primary {
            border-radius: 24px!important;
            background-color: #00b0ff!important;
            border: none!important;
        }
        .box.box-primary {
            border-top-color: #00b0ff!important;
        }
        .btn-success {
            color: #fff;
            border-radius: 0px!important;
            background-color: #009688;
            border-color: #009688;
        }
        .box.box-solid.box-primary > .box-header {
            color: #ffffff;
            background: #00b0ff!important;
            background-color: #00b0ff!important;
            border-radius: 0px!important!important;
        }
    </style>
    <?php _include_core_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDO7ux0CVng-weBxmxNS7GpThvJvFqtLAQ&amp;libraries=places"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144983683-1');
</script>

</head>
<body class="hold-transition <?php echo $app['skin']; ?> sidebar-mini">
    <div class="wrapper">

        <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/header"); ?>
        <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/sidebar"); ?>

        <div class="content-wrapper" style="min-height: 926px;">
            <section class="content-header">
                <h1>
                    <?php (isset($page_title)) ? $str = $page_title: $str = "Page Title Not Set"; echo $str; ?>
                    <small><?php (isset($page_subtitle)) ? $str = $page_subtitle: $str = "Page Sub Title Not Set"; echo $str; ?></small>
                </h1>
                <ol class="breadcrumb">
                    <?php (isset($breadcrumb)) ? $str = $breadcrumb: $str = ""; echo $str; ?>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <?php
                    if(isset($html)){
                        echo $html;
                    }else{
                        echo $this->load->view("../../themes/admin/{$this->theme}/module/{$this->theme_module}/{$module_view}"); 
                    }  
                ?>

            </section>
            
        </div>

        <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/footer"); ?>
        <!-- <?php echo $this->load->view("../../themes/admin/{$this->theme}/parts/sidebar_control"); ?>

        <div class="control-sidebar-bg"></div> -->
    </div>
    <div class="modal" id="change_password">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-key"></i> Change Password</h4>
                </div>
                <div class="modal-body">
                    <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-info"></i> Process</h4>
                        Authenticating... Please wait!
                    </div>
                    <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                        Invalid email or password. Please check your email or password!
                    </div>
                    <form id="frm_change_pass">
                        <input type="hidden" name="id" value="<?= $_SESSION['id'] ? $_SESSION['id'] : '' ?>">
                        <div class="form-group has-feedback">
                            <input type="password" name="old_password" id="old_password" class="validate[required]  form-control" placeholder="Old Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="new_password" id="new_password" class="validate[required] form-control" placeholder="New Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="confirm_password" id="confirm_password" class="validate[required,equals[new_password]] form-control" placeholder="Confirm Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn_update_pass" onclick="update_password()">Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="dynamicModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-dynamic" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="edit_profile">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> Edit Profile</h4>
                </div>
                <div class="modal-body">
                    <div id="loading" class="alert alert-info alert-dismissible" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-info"></i> Process</h4>
                        Authenticating... Please wait!
                    </div>
                    <div id="denied" class="alert alert-danger alert-dismissible" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
                        Invalid email or password. Please check your email or password!
                    </div>
                    <form id="frm_edit_profile">
                        <input type="hidden" name="old_pp" value="<?php echo $this->session->userdata('picture'); ?>">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="photo">Foto</label>
                                    <div class="img-prev" id="pp_preview">
                                        <?php
                                        if($this->session->userdata('picture') != ""){

                                            $root_ = $_SERVER['DOCUMENT_ROOT'].'/';
                                            // echo $root_;
                                            $file = $root_.$this->session->userdata('main');
                                            // echo $file;
                                            if(file_exists($file)){
                                                $path = $this->session->userdata('picture');
                                                echo "<img src='".$path."' class='img-responsive' />";
                                            }else{

                                                echo "<h1>Foto</h1>";

                                            }
                                        }else{ ?>

                                            <h1>Foto</h1>

                                        <?php } ?>
                                    </div>
                                    <br>
                                    <input type="file" class="form-control" name="photo" id="pp">

                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="first_name">Nama Depan</label>
                                    <input type="text" required name="first_name" class="validate[required] form-control" id="first_name" value="<?php echo $this->session->userdata('first_name'); ?>" placeholder="First Name (Required)">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Nama Belakang</label>
                                    <input type="text" required name="last_name" class="validate[required] form-control" id="last_name" value="<?php echo $this->session->userdata('last_name'); ?>" placeholder="Last Name (Required)">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn_update_profile" onclick="update_profile()">Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script>
        var _TRIGGER_CHANGE = {
            PROVINSI  : true,
            KABUPATEN : true,
            KECAMATAN : true,
            KELURAHAN : true,
        };
        var uri_special = {
            get_all_provinces : '<?= base_url("public_service/provinsi") ?>',
            get_cities_by_province_id : '<?= base_url("public_service/kota?provinsi_id=:id") ?>',
            get_districts_by_city_id  : '<?= base_url("public_service/kecamatan?kota_id=:id") ?>',
            get_villages_by_district_id : '<?= base_url("public_service/kelurahan?kecamatan_id=:id") ?>',
        };
    </script>
    <script type="text/javascript">
    var current_year = '<?php echo date("Y") ?>';
    </script>
    <?php _include_core_footer() ?>
    <?php
        /* generate js */
        $footer_js_key = array_keys($app['footer_js']);
        for($i=0;$i < count($app['footer_js']);$i++){
            echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
        }

        /* generate custom js */
        if(isset($custom_js)){
            if(count($custom_js)>0){
                $custom_js_key = array_keys($custom_js);
                for($i=0;$i < count($custom_js);$i++){
                    if($custom_js_key[$i]=="googlemaps"){
                        echo '<script src="'.$custom_js[$custom_js_key[$i]].'" async defer></script>';
                    }else{
                        echo '<script src="'.$custom_js[$custom_js_key[$i]].'"></script>';
                    }

                }
            }
        }
    ?>
    <script>
    $(() => {
        $('.select2').select2();
        <?= isset($excute_kimochi) ? $excute_kimochi : '' ?>
    })
    </script>
    <script src="<?= isset($ikeh_ikeh_js) ? $ikeh_ikeh_js : '' ?>"></script>
    <script> 
       tinymce.init({
        selector: '#tiny',
        height: 500,
        theme: 'silver',
        plugins: 'save print preview fullpage  searchreplace autolink directionality  visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools    contextmenu colorpicker textpattern help code', 
        toolbar1: ' code | image | nonbreaking | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | table | numlist bullist outdent indent  | removeformat ', 
        images_upload_url: '<?php echo base_url(); ?>upload.php',
        images_upload_base_path: '<?php echo base_url(); ?>/image',
         
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
        
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '<?php echo base_url(); ?>upload.php');
        
            xhr.onload = function() {
                var json;
            
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
            
                json = JSON.parse(xhr.responseText);
            
                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
            
                success(json.location);
            };
        
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
        
            xhr.send(formData);
        },
        // templates: [
        //     { title: 'Test template 1', content: 'Test 1' },
        //     { title: 'Test template 2', content: 'Test 2' }
        // ],forced_root_block: false,

        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            }); 
        },
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            // '//www.tinymce.com/css/codepen.min.css'
        ]
        });

    </script>
     <!-- Render Datatables -->
  <?php
    if (class_exists('datatables') && isset($dt_name) && $dt_name != '') {
      if (isset($param_dt)) {
        $this->datatables->jquery($dt_name, $param_dt);
      } else {
        $this->datatables->jquery($dt_name);
      }
    }
  ?>
</body>
</html>
