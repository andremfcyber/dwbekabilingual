<link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<style type="text/css">
    .labelfrm {
        display:block;
        font-size:small;
        margin-top:5px;
    }
    .error {
        font-size:small;
        color:red;
    }
</style>

<section class="content">
	<div class="box box-default">
		<div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                List Kelurahan
            </div>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <span class="btn-action" style="display: none;">
                    <button class="btn btn-default" onclick="back()" id="btn-back"><i class="fa fa-arrow-left"></i> Kembali</button>
                    <button class="btn btn-primary" onclick="save()" id="btn-back"> <i class="fa fa-save"></i> Simpan</button>
                </span>
            </div>
		</div>
		<div class="box-body" id="a-table">
            <div style="padding: 0px 0px 20px 0px;">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" id="dt_search" class="form-control" placeholder="Cari Kelurahan ..." aria-describedby="sizing-addon1">
                </div>
            </div>
            <table id="table" class="table table-bordered table-striped dt_custom" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>PROVINSI</th>
                        <th>KAB/KOTA</th>
                        <th>KECAMATAN</th>
                        <th>KELURAHAN</th>
                        <!-- <th style="width: 15%;">AKSI</th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th colspan="4" class="text-center">DATA TIDAK DITEMUKAN</th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="box-body" id="a-form" style="display: none;">
            <form id="myform">
                <div class="form-group">
                    <label>Kelurahan</label>
                    <input type="text" name="kelurahan" class="form-control">
                </div>
            </form>
        </div>
        <div class="box-footer with-border">
            <span class="title"></span>
            <div class="pull-right btn-action" style="display: none;">
                <button class="btn btn-default" onclick="back()" id="btn-back"><i class="fa fa-arrow-left"></i> Kembali</button>
                <button class="btn btn-primary" onclick="save()" id="btn-back"> <i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
	</div>
</section>


<script src="<?= base_url('/assets/dist/js/datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/bs3-datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/datatables.buttons.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/jszip.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/btn-print.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/validate/jquery.validate.min.js') ?>"></script>
<script>
    var url = {
        list_dt : "<?= base_url('dt_service/list_kelurahan'); ?>",
        create : "<?= base_url('wilayah/kelurahan/create'); ?>",
        update : "<?= base_url('wilayah/kelurahan/update'); ?>",
        show : "<?= base_url('wilayah/kelurahan/show'); ?>",
        delete : "<?= base_url('wilayah/kelurahan/delete'); ?>",
    }
    var table = $('#table').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: url.list_dt,
            type: "POST",
            data: (data) => {
                // data.filter_range = this.get_filter_range;
                // data.filter_status = this.get_filter_status;
                // data.filter_status_lunas = this.get_filter_status_lunas;
            }
        },
        processing: true,
        columns: [
            {data: 'provinsi', name: 'provinsi', 'className': '', 'searchable': true, 'orderable': true, render: (data, type, row) => {
                return data;
            }},
            {data: 'kabupaten_kota', name: 'kabupaten_kota', 'className': '', 'searchable': true, 'orderable': true, render: (data, type, row) => {
                return data;
            }},
            {data: 'kecamatan', name: 'kecamatan', 'className': '', 'searchable': true, 'orderable': true, render: (data, type, row) => {
                return data;
            }},
            {data: 'kelurahan', name: 'kelurahan', 'className': '', 'searchable': true, 'orderable': true, render: (data, type, row) => {
                return data;
            }},
            // {data: 'id', name: 'id', 'className': 'text-center', 'searchable': false, 'orderable': false, render: (data, type, row) => {
            //     let html = '<button class="btn btn-sm btn-success" onclick="edit('+data+')"><i class="fa fa-pencil"></i></button> ';
            //     html += '<button class="btn btn-sm btn-danger" onclick="del('+data+')"><i class="fa fa-trash"></i></button>';
            //     return html;
            // }},
            
        ],
        dom: "<'row'<'col-sm-6'l><'col-sm-6'B>>"+
            "<'row'<'col-sm-12'tr>>"+
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            {
                extend:    'print',
                text:      '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                tag: "button",
                className: ""
            },
            {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                tag: "button",
                className: ""
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                tag: "button",
                className: ""
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                tag: "button",
                className: ""
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                tag: "button",
                className: ""
            },
            // {
            //     text: '<i class="fa fa-plus"></i> Tambah',
            //     action: function ( e, dt, node, config ) {
            //         add()
            //     },
            //     className: ""
            // }
        ],
        drawCallback: (settings) => {
            // $('#approval_pos_manager').unblock();
        },
        aaSorting: [],
    })

    var save_url = '';
    function edit(id){
        $('.box-title').html('Ubah Wilayah');
        $('#a-table').hide();
        $('#a-form').show();
        $('.btn-action').show();
        save_url = url.update;
    }

    function add(){
        $('.box-title').html('Tambah Wilayah');
        $('#a-table').hide();
        $('#a-form').show();
        $('.btn-action').show();
        save_url = url.create;
    }

    function show(id){
        $.get(`${url.show}/${id}`).done((res) => {
            console.log(res);
        }).fail((xhr) => {
            
        })
    }

    function del(id){
        $.get(`${url.delete}/${id}`).done((res) => {
            console.log(res);
        }).fail((xhr) => {
            
        })
    }

    function back(){
        $('#a-form').hide();
        $('.btn-action').hide();
        $('#a-table').show();
    }

    function save(){

    }

    $("#dt_search").keyup(function(){
        table.search($(this).val()).draw();
    });
		
</script>