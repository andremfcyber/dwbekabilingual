<!-- <link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/toastr.min.css') ?>" rel="stylesheet"> -->
<style type="text/css">

</style>

<div class="col-md-5 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Jabatan
            </div>
            <div class="box-tools pull-right">
                
            </div>
        </div>
        <div class="box-body">
            <div class="alert alert-info">
                <h4><i class="icon fa fa-info"></i> <?= $jabatan['nama'] ?></h4>
                <?= $jabatan['description'] ?>
            </div>
            <button class="btn btn-sm btn-success" onclick="update_aturan()"><i class="fa fa-save"></i> Simpan</button>
            <a class="btn btn-sm btn-default" href="javascript:window.history.go(-1);">Kembali</a>
        </div>
    </div>
</div>
<div class="col-md-7 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Aturan Produk
            </div>
            <div class="box-tools pull-right">
                
            </div>
        </div>
        <div class="box-body">
            <form id="aturan">    
                <?php
                    if(count($produk) == 0){
                        echo "PRODUK BELUM ADA";
                    }

                    foreach($produk as $val){
                        $checked = ' ';
                        
                        if(is_produk_allow($jabatan['id'], $val['id'])){
                            $checked = 'checked=""';
                        }

                        echo '<input '.$checked.' name="rule[]" type="checkbox" value="'.$val['id'].'"> '.$val['nama_type_product'].'<br>';
                    }
                ?>
            </form>
        </div>
    </div>
</div>

<!-- <script src="<?= base_url('/assets/dist/js/datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/bs3-datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/datatables.buttons.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/jszip.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/btn-print.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/toastr.min.js') ?>"></script> -->
<script>
    let id_jabatan = '<?= $jabatan['id'] ?>'
    var url = {
        update_rule : "<?= base_url('master/jabatan/rule_produk_update'); ?>",
    }
    
    function update_aturan(){
        let formData = $('#aturan').serializeArray();
        // console.log(formData);
        $.post(`${url.update_rule}/${id_jabatan}`, formData).done((res) => {
            toastr.success(res.msg, 'Berhasil !');
        }).fail((xhr) => {
            consol.log(xhr);
        })
    }
</script>