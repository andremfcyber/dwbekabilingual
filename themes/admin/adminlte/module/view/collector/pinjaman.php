<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    
    .border-transparent{
        border-top: 1px solid transparent !important; 
    }
    .table tbody tr td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid transparent !important;
    }
    table tbody tr th{
        width : 30%;
    }
  
</style>

<div class="col-md-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Data Nasabah
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Nama Nasabah</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['nama'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">No Rekening</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['no_rek'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">No.Telepon</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['no_hp'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Alamat KTP</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['alamat'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Detail Pinjaman
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Plafond</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['plafond']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Bakidebet</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['bakidebet']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Angsuran Pokok</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['angsuran_pokok']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Angsuran Bunga</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['angsuran_bunga']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tunggakan Bulan Pokok</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['tgk_bulan_pokok']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tunggakan Pokok</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['tunggakan_pokok']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tunggakan Bulan Bunga</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['tgk_bulan_bunga']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tunggakan Bunga</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['tunggakan_bunga']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Jangka Waktu</th>
                                <td style="border-top: 1px solid transparent;">: <?= ($detail['jangka_waktu']) ?> bulan</td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Koll Lama</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['koll_lama'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tanggal Mulai</th>
                                <td style="border-top: 1px solid transparent;">: <?= date("d/m/Y", strtotime($detail['tgl_mulai']))?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Tanggal Jatuh Tempo</th>
                                <td style="border-top: 1px solid transparent;">: <?= date("d/m/Y", strtotime($detail['tgl_jatuh_tempo']))?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">AO</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['ao'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Koll Baru</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['koll_baru'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Minimal Bayar Pokok</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['min_bayar_pokok']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Minimal Bayar Bunga</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['min_bayar_bunga']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Biaya Perbaikan Koll</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['total_bayar_perbaikan_koll']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Total Angsuran</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['total_angsuran']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Total Tunggakan</th>
                                <td style="border-top: 1px solid transparent;">: <?= format_rupiah($detail['total_tunggakan']) ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 50%">Branch</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['branch'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>