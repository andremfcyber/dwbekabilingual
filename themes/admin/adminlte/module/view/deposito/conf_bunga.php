<!-- <link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/toastr.min.css') ?>" rel="stylesheet"> -->
<style type="text/css">

</style>

<div class="col-md-12">
    <!-- Custom Tabs (Pulled to the right) -->
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="pull-left header"><i class="fa fa-cog"></i> Aturan Bunga</li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1-1">
            <div class="row">
                <div class="col-md-6">
                    <b>List Bunga</b>
                    <table class="table table-bordered table-striped">    
                        <thead>
                            <tr>
                                <td>Bulan</td>
                                <td>Bunga</td>
                                <td style="width: 20%"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($_bunga_baru) && count($_bunga_baru) > 0) { ?>
                                <?php foreach($_bunga_baru as $data) { ?>
                                    <tr class="_info" id="tr_<?= $data['id'] ?>">
                                        <td><?= $data['bulan'] ?></td>
                                        <td><?= $data['bunga'] ?> %</td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" onclick="edit_inline(<?= $data['id'] ?>)"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-danger btn-sm" onclick="delete_data(<?= $data['id'] ?>)"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <tr class="_update" id="uid_tr_<?= $data['id'] ?>" style="display: none">
                                        <td>
                                            <input class="form-control" name="bulan" value="<?= $data['bulan'] ?>">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input class="form-control" name="bunga" value="<?= $data['bunga'] ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-success btn-sm" onclick="update_data(<?= $data['id'] ?>)"><i class="fa fa-save"></i> Simpan</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aturan</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <b>Tambah Baru : </b>
                    <br>
                    <form id="tambah_bunga">
                        <input type="hidden" value="-" name="jenis_bunga">
                        <div class="form-group">
                            <label>Bulan</label>
                            <input class="form-control" name="bulan">
                        </div>
                        <div class="form-group">
                            <label>Bunga</label>
                            <div class="input-group">
                                <input class="form-control" name="bunga">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success" onclick="tambah_bunga()"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</div>

<script src="<?= base_url('/assets/dist/js/jquery.number.min.js') ?>"></script>
<script>
    let id_produk = '<?= isset($id_produk) ? $id_produk : ''  ?>'
    var url = {
        add_bulan : "<?= base_url('product/deposito/add_conf_bunga/'); ?>",
        edit_bulan : "<?= base_url('product/deposito/edit_conf_bunga/'); ?>",
        delete_bulan : "<?= base_url('product/deposito/delete_conf_bunga/'); ?>",
    }

    $('[name="bunga"]').number( true, 2 );

    function check_is_valid(form){
        let validate = $(form).serializeArray();
        // console.log(validate)
        if(validate.length <= 0){
            return false;
        }
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }
    
    function bunga_valid(bunga){
        bunga = parseFloat(bunga);
        console.log(bunga)
        if(bunga > 100){
            return false;
        }else{
            return true;
        }
    }

    function bulan_numeric(bulan){
        if(!isNaN(bulan) && bulan >= 0){
            return true;
        }else{
            return false;
        }
    }

    
    function tambah_bunga(){
        let validate = check_is_valid('#tambah_bunga');
		if(validate){
            let formData = $('#tambah_bunga').serializeArray();
            //check bunga
            let bulan = $('#tambah_bunga [name="bulan"]').val();
            let bunga = $('#tambah_bunga [name="bunga"]').val();
            // console.log(bunga)
            if(!bunga_valid(bunga)){
                toastr.error('Bunga Melebihi 100');
                return;
            }
            // console.log(bulan, bulan_numeric(bulan));
            if(!bulan_numeric(bulan)){
                toastr.error('Bulan Harus berupa Angka dan tidak boleh bilangan negatif');
                return;
            }
            // console.log(formData);
            $.post(`${url.add_bulan}/${id_produk}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
                setTimeout(() => {                
                    window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
                }, 2000);
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        }else{
            $.unblockUI();
			toastr.error('Silahkan Lengkapi Form');
        }
    }

    function edit_inline(uid){
        $('._info').show();
        $('._update').hide();
        $('#uid_tr_'+uid).show();
        $('#tr_'+uid).hide();
    }

    function check_is_valid_update(uid){
        let bulan = $('#uid_tr_'+uid+' [name="bulan"]').val();
        let bunga = $('#uid_tr_'+uid+' [name="bunga"]').val();
        if(!bunga || !bulan){
            return false;
        }else{
            return true;
        }
    }

    function update_data(uid){
        let validate = check_is_valid_update(uid);
		if(validate){
            //check bunga
            let bulan = $('#uid_tr_'+uid+' [name="bulan"]').val();
            let bunga = $('#uid_tr_'+uid+' [name="bunga"]').val();
            let formData =  {
                bulan: bulan,
                bunga: bunga,
                id_produk: id_produk,
            };
            // console.log(bunga)
            if(!bunga_valid(bunga)){
                toastr.error('Bunga Melebihi 100');
                return;
            }
            // console.log(bulan, bulan_numeric(bulan));
            if(!bulan_numeric(bulan)){
                toastr.error('Bulan Harus berupa Angka dan tidak boleh bilangan negatif');
                return;
            }
            // console.log(formData);
            $.post(`${url.edit_bulan}/${uid}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
                setTimeout(() => {                
                    window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
                }, 2000);
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        }else{
            $.unblockUI();
			toastr.error('Silahkan Lengkapi Form');
        }
    }

    function delete_data(id){
        $.get(`${url.delete_bulan}/${id}`).done((res) => {
            toastr.success(res.msg);
            window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
        }).fail((xhr) => {
            toastr.error(xhr.responseJSON.msg);
        })
    }
</script>