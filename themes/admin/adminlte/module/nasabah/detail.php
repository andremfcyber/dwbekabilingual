<?php 
	$id_nasabah = $detail_nasabah['id_nasabah'];
	$id_product = $nasabah['id_product'];
	$dir = $this->router->fetch_directory();
	$class = $this->router->fetch_class();

	$x = explode("/", $dir);
	$module = $x[2];

	$module_url = base_url().$module.'/'.$class;
?>
<style type="text/css">
	#smartwizard > .navbar{
		position: relative;
	    display: -ms-flexbox;
	    display: flex;
	    -ms-flex-wrap: wrap;
	    flex-wrap: wrap;
	    -ms-flex-align: center;
	    align-items: center;
	    -ms-flex-pack: justify;
	    justify-content: space-between;
	    padding: .5rem 1rem;

	}
</style>
<section class="content">
	<div class="box box-primary box-solid">
		<div class="box-header">
			<div class="box-title">Formulir Pendaftaran Nasabah <?= $nasabah['name'];?></div>
		</div>
		<div class="box-body">
			<form id="form" action="" method="POST">
				<div id="smartwizard">
					<ul>
						<li><a href="#form-foto"><span>PHOTO KTP DAN SELFIE</span></a></li>
			        	<?php if (count($header_form) > 0 ){
			        		foreach($header_form as $key => $value) {?>
			            <li><a href="#form-<?= $value['id']?>"><span><?= $value['name_header']?></span></a></li>
				        	<?php } ?>
				        <?php } ?>
				        <li><a href="#form-appointment"><span>KERTERANGAN</span></a></li>
			        </ul>
			        <div>
			        	<div id="form-foto">
			        		<div class="row">
								<?php if ($detail_nasabah['photo_selfie']) {?>
								<div class="col-md-6">
									<a class="fancybox" href="<?= base_url('upload/photo/'.$detail_nasabah['photo_selfie'])?>">
										<img src="<?= base_url('upload/photo/'.$detail_nasabah['photo_selfie'])?>" class="img-thumbnail"/>
									</a>
								</div>
								<?php }?>
								<?php if ($detail_nasabah['photo_ktp']) {?>
								<div class="col-md-6">
									<a class="fancybox" href="<?= base_url('upload/photo/'.$detail_nasabah['photo_ktp'])?>">
										<img src="<?= base_url('upload/photo/'.$detail_nasabah['photo_ktp'])?>" class="img-thumbnail"/>
									</a>
								</div>
								<?php }?>
							</div>
			        	</div>
			        	<?php
			        	if (count($header_form)  > 0 ){
			        		foreach ($header_form as $key => $value) {
			        	?>
			        	<div id="form-<?= $value['id']?>">
			        		<?php 
			        		$query = "SELECT * FROM form_fileds WHERE form_header_id = ".$value['id']." ORDER BY urutan";
					        $form_attr = $this->db->query($query)->result_array();
					        foreach ($form_attr as $fa_key => $fa_value) {
					        	$input_type = $fa_value['input_type'];
					        	switch ($input_type) {
	            				case 'text':                                      
		            				$field_name = strtolower(str_replace(' ', '_', $fa_value['label']));
							        $field_name = (preg_replace('/[^A-Za-z0-9\_]/', '', $field_name));
							        $value_detail = $this->db->get_where('data_formulir_nasabah', array('id_nasabah' => $id_nasabah, 'id_product'=>$id_product, 'id_form_field'=>$fa_value['id']))->row_array();
							        if (isset($value_detail)){
							        	$data_value = $value_detail['value'];
							        } else if (isset($detail_nasabah[$field_name])){
							        	$data_value = $detail_nasabah[$field_name];
							        } else {
							        	$data_value = null;
							        }
							        generateTextInput($fa_value['label'],$fa_value['id'],$data_value);
							    break;
							    case 'checkbox' :
							    	$form_attr_source = $this->db->get_where("form_attribut",array("form_filed_id" => $fa_value['id']))->result_array();

                                    generateCheckbox($fa_value['label'],$fa_value['id'],$form_attr_source,$id_nasabah,$id_product);
							    break;
							    default: 
							    break;
								}
					        }
			        		?>
			        	</div>
			        	<?php
			        		}
			        	}
			        	?>
			        	<div id="form-appointment">
			        		<br/>
			        		<h3>Jadwal Pertemuan</h3>
			        		<div class="row">
			        			<div class="col-md-4">Tanggal Pertemuan</div>
			        			<div class="col-md-8"><?=$appointment['tanggal_appoiment']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Waktu Pertemuan</div>
			        			<div class="col-md-8"><?=$appointment['waktu_appoiment']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Lokasi</div>
			        			<div class="col-md-8"><?=$appointment['lokasi']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Pihak yang dapat dihubungi</div>
			        			<div class="col-md-8"><?=$appointment['pihak_lain']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Nomor Telepon</div>
			        			<div class="col-md-8"><?=$appointment['no_telp_pihak_lain']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Alamat</div>
			        			<div class="col-md-8"><?=$appointment['alamat_pihak_lain']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">hubungan</div>
			        			<div class="col-md-8"><?=$appointment['hubungan']?></div>
			        		</div>
			        		<br/>
			        		<br/>
			        		<h3>Petugas</h3>
			        		<div class="row">
			        			<div class="col-md-4">Petugas</div>
			        			<div class="col-md-8"><?=$appointment['nama_sales']?></div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-4">Nomor Telepon Petugas</div>
			        			<div class="col-md-8"><?=$appointment['no_telp']?></div>
			        		</div>
			        	</div>
			        </div>
				</div>
			</form>
		</div>
	</div>
</section>
		


<script type="text/javascript">
	
		var module_url = '<?= $module_url;?>';
		var btnFinish = 
			$('<button></button>').text('Finish')
                .attr('id','btn-finish')
                .addClass('btn btn-info')
                .on('click', function(){ 
	                $("#form").submit(function(e) {
						blockUI();
	                	e.preventDefault();
	                	var data = $(this).serialize();
	                	var target = module_url + '/do_save_detail/<?=$id_product?>/<?=$id_nasabah?>'
	                	$.post(target, data, function (response) {
	                		if(response.status == "1"){
	                			setTimeout(function(){
									window.location = module_url;
									toastr.success(response.msg, "Response Server");
									$.unblockUI();
	                			},2500);
					        }else if(response.status == "2"){
							  toastr.info(response.msg, "Response Server");
							  $.unblockUI();
					        }else{
							  toastr.error(response.msg, "Response Server");
							  $.unblockUI();
					        }
	                	}, 'json');
	                	return false;
	                });
            	});
        var btnCancel = 
        	$('<button></button>').text('Cancel')
            	.addClass('btn btn-danger')
                .on('click', function(){ 
                	$('#smartwizard').smartWizard("reset");  
                });
        var btnBack = 
        	$('<button></button>').text('Back')
        		.addClass('btn btn-warning')
        		.on('click', function(e){
        			e.preventDefault();
        			window.location = module_url;
        		});
		$('#smartwizard').smartWizard({ 
                    selected: 0, 
                    theme: 'dots',
                    transitionEffect:'fade',
                    toolbarSettings: {toolbarPosition: 'bottom',
                                      toolbarExtraButtons: [btnFinish, btnCancel, btnBack]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
                 });
		
	            $("#btn-finish").addClass('disabled');
	            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
	                   //alert("You are on step "+stepNumber+" now");
	               if(stepPosition == 'first'){
	                   $("#prev-btn").addClass('disabled');
	                   $("#btn-finish").addClass('disabled');
	               }else if(stepPosition == 'final'){
	                   $("#next-btn").addClass('disabled');
	                   $("#btn-finish").removeClass('disabled');
	               }else{
	                   $("#prev-btn").removeClass('disabled');
	                   $("#next-btn").removeClass('disabled');
	                   $("#btn-finish").addClass('disabled');
	               }
	            });                         
		 
	
</script>