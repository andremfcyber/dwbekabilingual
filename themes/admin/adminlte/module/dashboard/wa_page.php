<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="callout callout-info">
              <h4>Tata cara !</h4>
              <ul>
                <li>Pastikan nomor yang digunakan devicenya tidak terkoneksi dengan web.whatsapp.com</li>
                <li>Klik for get QR CODE</li>
                <li>Tunggu sampai muncul tulisan dan tombol Generate QR Code</li>
                <li>Scan Qr Code</li>
                <li>Close popup, tunggu sekitar 5 menitan sampai status connected</li>
                <li>Lanjut testing send wa di menu yang telah disediakan</li>
                <li>NB: Untuk check status bisa klik tombol check status wa apakah connected atau disconnected</li>
              </ul>
            </div>
          </div>
          <div class="col-md-6">
            <button onclick="openQr()" class="btn btn-success">KLIK FOR GET QR CODE</button>
            <button onclick="checkStatus()" class="btn btn-primary">Check Status WA</button>
          </div>
          <div class="col-md-6" id="detail_device">
          
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <form id="test_wa">
            <div class="form-group">
              <label>No WA</label>
              <input name="no_hp"  value="62" class="form-control number_only" placeholder="Awalan 62">
            </div>
            <div class="form-group">
              <label>Message</label>
              <textarea name="pesan" class="form-control">Hallo, ini pesan percobaan</textarea>
            </div>
            <div class="form-group">
              <button class="btn btn-sm btn-success"><i class="fa fa-paper-plane-o"></i> SEND WA</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var token = '<?= token_wa() ?>'; 
  function openQr(){
    blockUI('Harap bersabar, memakan waktu yang cukup lama....');
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>QR CODE</h3></span>`;
    var kimochi = 'https://console.wablas.com/generate/index.php?token='+token+'&url=aHR0cHM6Ly9jb25zb2xlLndhYmxhcy5jb20=';
    $('#dynamicModal .modal-header').html(title);
    
    $('#dynamicModal .modal-body').html('<iframe frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%" src="'+kimochi+'"></iframe>');
    $('#dynamicModal .modal-body').css({
      'height': '400px'
    })
    $('#dynamicModal').modal({show:true});
    $.unblockUI();
  }
  function checkStatus(){
    blockElement('#detail_device', 'Memuat');
    let url = base_url+'/dashboard/check_status_wa'
    $('#detail_device').load(url,function(){
      $.unblockUI('#detail_device');
    });
  }

  $('#test_wa').on('submit', (e) => {
    blockUI('Sedang Mengirim')
    e.preventDefault();
    let no_hp = $('[name="no_hp"]').val();
    let pesan = $('[name="pesan"]').val();
    let url = base_url+'/dashboard/send_wa'

    if(!no_hp){
      alert('No Wa Wajib Diisi')
    }
    if(!pesan){
      alert('Pesan Wajib Diisi')
    }

    let data = {
      'no_hp': no_hp,
      'pesan': pesan,
    }
    $.post(url, data).done(() => {
      alert('Berhasil')
      checkStatus();
      $.unblockUI();
    }).fail(() => {
      alert('Gagal')
      $.unblockUI();
    })
  })

  let menit = 1;
  let interval = 1000*60*menit;
  setInterval(() => {
    checkStatus();
  }, interval);
  $(() => {
    checkStatus();
  })
</script>