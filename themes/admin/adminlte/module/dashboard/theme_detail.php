<div class="box">
    <div class="box-header">
        <h3 class="box-title">Theme Detail</h3>
    </div>
    <!-- /.box-header -->
    <form id="form">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                
                <a href="#" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="<?php echo $theme_detail['thumb_url']; ?>" alt="">
                </a>
                            
            </div>
            <div class="col-md-8">
                <table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $theme_detail['name']; ?></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>:</td>
                        <td><?php echo $theme_detail['description']; ?></td>
                    </tr>
                    <tr>
                        <td>Version</td>
                        <td>:</td>
                        <td><?php echo $theme_detail['version']; ?></td>
                    </tr>
                    <tr>
                        <td>Author</td>
                        <td>:</td>
                        <td><?php echo $theme_detail['author']; ?></td>
                    </tr>
                    <tr>
                        <td>License</td>
                        <td>:</td>
                        <td><?php echo $theme_detail['license']; ?></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><?php ($theme_detail['active'])? $status='active':$status='inactive';echo $status; ?></td>
                    </tr>
                </table>
                <div class="btn-action">
                    <?php if(!$theme_detail['active']){ ?>
                    
                    <a href="javascript:setActiveTheme();" id="btn-activate" class="btn btn-primary">Activate</a>
                    <a href="javascript:uninstallTheme();" id="btn-uninstall" class="btn btn-danger">Delete</a>
                    
                    <?php } ?>
                   
                    <a href="<?php echo base_url(); ?>dashboard/manage_themes" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!-- /.box-body -->
</div>
<script type="text/javascript">
    $(document).ready(function(){

    });

    function setActiveTheme(){

        var target = base_url + 'dashboard/activate_theme';
        var data = { 
            meta_file : '<?php echo $theme_detail['meta_info_file'] ?>', 
            type : '<?php echo $theme_detail['type'] ?>', 
            dir : '<?php echo $theme_detail['dir'] ?>' 
        }

        $("#btn-activate").button("loading");
        $.post(target, data, function(res){

            if(res.status == "1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }
            $("#btn-activate").button("reset");

            setTimeout('window.location.href="'+base_url+'dashboard/manage_themes"',3000);

        },'json');


    }

    function uninstallTheme(){

        var target = base_url + 'dashboard/delete_theme';
        var data = { 
            type : '<?php echo $theme_detail['type'] ?>', 
            dir : '<?php echo $theme_detail['dir'] ?>' 
        }

        $("#btn-uninstall").button("loading");
        $.post(target, data, function(res){

            if(res.status == "1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }
            $("#btn-uninstall").button("reset");

            setTimeout('window.location.href="'+base_url+'dashboard/manage_themes"',3000);

        },'json');


    }
</script>