<style type="text/css">
    .card-visit {
        background-color: #009688;
        text-align: right;
    }
    .card-count-produk, .card-count-status, .card-chart{
        background-color: #ffffff;
    }
    .card {
        margin : 10px;
    }
    .card-body {
        padding: 20px; 
        color: #fff;
        min-height: 120px; 
    }
    .asset-icon {
        float: left;
    }
    .asset-img {
        height: 80px;
        margin-right: 10px;
    }
    h2,h3{
        margin-top: 5px; 
    }
    .label-count{
        color: #888888;
        font-weight: 600;
    }
    .count-product, .count-status{
        color: #1E6030;
    }
    #row_1_col_2{
        float: right;
    }
    .card-title span.title { 
        color: #000;
        font-size: 20px;
    }
    .card-title span.info { 
        color: #00c0ef;
        font-size: 11px;
        margin-left: 5px;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="card">
            <div class="card-body card-count-produk">
                <img src="<?= $theme_url?>/assets/img/tabungan.webp" class="asset-icon asset-img">
                <h2 class="count-product" id="count-Tabungan">0</h3>
                <h4 class="label-count">Tabungan</h4>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-4">
        <div class="card">
            <div class="card-body card-count-produk">
                <img src="<?= $theme_url?>/assets/img/pinjaman.webp" class="asset-icon asset-img">
                <h2 class="count-product" id="count-Kredit">0</h3>
                <h4 class="label-count">Pinjaman</h4>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-4">
        <div class="card">
            <div class="card-body card-count-produk">
                <img src="<?= $theme_url?>/assets/img/deposit.webp" class="asset-icon asset-img">
                <h2 class="count-product" id="count-Deposito">0</h3>
                <h4 class="label-count">Deposito</h4>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <!-- PENGUNJUNG WEBSITE COUNT | DISABLE | ACTIVEKAN JIKA ADA GOOGLE ANALITIC 
        <section class="col-lg-4 col-xs-12 connectedSortable" id="row_1_col_1">
        <div class="card">
            <div class="card-body card-visit">
                <i class="fa fa-users fa-5x asset-icon"></i>
                <h3>Jumlah Pengunjung</h3>
                <h2 id="visitedAll">0</h2>
            </div>
        </div>
    </section> -->
     <section class="col-lg-8 col-xs-12 connectedSortable" id="row_1_col_2">
        <div class="card">
            <div class="card-body card-chart">
              <div id="chartProduct"></div>  
            </div>
        </div>
    </section>
    <section class="col-lg-4 col-xs-12 connectedSortable" id="row_1_col_3">
        <div class="card">
            <div class="card-body card-chart">
              <div id="chartPieProduct"></div>  
            </div>
        </div>
    </section>
</div>
<div class="row">

    <section class="col-md-3 col-xs-6 connectedSortable">

        <div class="card">
            <div class="card-body card-count-status">
                <img src="<?= $theme_url?>/assets/img/valid.webp" class="asset-icon asset-img">
                <h2 class="count-status" id="count-Valid">0</h3>
                <h5 class="label-count">Tervalidasi</h5>
            </div>
        </div>

    </section>

    <section class="col-md-3 col-xs-6 connectedSortable">

        <div class="card">
            <div class="card-body card-count-status">
                <img src="<?= $theme_url?>/assets/img/invalid.webp" class="asset-icon asset-img">
                <h2 class="count-status" id="count-Invalid">0</h3>
                <h5 class="label-count">Invalid & Ditolak</h5>
            </div>
        </div>

    </section>

    <section class="col-md-3 col-xs-6 connectedSortable">

        <div class="card">
            <div class="card-body card-count-status">
                <img src="<?= $theme_url?>/assets/img/lengkapi.webp" class="asset-icon asset-img">
                <h2 class="count-status" id="count-Lengkapi">0</h3>
                <h5 class="label-count">Dilengkapi</h5>
            </div>
        </div>

    </section>

    <section class="col-md-3 col-xs-6 connectedSortable">

        <div class="card">
            <div class="card-body card-count-status">
                <img src="<?= $theme_url?>/assets/img/lengkap.webp" class="asset-icon asset-img">
                <h2 class="count-status" id="count-Lengkap">0</h3>
                <h5 class="label-count">Diterima</h5>
            </div>
        </div>

    </section>


</div>
<div class="row">
    <section class="col-md-12 col-xs-12 connectedSortable" id="parent_referal">
        <div class="card">
            <div class="card-body card-chart">
                <div class="card-title">
                    <span class="title"> Alamat Referal</span>
                    <span class="info"><i class="fa fa-info"></i> Gunakan Alamat dibawah untuk mendapatkan nasabah</span>
                </div>
                <div id="referal"></div>  
            </div>
        </div>
    </section>
</div>
<div class="row">
    <!-- PENGUMJUNG WEBSITE GRAPIC | DISABLE 
        <section class="col-md-6 col-xs-12 connectedSortable">
        <div class="card">
            <div class="card-body card-chart">
              <div id="chartDevice"></div>  
            </div>
        </div>
    </section> -->
    <section class="col-md-6 col-xs-12 connectedSortable">
        <div class="card">
            <div class="card-body card-chart">
              <div id="chartBySex"></div>  
            </div>
        </div>
    </section>
    <section class="col-md-6 col-xs-12 connectedSortable">
        <div class="card">
            <div class="card-body card-chart">
              <div id="chartByJob"></div>  
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(function () {
      $.post('<?= base_url('dashboard/getAllVisited');?>', (response) => {
        $("#visitedAll").html(response);
      });

      $.post('<?= base_url('dashboard/getSubByTypeProduct');?>', (response)=> {
        response.Tabungan && $('#count-Tabungan').html(response.Tabungan);
        response.Kredit && $('#count-Kredit').html(response.Kredit);
        response.Deposito && $('#count-Deposito').html(response.Deposito);
      }, 'json');

      $.post('<?= base_url('dashboard/getSubByStatus');?>', (response)=> {
        console.log(response)
        $('#count-Valid').html(response.Diterima + response.Dilengkapi + response.Verifikasi + response.Validasi);
        $('#count-Invalid').html(response.Invalid + response.Ditolak);
        $('#count-Lengkapi').html(response.Diterima + response.Dilengkapi + response.Verifikasi);
        $('#count-Lengkap').html(response.Diterima);

      }, 'json');

      $.post('<?= base_url('dashboard/getVisitByDevice');?>', (response) => {
        Highcharts.chart('chartDevice', {
            chart: {
                type: 'area'
            },
            title: {
                text: 'Grafik Pengunjung Website '
            },
            subtitle: {
                text: 'berdasarkan device'
            },
            xAxis: {
                categories : response.date
            },
            yAxis: {
                title: {
                    text: 'Jumlah Pengunjung'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                min : 0
            },
            tooltip: {
                pointFormat: '{series.name} <b>{point.y}</b>'
            },
            series: response.series
        });
      }, 'json');
    });
    $.post('<?= base_url('dashboard/getSubByProduct');?>', (response) => {
        Highcharts.chart('chartProduct', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Pengajuan Produk'
            },
            subtitle: {
                text: 'Klik untuk lihat detail pengajuan produk'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Jumlah Pengajuan Produk'
                },
                allowDecimals: false,
            },
            legend: {
                enabled: false
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
            },

            series: [
                {
                    name: "Produk",
                    colorByPoint: true,
                    data: response.tipe_product
                }
            ],
            drilldown: {
                series: response.product
            }

        });
        Highcharts.chart('chartPieProduct', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Grafik Pengajuan Produk',
                floating : true
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Jumlah Pengajuan Produk'
                },
                allowDecimals: false,
            },
            legend: {
                enabled: false
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [
                {
                    name: "Produk",
                    colorByPoint: true,
                    data: response.tipe_product
                }
            ],
            drilldown: {
                series: response.product
            }

        });
    }, 'json');

    $.post("<?=base_url('dashboard/getSubBySex')?>", (response) => {
        Highcharts.chart('chartBySex', {
           chart: {
                type: 'bar'
            },
            title: {
                text: 'Calon Nasabah'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Jumlah'
                },
                allowDecimals: false,
            },
            legend: {
                enabled: false
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
            },

            series: [
                {
                    name: "Jenis Kelamin",
                    colorByPoint: true,
                    data: response
                }
            ],
             
        });
    }, 'json');

    $.post("<?=base_url('dashboard/getSubByJob')?>", (response) => {
        Highcharts.chart('chartByJob', {
           chart: {
                type: 'column'
            },
            title: {
                text: 'Calon Nasabah'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Jumlah'
                },
                allowDecimals: false,
            },
            legend: {
                enabled: false
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
            },

            series: [
                {
                    name: "Jenis Kelamin",
                    colorByPoint: true,
                    data: response
                }
            ],
             
        });
    }, 'json');

    $(() => {
        <?php if(get_role() == 18) {?>
            blockElement('#parent_referal .card-body','Memuat Widget', '16px');
            let referal = base_url+'/sales/sales/clipboard/<?= get_ref() ?>';
            $('#referal').load(referal,function(){
                $('#parent_referal .card-body').unblock();
            });
        <?php } else { ?>
            $('#parent_referal').hide();
        <?php } ?>
    })

</script>


