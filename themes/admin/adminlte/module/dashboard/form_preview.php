

<form id="form">
  <input type="hidden" name="id_product" value="<?php echo $id_product;?>">
  <input type="hidden" name="id_nasabah" value="<?php echo $id_nasabah;?>">
<?php 
  foreach($form_header as $fh){
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $fh['name_header']?></h3>

            </div>
            

            <div class="box-body">
              
                
              <?php

                $form_attr = $this->db->order_by('urutan','ASC');
                $form_attr = $this->db->get_where("form_fileds",array("form_header_id" => $fh['id']))->result_array();

                foreach ($form_attr as $fa) {
                   
                  $input_type = $fa['input_type'];

                  switch ($input_type) {

                    case 'text':
                      
                      generateTextInput($fa['label'],$fa['id']);

                      break;

                    case 'checkbox':

                      $form_attr = $this->db->order_by('urutan','ASC');
                      $form_attr_source = $this->db->get_where("form_attribut",array("form_filed_id" => $fa['id']))->result_array();

                      generateCheckbox($fa['label'],$fa['id'],$form_attr_source);
                      
                      break;

                    // case 'radio':

                    //   $form_attr = $this->db->order_by('urutan','ASC');
                    //   $form_attr_source_radio = $this->db->get_where("form_attribut",array("form_filed_id" => $fa['id']))->result_array();

                    //   generateRadio($fa['label'],$fa['id'],$form_attr_source_radio);

                    // default:

                    //   break;
                  }


                }
                
              ?>
              
            </div>

        </div>

    </div>
   
</div>
<?php
  }
?>
<button type="submit">Simpan</button>
<!-- <button onclick="goBack()" class="btn btn-default">Back</button>  -->

</form>

<script>
    function goBack() {
        window.history.back();
    }
</script>
           
<script type="text/javascript">
  
  $(document).ready(function(){

    $("#form").submit(function(){

      var data = $(this).serialize();
      var target = base_url + 'product/save_data_formulir';

      $.post(target, data, function(res){

        if(res.status == "1"){

          toastr.success(res.msg, "Response Server");

        }else{

          toastr.error(res.msg, "Response Server");

        }

      },'json');

      return false;

    });

  });

</script>