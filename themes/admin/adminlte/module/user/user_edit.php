<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>

        <div class="col-md-12">
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Info !</h4>
                Untuk AO/FO, Kabid disediakan menu terpisah. Jangan membuat Kabid, AO/FO disini.
                Jangan Gunakan User Group *(Sales), Kabid dan komite di sini.
            </div>
        </div>
        <!-- left column -->
        <div class="col-md-6">
            <form role="form" id="form">
                <input type="hidden" name="old_photo" value="<?php echo $dataedit['picture']; ?>">
                <input type="hidden" name="old_user_group" value="<?php echo $dataedit['user_group']; ?>">
                <input type="hidden" name="username" value="<?php echo $dataedit['username']; ?>">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="photo">Photo</label>
                                    <div class="img-prev" id="photo_preview">
                                        <?php
                                        if($dataedit['picture'] != ""){

                                            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                                            $file = $upload_path.'/'.$dataedit['picture'];

                                            if(file_exists($file)){

                                                $path = base_url().'upload/photo/'.$dataedit['picture'];

                                                echo "<img src='".$path."' class='img-responsive' />
                                        <a class='btn btn-sm btn-danger btn-reset-preview' href='javascript:resetFileUpload(\"#photo_preview\");'><i class='fa fa-remove'></i>
                                        </a>";

                                            }else{

                                                echo "<h1>Photo Preview</h1>";

                                            }
                                        }else{ ?>

                                            <h1>Photo Preview</h1>

                                        <?php } ?>
                                    </div>
                                    <br>
                                    <input type="file" class="form-control" name="photo" id="photo">

                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" class="validate[required] form-control" id="first_name" value="<?php echo $dataedit['first_name']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" class="validate[required] form-control" id="last_name" value="<?php echo $dataedit['last_name']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="division">User Group</label>
                                    <select class="form-control" name="user_group" id="user_group">
                                        <?php
                                        foreach ($user_groups as $ug){
                                            if($dataedit['user_group']==$ug['id']){
                                                $selected = "selected";
                                            }else{
                                                $selected = "";
                                            }
                                            echo "<option ".$selected." value='".$ug['id']."'>".$ug['name']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" readonly class="form-control" value="<?php echo $dataedit['username']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text"  name="email" class="validate[required] form-control" id="email" value="<?php echo $dataedit['email']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" <?php ($dataedit['status']=="1")? $attr="selected": $attr="" ;echo $attr;?>>Active</option>
                                        <option value="0" <?php ($dataedit['status']=="0")? $attr="selected": $attr="" ;echo $attr;?>>InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                        &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                    </div>

                </div>
                <!-- /.box -->
            <!-- /.box -->
            </form>
        </div>

        <div class="col-md-6">
            <form id="change_pass">
                <input type="hidden" name="id" value="<?php echo $dataedit['id']; ?>">
                <input type="hidden" name="email" value="<?php echo $dataedit['email']; ?>">
                <input type="hidden" name="username" value="<?php echo $dataedit['username']; ?>">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change Password</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">

                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password"  name="new_password" class="validate[required] form-control" id="new_password" placeholder="New Password">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password</label>
                            <input type="password"  name="confirm_password" class="validate[required,equals[new_password]] form-control" id="confirm_password" placeholder="Confirm Password">
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                        &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                    </div>

                </div>
            </form>
        </div>

</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){

        $("#form").submit(function(){

            if($(this).validationEngine('validate')) {

                showLoading();

                setTimeout('saveFormData();',3000);

            }

            return false;

        });

        $("#photo").change(function(){
            readURL(this, "#photo");
        });

        $("#change_pass").submit(function(){

            if($(this).validationEngine('validate')) {

                var target = base_url + "user/user/change_pass";
                var data = $("#change_pass").serialize();

                $.post(target,data,function(res){

                    if (res.status == "1") {

                        toastr.success(res.msg, 'Response Server');

                    } else {

                        toastr.error(res.msg, 'Response Server');

                    }


                },'json');
            }

            return false;

        });

        $("#form").validationEngine();
        $("#change_pass").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"user/user/edit/<?php echo $dataedit['id']; ?>";

        var formData = new FormData($("#form")[0]);
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {
                hideLoading();
                if(data.status=="1"){
                    toastr.success(data.msg, 'Response Server');
                }else{
                    toastr.error(data.msg, 'Response Server');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function readURL(input, selector) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                    img.attr('src', e.target.result);
                    $(selector + '_preview').html(img);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    function resetFileUpload(selector){

        $(selector + '_preview').html("<h1>Photo Preview</h1>");
        $(selector).val('');

    }

    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
</script>