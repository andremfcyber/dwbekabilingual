<footer class="main-footer visible-lg visible-md hidden-sm hidden-xs">
    <div class="pull-right">
        <b>Version</b> Beta 0.9.2
    </div>
    <strong>Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a> <?= date('Y') ?> <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a></strong>
</footer>

<footer class="main-footer hidden-lg hidden-md visible-sm visible-xs pull-center text-center"> 
    <strong><b>Version</b> Beta 0.9.2 <br>  Powered by <a href="https://www.tranyar.co.id/"> Tranyar</a>  <?= date('Y') ?> <a href="https://www.tranyar.co.id/"> <img src="https://www.tranyar.co.id/assets/client-tranyar/newtemplate/images/logotranyar.png" style="height: 22px;"> </a></strong>
</footer>