<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $this->settings->site_name; ?> | <?=$title?></title>
    
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo $theme_url;?>/css/materialize.min.css"  media="screen,projection"/>

    <?php
    if ( isset( $css_files ) && is_array( $css_files ) ) :
      foreach ( $css_files as $css ) : ?>
        <link rel="stylesheet" href="<?=$css?><?php echo '?r='.rand(1,1000);?>"> <?php echo "\n";
      endforeach;
    endif;
    ?>

    <script>
        var site_url = '<?=site_url()?>';
        var base_url = '<?=base_url()?>';
    </script>
    
  
  </head>
<body <?=isset($body_class) ? ' class="'.$body_class.'"' : ''?> >
 



