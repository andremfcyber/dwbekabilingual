<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
</style> 
<section class="p-1"  >
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Whistle blowing System</li>
            </ol>
        </nav>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-9">
            <?php 
                foreach ($v_halaman as $v_half): 
            ?>
            <div class="text-center p-3">
                <h2><?php  echo $v_half['judul']; ?></h2>
            </div>
            <div class="img">
                <img width="100%" src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="Icon" />
            </div> 
            <div class="text-center p-3">
                <p><?php  echo $v_half['isi']; ?></p>
            </div>
            <?php 
                endforeach;
            ?>
            <!-- <img src="http://www.bank-eka.co.id/wp-content/uploads/2020/07/WBS-Banner.jpg" width="100%" > -->
            </div>
        </div>
    </div>
</section>