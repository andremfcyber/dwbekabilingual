<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }
   .brdash {
    height: 95%;
    background: #f9f9f9; padding: 15px; border: 2px dashed #ffc027;
   }
   .dex {
       /* height:174px; */
       overflow:hidden;
   }
   .dex h4 {
       color:#e6232b;
       font-weight:bold;
       font-size:17px;
       margin-bottom:5px;
   }
   .dex p{
    line-height: 25px;
       margin:0px;
   }

   .nopad {
       padding:0px;
   }
   
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Kantor Cabang</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container">
         <div class="row m-2 d-flex " >
    <?php 
            foreach ($v_halaman as $v_half): 
         ?>  
        <div class="col-md-6 nopad">
            <div class="row m-2 d-flex brdash " >
                <div class="col-md-6 align-selft">
                    <div class="img">
                        <img class="lazy" data-src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="img" width="100%" />
                    </div> 
                </div>
                <div class="col-md-6 align-selft pb-3">
                   <div class="dex">
                    <h4><?php  echo $v_half['judul']; ?></h4>
                        <?php  echo $v_half['isi']; ?>
                    </div>
                </div> 
            </div>
        </div>
        <?php
            endforeach;
        ?> 
        </div>
    </div>
</section>