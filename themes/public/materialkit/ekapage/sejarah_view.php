 
<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
    img {
        margin-right: 21px;
        vertical-align: middle;
        border-style: none;
    }
    ol, ul {
        margin-left: 36px!important;
        margin-bottom: 17px!important;
        padding: 0;
        margin: 0;
    }
</style> 
<section class="p-1"  >
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sejarah</li>
            </ol>
        </nav>
        <?php 
            foreach ($v_halaman as $v_half):
        ?> 
            <div class="text-center p-3">
                <h2 class="redbold"><?php  echo $v_half['judul']; ?></h2>
            </div><hr>
        <?php
                echo $v_half['isi'].'<hr>';
            endforeach;
        ?> 
        
    </div>
</section>