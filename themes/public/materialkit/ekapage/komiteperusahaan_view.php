<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
    strong { 
        color: #DE1213; 
        font-weight:bold;
    }
    .bl {
        border-left: 1px solid #DDD;
        padding-left: 34px;
    }
    ol, ul {
        padding: 0;
        margin: 0;
        margin-left: 34px;
    }
</style>
<!-- <section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Selamat Datang</h2>
        </div>
    </div>
</section> -->
<section class="p-31"  >
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Management</li>
            </ol>
        </nav>
        <h3 class="av-special-heading-tag" itemprop="headline">Komite-Komite Perusahaan</h3>
        <hr>
        <?php 
            foreach ($v_halaman as $v_half):  
         ?> 
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4 bl">
                <div class="img mt-2">
                    <img class="lazy" data-src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="img" width="100%"/>
                </div> 
            </div>
            <div class="col-md-8 bl">
                <div class="avia_textblock" itemprop="text">
                    <?php  echo $v_half['isi']; ?>
                </div> 
            </div>
        </div>
        <hr> 
        <?php 
            endforeach;
        ?> 
        <hr>
         
        <!-- <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
            <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/Bpk-Johansyah-2.png" alt="" title="Bpk Johansyah" itemprop="contentURL">
            </div>
            <div class="col-md-8 bl">
            <div class="avia_textblock" itemprop="text">
                <p style="text-align: justify;">
                    <span style="font-size: 14pt;"><strong>Komisaris </strong></span><br />
                    <span style="font-size: 14pt;"> Johansyah A. B.</span>
                </p>
                <p style="text-align: justify;">
                    <strong>Pribadi</strong><br />
                    Lahir di Palembang, 22 Oktober 1959.
                </p>
                <p style="text-align: justify;">
                    <strong>Kewarganegaraan dan Domisili</strong><br />
                    Warga Negara Indonesia dan berdomisili di Bandar Lampung Provinsi Lampung.
                </p>
                <p style="text-align: justify;">
                    <strong>Pendidikan</strong><br />
                    Memperoleh gelar Sarjana Sosial dan Ilmu Politik dari Sekolah Tinggi Ilmu Sosial dan Ilmu Politik Candradimuka tahun 1989.
                </p>
                <p style="text-align: justify;">
                    <strong>Jabatan dan Periode Jabatan</strong><br />
                    Menjabat sebagai Komisaris Independen PT BPR Eka Bumi Artha sejak 1 Juli 2017 berdasarkan hasil keputusan Rapat Umum Pemegang Saham Luar Biasa PT BPR Eka Bumi Artha pada tanggal 23 Mei 2017 yang dituangkan dalam “Akta Pernyataan
                    Keputusan Rapat PT BPR Eka Bumi Artha“ tertanggal 14 Juni 2017. Telah mendapat persetujuan dari Otoritas Jasa Keuangan Nomor S-163/KO.0741/2017 tanggal 13 Juli 2017. Periode jabatan 1 Juli 2017 sampai dengan 30 Juni 2022.
                </p>
                <p style="text-align: justify;">
                    <strong>Perjalanan Karir</strong><br />
                    Berkarir di Bank Indonesia (BI) (1995-2015) dengan jabatan terakhir sebagai Asisten Direktur.
                </p>
            </div>

            </div>
        </div>
        <hr>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
            <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/komisaris2-246x300.jpg" alt="" title="komisaris2" itemprop="contentURL">
            </div>
            <div class="col-md-8 bl">
            <div class="avia_textblock " itemprop="text"><p style="text-align: justify;"><span style="font-size: 14pt;"><strong>Komisaris </strong></span><br>
                <span style="font-size: 14pt;"> Muji</span></p>
                <p style="text-align: justify;"><strong>Pribadi</strong><br>
                Lahir di Sungai Pinang, Sumatera Selatan, 16 Agustus 1957.</p>
                <p style="text-align: justify;"><strong>Kewarganegaraan dan Domisili</strong><br>
                Warga Negara Indonesia, berdomisili di Bandar Lampung Provinsi Lampung.</p>
                <p style="text-align: justify;"><strong>Pendidikan</strong><br>
                Memperoleh gelar Sarjana Ekonomi Jurusan Manajamen dari Universitas Palembang pada tahun 1989, kemudian meraih gelar Magister Manajemen Jurusan Keuangan dari Universitas Lampung pada tahun 2010.</p>
                <p style="text-align: justify;"><strong>Jabatan dan Periode Jabatan</strong><br>
                Menjabat sebagai Komisaris Independen PT BPR Eka Bumi Artha sejak 1 Juli 2017 berdasarkan hasil keputusan Rapat Umum Pemegang Saham Luar Biasa PT BPR Eka Bumi Artha pada tanggal 23 Mei 2017 yang dituangkan dalam “Akta Pernyataan Keputusan Rapat PT BPR Eka Bumi Artha“ tertanggal 14 Juni 2017. Telah mendapat persetujuan dari Otoritas Jasa Keuangan Nomor S-163/KO.0741/2017 tanggal 13 Juli 2017. Periode jabatan 1 Juli 2017 sampai dengan 30 Juni 2022.</p>
                <p style="text-align: justify;"><strong>Perjalanan Karir</strong></p>
                <ul>
                <li style="text-align: justify;">Komisaris Independen, PT BPR Eka Bumi Artha (2014-2017)</li>
                <li style="text-align: justify;">Berkarir di Bank Indonesia (BI) (1981-2012) dengan jabatan terakhir sebagai Asisten Direktur.</li>
                </ul>
                </div>
            </div>
        </div>
        <br>
        <h3 class="av-special-heading-tag" itemprop="headline">DIREKSI</h3>
        <hr>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
                <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/dirut-248x300.jpg" alt="" title="dirut" itemprop="contentURL">
            </div>
            <div class="col-md-8 bl">
                <div class="avia_textblock" itemprop="text">
                    <p style="text-align: justify;">
                        <span style="font-size: 14pt; color: #ff0000;"><strong>Direktur Utama</strong></span><br />
                        <span style="font-size: 14pt; color: #000000;"> Eko Budiyono</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Pribadi</strong></span><br />
                        <span style="color: #000000;"> Lahir di Juwana, Pati, Jawa Tengah, 7 Desember 1967.</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Kewarganegaraan dan Domisili</strong></span><br />
                        <span style="color: #000000;"> Warga Negara Indonesia dan berdomisili di Metro Provinsi Lampung.</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Pendidikan</strong></span><br />
                        <span style="color: #000000;">
                            Memperoleh gelar Sarjana Ekonomi Jurusan Manajemen dari Universitas Muhammadiyah Metro tahun 1997. Kemudian mengikuti kursus Master of Bussiness Administration dari Institut Management John Lutter Indonesia periode tahun
                            1994-1995.
                        </span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Jabatan dan Periode Jabatan</strong></span><br />
                        <span style="color: #000000;">
                            Menjabat sebagai Direktur Utama PT BPR Eka Bumi Artha sejak 1 Juli 2017 berdasarkan hasil keputusan Rapat Umum Pemegang Saham Luar Biasa PT BPR Eka Bumi Artha pada tanggal 23 Mei 2017 yang dituangkan dalam “Akta Pernyataan
                            Keputusan Rapat PT BPR Eka Bumi Artha“ tertanggal 14 Juni 2017. Telah mendapat persetujuan dari Otoritas Jasa Keuangan Nomor S-163/KO.0741/2017 tanggal 13 Juli 2017. Periode jabatan 1 Juli 2017 sampai dengan 30 Juni 2022.
                        </span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Perjalanan Karir</strong></span>
                    </p>
                    <ul>
                        <li style="text-align: justify;"><span style="color: #333333;">Direktur Utama PT BPR Eka Bumi Artha (2012-2017).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Direktur PT BPR Eka Bumi Artha (1998-2012).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Financial dan Administrasi merangkap Kepala Bagian Service dan Pendanaan PT BPR Eka Bumi Artha (1993-1998).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Keuangan PT BPR Eka Bumi Artha (1990-1993).</span></li>
                    </ul>
                </div> 
            </div>
        </div>
    <hr>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
                <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/dirut-bisnis-241x300.jpg" alt="" title="dirut-bisnis" itemprop="contentURL">
            </div>
            <div class="col-md-8 bl">
                <div class="avia_textblock" itemprop="text">
                    <p style="text-align: justify;">
                        <span style="font-size: 14pt; color: #ff0000;"><strong>Direktur Kepatuhan</strong></span><br />
                        <span style="font-size: 14pt; color: #000000;"> Dari Lukito Sari</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Pribadi</strong></span><br />
                        <span style="color: #000000;"> Lahir di Metro, Lampung, 30 November 1969.</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Kewarganegaraan dan Domisili</strong></span><br />
                        <span style="color: #000000;"> Warga Negara Indonesia dan berdomisili di Metro Lampung</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Pendidikan</strong></span><br />
                        <span style="color: #000000;"> Memperoleh gelar Sarjana Ekonomi Jurusan Manajemen dari Universitas Satya Wacana Salatiga tahun 1996.</span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Jabatan dan Periode Jabatan</strong></span><br />
                        <span style="color: #000000;">
                            Menjabat sebagai Direktur Kepatuhan PT BPR Eka Bumi Artha sejak 1 Juli 2017 berdasarkan hasil keputusan Rapat Umum Pemegang Saham Luar Biasa PT BPR Eka Bumi Artha pada tanggal 23 Mei 2017 yang dituangkan dalam “Akta Pernyataan
                            Keputusan Rapat PT BPR Eka Bumi Artha“ tertanggal 14 Juni 2017. Telah mendapat persetujuan dari Otoritas Jasa Keuangan Nomor S-163/KO.0741/2017 tanggal 13 Juli 2017. Periode jabatan 1 Juli 2017 sampai dengan 30 Juni 2022.
                        </span>
                    </p>
                    <p style="text-align: justify;">
                        <span style="color: #ff0000;"><strong>Perjalanan Karir</strong></span>
                    </p>
                    <ul>
                        <li style="text-align: justify;"><span style="color: #333333;">Direktur Bisnis, PT BPR Eka Bumi Artha (2015-2017).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Divisi Keuangan dan Sumberdaya, PT BPR Eka Bumi Artha (2015).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Bidang Operasional &amp; Sumberdaya, PT BPR Eka Bumi Artha (2014-2015).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Kantor Pusat Operasional Metro, PT BPR Eka Bumi Artha (2012-2014).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Operasional, PT BPR Eka Bumi Artha (2007-2012).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Cabang PT BPR Eka Bumi Artha Way Jepara (2005-2007).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Cabang PT BPR Eka Bumi Artha Bandarjaya (2002-2005).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Kredit, PT BPR Eka Bumi Artha (2002).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Seksi Kredit Pegawai, PT BPR Eka Bumi Artha (2000-2002).</span></li>
                        <li style="text-align: justify;"><span style="color: #333333;">Kepala Sub Bagian Pengembangan Sumberdaya Manusia, PT BPR Eka Bumi Artha (2000).</span></li>
                    </ul>
                </div>

            </div>
        </div>
        <hr>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
                <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/dirut-oprasional-249x300.jpg" alt="" title="dirut-oprasional" itemprop="contentURL">
            </div>
            <div class="col-md-8 bl">
            <div class="avia_textblock" itemprop="text">
                <p style="text-align: justify;">
                    <span style="font-size: 14pt; color: #ff0000;"><strong>Direktur Operasional</strong></span><br />
                    <span style="font-size: 14pt; color: #000000;"> Nugroho Fuad Rifai</span>
                </p>
                <p style="text-align: justify;">
                    <span style="color: #ff0000;"><strong>Pribadi</strong></span><br />
                    <span style="color: #000000;"> Lahir di Magelang, Jawa Tengah, 30 September 1974.</span>
                </p>
                <p style="text-align: justify;">
                    <span style="color: #ff0000;"><strong>Kewarganegaraan dan Domisili</strong></span><br />
                    <span style="color: #000000;"> Warga Negara Indonesia dan berdomisili di Metro Lampung.</span>
                </p>
                <p style="text-align: justify;">
                    <span style="color: #ff0000;"><strong>Pendidikan</strong></span><br />
                    <span style="color: #000000;"> Memperoleh gelar Sarjana Ekonomi Jurusan Akuntansi dari Universitas Muhammadiyah Metro tahun 2016.</span>
                </p>
                <p style="text-align: justify;">
                    <span style="color: #ff0000;"><strong>Jabatan dan Periode Jabatan</strong></span><br />
                    <span style="color: #000000;">
                        Menjabat sebagai Direktur Operasional PT BPR Eka Bumi Artha sejak 1 Juli 2017 berdasarkan hasil keputusan Rapat Umum Pemegang Saham Luar Biasa PT BPR Eka Bumi Artha pada tanggal 23 Mei 2017 yang dituangkan dalam “Akta Pernyataan
                        Keputusan Rapat PT BPR Eka Bumi Artha“ tertanggal 14 Juni 2017. Telah mendapat persetujuan dari Otoritas Jasa Keuangan Nomor S-163/KO.0741/2017 tanggal 13 Juli 2017. Periode jabatan 1 Juli 2017 sampai dengan 30 Juni 2022.
                    </span>
                </p>
                <p style="text-align: justify;">
                    <span style="color: #ff0000;"><strong>Perjalanan Karir</strong></span>
                </p>
                <ul>
                    <li style="text-align: justify;"><span style="color: #333333;">Direktur Operasional, merangkap Direktur yang membawahkan fungsi kepatuhan, PT BPR Eka Bumi Artha (2015-2017).</span></li>
                    <li style="text-align: justify;"><span style="color: #333333;">Direktur Operasional, PT BPR Eka Bumi Artha (2012-2017).</span></li>
                    <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Teknologi Sistem Informasi, PT BPR Eka Bumi Artha (2007-2012).</span></li>
                    <li style="text-align: justify;"><span style="color: #333333;">Kepala Bagian Keuangan, Akuntansi &amp; Sistem Informasi, PT BPR Eka Bumi Artha (2002-2007).</span></li>
                </ul>
            </div> 
            </div>
        </div>
        <br>
        
        <h3 class="av-special-heading-tag" itemprop="headline">Komite-Komite Perusahaan </h3> 
        <hr>
        <div class="row justify-content-center m-2 d-flex" >
            <div class="col-md-4">
                <img src="https://everyonehome.org/wp-content/uploads/2016/02/committee.jpg" width="80%"> 
            </div>
            <div class="col-md-8 bl">
                <div class="entry-content-wrapper clearfix"> 
                    <div style="padding-bottom: 5px;" class="av-special-heading av-special-heading-h5 avia-builder-el-1 el_after_av_heading el_before_av_textblock">
                        <h5 class="av-special-heading-tag" itemprop="headline"><b>Komite-Komite di bawah Direksi</b></h5>
                        <div class="special-heading-border"><div class="special-heading-inner-border"></div></div>
                    </div>
                    <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="avia_textblock" itemprop="text">
                            <ol>
                                <li>Komite Kredit;</li>
                                <li>Komite Perencanaan &amp; <em>Asset and Liabilities Committee</em> <em>(ALCO)</em>;</li>
                                <li><em>General Committee</em> <em>(GECO)</em>;</li>
                                <li>Komite Pengarah Teknologi Informasi <em>(IT Steering Committee)</em>;</li>
                                <li>Komite APU &amp; PPT;</li>
                                <li>Komite Pelaporan Keuangan;</li>
                                <li>Komite Manajemen Risiko;</li>
                                <li>Komite Resolusi <em>Non Performing Loan (NPL)</em>;</li>
                                <li>Komite Pengaduan Nasabah;</li>
                                <li>Komite Pengelola <em>Whistle Blowing System (WBS)</em>;</li>
                                <li>Komite Kebijakan Perkreditan (KKP);</li>
                                <li>Komite Validasi CIF <em>(Customer Identification File)</em>;</li>
                                <li>Komite Manajemen Sumber Daya Manusia <em>(Human Resources Management Committee / HRMCO).</em></li>
                            </ol>
                        </div>
                    </section>
                    <div style="padding-bottom: 5px;" class="av-special-heading av-special-heading-h5 avia-builder-el-3 el_after_av_textblock el_before_av_textblock">
                        <h5 class="av-special-heading-tag" itemprop="headline">Komite-Komite di bawah Dewan Komisaris</h5>
                        <div class="special-heading-border"><div class="special-heading-inner-border"></div></div>
                    </div>
                    <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="avia_textblock" itemprop="text">
                            <ol>
                                <li>Komite Pemantau Risiko;</li>
                                <li>Komite Audit.</li>
                            </ol>
                        </div>
                    </section>
                    <div class="hr hr-default avia-builder-el-5 el_after_av_textblock avia-builder-el-last">
                        <span class="hr-inner"><span class="hr-inner-style"></span></span>
                    </div>
                </div>

            </div>
        </div>
        <br> -->
    </div>
</section>