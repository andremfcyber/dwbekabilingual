<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }
   .brdash {
    height: 95%;
    background: #fafafa; padding: 15px; border: 2px dashed #ffc027;
   }
   .dex {
       /* height:174px; */
       overflow:hidden;
   }
   .dex h3 {
       color:#e6232b;
       font-weight:bold; 
       margin-bottom:5px;
   }
   .dex p{
    line-height: 25px;
       margin:0px;
   }

   .nopad {
       padding:0px;
   }

    b, strong {
        color: #d61228;
        font-weight: bolder;
    }
   table {
        border: 1px solid #DDD;
   }
   table tr  {
        background:#f8f8f8;
   }

   table tr > td {
        padding: 12px;
        border: 1px solid #DDD;
        background: #FFF;
    }
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Mobile Banking</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container"> 
    <?php 
            foreach ($v_halaman as $v_half): 
         ?>   
            <div class="row m-2 d-flex brdash " >
                <div class="col-md-4  ">
                    <div class="img">
                        <img class="lazy" data-src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="img" width="100%" />
                    </div> 
                </div>
                <div class="col-md-8 align-selft pb-3">
                   <div class="dex">
                    <h3><?php  echo $v_half['judul']; ?></h3>
                        <?php  echo $v_half['isi']; ?>
                    </div>
                </div> 
            </div> 
        <?php
            endforeach;
        ?>  
    </div>
</section>