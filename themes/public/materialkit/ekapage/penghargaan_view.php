<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }

   ol {
    margin-left: 39px;
   }

   .alignleft {
        float: left!important;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
    
    .alignright { 
        float: right;
    }
    
    .entry-content table td {
        border-radius: 68px;
        background: #fcfcfc;
        border: 1px solid #f0d862;
        padding: 21px;
    }
    
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Penghargaan</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container">
        <?php 
            foreach ($v_halaman as $v_half):
                echo $v_half['isi'].'<hr>';
            endforeach;
        ?> 
        
        <!-- <main class="template-page content av-content-small units" role="main" itemprop="mainContentOfPage"> 
            <article class="post-entry post-entry-type-page post-entry-175" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                <div class="entry-content-wrapper clearfix">
                    <div class="entry-content" itemprop="text">
                        <p class="titlepage">&nbsp;</p>
                        <article class="row justify-content-center post-entry post-entry-type-page post-entry-175">
                            <div class="col-md-8 entry-content-wrapper clearfix">
                                <div class="entry-content">
                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr style="height: 55.6136px;">
                                                <td style="width: 696.719px; height: 55.6136px;">
                                                    <p style="text-align: center;">
                                                        <img
                                                            class="alignleft wp-image-905"
                                                            src="http://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017.jpg"
                                                            alt="Infobank 2017"
                                                            width="172"
                                                            height="129"
                                                            srcset="
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017.jpg           3420w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-300x226.jpg    300w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-768x578.jpg    768w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-1030x775.jpg  1030w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-1500x1128.jpg 1500w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-705x530.jpg    705w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-2017-450x338.jpg    450w
                                                            "
                                                            sizes="(max-width: 172px) 100vw, 172px"
                                                        />
                                                        <img
                                                            class="alignright wp-image-904"
                                                            src="http://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017.jpg"
                                                            alt="Infobank Golden 2017"
                                                            width="125"
                                                            height="125"
                                                            srcset="
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017.jpg           3296w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-80x80.jpg       80w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-300x300.jpg    300w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-768x768.jpg    768w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-1030x1030.jpg 1030w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-36x36.jpg       36w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-180x180.jpg    180w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-1500x1500.jpg 1500w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-705x705.jpg    705w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-120x120.jpg    120w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-450x450.jpg    450w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/09/Infobank-Golden-2017-45x45.jpg       45w
                                                            "
                                                            sizes="(max-width: 125px) 100vw, 125px"
                                                        />
                                                        <br />
                                                        Sertifikat Penghargaan dari Infobank tahun 2018<br />
                                                        Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                        Atas Kinerja Keuangan tahun 2017
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr style="height: 241px;">
                                                <td style="width: 696.719px; height: 241px;">
                                                    <p>
                                                        <img
                                                            class="wp-image-730 alignleft"
                                                            src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-243x300.jpg"
                                                            alt=""
                                                            width="170"
                                                            height="210"
                                                            srcset="
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-243x300.jpg    243w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-768x950.jpg    768w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-833x1030.jpg   833w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-1213x1500.jpg 1213w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-570x705.jpg    570w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Bank-Eka-450x556.jpg    450w
                                                            "
                                                            sizes="(max-width: 170px) 100vw, 170px"
                                                        />
                                                    </p>
                                                    <p style="text-align: center;"></p>
                                                    <p>
                                                        Sertifikat Penghargaan dari “ANUGERAH BPR INDONESIA 2017”<br />
                                                        Sebagai BPR Terbaik di Indonesia dengan Predikat “Peringkat I”<br />
                                                        Atas&nbsp;Aset Rp 1 Triliun sampai &lt; Rp 10 Triliun
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr style="height: 142px;">
                                                <td style="width: 696.719px; text-align: center; height: 142px;">
                                                    <img
                                                        class="wp-image-733 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-300x219.jpg"
                                                        alt=""
                                                        width="174"
                                                        height="127"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-300x219.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-768x562.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-1030x753.jpg  1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-1500x1097.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-705x516.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2016-450x329.jpg    450w
                                                        "
                                                        sizes="(max-width: 174px) 100vw, 174px"
                                                    />
                                                    <img
                                                        class="wp-image-735 alignright"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-298x300.jpg"
                                                        alt=""
                                                        width="125"
                                                        height="126"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-298x300.jpg    298w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-80x80.jpg       80w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-768x774.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-1022x1030.jpg 1022w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-36x36.jpg       36w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-180x180.jpg    180w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-1489x1500.jpg 1489w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-700x705.jpg    700w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-120x120.jpg    120w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-450x453.jpg    450w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2016-45x45.jpg       45w
                                                        "
                                                        sizes="(max-width: 125px) 100vw, 125px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2017<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2016
                                                </td>
                                            </tr>
                                            <tr style="height: 161px;">
                                                <td style="width: 696.719px; text-align: center; height: 161px;">
                                                    <p>
                                                        <img
                                                            class="wp-image-740 alignleft"
                                                            src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-300x223.jpg"
                                                            alt=""
                                                            width="174"
                                                            height="129"
                                                            srcset="
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-300x223.jpg    300w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-768x572.jpg    768w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-1030x767.jpg  1030w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-1500x1117.jpg 1500w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-705x525.jpg    705w,
                                                                https://www.bank-eka.co.id/wp-content/uploads/2018/07/INFOBANK-2017_-DIGITAL-450x335.jpg    450w
                                                            "
                                                            sizes="(max-width: 174px) 100vw, 174px"
                                                        />
                                                        Sertifikat Penghargaan dari&nbsp;Infobank dan Isentia tahun 2017<br />
                                                        dalam acara “DIGITAL BRAND AWARDS”<br />
                                                        dengan predikat “PERINGKAT III”<br />
                                                        DIGITAL BRAND BPR Beraset &gt; Rp 1 Triliun
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr style="height: 145px;">
                                                <td style="width: 696.719px; text-align: center; height: 145px;">
                                                    <img
                                                        class="wp-image-732 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-300x224.jpg"
                                                        alt=""
                                                        width="174"
                                                        height="130"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-300x224.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-768x574.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-1030x769.jpg  1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-1500x1120.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-705x526.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-2015-450x336.jpg    450w
                                                        "
                                                        sizes="(max-width: 174px) 100vw, 174px"
                                                    />
                                                    <img
                                                        class="wp-image-734 alignright"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-300x300.jpg"
                                                        alt=""
                                                        width="126"
                                                        height="126"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-300x300.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-80x80.jpg       80w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-768x766.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-1030x1027.jpg 1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-36x36.jpg       36w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-180x180.jpg    180w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-1500x1496.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-705x703.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-120x120.jpg    120w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-450x449.jpg    450w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Award-Infobank-a-2015-45x45.jpg       45w
                                                        "
                                                        sizes="(max-width: 126px) 100vw, 126px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2016<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2015
                                                </td>
                                            </tr>
                                            <tr style="height: 148px;">
                                                <td style="width: 696.719px; text-align: center; height: 148px;">
                                                    <img
                                                        class="wp-image-739 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-300x229.jpg"
                                                        alt=""
                                                        width="174"
                                                        height="133"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-300x229.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-768x586.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-1030x786.jpg  1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-1500x1144.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-705x538.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2013-450x343.jpg    450w
                                                        "
                                                        sizes="(max-width: 174px) 100vw, 174px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2014<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2013
                                                </td>
                                            </tr>
                                            <tr style="height: 146px;">
                                                <td style="width: 696.719px; text-align: center; height: 146px;">
                                                    <img
                                                        class="wp-image-738 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-300x222.jpg"
                                                        alt=""
                                                        width="176"
                                                        height="130"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-300x222.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-768x570.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-1030x764.jpg  1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-1500x1112.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-705x523.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2012-450x334.jpg    450w
                                                        "
                                                        sizes="(max-width: 176px) 100vw, 176px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2013<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2012
                                                </td>
                                            </tr>
                                            <tr style="height: 145px;">
                                                <td style="width: 696.719px; text-align: center; height: 145px;">
                                                    <img
                                                        class="wp-image-737 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/infobank-2011-300x223.jpg"
                                                        alt=""
                                                        width="175"
                                                        height="130"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/infobank-2011-300x223.jpg 300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/infobank-2011-450x335.jpg 450w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/infobank-2011.jpg         600w
                                                        "
                                                        sizes="(max-width: 175px) 100vw, 175px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2012<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2011
                                                </td>
                                            </tr>
                                            <tr style="height: 140px;">
                                                <td style="width: 696.719px; text-align: center; height: 140px;">
                                                    <img
                                                        class="wp-image-736 alignleft"
                                                        src="http://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-300x212.jpg"
                                                        alt=""
                                                        width="177"
                                                        height="125"
                                                        srcset="
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-300x212.jpg    300w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-768x543.jpg    768w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-1030x728.jpg  1030w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-1500x1061.jpg 1500w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-260x185.jpg    260w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-705x498.jpg    705w,
                                                            https://www.bank-eka.co.id/wp-content/uploads/2018/07/Infobank-2010-450x318.jpg    450w
                                                        "
                                                        sizes="(max-width: 177px) 100vw, 177px"
                                                    />
                                                    <br />
                                                    Sertifikat Penghargaan dari Infobank tahun 2011&nbsp;&nbsp;<br />
                                                    Sebagai BPR dengan Predikat “Sangat Bagus”<br />
                                                    Atas Kinerja Keuangan tahun 2012
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </article>
                    </div>
                    <footer class="entry-footer"></footer>
                </div>
            </article> 
        </main> -->

    </div>
</section>