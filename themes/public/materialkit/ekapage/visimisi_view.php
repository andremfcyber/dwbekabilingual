<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
    .red {
        color:#DE1213;
    }
</style> 
<section class="p-1">
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Visi dan Misi</li>
            </ol>
        </nav>
        <?php 
            foreach ($v_halaman as $v_half):
                echo $v_half['isi'].'<hr>';
            endforeach;
        ?> 
        <!-- <div class="row m-2 d-flex" >
            <div class="col-md-12 align-selft pb-3">
            <div class="text-center">
                <h2 class="redbold">Visi dan Misi</h2>
            </div>
            <h3> 
                <b class="redb">Visi : </b> <br>
                “Menjadi <b class="redb">bank terbaik </b> <br>
                dan <b class="redb">membanggakan</b> dalam  <br>
                pembiayaan <b class="redb">mikro</b>, <b class="redb">kecil</b>, <br> 
                dan <b class="redb">menengah</b>”
            </h3>
            <br> 
            <br> 
            <h3> 
                <b class="redb">Misi : </b> <br>
            </h3>
            <div class="list">
                <p>Dalam mewujudkan visi Bank Eka tersebut diatas, Bank Eka mengemban misi sebagai berikut :</p> 
                 <p><i class="fa fa-circle red"></i> Melayani pembiayaan mikro, kecil, dan menengah yang inklusif dan humanis untuk kemandirian masyarakat.</p>
                 <p><i class="fa fa-circle red "></i> Menyediakan produk dan jasa keuangan berdaya saing tinggi untuk kualitas hidup masyarakat yang lebih sejahtera.</p>
                 <p><i class="fa fa-circle red "></i> Memperkuat permodalan berkesinambungan untuk peningkatan kualitas dan kapasitas layanan.</p>
                 <p><i class="fa fa-circle red "></i> Membangun sumberdaya manusia yang berakhlak mulia, Loyal dan ungul melalui budaya bekerja untuk belajar dan visioner dalam berkarier.</p>
                 <p><i class="fa fa-circle red "></i>  Memperluas jaringan layanan berbasis teknologi informasi perbankan yang handal.</p>
            </div>
          </div>
            
        </div> -->
    </div>
</section>