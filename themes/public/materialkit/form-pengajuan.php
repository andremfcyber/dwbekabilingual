<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bank Nusamba BPR</title>
		<meta charset="UTF-8">
		<meta name="description" content="Logo">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->   
		<link href="<?php echo $theme_url;?>/img_frontend/favicon.ico" rel="shortcut icon"/>
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
		
		<!-- Stylesheets -->
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/animate.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/style.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
		<link href="<?php echo $theme_url;?>/css_frontend/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

		<style type="text/css">
            .green {
                 background-color: white !important; 
            }
            .grey {
                 background-color: white !important; 
            }
        </style>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144983683-1');
</script>	
	</head>
	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>
		
		<!-- Header section -->
		<header class="header-section">
			<div class="header-top">
				<div class="container">
					<marquee color="white" style="font-size: 12px;"><font color="white" >HUBUNGI LAYANAN PELANGGAN TLP:021-84931416 EMAIL:INFO@BPRNUSAMBACEPIRING.COM</font></marquee>
				</div>
			</div>
			<div class="header-bottom h-b">
				<div class="container">
					<a href="index.html"  class="site-logo">
						<img style="padding-top: 10px; padding-bottom:10px;" src="<?php echo $theme_url;?>/img_frontend/logo.png" alt="">
					</a>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
					<ul class="main-menu">
						<li class='submenu'><a href="#">Profil <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/profil') ?>"><i class="fa fa-chevron-right"></i> Tentang Kami</a></li>
								<li><a href="<?php echo base_url('public/home/visi_misi') ?>"><i class="fa fa-chevron-right"></i> Visi Misi</a></li>
								<li><a href="<?php echo base_url('public/home/struktur') ?>"><i class="fa fa-chevron-right"></i> Struktur Organisasi</a></li>
								<li><a href="<?php echo base_url('public/home/budaya_perusahaan') ?>"><i class="fa fa-chevron-right"></i> Budaya Perusahaan</a></li>
								<li><a href="<?php echo base_url('public/home/awards') ?>"><i class="fa fa-chevron-right"></i> Awards</a></li>
								<li><a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>"><i class="fa fa-chevron-right"></i> Kenapa memilih kami</a></li>
								<li><a href="<?php echo base_url('public/home/faq') ?>"><i class="fa fa-chevron-right"></i>FAQ</a></li>
								<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>"><i class="fa fa-chevron-right"></i> Hubungi Kami</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Berita <i class="fa fa-caret-down"></i> </a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/agenda') ?>"><i class="fa fa-chevron-right"></i> Agenda</a></li>
								<li><a href="<?php echo base_url('public/home/press_realese') ?>"><i class="fa fa-chevron-right"></i> Press Realese</a></li>
								<li><a href="<?php echo base_url('public/home/cerita_pelanggan') ?>"><i class="fa fa-chevron-right"></i> Cerita Pelanggan</a></li>
								<li><a href="<?php echo base_url('public/home/laporan') ?>"><i class="fa fa-chevron-right"></i> Laporan</a></li>
								<li><a href="<?php echo base_url('public/home/blog') ?>"><i class="fa fa-chevron-right"></i> Blog</a></li>
								<li><a href="<?php echo base_url('public/home/karir') ?>"><i class="fa fa-chevron-right"></i> Karir</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Media <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/galeri') ?>"><i class="fa fa-chevron-right"></i> Galeri</a></li>
								<li><a href="<?php echo base_url('public/home/download') ?>"><i class="fa fa-chevron-right"></i> Download &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
							</ul>
						</li>
						<li><a href="<?php echo base_url('public/home/kredit') ?>">Kredit</a></li>
                        <li><a href="<?php echo base_url('public/home/deposito') ?>">Deposito</a></li>
                        <li><a href="<?php echo base_url('public/home/tabungan') ?>">Tabungan</a></li>
						<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>">Kontak</a></li>
						<li><a href="<?php echo base_url('public/home/artikel') ?>">Artikel</a></li>
						
						<a class="gray" href="#">IND</a> <a class="gray" href="#"> | ENG</a>
						
					</ul>
				</div>
			</div>
		</header>
		<!-- Header section end -->
		
		<section class="add-section spad">
			<div class="container">
				<div class="add-warp">
					
					<div class="row add-text-warp">
						
						<div class="col-lg-12">
							<img src="<?php echo $theme_url;?>/img_frontend/Banner-1.png" class="rounded" alt="Responsive image">
						</div>
						<div class="col-lg-12">
							<br>
							<h2 class="green" style="text-align:left; font-size:30px">Form Pengajuan</h2>
							<p class="gray">
								Silahkan isi form berikut dengan lengkap. Staff kami akan segera menghubungi anda. pengiriman data anda diamankan dengan menggunakan teknologi TLS
							</p>
						</div>
						<div class="col-lg-6">
							<br>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">Nama Lengkap Sesuai KTP</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap Sesuai KTP">
							</div>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">No HP</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="No HP">
							</div>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">Photo Selfie</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="customFile">
									<label class="custom-file-label" for="customFile">Choose file</label>
								</div>
							</div>
							
						</div>
						<div class="col-lg-6">
							<br>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">NIK</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIK">
							</div>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">Email</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
							</div>
							<div class="form-group">
								<label class="green" for="exampleInputEmail1">Photo KTP</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="customFile">
									<label class="custom-file-label" for="customFile">Choose file</label>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="text-left">
								<button type="submit" class="btn btn-primary elips-btn">Ajukan</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- Footer section  -->
		<footer class="footer-section set-bg" >
			
			<div class="row">
				<div class="col-lg-3">
					<h3 style="color:white; font-size:18px">Kontak Kami</h3><br>
					<font style="color:white; font-size:11px">Jl. Raya Gondang No.30, Pencarikan, Gondang, Kec. Cepiring, Kabupaten Kendal, Jawa Tengah 51352</font><br>
					<font style="color:white; font-size:11px"><i class="fa fa-phone"></i>  Telepon :  (0294) 382234</font><br>
					<font style="color:white; font-size:11px"><i class="fa fa-fax"></i> Fax :  (0294) 382234</font><br>
					<font style="color:white; font-size:11px"><i class="fa fa-envelope"></i> Email :  (0294) 382234</font><br>
				</div>
				<div class="col-lg-3">
					<h3 style="color:white; font-size:18px">Tentang Kami</h3><br>
				<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Cara Pengajuan dan Pembayaran</a></font></a><br>
				<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Tanya Jawab</font></a><br>
				<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Ajukan Lagi</font></a><br>
				<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
				
			</div>
			<div class="col-lg-3">
				<h3 style="color:white; font-size:18px">Kebijakan</h3><br>
				<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Syarat dan Ketentuan</font></a><br>
				<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Privasi</font></a><br>
				<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Cookies</font></a><br>
				<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
				
			</div>
			<div class="col-lg-3">
				<div class="col-lg-12">
					<h3 style="color:white; font-size:18px">Ikuti Kami</h3><br>
					<a style="color:white; font-size:11px" href="#"><i class="fa fa-facebook"></i></a>
					<a style="color:white; font-size:11px" href="#"><i class="fa fa-twitter"></i></a>
					<a style="color:white; font-size:11px" href="#"><i class="fa fa-instagram"></i></a>
					<a style="color:white; font-size:11px" href="#"><i class="fa fa-google-plus-square"></i></a>
				</div>
				<div class="col-lg-12">
					<div class="text-left">
						<div class="form-group">
							<input type="text" class="form-control"  placeholder="">
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="text-left">
						<button type="submit" class="btn btn-primary elips-btn">Subscribe</button>
					</div>
				</div>
				
			</div>
		</div>
		
	</footer>
	<!-- Footer section end -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		} );
		</script>
		<script>
			// Open the Modal
			function openModal() {
				document.getElementById("myModal").style.display = "block";
			}
			
			// Close the Modal
			function closeModal() {
				document.getElementById("myModal").style.display = "none";
			}
			
			var slideIndex = 1;
			showSlides(slideIndex);
			
			// Next/previous controls
			function plusSlides(n) {
				showSlides(slideIndex += n);
			}
			
			// Thumbnail image controls
			function currentSlide(n) {
				showSlides(slideIndex = n);
			}
			
			function showSlides(n) {
				var i;
				var slides = document.getElementsByClassName("mySlides");
				var dots = document.getElementsByClassName("demo");
				var captionText = document.getElementById("caption");
				if (n > slides.length) {slideIndex = 1}
				if (n < 1) {slideIndex = slides.length}
				for (i = 0; i < slides.length; i++) {
					slides[i].style.display = "none";
				}
				for (i = 0; i < dots.length; i++) {
					dots[i].className = dots[i].className.replace(" active", "");
				}
				slides[slideIndex-1].style.display = "block";
				dots[slideIndex-1].className += " active";
				captionText.innerHTML = dots[slideIndex-1].alt;
			}
			
			
		</script>
		
		<!--====== Javascripts & Jquery ======-->
		
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo $theme_url;?>/js_frontend/owl.carousel.min.js"></script>
		<script src="<?php echo $theme_url;?>/js_frontend/main.js"></script>
		<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
		<script type="text/javascript">
			$('.form_datetime').datetimepicker({
				//language:  'fr',
				weekStart: 1,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
				showMeridian: 1
			});
			$('.form_date').datetimepicker({
				language:  'EN',
				weekStart: 1,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
			$('.form_time').datetimepicker({
				language:  'fr',
				weekStart: 1,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 1,
				minView: 0,
				maxView: 1,
				forceParse: 0
			});
		</script>
		
	</body>
</html>																																																																																																																																																																																																																																																																																																																																																																																																																																																					