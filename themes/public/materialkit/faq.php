
<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
</style>	
		<section class="add-section spada_">
			<div class="container">
				<div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
					<?php 
						$banner = get_baner_by_kode('faq');
					 	if($banner!=""){ 
					?>
					  <img src="<?php echo base_url('upload/photo/'.$banner['foto_baner']); ?> " style="width : 100%; height : auto;" class="rounded" alt="Responsive image"> -->
					<?php  } ?>
				</div>
				<div class="add-warp">
					<div class="row add-text-warp">
						<div class="col-lg-12">
							<ol class="breadcrumba _box">
								<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">FAQ</li>
							</ol>
							<!-- <div class=" topnav " id="myTopnav" >
								<a>
									<button type="button" class="btn btn-primary g hide_daftar">
									Daftar Menu
									</button>
								</a>
								<a href="<?php echo base_url('public/home/profil') ?>">
									<button type="button" class="btn btn-primary g">
										Tentang Kami
									</button>
								</a>
							
								<a href="<?php echo base_url('public/home/visi_misi') ?>">
									<button type="button" class="btn btn-primary g">
										Visi Misi
									</button>
								</a>
						
								<a href="<?php echo base_url('public/home/struktur') ?>">
									<button type="button" class="btn btn-primary g">
										Struktur Organisasi
									</button>
								</a>
							
								<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
									<button type="button" class="btn btn-primary g">
										Budaya Perusahaan
									</button>
								</a>
							
								<a href="<?php echo base_url('public/home/awards') ?>">
									<button type="button" class="btn btn-primary g">
										Awards
									</button>
								</a>
						
								<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
									<button type="button" class="btn btn-primary g">
										Kenapa Memilih Kami
									</button>
								</a>
						
								<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
									<button type="button" class="btn btn-primary g">
										Hubungi Kami
									</button>
								</a>
								
								<div class="text-center">
								<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
			<style>
				.wrap-faq .btn{
					font-size:21px!important;
					color:white!important;
    				text-decoration: unset!important;
				}

				.card-bodyd {
					-ms-flex: 1 1 auto;
					flex: 1 1 auto;
					padding: 19px;
				}

				.wrap-faq .card-bodyd {
					border-top:0px!important;
					border:2px dashed orange;
				}

				.bg-cepiri {
					background-color:#f78d2c!important
				}
			</style>
				<div class="row add-text-warp">
					<div class="col-lg-4">
						<div class="yt">
							FAQ
						</div>
						<div class="ytb">
						</div>
					</div>
					
					<div class="col-lg-12">
						<br>
						<?php $no =0;
						if($_faq){
							foreach($_faq as $_data){
						?>
							<div class="accordion wrap-faq" id="accordionExample">
								<div class="cards">
									<div class="card-headers" id="headingOne<?php echo $no;?>">
										<h5>
											<button class="btn btn-link btn-warning bg-cepiri btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne<?php echo $no;?>" aria-expanded="true" aria-controls="collapseOne">
												<i class="fa fa-plus" aria-hidden="true"></i>  
													<span  ><?php echo $_data['judul']; ?></span> 
											</button>
										</h5>
									</div>
									<div id="collapseOne<?php echo $no;?>" class="collapse show" aria-labelledby="headingOne<?php echo $no;?>" data-parent="#accordionExample">
										<div class="card-bodyd">
											<div class="media">
												<!-- <img class="mr-2" style="max-width: 6%;" src="<?php // if(!empty($_data['foto'])){ echo base_url('upload/photo/').$_data['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" width="20%" height="20%" alt="Generic placeholder image"> -->
												<div class="media-body">
												<?php echo $_data['isi']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php 
							$no++;
							}
						}
						?>
					</div>
					
				</div>
			</div>
		</div>
	</section>



