<style>
	/* iframe {
		width: 100%;
		border-radius: calc(.25rem - 1px);
		height: 170px;
	} */
	.Portfolio{
		min-height: 210px;
		max-height: 210px;
		width: 300px;
		border: none;
		object-fit: cover;
		border-radius: 11px;
		margin: 8px 14px;
	}
	.nav li a {
		/* margin: 5px; */
		/* padding: 15px 50px; */
		font-size: 12px;
		color: #f6861f !important;
		background: #fff;
		transition-duration: 0.4s;
		text-transform: uppercase;
	}

	.nav li a:hover {
		/* margin: 5px; */
		/* padding: 15px 50px; */
		background: #f6861f;
		color: white !important;
	}

	.nav .active {
		background-color: #fff !important;
		color: #fff;
		text-transform: uppercase;
		font-weight: bold;
		border-bottom: 3px solid #f6861f;
	}
	.nav .active:hover {
		background-color: #fff !important;
		color: #fff;
		text-transform: uppercase;
		font-weight: bold;
		border-bottom: 3px solid #f6861f;
	}
	.container-btn {
		margin-top: 31px;
		display: flex;
		flex-direction: row;
		justify-content: space-around;
	}
	a.btn-galeri {
		background: white;
		color: #f6861f;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #f6861f;
	}
	a.btn-galeri.disabled {
		background: white;
		color: #ddd;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #ddd;
	}
	a.btn-galeri.disabled:hover {
		background: white;
		color: #ddd;
		cursor: not-allowed;
	}
	a.btn-galeri:hover {
		background: #f6861f;
		color: #fff;
	}
	div#pills-tabContent {
		width: 100%;
	}
	.flex_center{
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	iframe{
		width: 100%;
		border-radius: 12px;
	    height: 100%;
	}

	.Portfolio img {
		height: auto;
		border-radius: 10px;
		max-height: 210px;
		min-height: 210px;
		object-fit: cover;
	}

</style>
		
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<br><br>
						<h2 style="text-align:center; font-size:35px; word-spacing: 1px;" class="gray">	
							Galeri Foto dan Video 
						</h2>
						<!-- <h2 style="text-align:center; font-size:35px; word-spacing: 1px;" class="gray">	
							Kegiatan Family gathering, Operasional 
						</h2> -->
						<br>
						<div class="row flex_center">
							<ul class="nav justify-content-center mb-3" id="pills-tab" role="tablist">
								<li class="nav-item">
									<a class="nav-link <?php if($status_tab == 0) echo 'active' ?>" href="<?= base_url('public/home/galeri') ?>" role="tab" aria-controls="showall" aria-selected="true">Show All</a>
								</li>
								<li class="nav-item">
									<a class="nav-link <?php if($status_tab == 1) echo 'active' ?>" href="<?= base_url('public/home/galeri?kategori=1') ?>" role="tab" aria-controls="Videos" aria-selected="false">Foto</a>
								</li>
								<li class="nav-item">
									<a class="nav-link <?php if($status_tab == 2) echo 'active' ?>" href="<?= base_url('public/home/galeri?kategori=2')?>" role="tab" aria-controls="Foto" aria-selected="false">Video</a>
								</li>
								
							</ul>	
						</div>
						<!-- <hr noshade style="margin-top:-20px;"> -->
						<div class="container">
							<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="showall" role="tabpanel" aria-labelledby="showall-tab">
									<div class="row flex_center">
										<?php if($results['results']){ ?>
											<?php foreach($results['results'] as $_data){ ?>
												<?php if($_data['id_kategori_galeri'] == 1){ ?>
													<div class="Portfolio">
														<img class="card-img" src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" alt="">
														<!-- <div class="desc"></div> -->
													</div>
												<?php }else{ ?>
													<div class="Portfolio videoYt" id="<?= $_data['id'] ?>" data-url="<?php echo $_data['link_yt'] ?>">
													</div>
												<?php } ?>
											<?php } ?> 
										
											<?php 
										
												$next = $page + 1;
												$next_status = $next > $results['pagination']['more'] ? 'disabled' : '';	
												$prev =  $page - 1;
												$prev_status = $prev < 1 ? 'disabled' : '';							
												
											?>
												
										<?php }else{ ?>
												<div class="col-sm-12">
													<div class="text-center">
														Belum Ada Foto atau Video
													</div>
												</div>
										<?php } ?>
									</div>
									<div class="row">
										<div class = "col-md-12">
											<?php if($results['results']) { ?>
											<div class="container-btn">
												<a class="btn-galeri <?php echo isset($prev_status) ? $prev_status : '' ?>" href="<?php echo base_url().'public/home/galeri?page='.$prev; ?>">
													<i class="fa fa-arrow-left"></i>
												</a>
												<a class="btn-galeri <?php echo isset($next_status) ? $next_status : '' ?>" href="<?php echo base_url().'public/home/galeri?page='.$next; ?>">
													<i class="fa fa-arrow-right"></i>
												</a>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script>
		function getId(url) {
			var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
			var match = url.match(regExp);

			if (match && match[2].length == 11) {
				return match[2];
			} else {
				return 'Link Tidak Valid';
			}
		}

		function set_iframe(el, url){
			var myId = getId(url);

			$(el).html(myId);
			
			if(myId == 'Link Tidak Valid'){
				$(el).html('<span class="img-c"><img src="'+base_url+'/assets/dist/img/404.png"></span>');
				return false;
			}
			$(el).html('<iframe width="100%" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe>');
		}

		$(() => {
			setTimeout(() => {
				$('.videoYt').each(function(e) {
					let el = $(this);
					let url = el.data('url');
					console.log('haha', el, url)
					set_iframe(el, url)
				})
			}, 1000);
		})
	</script>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																