<style>
.form-nominal{
   width: 100%;
   border: 1px solid #ccc;
   border-radius: 5px;
   padding: 10px 10px 10px 35px;
}
#span-rp{
   position: absolute;
   top: 43px;
   left: 14px;
}
.sp-blog-item {
   box-shadow: none !important;
   border: 1px #ddd solid;
}
.btn-calc{
   background: #366a34;
   color: white;
   font-weight: bolder;
   border: none;
}
._label_pilih{
   background: #ee832e;
   color: white !important;
   padding: 16px;
   text-align: center;
   margin: 52px 0px !important;
   font-size: 21px !important;
}
.bg-foot{
   background: #e6e6e6;
}
.bg-cepiring{
   background: #366a34;
}

.simulasi_kredit{
   font-size: 25px;
   padding: 30px 0px;
   color: #366a34;
}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
  
}
.box-grid{
   border: 1px #ddd solid;
   display: flex;
   border-radius: 10px;
   margin-bottom: 20px;
}
.box-img{
   padding: 23px;
}
.box-img img{
   width: 100px;
}
.box-content{
   padding: 12px 5px 20px 5px;
}
.box-content .title{
   color: #366a34;
   font-weight: 500;
   font-size: 19px;
}
.box-content .content{
   font-size: 13px;
}
.box-content .action{

}
.box-content .action a{
   font-size: 13px;
   background: #ee832e;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.box-content .action a:hover{
   cursor: pointer;
   color: #fff;
}
.spad {
    padding-top: 30px;
    padding-bottom: 40px !important;
}
.btn-orange{
   font-size: 13px;
   background: #ee832e;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
</style>
<section class="add-section spad">
   <div class="container">
      <div class="">
         <div class="row">
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
               <span class="simulasi_kredit" class="green"> Simulasi Produk Kredit <?php if($_profil){ echo $_profil['nama'];} ?></span>
            </div>
         </div>
         <div class="row add-text-warp">
            <div class="col-md-4">
               <div class="row" id="calc_scope">
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <label>Silahkan Pilih Produk</label>
                        <select class="form-control select2" onchange="change_product(this)" name="id_product">
                           <option value="" disabled selected>Pilih Disini</option>
                           <?php 
                              foreach($option_jenis_kredit as $data){
                                 echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
                              }
                           ?>
                        </select>
                        <input type="hidden" value="" name="jenis_bunga">
                        <input type="hidden" value="" name="bunga">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;"> 
                        <label>Jangka Waktu (Dalam Bulan)</label>
                        <select class="form-control select2" name="jangka_waktu">
                           <option value="" disabled selected>Pilih Disini</option>
                           <?php 
                              foreach($option_bulan as $data){
                                 echo '<option value="'.$data['bulan'].'">'.$data['bulan'].' bulan</option>';
                              }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;position: relative">
                        <label>Nominal</label>
                        <span id="span-rp">Rp.</span>
                        <input class="form-nominal rupiah" name="nominal">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <button class="btn btn-calc" onclick="kalkulasi()">Kalkulasi</button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="sp-blog-item">
                  <div class="blog-text">
                     <div class="_detail_produk" style="display: none;">
                        <h5 id="nama_produk"></h5>
                        <span style="font-size: 11px; color: #ee832e;" id="detail_bunga"></span>
                        <p id="detail_produk"><?php spoiler(0,99) ?></p>
                        <span style="display: inline;" id="detail_selengkapnya"></span>
                        <span style="display: inline;" id="detail_ajukan"></span>
                     </div>
                     <span class="_label_pilih">PILIH PRODUK</span>
                  </div>
               </div>
            </div>
            <div class="col-md-4" style="text-align: center;">
               <img src="<?= base_url('/assets/dist/img/bunga.png') ?>" alt="" style="height: 280px;">
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Add section end -->
<!-- Add section end -->
<section class="add-section spad pt-0">
   <div class="container">
   <div class="">
   <div class="row">
      <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
         <span class="simulasi_kredit" class="green"> Hasil Simulasi </span><span class="simulasi_kredit" id="hasil_nama_produk"></span>
      </div>
   </div>
   <div class="row add-text-warp">
      <br><br><br>
      <div class="col-lg-12">
         <div class="table-responsive">
            <table class="table table-bordered" style="border-radius: 10px;">
               <thead class="bg-cepiring text-light">
                  <tr>
                     <th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Angsuran</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Angsuran Bunga</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Angsuran Pokok</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Total Angsuran</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Sisa Angsuran</span></th>
                  </tr>
               </thead>
               <tbody id="show_data">
                  <tr>
                     <td colspan="5" class="text-center">Belum Melakukan Simulasi</td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</section>
<section class="add-section spad pt-0">
   <div class="container">
      <div class="row">
         <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
            <span class="simulasi_kredit" class="green"> Produk Kredit yang Kami Miliki </span>
         </div>
      </div>
      <div class="row add-text-warp">
         <?php if($show) { ?>
            <?php foreach($show as $re) { ?>
               <div class="col-lg-6 col-sm-12">
                  <div class="box-grid">
                     <div class="box-img">
                        <img src="<?php echo $theme_url;?>/img_frontend/tangan.png" alt="Kredit Gratis">
                     </div>
                     <div class="box-content">
                        <span class="title"><?= $re['name']; ?></span>
                        <p class="content">
                           <?= spoiler($re['deskripsi'],96).'...' ?>
                        </p>
                        <div class="action">
                           <a href="<?= base_url().'public/home/show/'.$re['id'].'/'.$re['product_type_id']; ?>">Ajukan</a>
                           <a href="<?php echo base_url().'public/home/detail_/'.$re['id'].'/'.$re['product_type_id'].'/kredit'; ?>">Detail</a>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
         <?php } ?>
      </div>
   </div>
</section>
<!-- <section class="add-section spad pt-0">
   <div class="container">
      <div class="">
         <div class="row add-text-warp">
            <div class="col-lg-12">
               <h2  class="green" style="text-align:left; font-size:20px;">Mengenal Lebih Dekat Kredit Multi Guna</h2>
            </div>
            <br>
            <div class="col-lg-12">
               <p style="font-size:12px" class="gray">
                  Masyarakat diberikan banyak pilihan dengan beragam produk Kredit Pemilikan Rumah (KPR). Suku bungan ringan, hingga tenor panjang menjadi pilihan yang tersedia. sebelum proses pengajuan, ketahui beragam informasi KPR berikut ini. 
               </p>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!-- <section class="add-section spad pt-0">
   <div class="container">
      <div class="">
         <div class="row add-text-warp">
            <div class="col-lg-12">
               <h2  class="green" style="text-align:left; font-size:20px;">Keuntungan Kredit Multi Guna</h2>
            </div>
            <br>
            <div class="col-lg-12">
               <p style="font-size:12px" class="gray">
                  Kredit Pemilikan Rumah (KPR) merupakan pembiaayaan yang digunakan untuk membeli rumah atau kebutuhan konsumtif lainnya dengan jaminan/angsuran berupa rumah dengan skema pembiayaan 80% dari harga rumah. Tahun 2015 kemarin, pemerintah melalui Kementrian Pekerjaan Umum dan Perumahan Rakyat(PUPR) mencanangkan ide untuk memperpanjang tenor Kredit Pemilikan Rumah(KPR) dari 20 tahun menjadi 30 tahun. Namun jika anda berniat mengajukan KPR, jangan hanya tergiur dengan tenor panjang ataupun dp murah. anda harus lebih banyak menggali informasi produk KPR, agar proses angsuran anda berjalan dengan lancar.
               </p>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Start of Tawk.to Script-->

<!--End of Tawk.to Script-->
<script type="text/javascript">
   $(document).ready(function() {
      // $('#example').DataTable();
   });
   
   function change_product(e){
      // console.log(e.value)
      let id = e.value;
      blockUI('Memuat ...');

      $.get(base_url+'public_service/get_product_detail_kredit/'+id).done((res) => {
         console.log(res)
         let html = '<button class="btn btn-orange" onclick="detail_kredit('+res.id+','+res.product_type_id+')">Selengkapnya</button>';
         let html_ajukan = '<button class="btn btn-orange" onclick="detail_ajukan('+res.id+','+res.product_type_id+')">Ajukan</button>';
         $('#nama_produk').html(res.name)
         $('#detail_bunga').html('Jenis Bunga : '+res.jenis_bunga+', Bunga : '+res.bunga+' %')
         $('#detail_produk').html(res.deskripsi)
         $('#detail_selengkapnya').html(html)
         $('#detail_ajukan').html(html_ajukan)
         $('#hasil_nama_produk').html(res.name)
         $('._detail_produk').show();
         $('._label_pilih').hide();

         $('[name="jenis_bunga"]').val(res.jenis_bunga)
         $('[name="bunga"]').val(res.bunga)
         $.unblockUI();
      }).fail((xhr) => {
         console.log(xhr)
      })
   }

   function detail_kredit(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/detail_/'+id+'/'+type_produk+'/kredit',
         '_blank' 
      );
   }
   function detail_ajukan(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/show/'+id+'/'+type_produk,
         '_blank' 
      );
   }

   
   function kalkulasi(){
      let nominal = $('[name="nominal"]').val();
      let jangka_waktu = $('[name="jangka_waktu"]').val();
      let jenis_bunga = $('[name="jenis_bunga"]').val();
      let bunga = $('[name="bunga"]').val();
      
      if(!nominal || !jangka_waktu || !jenis_bunga || !bunga){
         alert('Silahkan Lengkapi Form');
         return;
      }

      blockUI('Mohon Bersabar .. ');

      nominal = parseFloat(nominal);
      jangka_waktu = parseFloat(jangka_waktu);
      bunga = parseFloat(bunga);

      if (jenis_bunga == 'flat'){  
         flat(nominal,jangka_waktu, bunga);
      }else if(jenis_bunga == 'efektif'){
         efektif(nominal,jangka_waktu, bunga);
      }else if(jenis_bunga == 'anuitas'){
         anuitas(nominal,jangka_waktu, bunga);
      }
   }


   function flat(nominal, jangkawaktu, conf_bunga){
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = conf_bunga/12;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      
      bungaBulan = conf_bunga/12;
      bunga = parseFloat(nominal * parseFloat(bungaBulan));
      bungaFlat = parseFloat(bunga/100);
      jumlah = parseFloat(nominal);
      pokok = parseFloat(nominal/jangkawaktu);
      tobu = parseFloat(pokok + bungaFlat);
      total = parseFloat(nominal);

      bunga_dlm_bulan = bungaFlat * jangkawaktu;
      pinjaman_plus_bunga = parseFloat(total + bunga_dlm_bulan);

      var html = '<tr>'+
         '<td style="text-align:left"> Bulan 0</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
      '</tr>';
     
      var jumlah_angsuran_bunga = 0;
      var jumlah_angsuran_pokok = 0;
      var jumlah_total_angsuran = 0;
      var sisa_angsuran = total;
      
      for(i=1;i<= parseInt(jangkawaktu);i++){

         sisa_angsuran -= pokok;
         sisa_angsuran = parseFloat(sisa_angsuran);
         jumlah_angsuran_bunga += bungaFlat;
         jumlah_angsuran_pokok += pokok;
         jumlah_total_angsuran += tobu;

         if(sisa_angsuran <= 0){
            sisa_angsuran = 0;
         }
         html += '<tr>'+
         '<td style="text-align:left"> Bulan '+ i +'</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(bungaFlat.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(pokok.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(tobu.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
         '</tr>';
      }
      html += '<tr class="bg-cepiring text-light">'+
      '<td style="text-align:left">TOTAL</td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
      '<td></td>'+
      '</tr>';

      $('#show_data').html(html);
      $.unblockUI();
   }
   function anuitas(nominal,jangkawaktu, conf_bunga){
      // console.log('ini ',nominal,jangkawaktu);
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = 0;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      var bunga_bul = 0;
      var pembagi = 0;
      
      bungaBulan = conf_bunga/12;
      
      jumlah = parseFloat(nominal);
      
      bunga_bul = parseFloat(bungaBulan/100);
      // alert(bunga_bul);
      pembagi = 1-(1/(Math.pow(1+bunga_bul,jangkawaktu)));
      
      tobu = parseFloat(nominal/(pembagi/bunga_bul));
      
      total = parseFloat(nominal);

       var html = '<tr>'+
         '<td style="text-align:left"> Bulan 0</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
      '</tr>';
      // var total = (nominal -  (( - 1) * pokok )) * bungaBulan);
      //  alert(total);
      var jumlah_angsuran_bunga = 0;
      var jumlah_angsuran_pokok = 0;
      var jumlah_total_angsuran = 0;
      var sisa_angsuran = total;
      // alert(sisa_angsuran);
      for(i=1;i<= parseInt(jangkawaktu);i++){
         bunga = parseFloat(sisa_angsuran * parseFloat(bungaBulan));
         bungaFlat = parseFloat(bunga/100);
         // alert(bungaFlat);
         pokok = tobu - bungaFlat;
         sisa_angsuran -= pokok;
         sisa_angsuran = parseFloat(sisa_angsuran);
         jumlah_angsuran_bunga += bungaFlat;
         jumlah_angsuran_pokok += pokok;
         jumlah_total_angsuran += tobu;
         pokok = tobu - bungaFlat;
         if(sisa_angsuran <= 0){
            sisa_angsuran = 0;
         }
         html += '<tr>'+
         '<td style="text-align:left"> Bulan '+ i +'</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(bungaFlat.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(pokok.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(tobu.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
         '</tr>';
      }
       html += '<tr class="bg-cepiring text-light">'+
      '<td style="text-align:left">TOTAL</td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
      '<td></td>'+
      '</tr>';

      $('#show_data').html(html);
      $.unblockUI();
   }
   function efektif(nominal, jangkawaktu, conf_bunga){
      // console.log('ini ',nominal,jangkawaktu);
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = 0;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      var bunga_bul = 0;
      var pembagi = 0;
      
      bungaBulan = conf_bunga/12;
      
      jumlah = parseFloat(nominal);
      
      bungaFlat = parseFloat(bungaBulan/100);
      // alert(bunga_bul);
      pokok = parseFloat(nominal/jangkawaktu);
      
      
      
      // tobu = parseFloat(nominal/(pembagi/bunga_bul));
      
      total = parseFloat(nominal);

       var html = '<tr>'+
         '<td style="text-align:left"> Bulan 0</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
      '</tr>';
      // var total = (nominal -  (( - 1) * pokok )) * bungaBulan);
      //  alert(total);
      var jumlah_angsuran_bunga = 0;
      var jumlah_angsuran_pokok = 0;
      var jumlah_total_angsuran = 0;
      var sisa_angsuran = total;
      // alert(sisa_angsuran);
      for(i=1;i<= parseInt(jangkawaktu);i++){

         bunga = parseFloat((nominal -  ((i-1) * pokok.toFixed(2) )) * bungaFlat);
         // alert();
         
         tobu = pokok + bunga;
         // alert(bungaFlat);
         // pokok = tobu-bungaFlat;
         sisa_angsuran -= pokok;
         sisa_angsuran = parseFloat(sisa_angsuran);
         jumlah_angsuran_bunga += bunga;
         jumlah_angsuran_pokok += pokok;
         jumlah_total_angsuran += tobu;
         // pokok = tobu - bungaFlat;
         if(sisa_angsuran <= 0){
            sisa_angsuran = 0;
         }
         html += '<tr>'+
         '<td style="text-align:left"> Bulan '+ i +'</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(bunga.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(pokok.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(tobu.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
         '</tr>';
      }
       html += '<tr class="bg-cepiring text-light">'+
      '<td style="text-align:left">TOTAL</td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
      '<td></td>'+
      '</tr>';

      $('#show_data').html(html);
      $.unblockUI();
   }

   function formatRupiah(num) {
      return num.toString().replace(/\./g, ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
   }
</script>
