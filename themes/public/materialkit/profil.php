<style>
	.alert-cs{
		margin-top: 10px;
        color: #7db289;
        background-color: #fbfffc;
        border-color: #c3e6cb;
        border: 2px solid;
    }
	.spada_ {
		padding-top: 30px;
		padding-bottom: 30px;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
</style>

	<section class="add-section spada_">
		<div class="container">
			<!-- <div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_profil')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div> -->
			<div class="add-warp">
				<div class="row add-text-warp">
				<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Profil</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/visi_misi') ?>">
								<button type="button" class="btn btn-primary g">
									Visi Misi
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
								<button type="button" class="btn btn-primary g">
									Budaya Perusahaan
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/awards') ?>">
								<button type="button" class="btn btn-primary g">
									Awards
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Kenapa Memilih Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Hubungi Kami
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<!-- <div class="col-md-12">
						<h2 class="green" style="text-align:left; font-size:30px">Profile</h2>
					</div>
					<br><br> -->
					<div class="col-md-12">
						<div class="hero-slider owl-carousel" style="margin-bottom: 40px;" >
							<?php if($_foto){ foreach($_foto as $_data) { ?>
								<div class="hero-slide-item set-bg img-fluid"  data-setbg="<?php echo base_url().$_data['path'].$_data['file']  ?>">
									<div class="hs-text">
									</div>
								</div>
							<?php } } ?>
						</div>
					</div>

					<div class="col-md-12">
						<h2 class="green" style="text-align:left; font-size:22px">SEJARAH SINGKAT <?php if($_profil){ echo $_profil['nama']; }?></h2>
						
						<!-- <div class="alert alert-cs" role="alert">
							<div style="display: flex;"> -->
								
							<p style="text-align:left; font-size:15px" class="gray">
								<?php if($_profil){ echo $_profil['sejarah']; }?>
							</p>
							<!-- </div>
						</div> -->
					</div>
					<br>
					<div class="col-md-12">
						<h2 class="green" style="text-align:left; font-size:22px">Tujuan Utama Pendirian</h2>
						
						<p style="text-align:left; font-size:20px" class="gray">
							<?php if($_profil){ echo $_profil['tujuan']; }?>
							
						</p>
					</div>
					<br>
					<?php if($_profil['corevalue']){  ?>
						<div class="col-md-12">
							<h2 class="green" style="text-align:left; font-size:22px">PERKEMBANGAN <?php if($_profil){ echo $_profil['nama']; }?></h2>
							
							<p style="text-align:left; font-size:15px" class="gray">
								<?php echo $_profil['perkembangan']; ?>
							</p>
						</div>
						<div class="col-md-12">
							<h2 class="green" style="text-align:left; font-size:22px">CORE VALUE <?php if($_profil){ echo $_profil['nama']; }?></h2>
							
							<p style="text-align:left; font-size:15px" class="gray">
								<?php if($_profil){ echo $_profil['corevalue']; }?>
							</p>
						</div>
					<?php } ?>
					<!-- <div class="col-md-12">
						<h2 class="green" style="text-align:left; font-size:22px">Our Workplace</h2>
						<br>
						<div class="row">
							<div class="col-lg-3 col-md-4 col-xs-6 thumb">
								<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
								data-image="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
								data-target="#image-gallery">
									<img class="img-thumbnail"
									src="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
									alt="Another alt text">
								</a>
							</div>
							<div class="col-lg-3 col-md-4 col-xs-6 thumb">
								<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
								data-image="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
								data-target="#image-gallery">
									<img class="img-thumbnail"
									src="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
									alt="Another alt text">
								</a>
							</div>
							<div class="col-lg-3 col-md-4 col-xs-6 thumb">
								<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
								data-image="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
								data-target="#image-gallery">
									<img class="img-thumbnail"
									src="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
									alt="Another alt text">
								</a>
							</div>
							<div class="col-lg-3 col-md-4 col-xs-6 thumb">
								<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
								data-image="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
								data-target="#image-gallery">
									<img class="img-thumbnail"
									src="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG"
									alt="Another alt text">
								</a>
							</div>
							
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</section>																																																																																																																																																																																																																																																																																																																																																																																																																											