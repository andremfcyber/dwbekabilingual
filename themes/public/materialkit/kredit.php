<style>
.form-nominal{
   width: 100%;
   border: 1px solid #ccc;
   border-radius: 5px;
   padding: 10px 10px 10px 35px;
}
#span-rp{
   position: absolute;
   top: 43px;
   left: 14px;
}
.sp-blog-item {
   box-shadow: none !important;
   border: 1px #ddd solid;
}
.btn-calc{
   background: #21438b;
   color: white;
   font-weight: bolder;
   border: none;
}
._label_pilih{
   background: #f6861f;
   color: white !important;
   padding: 16px;
   text-align: center;
   margin: 52px 0px !important;
   font-size: 21px !important;
}
.bg-foot{
   background: #e6e6e6;
}
.bg-cepiring{
   background: #21438b;
}

.simulasi_kredit{
   font-size: 25px;
   padding: 30px 0px;
   color: #21438b;
}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
   .box-grid-n{
      flex-direction: column;
      text-align: center;
   }

   .box-grid-n .box-content {
      padding: 12px 5px 44px 5px !important;
      width: 100%;
   }
  
}

.box-grid-n .box-content {
   padding: 12px 5px 44px 5px !important;
   width: 100%;
}
.box-grid{
   border: 1px #ddd solid;
   display: flex;
   border-radius: 10px;
   margin-bottom: 20px;
}
.box-grid-n{
   /* border: 2px #ddd solid; */
   display: flex;
   border-radius: 10px;
   position: relative;
   min-height: 187px;
}
.act{
   position: absolute;
   right: 0px;
   bottom: 7px;
}
.act a{
   color: #fff !important;
   background: #a0a0a0;
   padding: 10px;
}
.act a:hover{
   color: #fff;
   background: #f6861f;
   padding: 10px;
   cursor: pointer;
}
.act  a:nth-child(1){
   border-top-left-radius: 10px;
}
.act  a:nth-child(2){
   border-bottom-right-radius: 10px;
}

.box-img{
   padding: 23px;
}
.box-img img{
   width: 100px;
}
.box-content{
   padding: 12px 5px 20px 5px;
}
.box-content .title{
   color: #21438b;
   font-weight: 500;
   font-size: 19px;
}
.box-content .content{
   font-size: 15px;
}
.box-grid-n .box-content .content{
   font-size: 15px;
   color: #838383;
}
.box-content .action{

}
.box-content .action a{
   font-size: 13px;
   background: #f6861f;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.box-content .action a:hover{
   cursor: pointer;
   color: #fff;
}
.spad {
    padding-top: 30px;
    padding-bottom: 40px !important;
}
.btn-orange{
   font-size: 13px;
   background: #f6861f;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.wrapper{
   border-radius: 10px;
   border: #ddd dashed 2px;
   min-height: 234px;
}
.wrap-title{
   color: #838383;
   padding: 10px 15px;
}
.box-content .bunga{
   color: #d65c29;
   font-size: 14px;
   padding: 6px 0px;
}
.btn-calc {
    background: #e91e63;
    color: white;
    font-weight: bolder;
    border: none;
}
.bg-cepiring {
    background: #e91e63;
}
.simulasi_kredit {
   margin: 0px!important;
    font-size: 25px;
    padding: 15px;
    color: #e91e63;
}
.redbl {
   color:red;
   font-weight:bold;
}
.redcard {
   background-image:url('<?php echo base_url(); ?>assets/images/redcard.webp');
   background-size: cover;
    background-repeat: round;
}
.box-content .title {
    color: #ffad00;
    font-weight: bold;
    font-size: 20px;
}
.box-content .content {
    font-size: 15px;
    color: #FFF;
    line-height: 20px;
    padding-right: 42px;
}
</style>
<?php 
   $banner = get_baner_by_kode('kredit');
   if($banner!=""){ 
?>
   <img src="<?php echo base_url('upload/photo/'.$banner['foto_baner']); ?> " style="width : 100%; height : auto;" class="rounded" alt="Responsive image"> -->
<?php  } ?>
<section class="add-section spad"  style="display:none;">
   <div class="container">
      <div class="">
         <div class="row">
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
               <span class="simulasi_kredit" class="green"> Simulasi Produk Kredit <?php if($_profil){ echo $_profil['nama'];} ?></span>
            </div>
         </div>
         <div class="row add-text-warp">
            <div class="col-md-4">
               <div class="row" id="calc_scope">
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <label>Silahkan Pilih Produk</label>
                        <input type="hidden" name ="musiman" id="st_musiman">
                        <select class="form-control select2" onchange="change_product(this)" name="id_product">
                           <option value="" disabled selected>Pilih Disini</option>
                           <?php 
                              foreach($option_jenis_kredit as $data){
                                 echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
                              }
                           ?>
                        </select>
                        <input type="hidden" value="" name="jenis_bunga">
                        <input type="hidden" value="" name="bunga">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;"> 
                        <label>Jangka Waktu (Dalam Bulan)</label>
                        <select class="form-control select2" name="jangka_waktu" id="show_bulan_kredit">
                         
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;position: relative">
                        <label>Nominal</label>
                        <input class="form-control aRupiah" name="nominal">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <button class="btn btn-calc" onclick="kalkulasi()">Kalkulasi</button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8" id="this-detail">
               <div class="wrapper">
                  <div class="wrap-title">
                     Detail Produk : 
                  </div>
                  <div class="wrap-body">
                     <input name="jenis_bunga" type="hidden">
                     <input name="bunga" type="hidden">
                     <div class="box-grid-n" style="display: none;">
                        <div class="box-img" >
                           <img id="detail_gambar" alt="Kredit Gratis" style="height: 90px; width: 90px; object-fit: cover;">
                        </div>
                        <div class="box-content">
                           <span class="title" id="nama_produk">-</span>
                           <div class="bunga" id="detail_bunga">-</div>
                           <div class="content" id="detail_produk">
                              -
                           </div>
                           <div class="act" id="btn-produk">
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section> 
<section class="add-section spad pt-0" style="display:none;">
   <div class="container">
   <div class="">
   <div class="row">
      <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
         <span class="simulasi_kredit" class="green"> Hasil Simulasi </span><span class="simulasi_kredit" id="hasil_nama_produk"></span>
      </div>
   </div>
   <div class="row add-text-warp">
      <br><br><br>
      <div class="col-lg-12">
         <div class="table-responsive">
            <table class="table table-bordered" style="font-size: 13px !important;">
               <thead class="bg-cepiring text-light">
                  <tr>
                     <th style="text-align:center; vertical-align: middle;"><span style="">Angsuran</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="">Angsuran Bunga</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="">Angsuran Pokok</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="">Total Angsuran</span></th>
                     <th style="text-align:center; vertical-align: middle;"><span style="">Sisa Angsuran</span></th>
                  </tr>
               </thead>
               <tbody id="show_data">
                  <tr>
                     <td colspan="5" class="text-center">Belum Melakukan Simulasi</td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</section>
<section class="add-section spad pt-3">
   <div class="container">
      <div class="row justify-content-center mt-3">
         <div class="col-md-9" style="text-align: center; margin-bottom: 20px;">
            <h3 class="simulasi_kredit" class="redbl">Produk Kredit yang kami miliki </h3>
            <p>Kami mempunyai beberapa layanan kredit yang dapat memudahkan anda untuk mengangsur <br>
               pinjaman sesuai layanan kami. Kredit #BersamaBankEka dapat digunakan untuk keperluan usaha dan sebagainya</p>
         </div>
      </div>
      <div class="row add-text-warp">
         <?php if($show) { ?>
            <?php foreach($show as $re) { ?>
               <div class="col-lg-6 col-sm-12">
                  <div class="box-grid redcard">
                     <div class="box-img">
                        <?php
                           if(!isset($re['thumbnail']) || $re['thumbnail'] == ''){
                              $src = $theme_url.'/img_frontend/tangan.png';
                           }else{
                              $src = base_url('upload/thumbnail/'.$re['thumbnail']);
                           }
                        ?>
                        <img class="lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?= $src ?>" alt="Kredit Gratis" style="height: 90px; width: 90px; object-fit: cover;">
                     </div>
                     <div class="box-content">
                        <span class="title"><?= $re['name']; ?></span>
                        <p class="content">
                           <?= spoiler($re['deskripsi'],96).'...' ?>
                        </p>
                        <div class="action">
                           <a href="<?= base_url().'public/home/show/'.$re['id'].'/'.$re['product_type_id']; ?>">Ajukan</a>
                           <a href="<?php echo base_url().'public/home/detail_/'.$re['id'].'/'.$re['product_type_id'].'/kredit'; ?>">Detail</a>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
         <?php } ?>
      </div>
   </div>
</section>
<!--Start of Tawk.to Script-->

<!--End of Tawk.to Script-->
<script src="<?= base_url('assets/dist/js/autoNumeric.min.js') ?>"></script>
<script type="text/javascript">
  
   $(document).ready(function() {
      // $('#example').DataTable();
   });
   
   function change_product(e){
      // console.log(e.value)
      let id = e.value;
      blockUI('Memuat ...');
      $.get(base_url + 'public_service/get_bulan_kredit/'+id).done(function(res){
         $('#show_bulan_kredit').html(res);
          
      }).fail(function(xhr){
            
         $('#show_bulan_kredit').html('Tidak ada Bulan');
      })

      $.get(base_url + 'public_service/get_musiman_kredit/'+id).done(function(res){
         console.log(res[0])
       
         $('[name="musiman"]').val(res[0].musiman)
          
      }).fail(function(xhr){
         console.log(xhr)
      })

      $.get(base_url+'public_service/get_product_detail_kredit/'+id).done((res) => {
         console.log(res)
         let action = `<a href="${base_url}/public/home/show/${res.id}/${res.product_type_id}">Ajukan</a>
         <a href="${base_url}/public/home/detail_/${res.id}/${res.product_type_id}">Detail</a>`;

         let src_gambar = '';
         $('#nama_produk').html(res.name)
         $('#detail_bunga').html('Jenis Bunga : '+res.jenis_bunga+', Bunga : '+res.bunga+' %')
         $('#detail_produk').html(res.deskripsi)
         $('#detail_gambar').attr('src', res.gambar)
         $('#hasil_nama_produk').html(res.name)
         $('#btn-produk').html(action)

         $('.box-grid-n').show();

         $('[name="jenis_bunga"]').val(res.jenis_bunga)
         $('[name="bunga"]').val(res.bunga)
         $.unblockUI();
      }).fail((xhr) => {
         console.log(xhr)
         $.unblockUI();
      })

   }

   function detail_kredit(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/detail_/'+id+'/'+type_produk+'/kredit',
         '_blank' 
      );
   }
   function detail_ajukan(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/show/'+id+'/'+type_produk,
         '_blank' 
      );
   }

   
   function kalkulasi(){

      let nominal = $('[name="nominal"]').autoNumeric('get');
      // alert(nominal);
      // return false;
      let jangka_waktu = $('[name="jangka_waktu"]').val();
      let jenis_bunga = $('[name="jenis_bunga"]').val();
      let bunga = $('[name="bunga"]').val();
      
      if(!nominal || !jangka_waktu || !jenis_bunga || !bunga){
         alert('Silahkan Lengkapi Form');
         return;
      }

      blockUI('Mohon Bersabar .. ');

      nominal = parseFloat(nominal);
      jangka_waktu = parseFloat(jangka_waktu);
      bunga = parseFloat(bunga);

      if (jenis_bunga == 'flat'){  
         flat(nominal,jangka_waktu, bunga);
      }else if(jenis_bunga == 'efektif'){
         efektif(nominal,jangka_waktu, bunga);
      }else if(jenis_bunga == 'anuitas'){
         anuitas(nominal,jangka_waktu, bunga);
      }
   }


   function flat(nominal, jangkawaktu, conf_bunga){
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = conf_bunga/12;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      var  musiman =0;
      
      musiman = $('[name="musiman"]').val();
      
      bungaBulan = conf_bunga/12;
      bunga = parseFloat(nominal * parseFloat(bungaBulan));
      bungaFlat = parseFloat(bunga/100);
      jumlah = parseFloat(nominal);
      pokok = parseFloat(nominal/jangkawaktu);
      
      total = parseFloat(nominal);

      bunga_dlm_bulan = bungaFlat * jangkawaktu;
      pinjaman_plus_bunga = parseFloat(total + bunga_dlm_bulan);

      var html = '<tr>'+
         '<td style="text-align:left"> Bulan 0</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
      '</tr>';
     
      var jumlah_angsuran_bunga = 0;
      var jumlah_angsuran_pokok = 0;
      var jumlah_total_angsuran = 0;
      var sisa_angsuran = total;
      
      for(i=1;i<= parseInt(jangkawaktu);i++){

         sisa_angsuran = parseFloat(sisa_angsuran);
         jumlah_angsuran_bunga += bungaFlat;
        
         //musiman         
         if(musiman == 1){
            if (i >= jangkawaktu){
               pokok = nominal;
            }else{
               pokok = 0;
            }
         }
         
         tobu = parseFloat(pokok + bungaFlat);
         jumlah_total_angsuran += tobu;
         jumlah_angsuran_pokok += pokok;
         sisa_angsuran -= pokok;

         if(sisa_angsuran <= 0){
            sisa_angsuran = 0;
         }
         html += '<tr>'+
         '<td style="text-align:left"> Bulan '+ i +'</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(bungaFlat.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(pokok.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(tobu.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
         '</tr>';
      }
      html += '<tr class="bg-cepiring text-light">'+
      '<td style="text-align:left">TOTAL</td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
      '<td></td>'+
      '</tr>';

      $('#show_data').html(html);
      $.unblockUI();
   }
   function anuitas(nominal,jangkawaktu, conf_bunga){
      // console.log('ini ',nominal,jangkawaktu);
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = 0;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      var bunga_bul = 0;
      var pembagi = 0;
      var  musiman =0;
      
      musiman = $('[name="musiman"]').val();

      bungaBulan = conf_bunga/12;

      let angsuran_perbulan = nominal * (conf_bunga/100/12) * (1/(1-(1/(1+(conf_bunga/100/12)*jangkawaktu))));
      angsuran_perbulan = angsuran_perbulan.toFixed(2);
      // alert(angsuran_perbulan)

      let bunga_efektif = 0;

      $.get(base_url+'public_service/get_bunga_efektif', {
         'nper' : jangkawaktu,
         'pmt' : angsuran_perbulan,
         'pv' : nominal,
      }).done((bunga) => {
         console.log(bunga)
         if(bunga) {
            bunga_efektif = bunga
            // alert(bunga_efektif);
            // $.unblockUI();
            // return false;

            jumlah = parseFloat(nominal);
            
            bunga_bul = parseFloat(bungaBulan/100);
            // alert(bunga_bul);
            pembagi = 1-(1/(Math.pow(1+bunga_bul,jangkawaktu)));
            
            tobu = parseFloat(nominal/(pembagi/bunga_bul));
            
            total = parseFloat(nominal);

            var html = '<tr>'+
               '<td style="text-align:left"> Bulan 0</td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
            '</tr>';
            // var total = (nominal -  (( - 1) * pokok )) * bungaBulan);
            //  alert(total);
            var jumlah_angsuran_bunga = 0;
            var jumlah_angsuran_pokok = 0;
            var jumlah_total_angsuran = 0;
            var sisa_angsuran = total;
            // alert(sisa_angsuran);
            for(i=1;i<= parseInt(jangkawaktu);i++){
               bunga = parseFloat(sisa_angsuran * parseFloat(bungaBulan));
               // alert(bungaFlat);
               pokok = tobu - bungaFlat;
               
               angsuran_bunga = sisa_angsuran * bunga_efektif/100/12;
                  
               console.log('Hai Tayo', sisa_angsuran, bunga_efektif, angsuran_bunga)

               if(musiman == 1){
                  if (i >= jangkawaktu){
                     angsuran_pokok = nominal;
                  }else{
                     angsuran_pokok = 0;
                  }
                  // angsuran_pokok = angsuran_perbulan - angsuran_bunga;
                  total_bunga = angsuran_bunga + angsuran_pokok;
                  sisa_angsuran -= angsuran_pokok;
               }else{
                  
                  angsuran_pokok = angsuran_perbulan - angsuran_bunga;
                  total_bunga = angsuran_bunga + angsuran_pokok;
                  sisa_angsuran -= angsuran_pokok;
               }
         
              
               sisa_angsuran = parseFloat(sisa_angsuran);
               jumlah_angsuran_bunga += angsuran_bunga;
               jumlah_angsuran_pokok += angsuran_pokok;
               jumlah_total_angsuran += total_bunga;
               pokok = tobu - bungaFlat;
               if(sisa_angsuran <= 0){
                  sisa_angsuran = 0;
               }
               html += '<tr>'+
               '<td style="text-align:left"> Bulan '+ i +'</td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(angsuran_bunga.toFixed(2), '')+'</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(angsuran_pokok.toFixed(2), '')+'</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(total_bunga.toFixed(2), '')+'</span></td>'+
               '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
               '</tr>';
            }
            html += '<tr class="bg-cepiring text-light">'+
            '<td style="text-align:left">TOTAL</td>'+
            '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
            '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
            '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
            '<td></td>'+
            '</tr>';

            $('#show_data').html(html);
            $.unblockUI();
         }else{
            toastr.error('Ada Gangguan Koneksi')
         }
      }).fail((bunga) => {
         toastr.error('Ada Gangguan Koneksi')
      })
   }
   function efektif(nominal, jangkawaktu, conf_bunga){
      // console.log('ini ',nominal,jangkawaktu);
      if(!nominal || !jangkawaktu){
         alert('Nominal / Jangka Waktu tidak boleh kosong');
      }
      var bungaBulan = 0;
      var bunga = 0;
      var bungaFlat = 0;
      var jumlah = 0;
      var pokok = 0;
      var tobu = 0;
      var total = 0;
      var bunga_dlm_bulan = 0;
      var pinjaman_plus_bunga = 0;
      var bunga_bul = 0;
      var pembagi = 0;
      var  musiman =0;
      
      musiman = $('[name="musiman"]').val();

      bungaBulan = conf_bunga/12;
      
      jumlah = parseFloat(nominal);
      
      bungaFlat = parseFloat(bungaBulan/100);
      // alert(bunga_bul);
      pokok = parseFloat(nominal/jangkawaktu);
      
      
      
      // tobu = parseFloat(nominal/(pembagi/bunga_bul));
      
      total = parseFloat(nominal);

       var html = '<tr>'+
         '<td style="text-align:left"> Bulan 0</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">0</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(nominal.toFixed(2), '')+'</span></td>'+
      '</tr>';
      // var total = (nominal -  (( - 1) * pokok )) * bungaBulan);
      //  alert(total);
      var jumlah_angsuran_bunga = 0;
      var jumlah_angsuran_pokok = 0;
      var jumlah_total_angsuran = 0;
      var sisa_angsuran = total;
      // alert(sisa_angsuran);
      for(i=1;i<= parseInt(jangkawaktu);i++){

         bunga = parseFloat((nominal -  ((i-1) * pokok.toFixed(2) )) * bungaFlat);
         // alert();
         if(musiman == 1){
            if (i >= jangkawaktu){
               pokok = nominal;
            }else{
               pokok = 0;
            }
            tobu = pokok + bunga;
            // // angsuran_pokok = angsuran_perbulan - angsuran_bunga;
            sisa_angsuran -= pokok;
         }
         else{
            tobu = pokok + bunga;
            sisa_angsuran -= pokok;
         }

         // tobu = pokok + bunga;
         // alert(bungaFlat);
         // pokok = tobu-bungaFlat;
         
         sisa_angsuran = parseFloat(sisa_angsuran);
         jumlah_angsuran_bunga += bunga;
         jumlah_angsuran_pokok += pokok;
         jumlah_total_angsuran += tobu;
         // pokok = tobu - bungaFlat;
         
         if(sisa_angsuran <= 0){
            sisa_angsuran = 0;
         }
         html += '<tr>'+
         '<td style="text-align:left"> Bulan '+ i +'</td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(bunga.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(pokok.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(tobu.toFixed(2), '')+'</span></td>'+
         '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(sisa_angsuran.toFixed(2), '')+'</span></td>'+
         '</tr>';
      }
       html += '<tr class="bg-cepiring text-light">'+
      '<td style="text-align:left">TOTAL</td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_bunga.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_angsuran_pokok.toFixed(2), '')+'</span></td>'+
      '<td><span class="float-left">Rp.</span><span class="float-right">'+formatRupiah(jumlah_total_angsuran.toFixed(2), '')+'</span></td>'+
      '<td></td>'+
      '</tr>';

      $('#show_data').html(html);
      $.unblockUI();
   }

   function formatRupiah(num) {
      return num.toString().replace(/\./g, ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
   }
</script>
