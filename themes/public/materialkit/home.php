<style>
.form-nominal{
   width: 100%;
   border: 1px solid #ccc;
   border-radius: 5px;
   padding: 10px 10px 10px 35px;
}
#span-rp{
   position: absolute;
   top: 43px;
   left: 14px;
}
.sp-blog-item {
   box-shadow: none !important;
   border: 1px #ddd solid;
}
.btn-calc{
   background: #366a34;
   color: white;
   font-weight: bolder;
   border: none;
}
._label_pilih{
   background: #ee832e;
   color: white !important;
   padding: 16px;
   text-align: center;
   margin: 52px 0px !important;
   font-size: 21px !important;
}
.bg-foot{
   background: #e6e6e6;
}
.bg-cepiring{
   background: #366a34;
}

.simulasi_kredit{
   font-size: 25px;
   padding: 30px 0px;
   color: #366a34;
}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
  
}
.box-grid{
   border: 1px #ddd solid;
   display: flex;
   border-radius: 10px;
   margin-bottom: 20px;
}
.box-img{
   padding: 23px;
}
.box-img img{
   width: 100px;
}
.box-content{
   padding: 12px 5px 20px 5px;
}
.box-content .title{
   color: #366a34;
   font-weight: 500;
   font-size: 19px;
}
.box-content .content{
   font-size: 13px;
}
.box-content .action{

}
.box-content .action a{
   font-size: 13px;
   background: #ee832e;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.box-content .action a:hover{
   cursor: pointer;
   color: #fff;
}
.spad {
    padding-top: 30px;
    padding-bottom: 40px !important;
}

.label-file {
    cursor: pointer;
    font-size: 14px;
    background: white;
    border: 2px #ddd solid;
    padding: 8px 15px;
    margin-bottom: 0px;
    border-radius: 4px;
    width: 50%;
    /* background: #28a745; */
    color: black;
    /* box-shadow: 1px 2px 3px 0px rgb(94, 94, 94); */
}
.file-custom {
    opacity: 0;
    position: relative;
    z-index: 0;
    cursor: pointer;
    top: -34px;
    width: 100%;
}
._box{
    border: 2px #ddd solid;
    padding: 28px 10px;
    margin: 17px 0px;
    border-radius: 8px;
    position: relative;
}
._box_title{
    position: absolute;
    top: -30px;
    left: 18px;
    text-align: center;
    font-size: 25px;
    background: white;
    /* height: 36px; */
    padding: 11px;
}
#kode_negara{
    position: absolute;
    top: 39px;
    left: 28px;
}
</style>
<section class="add-section spad">
    <div class="container">
        <div class="">
            <div class="row" style="justify-content: center;">
                <div class="col-xs-12 col-lg-12" style="padding: 0px 30px;">
                    <h2 class="green" style="text-align: center; font-size:25px;">Pengajuan <?= $nama_produk ?></h2>
                    <p style="text-align:  center; font-color : #686e75; font-size : 16px; margin-top: 10px;">
                        Silahkan isi form berikut dengan lengkap. Staff kami akan segera menghubungi anda. <br> pengiriman data anda diamankan dengan menggunakan teknologi TLS
                    </p>
                </div>
            </div>
            <form id="form_pengajuan" enctype='multipart/form-data'>
            <div class="container">
                <div class="row" style="justify-content: center;">
                    <div class="col-md-6">
                        <div class="_box">
                            <span class="_box_title green">Biodata</span>
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Nama Lengkap Sesuai KTP</label>
                                    <input class="form-control" name="nama_lengkap"> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">NIK KTP</label>
                                    <input class="form-control nik" name="nik"> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">No HP Aktif</label>
                                    <span id="kode_negara">+62</span>
                                    <input class="form-control no_hp" name="no_hp" style="padding-left: 48px;"> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Email Aktif</label>
                                    <input class="form-control" name="email"> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="green" style="padding: 0px 4px;">Foto Selfi</label>
                                    <div>
                                        <label class="label-file">Pilih File</label>
                                        <input type="file" name="photo_selfi" class="file-custom" accept="image/*" onchange="tampilkanPreview(this,'preview')" />
                                        <div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
                                            <img id="preview" src="" alt="" style="width: 200px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="green" style="padding: 0px 4px;">Foto KTP</label>
                                    <div>
                                        <label class="label-file">Pilih File</label>
                                        <input type="file" name="photo_ktp" class="file-custom" accept="image/*" onchange="tampilkanPreviewktp(this,'previewktp')" />
                                        <div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
                                            <img id="previewktp" src="" alt="" style="width: 200px" />
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="_box">
                            <span class="_box_title green">Tempat Tinggal</span>
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Provinsi Sekarang</label>
                                    <select class="select2 form-control" name="provinsi_nasabah" onchange="change_provinsi('nasabah')">
                                        <option value="">Pilih Sekarang</option>
                                    </select> 
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Kab/Kota Sekarang</label>
                                    <select class="select2 form-control" name="kota_nasabah" onchange="change_kabupaten('nasabah')">
                                        <option value="">Pilih Sekarang</option>
                                    </select> 
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Kecamatan Sekarang</label>
                                    <select class="select2 form-control" name="kecamatan_nasabah" onchange="change_kecamatan('nasabah')">
                                        <option value="">Pilih Sekarang</option>
                                    </select> 
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Kelurahan Sekarang</label>
                                    <select class="select2 form-control" name="kelurahan_nasabah" onchange="change_kelurahan('nasabah')">
                                        <option value="">Pilih Sekarang</option>
                                    </select> 
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Kode Pos Sekarang</label>
                                    <input class="form-control" readonly name="kodepos_nasabah">
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group" style="padding: 0px 4px;"> 
                                    <label class="green">Alamat Sekarang</label>
                                    <textarea name="alamat_lengkap_nasabah" class="form-control"></textarea>
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Apakah Alamat Domisili sama dengan Alamat di KTP ?</label>
                                    <select name="same_ktp" class="form-control">
                                        <option selected disabled>Jawaban Anda</option>
                                        <option value="true">Ya</option>
                                        <option value="false">Tidak</option>
                                    </select>
                                    <!-- <input type="checkbox" id="confirm_alamat" name="same_ktp">  -->
                                </div>
                            </div>
                            <div id="optional" style="display: none;">
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Provinsi KTP</label>
                                        <select class="select2 form-control" name="provinsi_optional" onchange="change_provinsi('optional')">
                                            <option value="">Pilih Sekarang</option>
                                        </select> 
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Kab/Kota KTP</label>
                                        <select class="select2 form-control" name="kota_optional" onchange="change_kabupaten('optional')">
                                            <option value="">Pilih Sekarang</option>
                                        </select> 
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Kecamatan KTP</label>
                                        <select class="select2 form-control" name="kecamatan_optional" onchange="change_kecamatan('optional')">
                                            <option value="">Pilih Sekarang</option>
                                        </select> 
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Kelurahan KTP</label>
                                        <select class="select2 form-control" name="kelurahan_optional" onchange="change_kelurahan('optional')">
                                            <option value="">Pilih Sekarang</option>
                                        </select> 
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Kode Pos KTP</label>
                                        <input class="form-control" name="kodepos_optional" readonly>
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Alamat KTP</label>
                                        <textarea name="alamat_lengkap_optional" class="form-control"></textarea>
                                    </div>
                                </div>  
                            </div>
                            <div class="col-md-12" style="text-align: center">
                                <div>
                                    <input type="checkbox" name="nik_aggrement"> Mengisi NIK berarti bersedia, menyerahkan NIK Anda utk keperluan Bank BPR Nusamba Cepiring
                                </div>
                                <div style="font-size: 14px; padding: 10px 8px;">
                                    <span>Dengan mengklik tombol pengajuan anda telah menyetujui semua peraturan yang berlaku di BPR Nusamba Cepiring.</span>
                                </div>
                                <div class="form-group" style="text-align: text-center">
                                    <button type="submit" class="btn btn-primary elips-btn">Pengajuan</button>
                                </div>
                            </div>  
                        </div>
                    </div>
               </div>
            </div>
            </form>
           
        </div>
    </div>
</section>

<script type="text/javascript">
    function tampilkanPreview(gambar, idpreview) {
        var gb = gambar.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreview);
            var reader = new FileReader();

            if (gbPreview.type.match(imageType)) {
                preview.file = gbPreview;
                reader.onload = (function(element) {
                    return function(e) {
                        element.src = e.target.result;
                    };
                })(preview);
                reader.readAsDataURL(gbPreview);
            } else {
                alert("file yang anda upload tidak sesuai. Khusus mengunakan image.");
            }

        }
    }
</script>

<script type="text/javascript">
    function tampilkanPreviewktp(gambar, idpreviewktp) {
        var gb = gambar.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreviewktp);
            var reader = new FileReader();

            if (gbPreview.type.match(imageType)) {
                preview.file = gbPreview;
                reader.onload = (function(element) {
                    return function(e) {
                        element.src = e.target.result;
                    };
                })(preview);
                reader.readAsDataURL(gbPreview);
            } else {
                alert("file yang anda upload tidak sesuai. Khusus mengunakan image.");
            }
        }
    }
</script>

<script>
    var url = {
        'pengajuan' : "<?= base_url('public/home/pengajuan/'.$id_product.'?type_id='.$id_type_product.'&ref='.$ref) ?>"
    }
    

    $(() => {
        set_provinsi('nasabah')
        set_provinsi('optional')
        
        blockUI('Memuat Formulir ...');
        setTimeout(() => {
            let nasabah_prov = $('[name="provinsi_nasabah"]');
            nasabah_prov.val(13).trigger('change');
            
            let ktp_prov = $('[name="provinsi_optional"]');
            ktp_prov.val(13).trigger('change');
            
            $.unblockUI();
        }, 2500);
    })

    $('form').on('submit', (e) => {
        if(!check_is_valid()){
            alert_error('','Anda belum melengkapi formulir')
            return false;
        }
        let aggre = $('[name="nik_aggrement"]')[0].checked;
        if(!aggre){
            alert_error('','Anda belum mencentang Disclaimer')
            return false;
        }
        e.preventDefault();
        blockUI();

        let no_hp = $('[name="no_hp"]').val();
        let nik = $('[name="nik"]').val();

        let err_msg = [];
        if(no_hp){
            let valid = validasi_nohp(no_hp);
            if(!valid){
                err_msg.push('Format No.HP Salah, ex: 08xxxxxxxxxx')
            } 
        }
        if(nik){
            let valid = validasi_nik(nik);
            if(!valid){
                err_msg.push('Format NIK Salah, ex: 32356xxxxxxxxxxx')
            } 
        }

        // let answer = $('[name="same_ktp"]').val();
        // if(!answer){
        //     alert_error('Anda belum mengisi','"Apakah Alamat Domisili sama dengan Alamat di KTP ?"')
        //     $.unblockUI();
        //     return false;

        // }

        if(err_msg.length > 0){
            let message = render_error(err_msg)
            set_modal_dinamis('',message,'');
            setTimeout(() => {
                open_modal_dinamis();
                $.unblockUI();
            }, 1000);
            return;
        }

        console.log(err_msg.length);

        let formData = new FormData($('#form_pengajuan')[0]);
        $.ajax({
            url: url.pengajuan,
            data: formData,
            type: 'POST',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: (res) => {
                toastr.success(res.msg);
                setTimeout(() => {
                    window.location.href = `${window.location.origin}/public/home/pengajuan_berhasil`;
                    $.unblockUI();
                }, 2500);
            },
            error: (xhr) => {
                console.log(xhr)
                if(xhr.status == 422){
                    let message = render_error(xhr.responseJSON)
                    set_modal_dinamis('',message,'');
                    setTimeout(() => {
                        open_modal_dinamis();
                    }, 1000);
                    $.unblockUI();
                }else if(xhr.status == 400){
                    toastr.error(xhr.responseJSON.msg)
                }
            },
        });
    })  

    function check_is_valid(){
        let validate = $('#form_pengajuan').serializeArray();
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }

    function validasi_nik(str){
        let count = str.match(/^((1[1-9])|(21)|([37][1-6])|(5[1-4])|(6[1-5])|([8-9][1-2]))[0-9]{2}[0-9]{2}(([0-6][0-9])|(7[0-1]))((0[1-9])|(1[0-2]))([0-9]{2})[0-9]{4}$/g)
        if(count){
            return true;
        }else{
            return false;
        }
    }

    function validasi_nohp(str){
        let count = str.match(/[8][0-9]{1,10}/g)
        if(count){
            return true;
        }else{
            return false;
        }
    }

    // $('#confirm_alamat').on('click', (e) => {
    $('[name="same_ktp"]').on('change', (e) => {
        // alert(e)
        // console.log(e.target.checked);
        let con = e.target.value;
        
        
        
        // alert(con)
        if(con == 'true'){
            $('[name="provinsi_optional"]').val(null).trigger('change').prop('disabled', true)
            $('[name="kota_optional"]').val(null).trigger('change').prop('disabled', true)
            $('[name="kecamatan_optional"]').val(null).trigger('change').prop('disabled', true)
            $('[name="kelurahan_optional"]').val(null).trigger('change').prop('disabled', true)
            $('[name="kodepos_optional"]').val(null).prop('disabled', true)
            $('[name="alamat_lengkap_optional"]').val(null).prop('disabled', true)
            $('#optional').hide();    
        }else{
            $('[name="provinsi_optional"]').val(null).trigger('change').prop('disabled', false)
            $('[name="kota_optional"]').val(null).trigger('change').prop('disabled', false)
            $('[name="kecamatan_optional"]').val(null).trigger('change').prop('disabled', false)
            $('[name="kelurahan_optional"]').val(null).trigger('change').prop('disabled', false)
            $('[name="kodepos_optional"]').val(null).prop('disabled', false)
            $('[name="alamat_lengkap_optional"]').val(null).prop('disabled', false)
            let ktp_prov = $('[name="provinsi_optional"]');
            ktp_prov.val(13).trigger('change');
            $('#optional').show();
        }
    })
</script>
