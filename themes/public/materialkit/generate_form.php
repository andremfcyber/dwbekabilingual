<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bank Nusamba BPR</title>
		<meta charset="UTF-8">
		<meta name="description" content="Logo">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->   
		<link href="<?php echo $theme_url;?>/img_frontend/favicon.ico" rel="shortcut icon"/>
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
		
		<!-- Stylesheets -->
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css"/>
		<!-- <link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/font-awesome.min.css"/> -->
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/animate.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/style.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/smart_wizard.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/smart_wizard_theme_dots.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/toastr/toastr.min.css')?>"/>
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="<?php echo $theme_url;?>/js_frontend/jquery.smartWizard.js"></script>
		<style type="text/css">
			.green {
			     background-color: white !important; 
			}
			.grey {
			     background-color: white !important; 
			}
		</style>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style type="text/css">
			
			[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
			    position: relative !important;
			    left: 0px !important;
			    opacity: 1 !important;
			}

		</style>
		    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144983683-1');
</script>
	</head>
	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>
		
		<!-- Header section -->
		<header class="header-section">
			<div class="header-top">
				<div class="container">
					<marquee color="white" style="font-size: 12px;"><font color="white" >HUBUNGI LAYANAN PELANGGAN TLP:021-84931416 EMAIL:INFO@BPRNUSAMBACEPIRING.COM</font></marquee>
				</div>
			</div>
			<div class="header-bottom h-b">
				<div class="container">
					<a href="index.html"  class="site-logo">
						<img style="padding-top: 10px; padding-bottom:10px;" src="<?php echo $theme_url;?>/img_frontend/logo.png" alt="">
					</a>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
					<ul class="main-menu">
						<li class='submenu'><a href="#">Profil <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/profil') ?>"><i class="fa fa-chevron-right"></i> Tentang Kami</a></li>
								<li><a href="<?php echo base_url('public/home/visi_misi') ?>"><i class="fa fa-chevron-right"></i> Visi Misi</a></li>
								<li><a href="<?php echo base_url('public/home/struktur') ?>"><i class="fa fa-chevron-right"></i> Struktur Organisasi</a></li>
								<li><a href="<?php echo base_url('public/home/budaya_perusahaan') ?>"><i class="fa fa-chevron-right"></i> Budaya Perusahaan</a></li>
								<li><a href="<?php echo base_url('public/home/awards') ?>"><i class="fa fa-chevron-right"></i> Awards</a></li>
								<li><a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>"><i class="fa fa-chevron-right"></i> Kenapa memilih kami</a></li>
								<li><a href="<?php echo base_url('public/home/faq') ?>"><i class="fa fa-chevron-right"></i>FAQ</a></li>
								<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>"><i class="fa fa-chevron-right"></i> Hubungi Kami</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Berita <i class="fa fa-caret-down"></i> </a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/agenda') ?>"><i class="fa fa-chevron-right"></i> Agenda</a></li>
								<li><a href="<?php echo base_url('public/home/press_realese') ?>"><i class="fa fa-chevron-right"></i> Press Realese</a></li>
								<li><a href="<?php echo base_url('public/home/cerita_pelanggan') ?>"><i class="fa fa-chevron-right"></i> Cerita Pelanggan</a></li>
								<li><a href="<?php echo base_url('public/home/laporan') ?>"><i class="fa fa-chevron-right"></i> Laporan</a></li>
								<li><a href="<?php echo base_url('public/home/blog') ?>"><i class="fa fa-chevron-right"></i> Blog</a></li>
								<li><a href="<?php echo base_url('public/home/karir') ?>"><i class="fa fa-chevron-right"></i> Karir</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Media <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/galeri') ?>"><i class="fa fa-chevron-right"></i> Galeri</a></li>
								<li><a href="<?php echo base_url('public/home/download') ?>"><i class="fa fa-chevron-right"></i> Download &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
							</ul>
						</li>
						<li><a href="<?php echo base_url('public/home/kredit') ?>">Kredit</a></li>
                        <li><a href="<?php echo base_url('public/home/deposito') ?>">Deposito</a></li>
                        <li><a href="<?php echo base_url('public/home/tabungan') ?>">Tabungan</a></li>
						<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>">Kontak</a></li>
						<li><a href="<?php echo base_url('public/home/artikel') ?>">Artikel</a></li>
						
						<a class="gray" href="#">IND</a> <a class="gray" href="#"> | ENG</a>
						
					</ul>
				</div>
			</div>
		</header> 
		<!-- Header section end -->
		
		<!-- Add section end -->
		<section class="add-section spad">
			<div class="container">
				<div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
					
					<form id="form" action="<?= base_url("public/home/save_data_formulir/$id_product/$id_nasabah");?>" method="POST">
					    <div id="smartwizard">
					        <ul>
					        	<?php if ($form_header > 0 ){
					        		foreach($form_header as $key => $value) {?>
					            <li><a href="#form-<?= $value['id']?>"><span><?= $value['name_header']?></span></a></li>
						        	<?php } ?>
						        <?php } ?>
					        </ul>
					     
					        <div>
					        	<?php if ($form_header > 0 ){
					        		foreach($form_header as $key => $value) {?>
					            <div id="form-<?= $value['id']?>">
					            	<?php
					            		$query = "SELECT * FROM form_fileds WHERE form_header_id = ".$value['id']." ORDER BY urutan";
					            		$form_attr = $this->db->query($query)->result_array();
					            		
					            		foreach ($form_attr as $key2 => $value2) {
					            			$input_type = $value2['input_type'];
					            			switch ($input_type) {
					            				case 'text':                                      
					            				$field_name = strtolower(str_replace(' ', '_', $value2['label']));
										        $field_name = (preg_replace('/[^A-Za-z0-9\_]/', '', $field_name));
										        $value_detail = $this->db->get_where('data_formulir_nasabah', array('id_nasabah' => $id_nasabah, 'id_product'=>$id_product, 'id_form_field'=>$value2['id']))->row_array();
										        if (isset($value_detail)){
										        	$data_value = $value_detail['value'];
										        } else if (isset($detail_nasabah[$field_name])){
										        	$data_value = $detail_nasabah[$field_name];
										        } else {
										        	$data_value = null;
										        }
					            				
                                      generateTextInput($value2['label'],$value2['id'],$data_value);

                                      break;

                                    case 'checkbox':

                                      $form_attr_source = $this->db->get_where("form_attribut",array("form_filed_id" => $value2['id']))->result_array();

                                      generateCheckbox($value2['label'],$value2['id'],$form_attr_source,$id_nasabah,$id_product);
                                    
                                    default:
                                      # code...
                                      break;

					                    // case 'radio':

					                    //   $form_attr = $this->db->order_by('urutan','ASC');
					                    //   $form_attr_source_radio = $this->db->get_where("form_attribut",array("form_filed_id" => $value2['id']))->result_array();

					                    //   generateRadio($value2['label'],$value2['id'],$form_attr_source_radio);

					                    // default:

					                    //   break;
					            			}
					            		}
					            	?>
					            </div>
						        	<?php } ?>
						        <?php } ?>
					        </div>
					    </div>
					</form>
				</div>
			</div>
		</section>
		<!-- Add section end -->
		
		
		
		
		
		<!-- Add section end -->
		
	
	
	
	
	
	
	
	
	
	
	
	<!-- Gallery section end -->
	
	
	<!-- Footer section  -->
	<footer class="footer-section set-bg" >
		
		<div class="row">
			<div class="col-lg-3">
				<h3 style="color:white; font-size:18px">Kontak Kami</h3><br>
				<font style="color:white; font-size:11px">Jl. Raya Gondang No.30, Pencarikan, Gondang, Kec. Cepiring, Kabupaten Kendal, Jawa Tengah 51352</font><br>
				<font style="color:white; font-size:11px"><i class="fa fa-phone"></i>  Telepon :  (0294) 382234</font><br>
				<font style="color:white; font-size:11px"><i class="fa fa-fax"></i> Fax :  (0294) 382234</font><br>
				<font style="color:white; font-size:11px"><i class="fa fa-envelope"></i> Email :  (0294) 382234</font><br>
			</div>
			<div class="col-lg-3">
				<h3 style="color:white; font-size:18px">Tentang Kami</h3><br>
			<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Cara Pengajuan dan Pembayaran</a></font></a><br>
			<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Tanya Jawab</font></a><br>
			<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Ajukan Lagi</font></a><br>
			<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
			
		</div>
		<div class="col-lg-3">
			<h3 style="color:white; font-size:18px">Kebijakan</h3><br>
			<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Syarat dan Ketentuan</font></a><br>
			<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Privasi</font></a><br>
			<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Cookies</font></a><br>
			<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
			
		</div>
		<div class="col-lg-3">
			<div class="col-lg-12">
				<h3 style="color:white; font-size:18px">Ikuti Kami</h3><br>
				<a style="color:white; font-size:11px" href="#"><i class="fa fa-facebook"></i></a>
				<a style="color:white; font-size:11px" href="#"><i class="fa fa-twitter"></i></a>
				<a style="color:white; font-size:11px" href="#"><i class="fa fa-instagram"></i></a>
				<a style="color:white; font-size:11px" href="#"><i class="fa fa-google-plus-square"></i></a>
			</div>
			<div class="col-lg-12">
				<div class="text-left">
					<div class="form-group">
						<input type="text" class="form-control"  placeholder="">
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="text-left">
					<button type="submit" class="btn btn-primary elips-btn">Subscribe</button>
				</div>
			</div>
			
		</div>
	</div>
	
</footer>
<!-- Footer section end -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  


<!-- <script src="https://cdn.datatables.net/1.10.19/js_frontend/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js_frontend/dataTables.bootstrap4.min.js"></script> -->
<script src="<?php echo $theme_url;?>/js_frontend/owl.carousel.min.js"></script>
<script src="<?php echo $theme_url;?>/js_frontend/main.js"></script>
<script src="<?php echo $theme_url;?>/js_frontend/bootstrap.js"></script>
<script src="<?= base_url('assets/plugins/toastr/toastr.min.js')?>"></script>
<!-- <script src="<?php echo $theme_url;?>/js_frontend/jquery-1.11.2.min.js"></script> -->

<script>
    function goBack() {
        window.history.back();
    }
</script>

<script type="text/javascript">
    function hilang(){
      $('#target1').hide();
      $('#target2').hide();
      $('#target3').hide();
      $('#target4').hide();
      $('#target5').hide();
      $('#target6').show();
      $('#target7').show();
      $('#target8').show();
      $('#target9').show();
      $('#target10').show();
      $('#target11').show();
      $('#target12').show();
      $('#target13').show();
    }
  </script>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
	
	function hanyaAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		
		return false;
		return true;
	}
	
	function hitung(){
		var nominal=document.getElementById("nominal").value;
		var jangkawaktu=document.getElementById("jangkawaktu").value;
		bungaBulan = 9.25/12;
		
		var bunga = nominal * parseFloat(bungaBulan);
		var bungaFlat = bunga/100;
		var pokok = nominal/jangkawaktu;
		//alert(jangkawaktu);
		var html = '';
		var total = nominal;
		for(i=1;i<= parseInt(jangkawaktu);i++)
		{
			total = total - (pokok );
			html += '<tr>'+
			'<td style="text-align:center"> Bulan '+ i +'</td>'+
			'<td style="text-align:center">'+formatNumber(pokok + bungaFlat)+'</td>'+
			'<td style="text-align:center">'+formatNumber(pokok)+'</td>'+
			'<td style="text-align:center">'+formatNumber(bungaFlat)+'</td>'+
			'<td style="text-align:center">'+formatNumber(total)+'</td>'+
			'</tr>';
		}
		$('#show_data').html(html);
	}
	
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}
</script>
<script>
	// Open the Modal
	function openModal() {
		document.getElementById("myModal").style.display = "block";
	}
	
	// Close the Modal
	function closeModal() {
		document.getElementById("myModal").style.display = "none";
	}
	
	var slideIndex = 1;
	showSlides(slideIndex);
	
	// Next/previous controls
	function plusSlides(n) {
		showSlides(slideIndex += n);
	}
	
	// Thumbnail image controls
	function currentSlide(n) {
		showSlides(slideIndex = n);
	}
	
	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("demo");
		var captionText = document.getElementById("caption");
		if (n > slides.length) {slideIndex = 1}
		if (n < 1) {slideIndex = slides.length}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[slideIndex-1].style.display = "block";
		dots[slideIndex-1].className += " active";
		captionText.innerHTML = dots[slideIndex-1].alt;
	}
	
	
</script> -->

<!--====== Javascripts & Jquery ======-->
</script>

<script type="text/javascript">
         
            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('Finish')
                                            .attr('id','btn-finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ 
                                                    $("#form").submit(function(e) {
                                                    	e.preventDefault();
                                                    	var data = $(this).serialize();
                                                    	var target = $(this).attr('action');
                                                    	$.post(target, data, function (response) {
                                                    		if(response.status == "1"){
                                                    			setTimeout(function(){
                                                    				toastr.success(response.msg, "Response Server");
                                                    			},2000);
													          	setTimeout(function(){
													          		window.history.go(-1);
													          	}, 2500);
													        }else{
													          toastr.error(response.msg, "Response Server");
													        }
                                                    	}, 'json');
                                                    	return false;
                                                    });
                                                });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ 
                                                    $('#smartwizard').smartWizard("reset");  
                                                });                         
            
            
            
            // Smart Wizard
            $('#smartwizard').smartWizard({ 
                    selected: 0, 
                    theme: 'dots',
                    transitionEffect:'fade',
                    toolbarSettings: {toolbarPosition: 'bottom',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
                 });
            
            $("#btn-finish").addClass('disabled');
             $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
                   //alert("You are on step "+stepNumber+" now");
                   if(stepPosition == 'first'){
                       $("#prev-btn").addClass('disabled');
                       $("#btn-finish").addClass('disabled');
                   }else if(stepPosition == 'final'){
                       $("#next-btn").addClass('disabled');
                       $("#btn-finish").removeClass('disabled');
                   }else{
                       $("#prev-btn").removeClass('disabled');
                       $("#next-btn").removeClass('disabled');
                       $("#btn-finish").addClass('disabled');
                   }
                });                               
            
         
    </script>


</body>
</html>																																																																																																																																																																																																																																													