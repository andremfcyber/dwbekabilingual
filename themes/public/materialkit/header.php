<?php defined( 'BASEPATH') OR exit( 'No direct script access allowed'); ?>
<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		<?php echo $this->settings->site_name; ?> |
		<?= $title ?>
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php if(isset($page_metas)){ ?>
		<?php foreach ($page_metas as $meta) { ?>
			<meta <?php echo ($meta['meta_name']!='') ? 'name="'.$meta['meta_name'].'"' : ''; ?> <?php echo ($meta['meta_property']!='') ? 'property="'.$meta['meta_property'].'"' : ''; ?> <?php echo ($meta['meta_charset']!='') ? 'charset="'.$meta['meta_charset'].'"' : ''; ?> <?php echo ($meta['meta_http_equiv']!='') ? 'http-equiv="'.$meta['meta_http_equiv'].'"' : ''; ?> content="<?= $meta['meta_content']; ?>">
		<?php } ?>	
	<?php } ?>

	<link rel="icon" sizes="192x192" href="<?php echo base_url(); ?>assets/image/bankeka.jpg">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/image/bankeka.jpg" type="image/svg">
	<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/image/bankeka.jpg" type="image/svg">

	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<!-- Favicon -->
  
	<link href="<?php echo base_url('upload/logo/favicon1.png')?>" rel="shortcut icon" />
	<!-- Fonts -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" /> -->
		<!-- Bootstrap CSS -->
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/animate.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/style.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?= base_url('assets/plugins/toastr/toastr.min.css')?>" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/select2.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/select2-bootstrap.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://fonts.googleapis.com/css?family=Rubik&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
		<link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/plugins/flag-icon/css/flag-icon.min.css"> 

		<noscript> 
			<link rel="stylesheet" media="all" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css" > 
			<link rel="stylesheet" media="all" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css" >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/animate.css" >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/style.css" >
			<link rel="stylesheet" media="all"  type="text/css" href="<?= base_url('assets/plugins/toastr/toastr.min.css')?>">
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/select2.min.css"   >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/select2-bootstrap.css" >
			<link rel="stylesheet" media="all"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" >
			<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
			<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
			
	 	</noscript>
		 
	<?php if ( isset( $css_files ) && is_array( $css_files ) ) : foreach ( $css_files as $css ) : ?>
	<!-- <link rel="stylesheet" href="<?= $css ?><?php echo '?r='.rand(1,1000);?>"> -->
	<?php echo "\n"; endforeach; endif; ?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		
		gtag('config', 'UA-144983683-1');
	</script> 
<style type="text/css">

</style>
	</head>
	<script>
		var site_url = '<?=site_url()?>';
		var base_url = '<?=base_url()?>';
	</script>
	<script src="<?= base_url('assets/dist/js/jquery.js') ?>"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha512-M5KW3ztuIICmVIhjSqXe01oV2bpe248gOxqmlcYrEzAvws7Pw3z6BK0iGbrwvdrUQUhi3eXgtxp5I8PDo9YfjQ==" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/dist/js/jquery.number.min.js') ?>"></script>
	<script src="<?= base_url('assets/dist/js/jquery.blockUI.js') ?>"></script>
	<script src="<?= base_url('assets/dist/js/sweetalert.min.js') ?>"></script>

	<style>
		a {
			color: #9c9fa9;
			text-decoration: underline;
		}
		 .dropdown-submenu{position:relative;}
		.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
		.dropdown-submenu:hover>.dropdown-menu{display:block;}
		.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
		.dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
		.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
		.list p { 
			margin: 4px;
		}
		.list i {
			padding-right: 10px;
		}
		.card {
			min-height:auto;
		}
		.radius {
			border-radius:21px;
		}
		.breadcrumb { 
			background-color: #ffffff; 
		}
		.nav-item .fa {
			color:#DDD;
		}

		.nav-item,.dropdown-item>a {
			font-variant: all-petite-caps;  
			font-size: 17px;
		}

		.navbar .nav-item .dropdown-menu { 
			border-radius:0px;
			border-left: 4px solid #ffaf00;
		}
		h5.card-title.card-title-cs { 
			font-size: 16px;
		}
		ol {
			list-style: decimal;
			margin-left: 25px;
		}
		.cf {
			max-width: 92%!important;
			transition: 0.7s ease;
		}
		
		.cf:hover {
			max-width: 100%!important;
			border: 6px dashed #d9152b;
			transition: 0.7s ease;
			border-radius: 80px!important;
		}
		.btn-warning {
			color: #ffffff;
			background-color: #ffc107;
			border-color: #ffc107;
		}
		.bootstrap-select{
			margin-left:50px !important;
		}
	</style>
</head>

<body style="display:none;">
	<!-- Page Preloder -->
	<!-- <div id="preloder">
		<div class="loader"></div>
	</div> -->
	<!-- Header section -->
 
<header>
	
	<nav class="navbar navbar-expand-lg navbar-white bg-white">
    	<div class="container-xl">
    		<a class="navbar-brand site-logo" href="<?php echo base_url('') ?>"> 
              	<img style="padding: 6px;height: 68px;" src="<?php echo base_url('upload/logo/').$_profil['logo']  ?>" alt=""> 
    		</a>
    		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
    				<i class="fa fa-bars"></i>
    		</span>
    		</button> 
    		<div class="collapse navbar-collapse" id="navbarsExample07XL">
        		<?= renderNavbarMenus(); ?>
        		<?= renderLangSelector(); ?>
    		</div>
    	</div>
	</nav>
</header>


<!--- MAIN --->
	<section class="body-content">