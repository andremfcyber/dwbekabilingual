<style>
	p {
		font-size: 11px;
		color: #838383;
		line-height: 2.1;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #d95c2a !important;
	}
	.card {
		position: relative;
		margin: 8px 0px;
		/* border-radius: 3px !important; */
		box-shadow: 3px 5px #ddd !important;
		border-radius: 15px;
		min-height: 503px;
	}
	.card-bodys.card-body-cs {
		border-radius: 15px;
	}
	img.card-img-top.card-img-cs {
		border-top-left-radius: 15px !important;
		border-top-right-radius: 15px !important;
		object-fit: cover;
		height: 280px;
	}
	h5.card-title.card-title-cs {
		text-transform: uppercase;
		color: #366a34;
		margin-top: 11px;
		font-size: 20px;
	}
	.card-date-cs {
		text-align: right;
		margin: 10px -6px;
	}
	.card-content-cs{
		font-size: 14px;
		margin-bottom: 45px;
	}
	a.btn-slk {
		font-size: 14px;
		background: #d95c2a;
		color: white;
		position: absolute;
		padding: 11px;
		right: 0px;
		bottom: 0px;
		border-bottom-right-radius: 15px;
		border-top-left-radius: 15px;
	}
	.right-bar {
		border: 2px #ddd dashed;
		border-radius: 5px;
		padding: 11px 15px;
		margin: 32px 0px;
	}
	.right-bar-title span{
		color: #366a34;
		font-size: 31px;
	}
	.right-bar-content {
		margin: 10px 0px;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
	}
	a.kategori_btn {
		padding: 10px;
		background: #d95c2a;
		color: #fff;
		text-transform: uppercase;
		border-bottom-right-radius: 10px;
		border-top-left-radius: 10px;
		font-size: 14px;
		/* margin-bottom: 10px; */
		margin: 3px;
	}
	.recent {
		display: flex;
		flex-direction: row;
		margin: 11px 0px;
	}
	img.recent-img {
		height: 100px;
		object-fit: cover;
		width: 100px;
		border-radius: 11px;
		border: 2px #ddd solid;
	}
	.recent-cover-img {
		margin-right: 11px;
	}
	.recent-content {
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	a.recent-content-title {
		color: #366a34 !important;
	}
</style>
	<section class="add-section spad">
		<div class="container">
			<div class="col-lg-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_blog')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-12">
								<ol class="breadcrumba _box" style="margin-bottom: 0px !important;">
									<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Beranda</a></li>
									<li class="breadcrumb-item" aria-current="page"><a class="gray" href="<?php echo base_url('public/home/blog') ?>">Blog</a></li>
									<li class="breadcrumb-item active" aria-current="page">Blog Detail</li>
								</ol>
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<br>
						<div class="card-deck">
							<div class="card">
								<!-- <img class="card-img-top"  src="<?php echo $theme_url;?>/img_frontend/beasiswa-korea-selatan.jpg" alt="Card image cap"> -->
								<div class="card-bodys">
									<div class="container">
										<p style="text-align:left; font-size:9px"><i class="fa fa-calendar" aria-hidden="true"></i> <?php if($_blog_detail){ echo $_blog_detail[0]['input_tgl']; }?> <i class="fa fa-user" aria-hidden="true"></i> <?php if($_blog_detail){ echo $_blog_detail[0]['input_oleh']; }?></p>
										<h5 class="card-title" style="text-align:left; color:#356a34; font-size:20px"> <?php if($_blog_detail){ echo $_blog_detail[0]['judul']; }?> </h5>
										<?php if($_blog_detail){ echo $_blog_detail[0]['isi']; }?> 
										
									</div>
								</div>
								
							</div>
						</div>
						
						<br>
					</div>
					<div class="col-md-4">
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Kategori</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_kategori) > 0) { ?>
									<?php foreach($_kategori as $_data) { ?>
										<a class="kategori_btn" href="<?= base_url('public/home/blog?kategori=').$_data['id']; ?>"><?php echo $_data['nama'] ?></a>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
						
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Blog Lainnya</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_lainnya) > 0) { ?>
									<?php foreach($_lainnya as $_data) { ?>
										<div class="recent">
											<div class="recent-cover-img">
												<img class="recent-img" src="<?php echo base_url('upload/photo/').$_data['file_foto']  ?>" alt="foto blog">
											</div>
											<div class="recent-content">
												<a class="recent-content-title" href="<?php echo base_url().'public/home/blog_detail/'.$_data['id']; ?>"><?= $_data['judul'] ?></a>
												<span class="recent-content-spoiler"><?= spoiler($_data['isi'], 68) ?></span>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
																																																																																																																																																																																																																																																																																																																																																																																																																																																					