<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
	@media (min-width: 320px) and (max-width: 480px) {
		.stengah-col{
			width : 75%;
		}
		.seper-col{
			width : 25%;
		}
	}
	iframe{
		width : 100% !important;
		border-radius: 5px;
	}
	.blue-baru{
		background: #21438b  !important;
	}
</style>
		
	<section class="add-section spada_">
		<div class="container">
			<!-- <div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php //echo base_url('upload/photo/').get_baner_by_kode('1_hubungi_kami')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div> -->
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Hubungi Kami</li>
						</ol>
						<!-- <div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							<a href="<?php echo base_url('public/home/profil') ?>">
								<button type="button" class="btn btn-primary g">
									Tentang Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/visi_misi') ?>">
								<button type="button" class="btn btn-primary g">
									Visi Misi
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
								<button type="button" class="btn btn-primary g">
									Budaya Perusahaan
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/awards') ?>">
								<button type="button" class="btn btn-primary g">
									Awards
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Kenapa Memilih Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-4">
						<div class="yt">
							Hubungi Kami
						</div>
						<div class="ytb">
						</div>
					</div>
					<div class="col-lg-12">
						<br>
						<!-- <p style="text-align:left; font-size:12px"  class="gray">
						Nullam venenatis cursus efficitur. Aliquam in velit nisi. Nullam ut justo non erat faucibus aliquet. Nam hendrerit arcu a orci dignissim, ut ultrices augue elementum.</i>
					</p> -->
				</div>
				<br>
				<div class="col-lg-6">
					<div class="card" style="width: 100%;">
						<div class="card-body blue-baru">
							<div class="container">
								<span style="text-align:left; font-size:20px; color:white"  >
									Silahkan jangan sungkan untuk menghubungi kami,<br> isi formulir berikut ini.
								</span>
								<br><br>
								<form action="<?php echo site_url('public/home/pesan') ?>" method="POST" enctype="multipart/form-data">
					
									<div class="form-group">
										<label style="text-align:left; font-size:14px; color:white" >Nama Lengkap</label>
										<input type="text" class="form-control" name="namalengkap" placeholder="Nama Lengkap" required>
									</div>
									<div class="form-group">
										<label style="text-align:left; font-size:14px; color:white" >No. Handphone</label>
										<input type="number" class="form-control" name="no_hp" placeholder="No. handphone" required>
									</div>
									<div class="form-group">
										<label style="text-align:left; font-size:14px; color:white" >Email</label>
										<input type="email" class="form-control" name="email" placeholder=" email" required>
									</div>
									<div class="form-group">
										<label style="text-align:left; font-size:14px; color:white" >Pesan</label>
										<textarea class="form-control" rows="5" name="pesan" required></textarea>
									</div>
									<button type="submit" class="btn btn-primary btn-ikeh ">Kirim</button>
								</form>
								<br><br><br>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-6">
							<img class="lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url();?>/upload/photo/<?php if($_kontak){ echo $_kontak['foto'];} ?>" class="rounded" width="100%" height="100%" alt="...">
						</div>
						<!-- <div class="col-lg-6">
							<img src="<?php echo $theme_url;?>/img_frontend/KAS-CEPIRING.JPG" class="rounded" width="100%" height="100%" alt="...">
						</div>
						<div class="col-lg-6">
							<br>
							<h2 style="text-align:left; font-size:17px; color:grey">BPR Nusamba Cepiring</h2><br>
							<p style="text-align:left; font-size:14px; color:grey">Jl. Raya Gondang No.30, Pencarikan, Gondang, Kec. Cepiring, Kabupaten Kendal, Jawa Tengah 51352</p>
							<a style="text-align:left; font-size:14px; color:grey">(0294) 382234</a>
						</div> -->
						<div class="col-lg-6">
							<br>
							<h2 style="text-align:left; font-size:17px; color:grey"><?php echo $_profil['nama'] ?></h2><br>
							<p style="text-align:left; font-size:14px; color:grey"><?php if($_kontak){ echo $_kontak['alamat'];} ?></p>
							<a style="text-align:left; font-size:14px; color:grey"><?php if($_kontak){ echo $_kontak['notelpon'];} ?></a>
						</div>
						<div class="col-lg-12">
							<hr>
						</div>
						<div class="col-lg-12">
							<h2 style="text-align:left; font-size:17px; color:grey">Customer Support</h2>
							<br>
						</div>
						<div class="col-lg-6 ">
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">Live Chat</p>
							</div>
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">
									<a href="javascript:void(Tawk_API.toggle())" style="color : #f6861f"> Klik Untuk Chat </a>
								</p>
							</div>
						</div>
						<div class="col-lg-6 stengah-col">
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">Email</p>
							</div>
							<div class="col-md-6">
								<a style="text-align:left; font-size:14px; color:grey"><?php if($_kontak){ echo $_kontak['email'];} ?></a>
							</div>
						</div>
						<div class="col-lg-12">
							<hr>
						</div>
						<div class="col-lg-12">
							<h2 style="text-align:left; font-size:17px; color:grey">Human Resource</h2>
							<br>
						</div>
						<div class="col-lg-6 ">
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">Live Chat</p>
							</div>
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">
									<a href="javascript:void(Tawk_API.toggle())" style="color : #f6861f"> Klik Untuk Chat </a>
								</p>
							</div>
						</div>
						<div class="col-lg-6 stengah-col">
							<div class="col-md-6">
								<p style="text-align:left; font-size:14px; color:grey">Email</p>
							</div>
							<div class="col-md-6">
								<a style="text-align:left; font-size:14px; color:grey"><?php if($_kontak){ echo $_kontak['email'];} ?></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-lg-4" style="margin: 20px 0px;">
						<div class="yt">
							Lokasi Kami
						</div>
						<div class="ytb"></div>
					</div>
					<div style="border-radius: 20px"><?php if($_kontak){ echo $_kontak['keterangan'];} ?></div>
				</div>
			</div>
		</div>
	</section>
	<script>
    // var url = {
    //     'pesan' : "<?= base_url('public/home/pesan') ?>"
    // }
	// let err_msg = [];
    // $('form').on('submit', (e) => {
    //     e.preventDefault();
    //     blockUI();
    // })  

</script>

																																																																																																																																																																																																																																																																																																																																																																																																																																																							