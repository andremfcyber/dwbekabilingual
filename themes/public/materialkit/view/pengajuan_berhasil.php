
    <style type="text/css">
      .green {
        background-color: white !important; 
      }
      .grey {
        background-color: white !important; 
      }
      .alert-cs{
        color: #00781b;
        background-color: #fbfffc;
        border-color: #c3e6cb;
        border: 2px solid;
      }
    </style>
  
    <section class="add-section spad">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="text-align: center;">
            <img class="img-responsive" src="<?= base_url('assets/images/bg.jpg')?>" style="height: 210px">
          </div>
          <div class="col-md-12">
            <div class="alert alert-cs" role="alert">
              <div style="display: flex;">
                <div style="width: 60px; margin-right: 15px;">
                  <img src="<?= base_url('assets/images/speak.svg')?>" style="width: 50px;">
                </div>
                <div>
                  <?= isset($message) ? $message : '' ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
 