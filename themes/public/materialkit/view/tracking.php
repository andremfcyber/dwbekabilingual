<!-- Add section end -->
<section class="add-section spad">
  <div class="container">
    <div class="row">
      <div class="col-md-6" style="text-align: center;">
        <img class="img-responsive img-1" src="<?= base_url('assets/dist/img/tracking.png')?>" style="height: 180px">
      </div>
      <div class="col-md-6">
        <table class="table table-bordered">
            <tr>
                <?php
                    $label_nasabah = isset($nasabah['status_label']) && $nasabah['status_label'] == 'Diterima' ? 'Nama Nasabah' : 'Calon Nasabah';
                ?>
                <td style="width: 45%;"><?= $label_nasabah ?></td>
                <td><?= isset($nasabah['nama']) ? $nasabah['nama'] : '-' ?></td>
            </tr>
            <tr>
                <td style="width: 45%;">Jenis Produk</td>
                <td><?= isset($nasabah['produk_type']) ? $nasabah['produk_type'] : '-' ?> </td>
            </tr>
            <tr>
                <td style="width: 45%;">Produk</td>
                <td><?= isset($nasabah['produk']) ? $nasabah['produk'] : '-' ?></td>
            </tr>
            <tr>
                <td style="width: 45%;">Tanggal Pengajuan</td>
                <td><?= isset($nasabah['tgl_pengajuan']) ? $nasabah['tgl_pengajuan'] : '-' ?> WIB </td>
            </tr>
            <tr>
                <td style="width: 45%;">Status</td>
                <td>
                    <?php
                        // $nasabah['status_label'] = 'Invalid';
                        if(isset($nasabah['status_label'])){
                            $status_class = 'success';
                            $status_label = $nasabah['status_label'];
                            $status_label = ($status_label == 'Lengkapi' ? 'Dilengkapi' : $status_label); 
                            if($status_label == 'Invalid'){
                                $status_class = 'danger';
                            }
                            echo '<span class="badge badge-'.$status_class.' text-light" style="float: left;">'.$status_label.'</span>';
                        }else{
                            echo '-';
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <td style="width: 45%;">Kode Tracking</td>
                <td style="font-size: 12px"><?= isset($nasabah['uid']) ? $nasabah['uid'] : '-' ?></td>
            </tr>
        </table>
      </div>
    </div>
    <!-- <?= $nasabah['status_label'] ?> -->
    <?php if($nasabah['status_label'] != 'Pengajuan') { ?>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No.Pertemuan</th>
              <th>Tanggal Pertemuan</th>
              <!-- <th>Waktu Pertemuan</th> -->
              <th>Lokasi</th>
            </tr>
            <tr>
              <td><?= isset($appointment['no_pertemuan']) ? $appointment['no_pertemuan'] : '' ?></td>
              <td><?= isset($appointment['tanggal_appoiment']) ? $appointment['tanggal_appoiment'] : '' ?>  <?= isset($appointment['waktu_appoiment']) ? $appointment['waktu_appoiment'].' WIB' : '' ?></td>
              <td><a target="_blank" href="http://www.google.com/maps/place/<?= isset($appointment['lat']) ? $appointment['lat'] : '' ?>,<?=  isset($appointment['lng']) ? $appointment['lng'] : '' ?>"><?= isset($appointment['lokasi']) ? $appointment['lokasi'] : '' ?></a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-7">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th colspan="3">Tim AO/FO Anda</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                  <td rowspan="4" style="text-align: center; vertical-align: middle;">
                    <?php
                      $img_profile = 'assets/dist/img/default-avatar.png';
                      if(isset($sales['picture']) && isset($sales['picture']) != null){
                        $img_profile = 'upload/photo/'.$sales['picture'];
                      }
                    ?>
                    <img style="height: 120px;" src="<?= base_url($img_profile) ?>">
                  </td>
                  <td>Nama</td>
                  <td><?= isset($sales['nama_sales']) ? $sales['nama_sales'] : '-' ?></td>
                </tr>
                <tr>
                  <td>No Hp</td>
                  <td><?= isset($sales['no_telp']) ? $sales['no_telp'] : '-' ?></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><?= isset($sales['email']) ? $sales['email'] : '-' ?></td>
                </tr>
                <tr>
                  <td><a href="#" class="btn btn-primary" target="__blank"> Print</a></td>
                  <td><a href="https://wa.me/<?= isset($sales['no_telp']) ? $sales['no_telp'] : '-' ?>" target="_blank"><img style="height: 45px;" src="<?= base_url('assets/dist/img/icon-wa.png') ?>"></a></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-5" style="text-align: center;position: relative;">
        <img class="img-responsive img-2" src="<?= base_url('assets/dist/img/sales.png')?>">
      </div>
    </div>
  </div>
</section>
<!-- Add section end -->
