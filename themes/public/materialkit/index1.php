

<section class="cepiri-slider">
  <div id="carouselExampleCaptions" class="carousel slide carousel-fade " data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
	<?php if($_slide){ $no = 1; foreach($_slide as $_data) { if($no == 1){ $action = "active"; }else{ $action = ""; }  ?>
		<div class="carousel-item <?php echo $action; ?>">
			<img src="<?php echo base_url(); ?>upload/slideno.jpg" data-src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" class="d-block w-100 lazy" alt="..."> 
		</div>
	<?php $no++; } } ?> 
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</section>
 

<section class="add-section _spad_pad"  >
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp text-produk pt-5  ">
				<div class="col-lg-12 pd-ju text-center text-hero wow animated fadeInUp animated" data-wow-delay=".1s" >
					<b>#BanggaBersamaEka</b>
					<h1 class="green f20">“Menjadi Bank terbaik”</h1> 
					<p class="abu">
					Kami terus bekerja, bekerja dan selalu bekerja untuk 
					mewujudkan cita-cita dan visi kami <br>
					“Menjadi bank terbaik dan membanggakan dalam pembiayaan mikro, kecil, dan menengah”
					</p>
					
					<a href="<?php echo base_url(); ?>public/home/selamatdatang" class="btn btn-warning radius">Selayang Pandang</a>
				</div> 
			</div>
		</div>
	</div>
</div>
<div style="background: #f8f9fa;"> 
<img src="<?php echo base_url(); ?>assets/images/bannerbottom.webp" width="100%">
</div>
<section class="add-section _spad_pad pt-3 pb-3" style="background: #f8f9fa;">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp text-produk text-center">
				<div class="col-lg-12 pd-ju  wow animated fadeInUp animated" data-wow-delay=".1s" >
					<h2 class="red">Layanan Kami</h2>
				</div>
				<div class="col-lg-12  wow animated fadeInUp animated" data-wow-delay=".2s" >
					<p class="abu">
					Bersama Bank Eka kami akan membantu anda mendapatkan pinjaman, deposito, dan tabungan <br>
					lebih mudah dan praktis. Tim kami akan senan tiasa menghubungi anda 
					</p>
				</div> 
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row pt-2" style="justify-content: center;">
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".1s" >
				<div class="text-center">
					<?php $d = h_get_icon('i_kredit','value');?>
					<a href="<?php echo base_url('public/home/kredit') ?>" target="_blank">
						<?php if(isset($d->value)){ $img1 = base_url('upload/photo/').$d->value; }else{$img1= $theme_url."/img_frontend/Ic-Pinjaman.png";} ?>
						<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?= $img1; ?>"  class="lazy rounded cf" alt="...">
					</a>
					<br>
					<p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Kredit</p>
				</div>
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".2s" >
				<div class="text-center">
					<?php $d2 = h_get_icon('i_tabungan','value');?>
					<a href="<?php echo base_url('public/home/tabungan') ?>" target="_blank">
						<?php if(isset($d2->value)){ $img2 = base_url('upload/photo/').$d2->value; }else{$img2= $theme_url."/img_frontend/Ic-Tabungan.png";} ?>
						<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?= $img2; ?>"  class="lazy rounded cf" alt="...">
					</a>
					<br>
					<p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Tabungan </p>
				</div>
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6  wow animated fadeInUp animated" data-wow-delay=".3s" >
				<div class="text-center">
					<?php $d3 = h_get_icon('i_deposito','value');?>
					<a href="<?php echo base_url('public/home/deposito') ?>" target="_blank">
						<?php if(isset($d3->value)){ $img3 = base_url('upload/photo/').$d3->value; }else{$img3= $theme_url."/img_frontend/Ic-Deposito.png";} ?>
						<img src="<?= $img3; ?>"  class="rounded cf" alt="...">
					</a>
					<br>
					<p class="red" style="text-align:center; font-size:15px; text-transform: uppercase;"> Deposito </p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="add-section spad pt-0 hilangkan">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
				<div class="col-lg-12">
					<h2 class="green" style="text-align:center; font-size:20px">Simulasi Kredit Pinjaman</h2>
				</div>
				<br><br><br>
				<div class="col-lg-2">
				</div>
				<div class="col-lg-8">
					<center>
						<p class="abu">
							Berbagai kemudahan dan manfaat yang bisa anda dapatkan apabila memiliki produk berikut  
						</p>
					</center>
				</div>
				<div class="col-lg-2">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="recipes-section spad pt-0 hilangkan">
	<div class="container">
		<div class="row elips">
			<div class="col-lg-4"></div>
			<div class="col-lg-2 col-sm-6 ">
				<div class="text-right">
					<br>
					<a href="#">
						<img  src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Kredit-Multi-Guna.png"  class="lazy rounded df circle1" alt="...">
					</a>
					<br><br>
				</div>
			</div>
			<div class="col-lg-2 col-sm-6 ">
				<div class="text-left">
					<br>
					<a href="#">
						<img  src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Kredit-Perusahaan.png" class="lazy rounded df circle1" alt="...">
					</a>
					<br><br>
				</div>
			</div>
			<div class="col-lg-4"></div>
		</div>
	</div>
</section>

<section class="recipes-section spad pt-0 hilangkan">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h2 style="color: #abb4bd; font-size: 20px; ">Silahkan pilih nilai pinjaman dengan tenor yang akan diajukan:</h2>
				<hr>
				<p>Pinjaman :</p>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" class="form-control" id="exampleInputEmail1" >
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<input type="range" class="form-control-range" id="formControlRange">
						</div>
					</div>
				</div>
				<hr>
				<p>Tenor :</p>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<select id="inputState" class="form-control">
								<option selected>10</option>
								<option>12</option>
								<option>15</option>
								<option>24</option>
								<option>36</option>
								<option>48</option>
								<option>60</option>
								<option>72</option>
							</select>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<input type="range" class="form-control-range" id="formControlRange">
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group row">
					<div class="col-lg-4">
					</div>
					<div class="col-lg-4">
						<div class="text-center">
							<button type="submit" class="btn btn-primary elips-btn">Lihat Rincian</button>
						</div>
					</div>
					<div class="col-lg-4">
					</div>
				</div>
				
				<div class="card" style="width: 100%;">
					<div class="card-body">
						<center>
							<a class="pad">Butuh Bantuan ? </a>  <a href="#"><span color="#1c9c48"><i class="fa fa-phone"></i></span></a>&nbsp;  <a href="#"><span color="#1c9c48"><i class="fa fa-envelope"></i></span></a>&nbsp; <a href="#"><span color="#1c9c48"><i class="fa fa-whatsapp"></i></span></a>
						</center>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="review-item">
					<br>
					
					<h2 class="abu" style="text-align:center; line-height: 20px; ">Cicilan Bulan Anda</h2>
					<br>
					<h2 class="abu" style="text-align:center; font-size:20px">IDR 100.000</h2>
					<br>
					<h2  style="text-align:center; color: #abb4bd; font-size: 10px;">Termasuk Biaya bulanan</h2>
					<br>
					<h2  style="text-align:center; color: #abb4bd; font-size: 9px;"><i>Perhitungan bersifat dan bertujuan sebagai ilustrasi</i></h2>
					<br>
					<br>
					<div class="container">
						<div class="row">
							<div class="col-lg-3">
								<div class="text-right">
									<img  src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Ic-waktu.png" class="lazy rounded pl waktu" alt="...">
								</div>
							</div>
							<div class="col-lg-4">
								<h2 class="abu">Berapa lama proses pengajuannya?</h2>
								<h4 class="abu2">Sekitar 1 Jam </h4>
							</div>
							<hr>
						</div>
						<br> 
						<hr>
						<div class="row">
							<div class="col-lg-3">
								<div class="text-right">
									<img  src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Ic-Apa-saja-yang-diperlukan.png" class="lazy rounded pl waktu" alt="...">
								</div>
							</div>
							<div class="col-lg-8">
								<h2 class="abu">Apa saja yang diperlukan?</h2>
								<h4 class="abu2">Jaminan BPKB motor & Mobil  </h4>
								<h4 class="abu2">Jaminan Sertifikat Bangunan  </h4>
							</div>
							
						</div>
						<br>
						<hr>
						<div class="row">
							<div class="col-lg-3">
								<div class="text-right">
									<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Ic-Digunakan-Untuk-apa-pinjaman-anda.png" class="lazy rounded pl waktu" alt="...">
								</div>
							</div>
							<div class="col-lg-8">
								<h2 class="abu">Digunakan untuk apa pinjaman anda?</h2>
								<div class="form-group">
									<select class="form-controls" id="exampleFormControlSelect1">
										<option>Kredit Buka Cabang</option>
										<option></option>
										<option></option>
										<option></option>
										<option></option>
									</select>
								</div>
							</div>
							
						</div>
						<hr>
						<div class="form-group row">
							<div class="col-lg-12">
							</div>
							<div class="col-lg-12">
								<div class="text-center">
									<button type="submit" class="btn btn-primary elips-btn">Lihat Rincian</button>
								</div>
							</div>
							<div class="col-lg-4">
							</div>
						</div>
					</div>
					<br>
					<br>
					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="add-section spad pt-0 hilangkan">
	<div class="container">
		<div class="add-warp">
			
			<div class="row add-text-warp">
				<div class="col-lg-12">
					<h2 class="green" style="text-align:center; font-size:20px">Dapatkan Promosi dan Manfaat Menarik</h2>
				</div>
				<br><br><br>
				<div class="col-lg-2">
				</div>
				<div class="col-lg-8">
					<p class="abu" style="text-align : center">
						Berbagai kemudahan dan manfaat yang bisa anda dapatkan apabila memiliki produk berikut  
					</p>
				</div>
				<div class="col-lg-2">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="add-section spad pt-0 hilangkan">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
				<div class="col-lg-1 text-center align-self-center">
					<div class="customPreviousBtn"><span color="#356a34" style="font-size: 0px;"><i class="fa fa-angle-left"></i></span></div>
				</div>
				<div class="col-lg-10">
					
					<div class="ow owl-carousel owl-theme">
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-1.png" alt="lazy Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan rumah idaman anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-2.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan kendaraan impian anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-3.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan kendaraan impian anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-1.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan rumah idaman anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-2.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan kendaraan impian anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						<div class="item">
							<div class="card">
								<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/Promosi-3.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="green" style="text-align:left; font-size:15px">Pinjaman uang tunai untuk cicilan kendaraan impian anda</h5>
									<br>
									<p class="card-text"><i class="fa fa-circle"></i> Dokumen lengkap 2 hari cair</p>
									<p class="card-text"><i class="fa fa-circle"></i> Tanpa syarat jaminan</p>
									<a href="#" class="btn btn-primarys">Ajukan Sekarang</a>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				<div class="col-lg-1 text-center align-self-center">
					<div class="customNextBtn"><span color="#356a34" style="font-size: 0px;"><i class="fa fa-angle-right"></i></span></div>
				</div>
			</div>
		</div>
	</div>
</section>  

<section class="review-section" style="background-image:url('<?php base_url(); ?>assets/images/bgvectorredx1.png');background-size:cover;">
	<div class="container">
		<div class="row flex">
			<div class="col-lg-6"> 
				<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo base_url()."/assets/dist/img/empeloye.png"; ?>" alt="Kredit Rakyat Menggelegar" class="lazy img-thumbnaild">
			</div>
			<div class="col-lg-6 ujang align-selft text-center">
				<h2 class="text-white">Menjadi Bagian kami</h2>
				<p style="color:#dfdfdf">
				Tahapan mudah dan praktis #BersamaBankEka <br> untuk dapat membantu bisnis anda
				</p>
				
				<a href="<?php echo base_url(); ?>public/home/mengapamemilihkami" class="btn btn-warning radius">Selengkapnya</a> 
			</div>
		</div>
	</div>
</section>

<section  style="background-image:url('<?php base_url(); ?>assets/images/promobanner.png');background-size:cover;background-color: #f8f9fa;">
<div class="section bg-lights block-11 pt-5 pb-5"  >
    <div class="container">
		<div class="row justify-content-center mb-5">
			<div class="col-md-8 text-center">
				<h2 class="red" style="text-align:center;">Apa Kata Mereka ?</h2>
				<p class="abu" style="text-align : center; font-size: 17px;">
				Yuk dengar apa kata mereka setelah sukses #BersamaBankEka
				</p>
			</div>
		</div>
		<div class="nonloop-block-11 owl-carousel">
			<?php foreach($cerpel as $data) { ?>
				<div class="item">
					<div class="block-33 h-100">
						<div class="vcard d-flex mb-3">
							<div class="image align-self-center">
								<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?= base_url('upload/photo/').$data['foto'] ?>" class="lazy avatar" alt="Person here">
							</div>
							<div class="name-text align-self-center" style="margin-left: 16px;">
								<h2 class="heading" style="font-size: 23px;"><?= $data['nama'] ?></h2>
								<span class="meta" style="font-size: 13px;"><?= $data['jabatan'] ?></span>
								<div>
									<?php star_render($data['bintang']) ?>
								</div>
							</div>
						</div>
						<div class="text">
							<blockquote>
								<p>&rdquo; <?= $data['keterangan'] ?> &ldquo;</p>
							</blockquote>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</section>
 
<div id="myModal" class="modal">
	<span class="close cursor" onclick="closeModal()">&times;</span>
	<div class="modal-content">
		<div class="mySlides">
			<div class="numbertext">1 / 4</div>
			<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo $theme_url;?>/img_frontend/DJI_0174.JPG" class="lazy" style="width:100%">
		</div>
	</div>
</div>

<section class="add-section _spad_pad pt-4">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
				<div class="col-lg-12 pd-ju">
					<h2 class="red" style="text-align:center;">News</h2>
				</div>
				
				<div class="col-lg-2">
				</div>
				<div class="col-lg-8">
					<p class="abu" style="text-align : center; font-size: 17px;">
					Dapatkan informasi dan tips praktis keuangan lewat kumpulan artikel berikut ini
					</p>
				</div>
				<div class="col-lg-2">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="add-section _spad_pad pt-0">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
				<div class="col-lg-1 text-center align-self-center owl-left-ya">
					<div class="customPreviousBtnc"><span color="#356a34" style="font-size: 0px;"><i class="fa fa-angle-left"></i></span></div>
				</div>
				<div class="col-lg-10 owl-center-ya">
					<div class="owc owl-carousel owl-theme" id="top-article">
						<?php foreach($_pressrealese  as $data){ ?>
							<div class="item">
								<div class="card card-cs">
									<img class="card-img-top card-img-cs lazy" src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo base_url('upload/photo/').$data['foto']  ?>" alt="Kredit Merakyat">
									<div class="card-bodys card-body-cs">
										<div class="container-x">
							
											<a href="<?php echo base_url().'public/home/press_realese_detail/'.$data['id']?>">
												<h5 class="card-title card-title-cs"><?= spoiler($data['judul'], 24) ?></h5>
											</a>
											<p class="card-content-cs">
											<?= spoiler($data['isi'], 99) ?>

											</p> 
										</div>
									</div>
								</div>
							</div>
						<?php } ?>					
					</div>
				</div>
				<div class="col-lg-1 text-center align-self-center owl-right-ya" >
					<div class="customNextBtnc"><span color="#356a34" style="font-size: 0px;"><i class="fa fa-angle-right"></i></span></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="add-section _spad_pad">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
			 
				<div class="col-lg-12">
				 
					<div class="mitra owl-carousel">
						<?php foreach($_pengawas as $data) { ?>
							<div class="item mx-auto">
								<div class="frame gmbr-wi1">
									<img src="<?php echo base_url(); ?>upload/noimg.webp" data-src="<?php echo base_url('upload/logo/'.$data['logo'])?>" alt="pengawas bpr" class="lazy img-thumbnailwa rounded mx-auto d-block" >	
								</div>
							</div>
						<?php } ?>
					</div>		


 
				</div>
			</div>
		</div>
	</div>
</section> 