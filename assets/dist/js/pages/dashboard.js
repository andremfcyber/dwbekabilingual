/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  // $(".connectedSortable").sortable({
  //   placeholder: "sort-highlight",
  //   connectWith: ".connectedSortable",
  //   handle: ".box-header, .nav-tabs, .icon",
  //   forcePlaceholderSize: true,
  //   zIndex: 999999,
  //   start: function(event, ui) {
  //     ui.item.startPos = ui.item.index();
  //     ui.item.startSection = $(this).attr("id");
  //   },
  //   update: function(event, ui) {

  //     var start_section = ui.item.startSection;
  //     var end_section = $(this).attr('id');
  //     var target = base_url+'dashboard/dashboard/update_widget_position';

  //     if(start_section!=end_section){

  //       var data = { section: end_section, position: ui.item.index(), id: ui.item.attr('id') }

  //     }else{

  //       var data = { section: start_section, position: ui.item.index(), id: ui.item.attr('id') }

  //     }

  //     $.post(target,data);

  //   }
  // });
  // $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom, .connectedSortable .small-box .icon").css("cursor", "move");


  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    // line.redraw();
  });

  $('.img-fill').imagefill({target:'.background-image'});

});
